import * as moment from 'moment-timezone';

export const DEFAULT_DATE_FORMAT = 'YYYY-MM-DD';
const DEFAULT_TIMEZONE = 'America/Guayaquil';
const DEFAULT_FORMAT_DATE = 'YYYY-MM-DD';

export class DateHandlerUtil {
  static getCurrentDate(format?: string): string {
    return moment().format(format || DEFAULT_DATE_FORMAT);
  }

  static formatDate(date: moment.MomentInput, format?: string): string | null {
    if (!date) {
      return null;
    }
    return moment
      .tz(date, DEFAULT_TIMEZONE)
      .format(format || DEFAULT_DATE_FORMAT);
  }

  static validateDate(date: moment.MomentInput, format?: string): boolean {
    return moment(date, format || DEFAULT_FORMAT_DATE, true).isValid();
  }

  static isAfterDate(
    dateOne: moment.MomentInput,
    dateTwo: moment.MomentInput,
    format?: string
  ): boolean {
    return moment(dateOne, format).isAfter(moment(dateTwo, format));
  }

  static dateToTimestamp(date: moment.MomentInput): number | null {
    if (!date) {
      return null;
    }
    return moment.tz(date, DEFAULT_TIMEZONE).valueOf();
  }

  static getDifferenceBetweenTwoDates(
    initialDate: moment.MomentInput,
    finalDate: moment.MomentInput,
    unit: moment.unitOfTime.Diff
  ): number {
    const _initialDate = moment.tz(initialDate, DEFAULT_TIMEZONE);
    const _finalDate = moment.tz(finalDate, DEFAULT_TIMEZONE);
    return _finalDate.diff(_initialDate, unit, false);
  }
}
