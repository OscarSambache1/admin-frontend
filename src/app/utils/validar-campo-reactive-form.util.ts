import { AbstractControl, FormGroup } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';

interface ObjectoArregloErroresInterface {
  [key: string]: string[];
}

export interface MensajesErrorInterface {
  [key: string]: {
    required?: string;
    minlength?: string;
    maxlength?: string;
    pattern?: string;
    min?: string;
    max?: string;
  };
}

export const escucharCampo = (
  nombreCampo: string,
  formulario: FormGroup,
  objetoArregloErrores: ObjectoArregloErroresInterface,
  mensajesError: MensajesErrorInterface
): void => {
  const campo$ = formulario.get(nombreCampo);
  objetoArregloErrores[nombreCampo] = llenarMensajesErrorCampo(
    campo$,
    nombreCampo,
    mensajesError
  );
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  campo$.valueChanges.pipe(debounceTime(10)).subscribe(() => {
    objetoArregloErrores[nombreCampo] = llenarMensajesErrorCampo(
      campo$,
      nombreCampo,
      mensajesError
    );
  });
};

const llenarMensajesErrorCampo = (
  control: AbstractControl | any,
  nombreCampo: string,
  mensajesError: MensajesErrorInterface
) => {
  let arregloErrores: string[] = [];
  if (control.controls || (control.touched && control.errors)) {
    arregloErrores = Object.keys(control.errors).map((llave: string) => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      return mensajesError[nombreCampo][llave];
    });
  }
  return arregloErrores;
};

export const cargarDatosDeInicioFormulario = <T>(
  formulario: FormGroup,
  data: T
): void => {
  const controles = Object.keys(formulario.controls);
  controles.forEach((control: string) => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    formulario.controls[control].patchValue(data[control]);
  });
};
