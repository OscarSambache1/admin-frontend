export function RedondearNumero(numero: number, redondear = 2): number {
  const multiplo = Math.pow(10, redondear);
  return Math.round((numero + Number.EPSILON) * multiplo) / multiplo;
}
