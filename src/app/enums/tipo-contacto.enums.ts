export enum TipoContactoEnums {
  E = 'Correo',
  T = 'Teléfono',
  C = 'Celular',
}

export enum TipoGasto {
  Repuesto = 'R',
  Producto = 'P',
}

export enum TipoGastoEnum {
  R = 'Repuesto',
  P = 'Producto / Servicio / Extra',
}

export enum EstadoGasto {
  PorPagar = 'PP',
  Pagado = 'P',
  Abonado = 'A',
}

export enum TipoFactura {
  Venta = 'V',
  Gasto = 'G',
}

export enum EstadoAbono {
  PorPagar = 'PP',
  Pagado = 'P',
}

export enum EstadoAbonoEnum {
  PP = 'Por Pagar',
  P = 'Pagado',
  A = 'Abonado',
}

export enum TarfifasIvaEnum {
  'Iva12' = '12 %',
  'Iva0' = '0 %',
  'IvaExtento' = 'EXENTO IVA',
  'IvaNoObjeto' = 'NO OBJETO',
}

export const TARIFAS_TABLA_DETALLE = [TarfifasIvaEnum.Iva0, TarfifasIvaEnum.Iva12, TarfifasIvaEnum.IvaExtento, TarfifasIvaEnum.IvaNoObjeto];

export const TARIFAS_NO_TOTALES = [TarfifasIvaEnum.Iva0, TarfifasIvaEnum.IvaExtento, TarfifasIvaEnum.IvaNoObjeto];

export enum EstadoGastoAccion {
  GUARDADO = 'GUARDADO',
  FINALIZADO = 'FINALIZADO',
}

export enum EstadoStockEnum {
  Agotado = 'A',
  Disponible = 'D',
  PorAgotarse = 'PA',
}

export enum EstadoStockEnumLabel {
  A = 'Agotado',
  D = 'Disponible',
  PA = 'Por Agotarse',
}

export enum EstadoTrabajo {
  NoGuardado = 'NG',
  Guardado = 'G',
  Finalizado = 'F',
}

export enum EstadoTrabajoEnum {
  NG = 'NO REGISTRADA',
  G = 'PENDIENTE',
  F = 'FINALIZADO',
}

export enum TipoCuentaBancoEnum {
  C = 'Corriente',
  A = 'Ahorros',
}
