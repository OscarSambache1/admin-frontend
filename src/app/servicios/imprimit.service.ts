import {EventEmitter, Injectable} from '@angular/core';
// @ts-ignore
import pdfMake from "pdfmake/build/pdfmake";
// @ts-ignore
import pdfFonts from "pdfmake/build/vfs_fonts";
import {GastoInterface} from "../modulos/gastos/interfaces/gasto.interface";
import {
  EstadoAbonoEnum, EstadoTrabajo, EstadoTrabajoEnum,
  TarfifasIvaEnum,
  TARIFAS_NO_TOTALES,
  TipoGasto,
  TipoGastoEnum
} from "../enums/tipo-contacto.enums";
import {TarifaImpuestoRestService} from "../modulos/configuracion-datos/servicios/tarifa-impuesto-rest.service";
import {CargandoService} from "./cargando.service";
import {TarifaImpuestoInterface} from "../modulos/configuracion-datos/interfaces/tarifa-impuesto.interface";
import {FacturaCabeceraInterface} from "../modulos/gastos/interfaces/factura-cabecera.interface";
import {TrabajoRealizadoInterface} from "../modulos/trabajos/interfaces/trabajo-realizado.interface";
import {OrdenTrabajoInterface} from "../modulos/trabajos/interfaces/orden-trabajo.interface";
import {RecepcionInterface} from "../modulos/trabajos/interfaces/recepcion.interface";
import * as fs from 'fs';
import {setearFecha} from "../constantes/setear-fecha";

pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Injectable({
  providedIn: 'root',
})
export class ImprimitService {
  tarifas: TarifaImpuestoInterface[] = [];
  tarifaSeleccionadaTotales = TarfifasIvaEnum.Iva12;

  constructor(
    private readonly _tarifaImpuestoRestService: TarifaImpuestoRestService,
  ) {
  }

  async obtenerTarifas() {
    const consulta = {
      where: {},
      relations: ['impuesto']
    };
    const tarifaImpuesto$ = this._tarifaImpuestoRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    const respuesta = await (tarifaImpuesto$.toPromise());
    this.tarifas = respuesta[0]
      .filter(tarifaAux => tarifaAux.impuesto?.nombre === 'IVA');
  }

  async obtenerImagenUrlBase64(imagenUrl: any) {

    const response = await fetch(imagenUrl);
    const dataImagen = await response.blob();

    return new Promise(
      (resolve, reject) => {
        const fileReader = new FileReader();
        fileReader.addEventListener(
          'load', () => {
            resolve(fileReader.result);
          }, false);
        fileReader.onerror = () => {
          return reject(this);
        };
        fileReader.readAsDataURL(dataImagen);
      });
  }

  async imprimir(pdfDefinition: any) {
    const pdf = pdfMake.createPdf({
      ...pdfDefinition,
      styles: {
        header: {
          fontSize: 15,
          bold: true,
          margin: [0, 0, 0, 10],
          color: '#275789'
        },
        subheader: {
          fontSize: 16,
          bold: true,
          margin: [0, 10, 0, 5],
          color: '#275789'
        },
        tableExample: {
          margin: [0, 0, 0, 0],
          color: '#275789'
        },
        tableDetalle: {
          margin: [0, 0, 0, 0],
          fontSize: 10,
          color: '#275789'
        },
        tableHeader: {
          bold: true,
          fontSize: 13,
          color: '#275789'
        }
      }

    });
    pdf.open();
  }

  setearArregloDetalles(detalles: any[]) {
    return detalles.map(detalle => {
      const arreglo = [];
      arreglo.push((detalle.cantidad || 0).toFixed(0));
      arreglo.push(detalle.descripcion || '');
      arreglo.push((detalle.precioUnitario || 0).toFixed(2));
      arreglo.push(detalle.ivaPorcentaje || '');
      arreglo.push((detalle.valorIva || 0).toFixed(2));
      arreglo.push((detalle.descuento || 0).toFixed(2));
      arreglo.push((detalle.total || 0).toFixed(2));
      arreglo.push((detalle.valorIce || 0).toFixed(2));
      return arreglo;
    });
  }

  setearArregloDetallesRepuestos(detalles: any[]) {
    return detalles
      .filter(detalleAux => !detalleAux.articulo?.esServicio)
      .map((detalle: any) => {
        const arreglo = [];
        arreglo.push(
          {
            text: `- (${detalle.cantidad}) ${detalle.descripcion}` || ''
          }
        );
        arreglo.push(
          {
            text: (detalle.total || 0).toFixed(2),
            alignment: 'right'
          }
        );
        return arreglo;
      });
  }

  setearArregloDetallesServicios(detalles: any[]) {
    return detalles
      .filter(detalleAux => detalleAux.articulo?.esServicio)
      .map((detalle: any) => {
        const arreglo = [];
        arreglo.push(
          {
            text: `- (${detalle.cantidad}) ${detalle.descripcion}` || ''
          }
        );
        arreglo.push(
          {
            text: (detalle.total || 0).toFixed(2),
            alignment: 'right'
          }
        );
        return arreglo;
      });
  }

  async setearArregloTotales(
    detalles: any[],
    factura: any,
  ) {
    await this.obtenerTarifas();
    const arregloTotales: any[] = [];
    const arregloSubtotalesTarifa: any[] = [];
    const arregloTarifasValidas = [...TARIFAS_NO_TOTALES, this.tarifaSeleccionadaTotales];
    this.tarifas.map(tarifa => {
      if (arregloTarifasValidas.includes(tarifa.descripcion as any)) {
        const detallesTarifas = detalles?.filter(detalle => detalle.ivaDescripcion === tarifa.descripcion);
        const sumaDetallesValorTotalTarifa = detallesTarifas?.reduce((acum: any, detalle) => {
          return +(detalle.total as any) + acum;
        }, 0) || 0;
        const sumaDetallesValorTarifa = detallesTarifas?.reduce((acum: any, detalle) => {
          return +(detalle.valorIva as any) + acum;
        }, 0) || 0;
        arregloSubtotalesTarifa.push(
          {
            sumaDetallesValorTotalTarifa,
            sumaDetallesValorTarifa,
            tarifa: tarifa.descripcion
          }
        );
      }
    });

    arregloTotales.push(['Subtotal sin impuestos', factura.subtotalSinImpuestos.toFixed(2)]
    );
    // const arregloTarifas = [...TARIFAS_NO_TOTALES, this.tarifaSeleccionadaTotales];
    // arregloSubtotalesTarifa.map(datosTarifa => {
    //   if (arregloTarifas.includes(datosTarifa.tarifa)) {
    //     arregloTotales.push(
    //       [
    //         `Subtotal ${datosTarifa.tarifa}`,
    //         datosTarifa.sumaDetallesValorTotalTarifa.toFixed(2),
    //       ]
    //     )
    //   }
    // });

    // arregloTotales.push(['Valor ICE', (factura.iceValor || 0).toFixed(2),]);
    const tarifaIva = arregloSubtotalesTarifa.find(datosTarifa => datosTarifa.tarifa === this.tarifaSeleccionadaTotales);
    if (tarifaIva) {
      arregloTotales.push([`Iva ${tarifaIva.tarifa}`, (tarifaIva.sumaDetallesValorTarifa).toFixed(2),]);
    }
    // arregloTotales.push([`Propina`, (factura.propina || 0).toFixed(2)]);
    // arregloTotales.push(['Descuento General', (factura.descuento || 0).toFixed(2)]);
    arregloTotales.push([`Valor a pagar`, (factura.total || 0).toFixed(2),]);
    return arregloTotales.map(arregloValorTotal => {
      return [
        {
          text: arregloValorTotal[0],
          // bold: true,
          alignment: 'right',
        },
        {
          text: arregloValorTotal[1],
          alignment: 'right',
        }

      ]
    });
  }

  async setearObjetoGasto(gasto: GastoInterface) {
    const factura = gasto.facturaCabecera;
    const arregloDetalles = this.setearArregloDetalles(factura?.facturaDetalles as any[]);
    const arregloTotales = await this.setearArregloTotales(factura?.facturaDetalles as any[], factura);
    if (gasto) {
      return {
        content: [
          {
            style: 'tableExample',
            table: {
              widths: ['*', '*'],
              body: [
                [
                  {
                    colSpan: 2,
                    text: 'Información del gasto',
                    style: 'header'
                  },
                  {}
                ],
                [
                  {
                    widths: ['*', '*'],
                    columns: [
                      {
                        text: 'Tipo de Gasto:',
                        bold: true,
                      },
                      {
                        // @ts-ignore
                        text: TipoGastoEnum[(gasto.tipoGasto)],
                      }
                    ]
                  },
                  {
                    widths: ['*', '*'],
                    columns: [
                      {
                        text: 'Estado de Pago:',
                        bold: true,
                      },
                      {
                        // @ts-ignore
                        text: EstadoAbonoEnum[(gasto.estadoGasto)],
                      }
                    ]
                  }
                ]
              ]
            }
          },

          '\n',
          {
            style: 'tableExample',
            table: {
              widths: ['*', '*'],
              body: [
                [
                  {
                    colSpan: 2,
                    text: 'Información de la Factura',
                    style: 'header'
                  },
                  {}
                ],
                [
                  {
                    widths: ['*', '*'],
                    columns: [
                      {
                        text: 'Fecha Emisión:',
                        bold: true,
                      },
                      {
                        text: factura?.fechaEmision,
                      }
                    ]
                  },
                  {
                    widths: ['*', '*'],
                    columns: [
                      {
                        text: 'N° Factura:',
                        bold: true,
                      },
                      {
                        text: factura?.numero,
                      }
                    ]
                  }
                ],
                [
                  {
                    colSpan: 2,
                    text: 'Proveedor:',
                    style: 'header'
                  },
                  {}
                ],
                [
                  {
                    widths: ['*', '*'],
                    columns: [
                      {
                        text: 'Razón Social:',
                        bold: true,
                      },
                      {
                        text: factura?.razonSocialProveedor,
                        alignment: 'left',
                      }
                    ]
                  },
                  {
                    widths: ['*', '*'],
                    columns: [
                      {
                        text: 'Nombre Comercial:',
                        bold: true,
                      },
                      {
                        text: factura?.nombreComercialProveedor,
                        alignment: 'left',
                      }
                    ]
                  }
                ],
                [
                  {
                    widths: ['*', '*'],
                    columns: [
                      {
                        text: 'Identificación:',
                        bold: true,
                      },
                      {
                        text: factura?.identificacionProveedor,
                        alignment: 'left',
                      }
                    ]
                  },
                  {
                    widths: ['*', '*'],
                    columns: [
                      {
                        text: 'Teléfono:',
                        bold: true,
                      },
                      {
                        text: factura?.telefonoProveedor,
                        alignment: 'left',
                      }
                    ]
                  }
                ],
                [
                  {
                    widths: ['*', '*'],
                    colSpan: 2,
                    columns: [
                      {
                        width: 'auto',
                        text: 'Dirección:',
                        bold: true,
                      },
                      {
                        width: 'auto',
                        text: factura?.direccionProveedor,
                        alignment: 'left',
                      }
                    ]
                  },
                  {}
                ],
                [
                  {
                    colSpan: 2,
                    columns: [
                      {
                        width: 'auto',
                        text: 'Correo:',
                        bold: true,
                      },
                      {
                        width: 'auto',
                        text: factura?.correoProveedor,
                        alignment: 'left',
                      }
                    ]
                  },
                  {}
                ],

              ]
            }
          },

          '\n',
          {
            style: 'tableDetalle',
            table: {
              widths: ['*'],
              body: [
                [
                  {
                    text: 'Detalle de la Factura',
                    style: 'header',
                    border: [true, true, true, false],
                  },
                ],
              ]
            }
          },
          {
            margin: [0, 0, 0, 0],
            style: 'tableDetalle',
            table: {
              widths: ['*', '*', '*', '*', '*', '*', '*', '*'],
              body: [
                [
                  {
                    text: 'Cant.',
                    bold: true,
                  },
                  {
                    text: 'Descripción.',
                    bold: true,
                  },
                  {
                    text: 'Precio Uni.',
                    bold: true,
                  },
                  {
                    text: 'IVA',
                    bold: true,
                  },
                  {
                    text: 'Val. IVA',
                    bold: true,
                  },
                  {
                    text: 'Desc.',
                    bold: true,
                  },
                  {
                    text: 'Total',
                    bold: true,
                  },
                  {
                    text: 'ICE',
                    bold: true,
                  },
                ],
                ...arregloDetalles,
              ]
            }
          },
          {
            margin: [0, 10, 0, 0],
            columns: [
              {},
              {
                style: 'tableDetalle',
                table: {
                  widths: ['*', '*'],
                  body: [
                    ...arregloTotales,
                  ]
                }
              },

            ]
          }
        ]
      }
    } else {
      return {}
    }
  }

  async setearObjetoFactura(factura: FacturaCabeceraInterface) {
    const arregloDetalles = this.setearArregloDetalles(factura?.facturaDetalles as any[]);
    const arregloTotales = await this.setearArregloTotales(factura?.facturaDetalles as any[], factura);
    return {
      content: [
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  colSpan: 2,
                  text: 'Información de la Factura',
                  style: 'header'
                },
                {}
              ],
              [
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Fecha Emisión:',
                      bold: true,
                    },
                    {
                      text: factura?.fechaEmision,
                    }
                  ]
                },
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'N° Factura:',
                      bold: true,
                    },
                    {
                      text: factura?.numero,
                    }
                  ]
                }
              ],
              [
                {
                  colSpan: 2,
                  text: 'Cliente:',
                  style: 'header'
                },
                {}
              ],
              [
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Razón Social:',
                      bold: true,
                    },
                    {
                      text: factura?.razonSocialCliente || '-',
                      alignment: 'left',
                    }
                  ]
                },
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Nombre Comercial:',
                      bold: true,
                    },
                    {
                      text: factura?.nombreComercialCliente || '-',
                      alignment: 'left',
                    }
                  ]
                }
              ],
              [
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Identificación:',
                      bold: true,
                    },
                    {
                      text: factura?.identificacionCliente || '-',
                      alignment: 'left',
                    }
                  ]
                },
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Teléfono:',
                      bold: true,
                    },
                    {
                      text: factura?.telefonoCliente || '-',
                      alignment: 'left',
                    }
                  ]
                }
              ],
              [
                {
                  widths: ['*', '*'],
                  colSpan: 2,
                  columns: [
                    {
                      width: 'auto',
                      text: 'Dirección:',
                      bold: true,
                    },
                    {
                      width: 'auto',
                      text: factura?.direccionCliente || '-',
                      alignment: 'left',
                    }
                  ]
                },
                {}
              ],
              [
                {
                  colSpan: 2,
                  columns: [
                    {
                      width: 'auto',
                      text: 'Correo:',
                      bold: true,
                    },
                    {
                      width: 'auto',
                      text: factura?.correoCliente || '-',
                      alignment: 'left',
                    }
                  ]
                },
                {}
              ],

            ]
          }
        },

        '\n',
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  colSpan: 2,
                  text: 'Información del Vehículo',
                  style: 'header'
                },
                {}
              ],
              [
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Propietario:',
                      bold: true,
                    },
                    {
                      text: factura.vehiculo?.cliente?.informacionTributaria?.nombreComercial,
                      alignment: 'left',
                    }
                  ]
                },
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Vehículo:',
                      bold: true,
                    },
                    {
                      text: factura.vehiculo?.placa,
                    }
                  ]
                }
              ],
              [
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Marca:',
                      bold: true,
                    },
                    {
                      text: factura.vehiculo?.marca?.nombre,
                      alignment: 'left',
                    }
                  ]
                },
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Modelo:',
                      bold: true,
                    },
                    {
                      text: factura.vehiculo?.modelo,
                    }
                  ]
                }
              ]

            ]
          }
        },

        '\n',
        {
          style: 'tableDetalle',
          table: {
            widths: ['*'],
            body: [
              [
                {
                  text: 'Detalle de la Factura',
                  style: 'header',
                  border: [true, true, true, false],
                },
              ],
            ]
          }
        },
        {
          margin: [0, 0, 0, 0],
          style: 'tableDetalle',
          table: {
            widths: ['*', '*', '*', '*', '*', '*', '*', '*'],
            body: [
              [
                {
                  text: 'Cant.',
                  bold: true,
                },
                {
                  text: 'Descripción.',
                  bold: true,
                },
                {
                  text: 'Precio Uni.',
                  bold: true,
                },
                {
                  text: 'IVA',
                  bold: true,
                },
                {
                  text: 'Val. IVA',
                  bold: true,
                },
                {
                  text: 'Desc.',
                  bold: true,
                },
                {
                  text: 'Total',
                  bold: true,
                },
                {
                  text: 'ICE',
                  bold: true,
                },
              ],
              ...arregloDetalles,
            ]
          }
        },
        {
          margin: [0, 10, 0, 0],
          columns: [
            {},
            {
              style: 'tableDetalle',
              table: {
                widths: ['*', '*'],
                body: [
                  ...arregloTotales,
                ]
              }
            },

          ]
        }
      ]
    }
  }

  async setearObjetoTrabajoRealizado(trabajoRealizado: TrabajoRealizadoInterface) {
    const arregloDetalles = this.setearArregloDetalles(trabajoRealizado?.trabajoRealizadoDetalles as any[]);
    const arregloTotales = await this.setearArregloTotales(trabajoRealizado?.trabajoRealizadoDetalles as any[], trabajoRealizado);
    return {
      content: [
        {
          style: 'tableExample',
          table: {
            widths: ['*'],
            body: [
              [
                {
                  text: 'Información del Trabajo Realizado',
                  style: 'header'
                },
              ],
              [
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Fecha:',
                      bold: true,
                    },
                    {
                      text: trabajoRealizado?.fechaTrabajoRealizado,
                    }
                  ]
                },
              ],
            ]
          }
        },

        '\n',
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  colSpan: 2,
                  text: 'Información del Vehículo',
                  style: 'header'
                },
                {}
              ],
              [
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Propietario:',
                      bold: true,
                    },
                    {
                      text: trabajoRealizado.vehiculo?.cliente?.informacionTributaria?.nombreComercial,
                      alignment: 'left',
                    }
                  ]
                },
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Vehículo:',
                      bold: true,
                    },
                    {
                      text: trabajoRealizado.vehiculo?.placa,
                    }
                  ]
                }
              ],
              [
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Marca:',
                      bold: true,
                    },
                    {
                      text: trabajoRealizado.vehiculo?.marca?.nombre,
                      alignment: 'left',
                    }
                  ]
                },
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Modelo:',
                      bold: true,
                    },
                    {
                      text: trabajoRealizado.vehiculo?.modelo,
                    }
                  ]
                }
              ]

            ]
          }
        },

        '\n',
        {
          style: 'tableDetalle',
          table: {
            widths: ['*'],
            body: [
              [
                {
                  text: 'Detalle del Trabajo Realizado',
                  style: 'header',
                  border: [true, true, true, false],
                },
              ],
            ]
          }
        },
        {
          margin: [0, 0, 0, 0],
          style: 'tableDetalle',
          table: {
            widths: ['*', '*', '*', '*', '*', '*', '*', '*'],
            body: [
              [
                {
                  text: 'Cant.',
                  bold: true,
                },
                {
                  text: 'Descripción.',
                  bold: true,
                },
                {
                  text: 'Precio Uni.',
                  bold: true,
                },
                {
                  text: 'IVA',
                  bold: true,
                },
                {
                  text: 'Val. IVA',
                  bold: true,
                },
                {
                  text: 'Desc.',
                  bold: true,
                },
                {
                  text: 'Total',
                  bold: true,
                },
                {
                  text: 'ICE',
                  bold: true,
                },
              ],
              ...arregloDetalles,
            ]
          }
        },
        {
          margin: [0, 10, 0, 0],
          columns: [
            {},
            {
              style: 'tableDetalle',
              table: {
                widths: ['*', '*'],
                body: [
                  ...arregloTotales,
                ]
              }
            },

          ]
        }
      ]
    }
  }

  async setearObjetoOrdenTRabajo(ordenTrabajo: OrdenTrabajoInterface) {
    const arregloDetalles = this.setearArregloDetalles(ordenTrabajo?.ordenTrabajoDetalles as any[]).map(registro => {
      return [
        registro[0],
        registro[1]
      ]
    });
    return {
      content: [
        {
          style: 'tableExample',
          table: {
            widths: ['*'],
            body: [
              [
                {
                  text: 'Información de la Orden de Trabajo',
                  style: 'header'
                },
              ],
              [
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Fecha:',
                      bold: true,
                    },
                    {
                      text: ordenTrabajo?.fechaOrdenTrabajo,
                    }
                  ]
                },
              ],
            ]
          }
        },

        '\n',
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  colSpan: 2,
                  text: 'Información del Vehículo',
                  style: 'header'
                },
                {}
              ],
              [
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Propietario:',
                      bold: true,
                    },
                    {
                      text: ordenTrabajo.vehiculo?.cliente?.informacionTributaria?.nombreComercial,
                      alignment: 'left',
                    }
                  ]
                },
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Vehículo:',
                      bold: true,
                    },
                    {
                      text: ordenTrabajo.vehiculo?.placa,
                    }
                  ]
                }
              ],
              [
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Marca:',
                      bold: true,
                    },
                    {
                      text: ordenTrabajo.vehiculo?.marca?.nombre,
                      alignment: 'left',
                    }
                  ]
                },
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Modelo:',
                      bold: true,
                    },
                    {
                      text: ordenTrabajo.vehiculo?.modelo,
                    }
                  ]
                }
              ]

            ]
          }
        },

        '\n',
        {
          style: 'tableDetalle',
          table: {
            widths: ['*'],
            body: [
              [
                {
                  text: 'Detalle de la Orden de Trabajo',
                  style: 'header',
                  border: [true, true, true, false],
                },
              ],
            ]
          }
        },
        {
          margin: [0, 0, 0, 0],
          style: 'tableDetalle',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  text: 'Cantidad',
                  bold: true,
                },
                {
                  text: 'Descripción',
                  bold: true,
                },
              ],
              ...arregloDetalles,
            ]
          }
        },
      ]
    }
  }

  async setearObjetoRecepcion(recepcion: RecepcionInterface) {
    return {
      content: [
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  colSpan: 2,
                  text: 'Información del Vehículo',
                  style: 'header'
                },
                {}
              ],
              [
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Propietario:',
                      bold: true,
                    },
                    {
                      text: recepcion.vehiculo?.cliente?.informacionTributaria?.nombreComercial,
                      alignment: 'left',
                    }
                  ]
                },
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Vehículo:',
                      bold: true,
                    },
                    {
                      text: recepcion.vehiculo?.placa,
                    }
                  ]
                }
              ],
              [
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Marca:',
                      bold: true,
                    },
                    {
                      text: recepcion.vehiculo?.marca?.nombre,
                      alignment: 'left',
                    }
                  ]
                },
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Modelo:',
                      bold: true,
                    },
                    {
                      text: recepcion.vehiculo?.modelo,
                    }
                  ]
                }
              ]

            ]
          }
        },
        '\n',
        {
          style: 'tableExample',
          table: {
            widths: ['*', '*'],
            body: [
              [
                {
                  colSpan: 2,
                  text: 'Información de la Recepción del Vehículo',
                  style: 'header'
                },
                {}
              ],
              [
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Fecha:',
                      bold: true,
                    },
                    {
                      text: recepcion?.fechaRecepcion,
                    }
                  ]
                },
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Estado:',
                      bold: true,
                    },
                    {
                      // @ts-ignore
                      text: EstadoTrabajoEnum[recepcion?.estadoAccion as EstadoTrabajo],
                    }
                  ]
                },
              ],
              [
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'KM de Entrada:',
                      bold: true,
                    },
                    {
                      text: recepcion?.kmEntrada,
                    }
                  ]
                },
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Gasolina Entrada:',
                      bold: true,
                    },
                    {
                      text: recepcion?.gasolinaEntrada,
                    }
                  ]
                },
              ],
              [
                {
                  colSpan: 2,
                  text: [
                    {
                      text: 'Observaciones del Taller: \n\n',
                      bold: true,
                    },
                    {
                      text: recepcion?.observacionesTaller,
                      alignment: 'left',
                    }
                  ],
                },
                {}
              ],
              [
                {
                  colSpan: 2,
                  text: [
                    {
                      text: 'Observaciones del Cliente: \n\n',
                      bold: true,
                    },
                    {
                      text: recepcion?.observacionesCliente,
                      alignment: 'left',
                    }
                  ],
                },
                {}
              ],
              [
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Piezas Recambio:',
                      bold: true,
                    },
                    {
                      text: recepcion?.piezasRecambio ? 'Si' : 'No',
                    }
                  ]
                },
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'En Garantía:',
                      bold: true,
                    },
                    {
                      text: recepcion?.enGarantia ? 'Si' : 'No',
                    }
                  ]
                },
              ],
              [
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Siniestro:',
                      bold: true,
                    },
                    {
                      text: recepcion?.siniestro ? 'Si' : 'No',
                    }
                  ]
                },
                {
                  widths: ['*', '*'],
                  columns: [
                    {
                      text: 'Grua:',
                      bold: true,
                    },
                    {
                      text: recepcion?.grua ? 'Si' : 'No',
                    }
                  ]
                },
              ],

            ]
          }
        },

      ]
    }
  }


  async setearObjetoProforma(trabajoRealizado: TrabajoRealizadoInterface) {
    const arregloDetallesRepuestos = this.setearArregloDetallesRepuestos(trabajoRealizado?.trabajoRealizadoDetalles as any[]) || ['', ''];
    const arregloDetallesServicios = this.setearArregloDetallesServicios(trabajoRealizado?.trabajoRealizadoDetalles as any[]) || ['', ''];
    const arregloTotales = await this.setearArregloTotales(trabajoRealizado?.trabajoRealizadoDetalles as any[], trabajoRealizado);
    const pathImagenHeader = '/assets/images/header-doc.png';
    const imagenBase64Header = await this.obtenerImagenUrlBase64(pathImagenHeader);
    const pathImagenFooter = '/assets/images/footer-doc.png';
    const imagenBase64Footer = await this.obtenerImagenUrlBase64(pathImagenFooter);
    return {
      pageMargins: [ 30, 200, 0, 200 ],

      // pageSize: 'LEGAL',
      header: function (currentPage: any, pageCount: any, pageSize: any) {
        return [
          {image: imagenBase64Header, height: 200, width: 615}
        ]
      },
      footer: function (currentPage: any, pageCount: any, pageSize: any) {
        return [
          {
            image: imagenBase64Footer, height: 200, width: 615}
        ]
      },
      content: [
        {
          margin: [50, 10, 0, 0],
          text: setearFecha(trabajoRealizado.fechaTrabajoRealizado),
          color: '#275789'
        },
        {
          margin: [50, 20, 0, 0],
          text: `PROFORMA. ${trabajoRealizado.trabajo?.nombreComercial} / ${trabajoRealizado.trabajo?.placa}`,
          color: '#275789'
        },
        arregloDetallesRepuestos.length ? {
          margin: [50, 25, 0, 0],
          text: `REPUESTOS`,
          color: '#275789'
        } : {},
        arregloDetallesRepuestos.length ?  {
          style: 'tableExample',
          margin: [50, 10, 180, 0],
          table: {
            widths: [250, 100],
            body: [
              ...arregloDetallesRepuestos
            ]
          },
          layout: 'noBorders'

        } : {},
        arregloDetallesServicios.length ? {
          margin: [50, 25, 0, 0],
          text: `SERVICIOS`,
          color: '#275789'
        } : {},
        arregloDetallesServicios.length ? {
          style: 'tableExample',
          margin: [50, 10, 50, 0],
          table: {
            widths: [250, 100],
            body: [
              ...arregloDetallesServicios
            ]
          },
          layout: 'noBorders'
        } : {},
        {
          style: 'tableExample',
          margin: [50, 20, 50, 0],
          table: {
            widths: [250, 100],
            body: [
              ...arregloTotales,
            ]
          },
          layout: 'noBorders',
        }
      ]
    }
  }

}
