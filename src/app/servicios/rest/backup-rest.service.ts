import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfiguracionInterface} from "../../interfaces/configuracion.interface";
import {ServicioPrincipalService} from "../servicio-principal.service";
import {NotificacionService} from "../notificacion/notificacion.service";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class BackupRestService {

  constructor(
    protected readonly _httpClient: HttpClient,
  ) {

  }

  crearBackup(correo: string): Observable<any> {
    const url = environment.api.url;
    const puerto = environment.api.puerto;
    return this._httpClient.get(
      `${url}:${puerto}/backup?correo=${correo}`,
    ) as Observable<any>;
  }

}
