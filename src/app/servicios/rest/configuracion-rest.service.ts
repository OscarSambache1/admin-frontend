import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfiguracionInterface} from "../../interfaces/configuracion.interface";
import {ServicioPrincipalService} from "../servicio-principal.service";
import {NotificacionService} from "../notificacion/notificacion.service";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ConfiguracionRestService extends ServicioPrincipalService<ConfiguracionInterface>  {

  constructor(
    protected readonly _httpClient: HttpClient,
    private readonly _notificacionService: NotificacionService,
  ) {
    super(_httpClient);
    this.url = environment.api.url;
    this.puerto = environment.api.puerto;
    this.segmento = 'config';
  }

  crearEditarConfiguracion(datos: any): Observable<any> {
    return this._httpClient.post(
      `${this.url}:${this.puerto}/${this.segmento}/crear-editar-configuracion`,
      datos,
    );
  }
}
