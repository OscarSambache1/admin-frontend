import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ServicioPrincipalService<Interfaz> {
  protected readonly _httpClient: HttpClient;
  url = '';
  puerto = '';
  segmento = '';

  constructor(_httpClient: HttpClient) {
    this._httpClient = _httpClient;
  }

  findAll(consulta?: string): Observable<[Interfaz[], number]> {
    let baseUrl = `${this.url}:${this.puerto}/${this.segmento}`;
    if (consulta) {
      baseUrl = `${baseUrl}?${consulta}`;
    }
    return this._httpClient.get(baseUrl) as Observable<[Interfaz[], number]>;
  }

  updateOne(id: number, datos: Partial<Interfaz>): Observable<Interfaz> {
    return this._httpClient.put(
      `${this.url}:${this.puerto}/${this.segmento}/${id}`,
      datos
    ) as Observable<Interfaz>;
  }

  create(datos: Interfaz): Observable<Interfaz> {
    return this._httpClient.post(
      `${this.url}:${this.puerto}/${this.segmento}`,
      datos
    ) as Observable<Interfaz>;
  }

  finOneById(id: number): Observable<Interfaz> {
    return this._httpClient.get(
      `${this.url}:${this.puerto}/${this.segmento}/${id}`
    ) as Observable<Interfaz>;
  }

  delete(id: number): Observable<any> {
    return this._httpClient.delete(
      `${this.url}:${this.puerto}/${this.segmento}/${id}`
    ) as Observable<Interfaz>;
  }
}
