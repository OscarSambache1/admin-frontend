import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ActualizarPagosService {
  eventoActualizarPagos: EventEmitter<boolean> = new EventEmitter();

  actualizarPagos(actualizarPagos: boolean): void {
    this.eventoActualizarPagos.emit(actualizarPagos);
  }
}
