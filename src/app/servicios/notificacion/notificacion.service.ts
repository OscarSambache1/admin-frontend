import {Injectable} from '@angular/core';
import {Notificacion} from './interfaces/notificacion';
import {ToasterService} from 'angular2-toaster';
import Swal, {SweetAlertIcon, SweetAlertOptions, SweetAlertResult} from "sweetalert2";
import {ToastrService} from "ngx-toastr";

@Injectable({
  providedIn: 'root'
})
export class NotificacionService {

  constructor(
    private toastr: ToastrService
  ) {
  }

  public modalInfo(eIconType: SweetAlertIcon, sTitle: string, sText: string) {
    // Swal.fire({
    //   title: sTitle,
    //   text: sText,
    //   icon: eIconType,
    // });
    switch (eIconType) {
      case 'success': {
        this.toastr.success(sText, 'Éxito');
        break;
      }
      case 'error': {
        this.toastr.error(sText, 'Error');
        break;
      }
      case 'info': {
        this.toastr.info(sText, 'Info');
        break;
      }
      case 'warning': {
        this.toastr.warning(sText, 'Advertencia');
        break;
      }
      default: {
        this.toastr.info(sText, sTitle);
      }
    }

  }

  public async modalInfoAsync(eIconType: SweetAlertIcon, sTitle: string, sText: string) {
    return await Swal.fire({
      title: sTitle,
      text: sText,
      icon: eIconType,
    }).then((result: any) => {
      return result;
    });
  }

  public modalInfoWithOptions(eIconType: SweetAlertIcon, oOptions: SweetAlertOptions) {
    Swal.fire({
      icon: eIconType,
      ...oOptions
    });
  }

  public modalInfoHtml(eIconType: SweetAlertIcon, sTitle: string, oText: HTMLSpanElement) {
    Swal.fire({
      title: sTitle,
      html: oText,
      icon: eIconType,
    });
  }

  public async modalInfoHtmlAsync(eIconType: SweetAlertIcon, sTitle: string, oText: HTMLSpanElement) {
    return await Swal.fire({
      title: sTitle,
      html: oText,
      icon: eIconType,
    });
  }


  public async modalConfirmsSwal(eIconType: SweetAlertIcon, sTitle: string, sText: string) {
    let bResult: boolean = <boolean>await Swal.fire({
      title: sTitle,
      text: sText,
      icon: eIconType,
      showCancelButton: true,
      confirmButtonText: 'Sí, continuar',
      cancelButtonText: 'Cancelar',
      focusConfirm: true
    }).then((result: SweetAlertResult) => {
      if (result.value) {
        return true;
      } else {
        return false;
      }
    });

    return bResult;
  }

  public async modalConfirmsSwalWithOptions(eIconType: SweetAlertIcon, oOptions: SweetAlertOptions) {
    let bResult: boolean = <boolean>await Swal.fire({
      icon: eIconType,
      showCancelButton: true,
      confirmButtonText: 'Sí, continuar',
      cancelButtonText: 'Cancelar',
      focusConfirm: true,
      ...oOptions
    }).then((result: SweetAlertResult) => {
      if (result.value) {
        return true;
      } else {
        return false;
      }
    });

    return bResult;
  }
}
