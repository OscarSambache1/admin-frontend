export interface Notificacion {
  type?: any;
  title?: string;
  body?: string;
  showCloseButton?: boolean;
}
