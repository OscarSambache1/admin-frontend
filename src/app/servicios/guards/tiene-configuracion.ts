import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {ConfiguracionRestService} from "../rest/configuracion-rest.service";

@Injectable()
export class TieneConfiguracionGuardService implements CanActivate {
  constructor(
    private _configuracionRestService: ConfiguracionRestService,
    private readonly _router: Router) {
  }

  async canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Promise<any> {
    const consulta = {
      where: {},
    };
    const configuracion$ = this._configuracionRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    const respuesta = await (configuracion$.toPromise());
    if (respuesta && respuesta[0] && respuesta[0][0]) {
      return true;
    } else {
      this._router.navigate(['/app', 'ruta-configuracion']);
      return false;
    }
  }

}
