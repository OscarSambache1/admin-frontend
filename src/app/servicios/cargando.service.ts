import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CargandoService {
  cargando = false;
  cambioEstado: EventEmitter<boolean> = new EventEmitter();

  habilitar(): void {
    this.cargando = true;
    this.cambioEstado.emit(true);
  }

  deshabilitar(): void {
    setTimeout(() => {
      this.cargando = false;
      this.cambioEstado.emit(false);
    }, 600);
  }
}
