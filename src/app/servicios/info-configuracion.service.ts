import { EventEmitter, Injectable } from '@angular/core';
import {ConfiguracionInterface} from "../interfaces/configuracion.interface";
import {ConfiguracionRestService} from "./rest/configuracion-rest.service";

@Injectable({
  providedIn: 'root',
})
export class InfoConfiguracionService {
  cambioInfoConfig: EventEmitter<ConfiguracionInterface> = new EventEmitter();

  cambiarInfo(infoConfiguracion: ConfiguracionInterface): void {
    this.cambioInfoConfig.emit(infoConfiguracion);
  }
}
