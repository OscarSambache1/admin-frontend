import {EstadoAbono, EstadoGasto, EstadoTrabajo, TipoGasto} from "../enums/tipo-contacto.enums";

export const TIPOS_IDENTIFICACION =
  [
    {
      label: 'Cédula',
      value: 'C',
    },
    {
      label: 'RUC',
      value: 'R',
    },
    {
      label: 'Pasaporte',
      value: 'P',
    },
  ];

export const TIPOS_CONTACTO =
  [
    {
      label: 'Teléfono',
      value: 'T',
    },
    // {
    //   label: 'Celular',
    //   value: 'C',
    // },
    {
      label: 'Correo',
      value: 'E',
    },
  ];

export const TIPOS_CUENTA_BANCO =
  [
    {
      label: 'Corriente',
      value: 'C',
    },
    {
      label: 'Ahorros',
      value: 'A',
    },
  ];

export const TIPOS_GASTO =
    [
        {
            label: 'Repuestos de Inventario',
            value: TipoGasto.Repuesto,
        },
        {
            label: 'Productos / Servicios / Repuestos Extras',
            value: TipoGasto.Producto,
        },
    ];

export const ESTADOS_GASTO =
    [
        {
            label: 'Por Pagar',
            value: EstadoGasto.PorPagar,
        },
        {
            label: 'Abonado',
            value: EstadoGasto.Abonado,
        },
        {
            label: 'Pagado',
            value: EstadoGasto.Pagado,
        },
    ];

export const ESTADOS_ABONO =
  [
    {
      label: 'Por Pagar',
      value: EstadoAbono.PorPagar,
    },
    {
      label: 'Pagado',
      value: EstadoAbono.Pagado,
    },
  ];

export const ESTADOS_TRABAJO =
  [
    {
      label: 'No Guardado',
      value: EstadoTrabajo.NoGuardado,
    },
    {
      label: 'Guardado',
      value: EstadoTrabajo.Guardado,
    },
    {
      label: 'Finalizado',
      value: EstadoTrabajo.Finalizado,
    },
  ];
