import * as moment from 'moment';

export function obtenerRangoFechas(): {
  fechaInicio: string;
  fechaFin: string;
} {
  const fechaDosMesesAtras = moment()
    .subtract(2, 'M')
    .format(FORMATO_RANGO_FECHA_FILTROS);
  const mesActual = moment(fechaDosMesesAtras).month();
  const anioActual = moment(fechaDosMesesAtras).year();
  const fechaInicio = moment(`${anioActual}-${mesActual + 1}-01`).format(
    FORMATO_RANGO_FECHA_FILTROS
  );
  const fechaFin = moment().format(FORMATO_RANGO_FECHA_FILTROS);
  return {
    fechaInicio,
    fechaFin,
  };
}

export const FORMATO_RANGO_FECHA_FILTROS = 'YYYY-MM-DD';

export function setearStringFechasActuales(): string {
  return setearStringFechasConsulta(
    obtenerRangoFechas().fechaInicio,
    obtenerRangoFechas().fechaFin
  );
}

export function setearStringFechasConsulta(
  fechaInicio: any,
  fechaFin: any
): string {
  return `Between(\"${moment(`${fechaInicio}`).format(
    'YYYY-MM-DD'
  )}\", \"${moment(`${fechaFin}`).format('YYYY-MM-DD')}\")`;
}
