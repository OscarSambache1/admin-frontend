import * as moment from 'moment';

export function setearFecha (fecha: any) {
  const mes = moment(fecha).month();
  const anio = moment(fecha).year();
  const dia = moment(fecha).format('DD');
  const nombreMes = MESES[mes];
  return `Quito: ${dia} de ${nombreMes} del ${anio}.`
}

export const MESES = [
  'Enero',
  'Febrero',
  'Marzo',
  'Abril',
  'Mayo',
  'Junio',
  'Julio',
  'Agosto',
  'Septiembre',
  'Octubre',
  'Noviembre',
  'Diciembre',
]
