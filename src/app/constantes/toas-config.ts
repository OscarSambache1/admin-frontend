import { IndividualConfig } from 'ngx-toastr';

export const TOAST_CONFIG: Partial<IndividualConfig> = {
  progressBar: true,
  enableHtml: true,
  closeButton: true,
  progressAnimation: 'increasing',
  tapToDismiss: true,
};
