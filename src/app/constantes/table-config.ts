export const ROWS = 10;

export const ROWS_PER_PAGE = [5, 10, 15, 20, 25, 30, 40, 50, 100, 200, 500];

export const DECIMALS_DIGITS = 3;
