import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaStockBodegaComponent} from "./rutas/ruta-stock-bodega/ruta-stock-bodega.component";

const routes: Routes = [
  {
    path: '',
    component: RutaStockBodegaComponent,
    data: {
      breadcrumb: '',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StockBodegaRoutingModule { }
