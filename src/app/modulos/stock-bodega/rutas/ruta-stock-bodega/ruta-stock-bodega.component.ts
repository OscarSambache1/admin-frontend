import {Component, OnInit, ViewChild} from '@angular/core';
import {DxDataGridComponent} from "devextreme-angular";
import {ROWS, ROWS_PER_PAGE} from "../../../../constantes/table-config";
import {ArticuloInterface} from "../../../configuracion-datos/interfaces/articulo.interface";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../../servicios/cargando.service";
import {ArticuloRestService} from "../../../configuracion-datos/servicios/articulo-rest.service";
import {NotificacionService} from "../../../../servicios/notificacion/notificacion.service";
import {ModalCrearEditarProductoComponent} from "../../../../componentes/modales/modal-crear-editar-producto/modal-crear-editar-producto.component";
import {environment} from "../../../../../environments/environment";
import {EstadoStockEnum, EstadoStockEnumLabel} from "../../../../enums/tipo-contacto.enums";
import {ModalStockBodegaComponent} from "../../../../componentes/modales/modal-stock-bodega/modal-stock-bodega.component";
import {DomSanitizer} from "@angular/platform-browser";
import {Buffer} from 'buffer';

@Component({
  selector: 'app-ruta-stock-bodega',
  templateUrl: './ruta-stock-bodega.component.html',
  styleUrls: ['./ruta-stock-bodega.component.scss']
})
export class RutaStockBodegaComponent implements OnInit {

  @ViewChild(DxDataGridComponent)
  dataGrid!: DxDataGridComponent;

  rows = ROWS;
  rowsPerPage = ROWS_PER_PAGE;
  datos: ArticuloInterface[] = [];
  pageSize!: number;
  disabledTable!: boolean;
  pagination: number[] = [];
  paginationStart!: number;


  consulta: any = {
    relations: [
      'seccion',
      'subseccion',
      'proveedoresArticulo',
      'proveedoresArticulo.proveedor',
      'articulosTarifaImpuesto',
      'articulosTarifaImpuesto.tarifaImpuesto',
      'articulosTarifaImpuesto.tarifaImpuesto.impuesto',
      'archivos'
    ],
    where: {
      esServicio: 0,
    },
    order: {
      id: 'DESC',
    },
  };

  estadoStock = [
    {
      value: EstadoStockEnum.Disponible,
      label: 'DISPONIBLE'
    },
    {
      value: EstadoStockEnum.PorAgotarse,
      label: 'POR AGOTARSE'
    },
    {
      value: EstadoStockEnum.Agotado,
      label: 'AGOTADO'
    },
  ];

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _articuloRestService: ArticuloRestService,
    private readonly _notificacionService: NotificacionService,
    public readonly _domSanitizer: DomSanitizer,
  ) {
  }

  ngOnInit(): void {
    this.pagination = ROWS_PER_PAGE;
    this.paginationStart = ROWS;
    this.cargarDatosIniciales(this.consulta);
  }

  cargarDatosIniciales(consulta: {}): void {
    this._cargandoService.habilitar();
    const producto$ = this._articuloRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    producto$.subscribe({
      next: (producto) => {
        this.datos = producto[0];
        this.pageSize = producto[1];
        this.disabledTable = false;
        this._cargandoService.deshabilitar();
      },
      error: (error) => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
      },
    });
  }

  onToolbarPreparing(evento: any): void {
    evento.toolbarOptions.items.unshift(
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Agregar nuevo registro',
          onClick: this.add.bind(this, undefined, false),
          icon: 'plus',
        },
      },
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Recargar datos de la tabla',
          icon: 'refresh',
          onClick: this.onRefreshDataGrid.bind(this),
        },
      }
    );
  }

  private onRefreshDataGrid(): void {
    this.cargarDatosIniciales(this.consulta);
    void this.dataGrid.instance.refresh();
  }

  add(producto?: ArticuloInterface, soloVer?: boolean): void {
    const dialogRef = this._matDialog.open(ModalCrearEditarProductoComponent, {
      width: '1550px',
      data: {
        producto,
        soloVer,
      },
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          if (producto) {
            response.id = producto.id;
          }
          this.crearEditar(response);
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

  crearEditar(producto: ArticuloInterface): void {
    const formData: FormData = new FormData();
    formData.append('imagen', producto.imagen);
    formData.append('datos', JSON.stringify(producto));
    const producto$ = this._articuloRestService.crearEditarProductoServicio(formData, 0);

    producto$.subscribe({
      next: () => {
        this._notificacionService.modalInfo(
          'success',
          'Info',
          'Registro guardado correctamente',
        );
        this.onRefreshDataGrid();
      },
      error: (err) => {
        console.error({
          mensage: 'Error guardar registro',
          error: err,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al guardar el registro',
        );
      },
    });
  }

  setearUrlImagen(producto: ArticuloInterface) {
    if (producto) {
      if (producto.archivos?.length) {
        const imagen = producto.archivos[0];
        const buffer = imagen.buffer?.data;
        return this._domSanitizer.bypassSecurityTrustUrl(`data:${imagen.mimetype};base64, ${Buffer.from(buffer)}`) as any;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }

  setearLabelEstadoStock(articulo: ArticuloInterface) {
    // @ts-ignore
    return EstadoStockEnumLabel[articulo.estadoStock as any];
  }

  setearClassEStadoStock(articulo: ArticuloInterface) {
    const clase = 'p-button-rounded p-button-infop-button-rounded btn-block';
    let tipo;
    if (articulo.estadoStock === EstadoStockEnum.Agotado) {
      tipo = 'p-button-danger';
    }
    if (articulo.estadoStock === EstadoStockEnum.Disponible) {
      tipo = 'p-button-success';
    }
    if (articulo.estadoStock === EstadoStockEnum.PorAgotarse) {
      tipo = 'p-button-warning';
    }
    return clase + ' ' + tipo;
  }

  async eliminar(registroAEliminar: ArticuloInterface) {
    const respuestaModalConfirmacion = await this._notificacionService.modalConfirmsSwal(
      'question',
      'Advertencia',
      'Está seguro de borrar el registro',
    );
    if (respuestaModalConfirmacion) {
      const promesaTieneRegistrosHijos = await (this._articuloRestService.findAll(
        `busqueda=${JSON.stringify(
          {
            where: {
              id: registroAEliminar.id,
            },
            relations: [
              'trabajoRealizadoDetalles',
              'ordenTrabajoDetalles',
              'facturaDetalles',
            ],
          }
        )}`
      ).toPromise());
      const articulo = promesaTieneRegistrosHijos[0][0];
      const trabajoRealizadoDetalles = articulo.trabajoRealizadoDetalles;
      const ordenTrabajoDetalles = articulo.ordenTrabajoDetalles;
      const detallesFactura = articulo.facturaDetalles;
      if (!trabajoRealizadoDetalles?.length && !ordenTrabajoDetalles?.length && !detallesFactura?.length) {
        const producto$ = this._articuloRestService.eliminarArticulo(registroAEliminar.id as number);

        producto$.subscribe({
          next: () => {
            this._notificacionService.modalInfo(
              'success',
              'Info',
              'Registro borrado correctamente',
            );
            this.onRefreshDataGrid();
          },
          error: (err) => {
            console.error({
              mensage: 'Error al borrar registro',
              error: err,
            });
            this._notificacionService.modalInfo(
              'error',
              'Error',
              'Error al borrar el registro',
            );
          },
        });
      } else {
        this._notificacionService.modalInfo(
          'warning',
          'Advertencia',
          'El registro no puede ser borrado porque ya es usado en otros procesos del sistema',
        );
      }
    }
  }

}
