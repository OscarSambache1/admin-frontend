import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StockBodegaRoutingModule } from './stock-bodega-routing.module';
import { RutaStockBodegaComponent } from './rutas/ruta-stock-bodega/ruta-stock-bodega.component';
import {IMPORTS_MODULO} from "../imports/imports-modulo";
import {ComponentesModule} from "../../componentes/componentes.module";


@NgModule({
  declarations: [
    RutaStockBodegaComponent
  ],
  imports: [
    CommonModule,
    StockBodegaRoutingModule,
    ...IMPORTS_MODULO,
    ComponentesModule,
  ]
})
export class StockBodegaModule { }
