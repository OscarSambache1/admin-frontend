import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProformasRoutingModule } from './proformas-routing.module';
import { RutaProformaComponent } from './ruta/ruta-proforma/ruta-proforma.component';
import { RutaCrearEditarProformaComponent } from './ruta/ruta-crear-editar-proforma/ruta-crear-editar-proforma.component';
import {IMPORTS_MODULO} from "../imports/imports-modulo";
import {ComponentesModule} from "../../componentes/componentes.module";


@NgModule({
  declarations: [
    RutaProformaComponent,
    RutaCrearEditarProformaComponent
  ],
  imports: [
    CommonModule,
    ProformasRoutingModule,
    ...IMPORTS_MODULO,
    ComponentesModule,
  ]
})
export class ProformasModule { }
