import {Component, OnInit, ViewChild} from '@angular/core';
import {DxDataGridComponent} from "devextreme-angular";
import {ROWS, ROWS_PER_PAGE} from "../../../../constantes/table-config";
import {TrabajoRealizadoInterface} from "../../../trabajos/interfaces/trabajo-realizado.interface";
import {EstadoTrabajo, EstadoTrabajoEnum} from "../../../../enums/tipo-contacto.enums";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../../servicios/cargando.service";
import {TrabajoRealizadoRestService} from "../../../trabajos/servicios/trabajo-realizado-rest.service";
import {NotificacionService} from "../../../../servicios/notificacion/notificacion.service";
import {ImprimitService} from "../../../../servicios/imprimit.service";

@Component({
  selector: 'app-ruta-proforma',
  templateUrl: './ruta-proforma.component.html',
  styleUrls: ['./ruta-proforma.component.scss']
})
export class RutaProformaComponent implements OnInit {

  @ViewChild(DxDataGridComponent)
  dataGrid!: DxDataGridComponent;

  rows = ROWS;
  rowsPerPage = ROWS_PER_PAGE;
  datos: TrabajoRealizadoInterface[] = [];
  pageSize!: number;
  disabledTable!: boolean;
  pagination: number[] = [];
  paginationStart!: number;
  consulta: any = {
    relations: [
      'vehiculo',
      'vehiculo.marca',
      'cliente',
      'cliente.informacionTributaria',
      'trabajo',
      'trabajoRealizadoDetalles',
      'trabajoRealizadoDetalles.articulo'
    ],
    where: {
      esProforma: 1,
    },
    order: {
      id: 'DESC',
    },
  };
  estadoAccionEnum: any = EstadoTrabajoEnum;
  estadosAccion = [
    {
      value: EstadoTrabajo.Guardado,
      label: EstadoTrabajoEnum.G
    },
    {
      value: EstadoTrabajo.NoGuardado,
      label: EstadoTrabajoEnum.NG,
    },
    {
      value: EstadoTrabajo.Finalizado,
      label: EstadoTrabajoEnum.F
    },
  ];
  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _trabajoRealizadoRestService: TrabajoRealizadoRestService,
    private readonly _notificacionService: NotificacionService,
    private readonly _imprimirService: ImprimitService,
  ) {}

  ngOnInit(): void {
    this.pagination = ROWS_PER_PAGE;
    this.paginationStart = ROWS;
    this.cargarDatosIniciales(this.consulta);
  }

  cargarDatosIniciales(consulta: {}): void {
    this._cargandoService.habilitar();
    const trabajoRealizado$ = this._trabajoRealizadoRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    trabajoRealizado$.subscribe({
      next: (trabajoRealizado) => {
        this.datos = trabajoRealizado[0];
        this.pageSize = this.datos.length;
        this.disabledTable = false;
        this._cargandoService.deshabilitar();
      },
      error: (error) => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
      },
    });
  }

  onToolbarPreparing(evento: any): void {
    evento.toolbarOptions.items.unshift(
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Recargar datos de la tabla',
          icon: 'refresh',
          onClick: this.onRefreshDataGrid.bind(this),
        },
      },
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Agregar nueva proforma',
          onClick: this.add.bind(this, undefined),
          icon: 'plus',
        },
      },
    );
  }

  private onRefreshDataGrid(): void {
    this.cargarDatosIniciales(this.consulta);
    void this.dataGrid.instance.refresh();
  }

  add(trabajoRealizado?: TrabajoRealizadoInterface) {
    const ruta = ['/app', 'proformas', 'crear-editar-proforma', (trabajoRealizado?.id || -1)];
    void this._router.navigate(ruta);
  }

  setearCliente(trabajoRealizado?: TrabajoRealizadoInterface) {
    return trabajoRealizado?.trabajo?.nombreComercial || 'Consumidor Final';
  }

  async imprimir(trabajoRealizado: any) {
    const pdfDefinition: any = await this._imprimirService.setearObjetoProforma(trabajoRealizado);
    await this._imprimirService.imprimir(pdfDefinition);
  }

  async eliminar(data: any) {
    const respuestaModalConfirmacion = await this._notificacionService.modalConfirmsSwal(
      'question',
      'Advertencia',
      `¿Esta seguro de eliminar la proforma?`,
    );
    if (respuestaModalConfirmacion) {
      const trabajo$ = this._trabajoRealizadoRestService
        .eliminarTrabajo(data?.id);

      trabajo$
        .subscribe(
          (respuesta) => {
            this._notificacionService.modalInfo(
              'success',
              'Éxito',
              `El registro ha sido eliminado con éxito`,
            );
            this._cargandoService.deshabilitar();
            this.cargarDatosIniciales(this.consulta);
          },
          error => {
            this._notificacionService.modalInfo(
              'error',
              'Error',
              `Error al eliminar`,
            );
            this.cargarDatosIniciales(this.consulta);
            this._cargandoService.deshabilitar();
            console.error({
              mensaje: `Error al eliminar`,
              error,
            });
          }
        );
    }
  }

}
