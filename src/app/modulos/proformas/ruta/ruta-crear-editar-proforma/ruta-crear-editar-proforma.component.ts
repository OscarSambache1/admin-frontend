import { Component, OnInit } from '@angular/core';
import {TrabajoRealizadoInterface} from "../../../trabajos/interfaces/trabajo-realizado.interface";
import {TrabajoInterface} from "../../../trabajos/interfaces/trabajo.interface";
import {EstadoTrabajo, TipoGasto} from "../../../../enums/tipo-contacto.enums";
import {NotificacionService} from "../../../../servicios/notificacion/notificacion.service";
import {CargandoService} from "../../../../servicios/cargando.service";
import {TrabajoRealizadoRestService} from "../../../trabajos/servicios/trabajo-realizado-rest.service";
import {ActivatedRoute, Router} from "@angular/router";
import {mergeMap} from "rxjs/operators";

@Component({
  selector: 'app-ruta-crear-editar-proforma',
  templateUrl: './ruta-crear-editar-proforma.component.html',
  styleUrls: ['./ruta-crear-editar-proforma.component.scss']
})
export class RutaCrearEditarProformaComponent implements OnInit {

  formularioTrabajoRealizadoValido!: boolean;

  formularioProformaValido!: boolean;

  trabajoRealizadoFormulario!: TrabajoRealizadoInterface | undefined;
  // proformaFormulario!: TrabajoRealizadoInterface | undefined;
  trabajoFormulario!: TrabajoInterface | undefined;

  trabajoRealizadoPrevio!: TrabajoRealizadoInterface;

  estaCargando = false;

  estadosTrabajo = EstadoTrabajo;

  constructor(
    private readonly _notificacionService: NotificacionService,
    private readonly _cargandoService: CargandoService,
    private readonly _trabajoRealizadoRestService: TrabajoRealizadoRestService,
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.estaCargando = true;
    this._cargandoService.habilitar();
    this._activatedRoute.params
      .pipe(
        mergeMap(params => {
          const idTrabajoRealizado = params.idTrabajoRealizado;
          const consulta: any = {
            relations: [
              'cliente',
              'cliente.informacionTributaria',
              'trabajo',
              'trabajo.vehiculo',
              'trabajo.vehiculo.marca',
              'trabajo.vehiculo.cliente',
              'trabajo.vehiculo.cliente.informacionTributaria',
              'trabajoRealizadoDetalles',
              'trabajoRealizadoDetalles.articulo',
            ],
            where: {
              id: idTrabajoRealizado,
            },
            order: {
              id: 'DESC',
            },
          };
          return this._trabajoRealizadoRestService.findAll(
            `busqueda=${JSON.stringify(consulta)}`
          );
        })
      )
      .subscribe(respuesta => {
        if (respuesta && respuesta[0] && respuesta[0][0]) {
          this.trabajoRealizadoPrevio = respuesta[0][0];
          if (this.trabajoRealizadoPrevio) {
            this.trabajoRealizadoPrevio?.trabajoRealizadoDetalles?.map(trabajoRealizadoDetalle => {
              trabajoRealizadoDetalle.tipo = TipoGasto.Repuesto;
            });
          }
        }
        this._cargandoService.deshabilitar();
        this.estaCargando = false;
      }, error => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
        this.estaCargando = false;
      });
  }

  obtenerTrabajoRealizado(trabajoRealizado: TrabajoRealizadoInterface | boolean) {
    if (trabajoRealizado) {
      this.trabajoRealizadoFormulario = trabajoRealizado as TrabajoRealizadoInterface;
      this.formularioTrabajoRealizadoValido = true;
    } else {
      this.formularioTrabajoRealizadoValido = false;
    }
  }

  // obtenerProforma(trabajoRealizado: TrabajoRealizadoInterface | boolean) {
  //   if (trabajoRealizado) {
  //     this.proformaFormulario = trabajoRealizado as TrabajoRealizadoInterface;
  //     this.formularioProformaValido = true;
  //   } else {
  //     this.formularioProformaValido = false;
  //   }
  // }

  async guardarTrabajoRealizado(accion: EstadoTrabajo) {
    let mensaje = '';
    if (accion === EstadoTrabajo.Guardado) {
      mensaje = 'Esta seguro de guardar la proforma, en caso de hacerlo podrá seguir editando la información hasta finalizarlo';
    }
    if (accion === EstadoTrabajo.Finalizado) {
      mensaje = 'Esta seguro de finalizar la proforma, tenga en cuenta que ya no podrá editar la información';
    }
    const respuestaModalConfirmacion = await this._notificacionService.modalConfirmsSwal(
      'question',
      'Advertencia',
      mensaje,
    );
    if (respuestaModalConfirmacion) {
      if (this.trabajoRealizadoFormulario) {
        this.trabajoRealizadoFormulario.estadoAccion = accion;
        if (this.trabajoFormulario) {
          this.trabajoRealizadoFormulario.esProforma = 1;
          this.trabajoRealizadoFormulario.vehiculo = this.trabajoFormulario.vehiculo;
        }
      }

      if (this.trabajoRealizadoPrevio && this.trabajoRealizadoFormulario) {
        this.trabajoRealizadoFormulario.id = this.trabajoRealizadoPrevio.id;
      }
      const datos: any = this.trabajoRealizadoFormulario;
      datos.esProforma = true;

      this._cargandoService.habilitar();
      const trabajoRealizado$ = this._trabajoRealizadoRestService
        .crearEditarTrabajoRealizado(datos);

      trabajoRealizado$
        .subscribe(
          (respuesta: TrabajoInterface) => {
            this.estaCargando = true;
            let mensajeExito = '';
            if (accion === EstadoTrabajo.Guardado) {
              mensajeExito = `La Proforma ha sido guardada con éxito`
            }
            if (accion === EstadoTrabajo.Finalizado) {
              mensajeExito = `La Proforma ha sido finalizada con éxito`
            }

            this._notificacionService.modalInfo(
              'success',
              'Éxito',
              mensajeExito,
            );
            this._cargandoService.deshabilitar();
            this.estaCargando = false;
            const ruta = ['/app', 'proformas'];
            void this._router.navigate(ruta);
          },
          error => {
            let mensajeError = '';
            if (accion === EstadoTrabajo.Guardado) {
              mensajeError = `Error al guardar la proforma `
            }
            if (accion === EstadoTrabajo.Finalizado) {
              mensajeError = `Error al finalizar la proforma `
            }
            this._notificacionService.modalInfo(
              'error',
              'Error',
              mensajeError,
            );
            this._cargandoService.deshabilitar();
            console.error({
              mensaje: mensajeError,
              error,
            });
          }
        );
    }
  }

  obtenerTrabajo(trabajo: TrabajoInterface | boolean) {
    if (trabajo) {
      this.formularioProformaValido = true;
      this.trabajoFormulario = trabajo as TrabajoInterface;
    } else {
      this.formularioProformaValido = false;
    }
  }

  setearSoloVer() {
    return this.trabajoRealizadoPrevio?.estadoAccion === EstadoTrabajo.Finalizado;
  }
}
