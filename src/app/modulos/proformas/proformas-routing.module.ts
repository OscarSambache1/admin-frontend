import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaCrearEditarProformaComponent} from "./ruta/ruta-crear-editar-proforma/ruta-crear-editar-proforma.component";
import {RutaProformaComponent} from "./ruta/ruta-proforma/ruta-proforma.component";

const routes: Routes = [
  {
    path: '',
    component: RutaProformaComponent,
    data: {
      breadcrumb: '',
    },
  },
  {
    path: 'crear-editar-proforma/:idTrabajoRealizado',
    component: RutaCrearEditarProformaComponent,
    data: {
      breadcrumb: 'Guardar',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProformasRoutingModule { }
