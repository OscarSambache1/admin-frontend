import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModulosRoutingModule } from './modulos-routing.module';
import {RutaMenuInicioComponent} from "./rutas/ruta-menu-inicio/ruta-menu-inicio.component";
import {RutaConfiguracionComponent} from "../rutas/ruta-configuracion/ruta-configuracion.component";
import {IMPORTS_MODULO} from "./imports/imports-modulo";
import {TieneConfiguracionGuardService} from "../servicios/guards/tiene-configuracion";
import {ComponentesModule} from "../componentes/componentes.module";


@NgModule({
  declarations: [
    RutaMenuInicioComponent,
    RutaConfiguracionComponent
  ],
  imports: [
    CommonModule,
    ModulosRoutingModule,
    ...IMPORTS_MODULO,
    ComponentesModule,
  ],
  providers: [
    TieneConfiguracionGuardService,
  ]
})
export class ModulosModule { }
