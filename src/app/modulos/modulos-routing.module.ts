import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaMenuInicioComponent} from "./rutas/ruta-menu-inicio/ruta-menu-inicio.component";
import {RutaConfiguracionComponent} from "../rutas/ruta-configuracion/ruta-configuracion.component";
import {TieneConfiguracionGuardService} from "../servicios/guards/tiene-configuracion";

const routes: Routes = [
  {
    canActivate: [TieneConfiguracionGuardService],
    path: 'configuracion-datos',
    loadChildren: () =>
      import('./configuracion-datos/configuracion-datos.module').then((modulo) => modulo.ConfiguracionDatosModule),
    data: {
      breadcrumb: 'Base de Datos',
    },
  },
  {
    canActivate: [TieneConfiguracionGuardService],
    path: 'gastos',
    loadChildren: () =>
      import('./gastos/gastos.module').then((modulo) => modulo.GastosModule),
    data: {
      breadcrumb: 'Gastos',
    },
  },
  {
    canActivate: [TieneConfiguracionGuardService],
    path: 'stock-bodega',
    loadChildren: () =>
      import('./stock-bodega/stock-bodega.module').then((modulo) => modulo.StockBodegaModule),
    data: {
      breadcrumb: 'Stock Bodega',
    },
  },
  {
    canActivate: [TieneConfiguracionGuardService],
    path: 'trabajos',
    loadChildren: () =>
      import('./trabajos/trabajos.module').then((modulo) => modulo.TrabajosModule),
    data: {
        breadcrumb: 'Trabajos',
    },
  },
  {
    canActivate: [TieneConfiguracionGuardService],
    path: 'proformas',
    loadChildren: () =>
      import('./proformas/proformas.module').then((modulo) => modulo.ProformasModule),
    data: {
      breadcrumb: 'Proformas',
    },
  },
  {
    canActivate: [TieneConfiguracionGuardService],
    path: 'proforma-rapida',
    loadChildren: () =>
      import('./proforma-rapida/proforma-rapida.module').then((modulo) => modulo.ProformaRapidaModule),
    data: {
      breadcrumb: 'Proforma Rápida',
    },
  },
  {
    canActivate: [TieneConfiguracionGuardService],
    path: 'listados',
    loadChildren: () =>
      import('./listados/listados.module').then((modulo) => modulo.ListadosModule),
    data: {
      breadcrumb: 'Listados',
    },
  },
  {
    canActivate: [TieneConfiguracionGuardService],
    path: '',
    component: RutaMenuInicioComponent,
    data: {
      breadcrumb: 'Inicio',
    },
  },
  {
    path: 'ruta-configuracion',
    component: RutaConfiguracionComponent,
    data: {
      breadcrumb: 'Configuración',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModulosRoutingModule { }
