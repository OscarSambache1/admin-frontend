import { InputSwitchModule } from 'primeng/inputswitch';
import { DialogModule } from 'primeng/dialog';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { AccordionModule } from 'primeng/accordion';
import { DropdownModule } from 'primeng/dropdown';
import { ToggleButtonModule } from 'primeng/togglebutton';
import {
  DxButtonModule,
  DxDataGridModule,
  DxSelectBoxModule, DxSliderModule,
} from 'devextreme-angular';
import { CheckboxModule } from 'primeng/checkbox';
import { FileUploadModule } from 'primeng/fileupload';
import { TableModule } from 'primeng/table';
import { PickListModule } from 'primeng/picklist';
import { RadioButtonModule } from 'primeng/radiobutton';
import { TriStateCheckboxModule } from 'primeng/tristatecheckbox';
import { SelectButtonModule } from 'primeng/selectbutton';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { MultiSelectModule } from 'primeng/multiselect';
import { InputTextModule } from 'primeng/inputtext';
import { BadgeModule } from 'primeng/badge';
import { CascadeSelectModule } from 'primeng/cascadeselect';
import {NgbTooltipModule} from "@ng-bootstrap/ng-bootstrap";
import {RippleModule} from "primeng/ripple";
import {InputNumberModule} from "primeng/inputnumber";
import {MatStepperModule} from "@angular/material/stepper";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatButtonModule} from "@angular/material/button";
import {KeyFilterModule} from "primeng/keyfilter";
import {TabMenuModule} from 'primeng/tabmenu';
import {ImageModule} from 'primeng/image';
import {DividerModule} from 'primeng/divider';
import {TabViewModule} from "primeng/tabview";
import {SkeletonModule} from "primeng/skeleton";
import {PanelModule} from "primeng/panel";
import { PdfViewerModule } from 'ng2-pdf-viewer';
import {AvatarModule} from 'primeng/avatar';
import {AvatarGroupModule} from 'primeng/avatargroup';
import {InputTextareaModule} from "primeng/inputtextarea";
import {DataViewModule} from "primeng/dataview";
import {ButtonModule} from "primeng/button";

export const IMPORTS_MODULO = [
  MatDialogModule,
  ReactiveFormsModule,
  AutoCompleteModule,
  FormsModule,
  DxButtonModule,
  DxDataGridModule,
  DxSelectBoxModule,
  DialogModule,
  InputSwitchModule,
  AccordionModule,
  DropdownModule,
  ToggleButtonModule,
  CheckboxModule,
  FileUploadModule,
  TableModule,
  PickListModule,
  RadioButtonModule,
  TriStateCheckboxModule,
  SelectButtonModule,
  ScrollPanelModule,
  MultiSelectModule,
  InputTextModule,
  BadgeModule,
  CascadeSelectModule,
  NgbTooltipModule,
  RippleModule,
  InputNumberModule,
  MatStepperModule,
  MatFormFieldModule,
  MatButtonModule,
  DropdownModule,
  KeyFilterModule,
  TabMenuModule,
  ImageModule,
  DividerModule,
  TabViewModule,
  SkeletonModule,
  DxSliderModule,
  PanelModule,
  PdfViewerModule,
  InputSwitchModule,
  AvatarModule,
  AvatarGroupModule,
  InputTextareaModule,
  DataViewModule,
  ButtonModule,
  RippleModule,
];
