import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-ruta-menu-inicio',
  templateUrl: './ruta-menu-inicio.component.html',
  styleUrls: ['./ruta-menu-inicio.component.scss']
})
export class RutaMenuInicioComponent implements OnInit {
  arregloItemsMenuInicio = [
    {
      nombre: 'Base de Datos',
      texto: 'Base de Datos',
      imagen: '/assets/images/base-datos.png',
      ruta: 'configuracion-datos'
    },
    {
      nombre: 'Stock Boedga',
      texto: 'Stock Bodega',
      imagen: '/assets/images/stock.png',
      ruta: 'stock-bodega'
    },
    {
      nombre: 'Gastos',
      texto: 'Gastos',
      imagen: '/assets/images/gasto.png',
      ruta: 'gastos'
    },
    {
      nombre: 'Trabajos',
      texto: 'Trabajos',
      imagen: '/assets/images/trabajos.png',
      ruta: 'trabajos'
    },
    {
      nombre: 'Proformas',
      texto: 'Proformas',
      imagen: '/assets/images/proforma.png',
      ruta: 'proformas'
    },
    {
      nombre: 'Proforma Rápida',
      texto: 'Proforma Rápída',
      imagen: '/assets/images/proforma-rapida.png',
      ruta: 'proforma-rapida'
    },
    {
      nombre: 'Listados',
      texto: 'Listados',
      imagen: '/assets/images/listado.png',
      ruta: 'listados'
    },
    {
      nombre: 'Configuración',
      texto: 'Configuración',
      imagen: '/assets/images/configuracion.png',
      ruta: 'ruta-configuracion'
    },
  ];
  constructor(
    private readonly _router: Router
  ) { }

  ngOnInit(): void {
  }

  irARuta(rutaOpcion: string) {
    const ruta = ['/app', rutaOpcion];
    void this._router.navigate(ruta);
  }
}
