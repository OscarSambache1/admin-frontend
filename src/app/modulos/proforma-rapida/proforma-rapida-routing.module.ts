import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ProformaRapidaComponent} from "./proforma-rapida.component";

const routes: Routes = [
  {
    path: '',
    component: ProformaRapidaComponent,
    data: {
      breadcrumb: 'Proforma Rápida',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProformasRoutingModule { }
