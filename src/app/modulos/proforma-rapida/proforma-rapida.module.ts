import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProformaRapidaComponent } from './proforma-rapida.component';
import {ProformasRoutingModule} from "./proforma-rapida-routing.module";
import {IMPORTS_MODULO} from "../imports/imports-modulo";
import {ComponentesModule} from "../../componentes/componentes.module";



@NgModule({
  declarations: [
    ProformaRapidaComponent
  ],
  imports: [
    CommonModule,
    ProformasRoutingModule,
    ...IMPORTS_MODULO,
    ComponentesModule,
  ]
})
export class ProformaRapidaModule { }
