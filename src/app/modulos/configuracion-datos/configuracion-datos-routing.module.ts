import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {MenuConfiguracionDatosComponent} from "./rutas/menu-configuracion-datos/menu-configuracion-datos.component";

const routes: Routes = [
  {
    path: '',
    component: MenuConfiguracionDatosComponent,
    data: {
      breadcrumb: '',
    },
  },
  {
    path: 'marca',
    loadChildren: () =>
      import('./marca/marca.module').then((modulo) => modulo.MarcaModule),
    data: {
      breadcrumb: '',
    },
  },
  {
    path: 'modelo',
    loadChildren: () =>
      import('./modelo/modelo.module').then((modulo) => modulo.ModeloModule),
    data: {
      breadcrumb: '',
    },
  },
  {
    path: 'accesorio',
    loadChildren: () =>
      import('./accesorio/accesorio.module').then((modulo) => modulo.AccesorioModule),
    data: {
      breadcrumb: '',
    },
  },
  {
    path: 'banco',
    loadChildren: () =>
      import('./banco/banco.module').then((modulo) => modulo.BancoModule),
    data: {
      breadcrumb: '',
    },
  },
  {
    path: 'forma-pago',
    loadChildren: () =>
        import('./forma-pago/forma-pago.module').then((modulo) => modulo.FormaPagoModule),
    data: {
      breadcrumb: '',
    },
  },
  {
    path: 'tipo-cambio',
    loadChildren: () =>
      import('./tipo-cambio/tipo-cambio.module').then((modulo) => modulo.TipoCambioModule),
    data: {
      breadcrumb: '',
    },
  },
  {
    path: 'tipo-combustible',
    loadChildren: () =>
      import('./tipo-combustible/tipo-combustible.module').then((modulo) => modulo.TipoCombustibleModule),
    data: {
      breadcrumb: '',
    },
  },
  {
    path: 'tipo-vehiculo',
    loadChildren: () =>
      import('./tipo-vehiculo/tipo-vehiculo.module').then((modulo) => modulo.TipoVehiculoModule),
    data: {
      breadcrumb: '',
    },
  },
  {
    path: 'impuesto',
    loadChildren: () =>
      import('./impuesto/impuesto.module').then((modulo) => modulo.ImpuestoModule),
    data: {
      breadcrumb: '',
    },
  },
  {
    path: 'seccion',
    loadChildren: () =>
      import('./seccion/seccion.module').then((modulo) => modulo.SeccionModule),
    data: {
      breadcrumb: '',
    },
  },
  {
    path: 'subseccion',
    loadChildren: () =>
      import('./subseccion/subseccion.module').then((modulo) => modulo.SubseccionModule),
    data: {
      breadcrumb: '',
    },
  },
  {
    path: 'proveedor',
    loadChildren: () =>
      import('./proveedor/proveedor.module').then((modulo) => modulo.ProveedorModule),
    data: {
      breadcrumb: '',
    },
  },
  {
    path: 'proveedor',
    loadChildren: () =>
      import('./proveedor/proveedor.module').then((modulo) => modulo.ProveedorModule),
    data: {
      breadcrumb: '',
    },
  },
  {
    path: 'producto',
    loadChildren: () =>
      import('./producto/producto.module').then((modulo) => modulo.ProductoModule),
    data: {
      breadcrumb: '',
    },
  },
  {
    path: 'servicio',
    loadChildren: () =>
      import('./servicio/servicio.module').then((modulo) => modulo.ServicioModule),
    data: {
      breadcrumb: '',
    },
  },
  {
    path: 'cliente',
    loadChildren: () =>
      import('./cliente/cliente.module').then((modulo) => modulo.ClienteModule),
    data: {
      breadcrumb: '',
    },
  },
  {
    path: 'vehiculo',
    loadChildren: () =>
      import('./vehiculo/vehiculo.module').then((modulo) => modulo.VehiculoModule),
    data: {
      breadcrumb: '',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfiguracionDatosRoutingModule { }
