import {Component, OnInit, ViewChild} from '@angular/core';
import {DxDataGridComponent} from "devextreme-angular";
import {ROWS, ROWS_PER_PAGE} from "../../../../constantes/table-config";
import {SubseccionInterface} from "../../interfaces/subseccion.interface";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../../servicios/cargando.service";
import {NotificacionService} from "../../../../servicios/notificacion/notificacion.service";
import {ModalCrearEditarSubseccionComponent} from "../../../../componentes/modales/modal-crear-editar-subseccion/modal-crear-editar-subseccion.component";
import {SubseccionRestService} from "../../servicios/subseccion-rest.service";

@Component({
  selector: 'app-ruta-subseccion',
  templateUrl: './ruta-subseccion.component.html',
  styleUrls: ['./ruta-subseccion.component.scss']
})
export class RutaSubseccionComponent implements OnInit {


  @ViewChild(DxDataGridComponent)
  dataGrid!: DxDataGridComponent;

  rows = ROWS;
  rowsPerPage = ROWS_PER_PAGE;
  datos: SubseccionInterface[] = [];
  pageSize!: number;
  disabledTable!: boolean;
  pagination: number[] = [];
  paginationStart!: number;


  consulta: any = {
    relations: ['seccion'],
    where: {},
    order: {
      id: 'DESC',
    },
  };

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _subseccionRestService: SubseccionRestService,
    private readonly _notificacionService: NotificacionService,
  ) {}

  ngOnInit(): void {
    this.pagination = ROWS_PER_PAGE;
    this.paginationStart = ROWS;
    this.cargarDatosIniciales(this.consulta);
  }

  cargarDatosIniciales(consulta: {}): void {
    this._cargandoService.habilitar();
    const subseccion$ = this._subseccionRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    subseccion$.subscribe({
      next: (subseccion) => {
        this.datos = subseccion[0];
        this.pageSize = subseccion[1];
        this.disabledTable = false;
        this._cargandoService.deshabilitar();
      },
      error: (error) => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
      },
    });
  }

  onToolbarPreparing(evento: any): void {
    evento.toolbarOptions.items.unshift(
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Agregar nuevo registro',
          onClick: this.add.bind(this, undefined),
          icon: 'plus',
        },
      },
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Recargar datos de la tabla',
          icon: 'refresh',
          onClick: this.onRefreshDataGrid.bind(this),
        },
      }
    );
  }

  private onRefreshDataGrid(): void {
    this.cargarDatosIniciales(this.consulta);
    void this.dataGrid.instance.refresh();
  }

  add(subseccion?: SubseccionInterface): void {
    const dialogRef = this._matDialog.open(ModalCrearEditarSubseccionComponent, {
      width: '750px',
      data: {
        subseccion,
      },
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          if (subseccion) {
            this.editar(response, subseccion.id as number);
          } else {
            this.crear(response);
          }
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

  crear(subseccion: SubseccionInterface): void {
    const subseccion$ = this._subseccionRestService.create(subseccion);

    subseccion$.subscribe({
      next: () => {
        this._notificacionService.modalInfo(
          'success',
          'Info',
          'Registro guardado correctamente',
        );
        this.onRefreshDataGrid();
      },
      error: (err) => {
        console.error({
          mensage: 'Error crear Subseccion',
          error: err,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al guardar el registro',
        );
      },
    });
  }

  editar(subseccion: SubseccionInterface, id: number): void {
    const subseccion$ = this._subseccionRestService.updateOne(id, subseccion);

    subseccion$.subscribe({
      next: () => {
        this._notificacionService.modalInfo(
          'success',
          'Info',
          'Registro editado correctamente',
        );
        this.onRefreshDataGrid();
      },
      error: (err) => {
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al editar el registro',
        );
        console.error({
          mensage: 'Error editar Subseccion',
          error: err,
        });
      },
    });
  }

  async cambiarEstado(data: any) {
    const respuestaModalConfirmacion = await this._notificacionService.modalConfirmsSwal(
      'question',
      'Advertencia',
      'Está seguro de cambiar el estado del registro',
    );
    if (respuestaModalConfirmacion) {
      const registroEditar: any = {
        estado: data.estado ? 0 : 1,
      }
      this.editar(registroEditar, data.id);
    }
  }
}
