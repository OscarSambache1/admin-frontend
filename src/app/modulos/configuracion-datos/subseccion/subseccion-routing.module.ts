import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaSubseccionComponent} from "./ruta-subseccion/ruta-subseccion.component";

const routes: Routes = [
  {
    path: '',
    component: RutaSubseccionComponent,
    data: {
      breadcrumb: 'Subsección',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubseccionRoutingModule { }
