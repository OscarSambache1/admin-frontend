import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubseccionRoutingModule } from './subseccion-routing.module';
import { RutaSubseccionComponent } from './ruta-subseccion/ruta-subseccion.component';
import {IMPORTS_MODULO} from "../../imports/imports-modulo";
import {ComponentesModule} from "../../../componentes/componentes.module";


@NgModule({
  declarations: [
    RutaSubseccionComponent
  ],
  imports: [
    CommonModule,
    SubseccionRoutingModule,
    ...IMPORTS_MODULO,
    ComponentesModule,
  ]
})
export class SubseccionModule { }
