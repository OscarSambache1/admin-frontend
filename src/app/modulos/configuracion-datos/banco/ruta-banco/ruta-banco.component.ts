import {Component, OnInit, ViewChild} from '@angular/core';
import {DxDataGridComponent} from "devextreme-angular";
import {ROWS, ROWS_PER_PAGE} from "../../../../constantes/table-config";
import {BancoInterface} from "../../interfaces/banco.interface";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../../servicios/cargando.service";
import {BancoRestService} from "../../servicios/banco-rest.service";
import {NotificacionService} from "../../../../servicios/notificacion/notificacion.service";
import {ModalCrearEditarBancoComponent} from "../../../../componentes/modales/modal-crear-editar-banco/modal-crear-editar-banco.component";

@Component({
  selector: 'app-ruta-banco',
  templateUrl: './ruta-banco.component.html',
  styleUrls: ['./ruta-banco.component.scss']
})
export class RutaBancoComponent implements OnInit {

  @ViewChild(DxDataGridComponent)
  dataGrid!: DxDataGridComponent;

  rows = ROWS;
  rowsPerPage = ROWS_PER_PAGE;
  datos: BancoInterface[] = [];
  pageSize!: number;
  disabledTable!: boolean;
  pagination: number[] = [];
  paginationStart!: number;


  consulta: any = {
    relations: [],
    where: {},
    order: {
      id: 'DESC',
    },
  };

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _bancoRestService: BancoRestService,
    private readonly _notificacionService: NotificacionService,
  ) {}

  ngOnInit(): void {
    this.pagination = ROWS_PER_PAGE;
    this.paginationStart = ROWS;
    this.cargarDatosIniciales(this.consulta);
  }

  cargarDatosIniciales(consulta: {}): void {
    this._cargandoService.habilitar();
    const banco$ = this._bancoRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    banco$.subscribe({
      next: (banco) => {
        this.datos = banco[0];
        this.pageSize = banco[1];
        this.disabledTable = false;
        this._cargandoService.deshabilitar();
      },
      error: (error) => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
      },
    });
  }

  onToolbarPreparing(evento: any): void {
    evento.toolbarOptions.items.unshift(
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Agregar nuevo registro',
          onClick: this.add.bind(this, undefined, false),
          icon: 'plus',
        },
      },
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Recargar datos de la tabla',
          icon: 'refresh',
          onClick: this.onRefreshDataGrid.bind(this),
        },
      }
    );
  }

  private onRefreshDataGrid(): void {
    this.cargarDatosIniciales(this.consulta);
    void this.dataGrid.instance.refresh();
  }

  add(banco?: BancoInterface, soloVer?: boolean): void {
    const dialogRef = this._matDialog.open(ModalCrearEditarBancoComponent, {
      width: '450px',
      data: {
        banco,
        soloVer,
      },
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          if (banco) {
            this.editar(response, banco.id as number);
          } else {
            this.crear(response);
          }
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

  crear(banco: BancoInterface): void {
    const banco$ = this._bancoRestService.create(banco);

    banco$.subscribe({
      next: () => {
        this._notificacionService.modalInfo(
          'success',
          'Info',
          'Registro guardado correctamente',
        );
        this.onRefreshDataGrid();
      },
      error: (err) => {
        console.error({
          mensage: 'Error crear Banco',
          error: err,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al guardar el registro',
        );
      },
    });
  }

  editar(banco: BancoInterface, id: number): void {
    const banco$ = this._bancoRestService.updateOne(id, banco);

    banco$.subscribe({
      next: () => {
        this._notificacionService.modalInfo(
          'success',
          'Info',
          'Registro editado correctamente',
        );
        this.onRefreshDataGrid();
      },
      error: (err) => {
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al editar el registro',
        );
        console.error({
          mensage: 'Error editar Banco',
          error: err,
        });
      },
    });
  }


  async cambiarEstado(data: any) {
    const respuestaModalConfirmacion = await this._notificacionService.modalConfirmsSwal(
      'question',
      'Advertencia',
      'Está seguro de cambiar el estado del registro',
    );
    if (respuestaModalConfirmacion) {
      const registroEditar: any = {
        estado: data.estado ? 0 : 1,
      }
      this.editar(registroEditar, data.id);
    }
  }
}
