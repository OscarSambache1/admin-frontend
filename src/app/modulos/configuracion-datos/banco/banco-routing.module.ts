import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaBancoComponent} from "./ruta-banco/ruta-banco.component";

const routes: Routes = [
  {
    path: '',
    component: RutaBancoComponent,
    data: {
      breadcrumb: 'Banco',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BancoRoutingModule { }
