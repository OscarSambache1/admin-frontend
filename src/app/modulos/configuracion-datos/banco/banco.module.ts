import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BancoRoutingModule } from './banco-routing.module';
import { RutaBancoComponent } from './ruta-banco/ruta-banco.component';
import {IMPORTS_MODULO} from "../../imports/imports-modulo";
import {ComponentesModule} from "../../../componentes/componentes.module";


@NgModule({
  declarations: [
    RutaBancoComponent
  ],
  imports: [
    CommonModule,
    BancoRoutingModule,
    ...IMPORTS_MODULO,
    ComponentesModule,
  ]
})
export class BancoModule { }
