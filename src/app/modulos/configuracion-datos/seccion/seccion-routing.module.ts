import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaSeccionComponent} from "./ruta-seccion/ruta-seccion.component";

const routes: Routes = [
  {
    path: '',
    component: RutaSeccionComponent,
    data: {
      breadcrumb: 'Sección',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SeccionRoutingModule { }
