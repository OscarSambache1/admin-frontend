import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SeccionRoutingModule } from './seccion-routing.module';
import { RutaSeccionComponent } from './ruta-seccion/ruta-seccion.component';
import {IMPORTS_MODULO} from "../../imports/imports-modulo";
import {ComponentesModule} from "../../../componentes/componentes.module";


@NgModule({
  declarations: [
    RutaSeccionComponent
  ],
  imports: [
    CommonModule,
    SeccionRoutingModule,
    ...IMPORTS_MODULO,
    ComponentesModule,
  ]
})
export class SeccionModule { }
