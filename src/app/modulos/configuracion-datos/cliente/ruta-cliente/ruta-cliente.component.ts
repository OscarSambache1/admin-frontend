import {Component, OnInit, ViewChild} from '@angular/core';
import {DxDataGridComponent} from "devextreme-angular";
import {ROWS, ROWS_PER_PAGE} from "../../../../constantes/table-config";
import {ClienteInterface} from "../../interfaces/cliente.interface";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../../servicios/cargando.service";
import {ClienteRestService} from "../../servicios/cliente-rest.service";
import {NotificacionService} from "../../../../servicios/notificacion/notificacion.service";
import {ModalCrearEditarClienteComponent} from "../../../../componentes/modales/modal-crear-editar-cliente/modal-crear-editar-cliente.component";
import {ProveedorInterface} from "../../interfaces/proveedor.interface";
import {ArticuloInterface} from "../../interfaces/articulo.interface";

@Component({
  selector: 'app-ruta-cliente',
  templateUrl: './ruta-cliente.component.html',
  styleUrls: ['./ruta-cliente.component.scss']
})
export class RutaClienteComponent implements OnInit {


  @ViewChild(DxDataGridComponent)
  dataGrid!: DxDataGridComponent;

  rows = ROWS;
  rowsPerPage = ROWS_PER_PAGE;
  datos: ClienteInterface[] = [];
  pageSize!: number;
  disabledTable!: boolean;
  pagination: number[] = [];
  paginationStart!: number;


  consulta: any = {
    relations: [
      'informacionTributaria',
      'contactos',
      'cuentasBanco',
      'cuentasBanco.banco',
      'vehiculos',
      'vehiculos.marca',
      'vehiculos.tipoVehiculo',
      'vehiculos.tipoCambio',
      'vehiculos.tipoCombustible',
    ],
    where: {
    },
    order: {
      id: 'DESC',
    },
  };

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _clienteRestService: ClienteRestService,
    private readonly _notificacionService: NotificacionService,
  ) {}

  ngOnInit(): void {
    this.pagination = ROWS_PER_PAGE;
    this.paginationStart = ROWS;
    this.cargarDatosIniciales(this.consulta);
  }

  cargarDatosIniciales(consulta: {}): void {
    this._cargandoService.habilitar();
    const cliente$ = this._clienteRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    cliente$.subscribe({
      next: (cliente) => {
        this.datos = cliente[0];
        this.pageSize = cliente[1];
        this.disabledTable = false;
        this._cargandoService.deshabilitar();
      },
      error: (error) => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
      },
    });
  }

  onToolbarPreparing(evento: any): void {
    evento.toolbarOptions.items.unshift(
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Agregar nuevo registro',
          onClick: this.add.bind(this, undefined, false),
          icon: 'plus',
        },
      },
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Recargar datos de la tabla',
          icon: 'refresh',
          onClick: this.onRefreshDataGrid.bind(this),
        },
      }
    );
  }

  private onRefreshDataGrid(): void {
    this.cargarDatosIniciales(this.consulta);
    void this.dataGrid.instance.refresh();
  }

  add(cliente?: ClienteInterface, soloVer?: boolean): void {
    if (cliente) {
      setearClienteAEditar(cliente);
    }
    const dialogRef = this._matDialog.open(ModalCrearEditarClienteComponent, {
      width: '1750px',
      data: {
        cliente,
        soloVer,
      },
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          if (cliente) {
            response.id = cliente.id;
            response.idInfoTributaria = cliente.informacionTributaria?.id;
          }
          this.crearEditar(response);
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

  crearEditar(cliente: ClienteInterface): void {
    const cliente$ = this._clienteRestService.crearEditarCliente(cliente);

    cliente$.subscribe({
      next: () => {
        this._notificacionService.modalInfo(
          'success',
          'Info',
          'Registro guardado correctamente',
        );
        this.onRefreshDataGrid();
      },
      error: (err) => {
        console.error({
          mensage: 'Error guardar Cliente',
          error: err,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al guardar el registro',
        );
      },
    });
  }

  editar(cliente: ClienteInterface, id: number): void {
    const cliente$ = this._clienteRestService.updateOne(id, cliente);

    cliente$.subscribe({
      next: () => {
        this._notificacionService.modalInfo(
          'success',
          'Info',
          'Registro editado correctamente',
        );
        this.onRefreshDataGrid();
      },
      error: (err) => {
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al editar el registro',
        );
        console.error({
          mensage: 'Error editar Cliente',
          error: err,
        });
      },
    });
  }

  async cambiarEstado(data: any) {
    const respuestaModalConfirmacion = await this._notificacionService.modalConfirmsSwal(
      'question',
      'Advertencia',
      'Está seguro de cambiar el estado del registro',
    );
    if (respuestaModalConfirmacion) {
      const registroEditar: any = {
        estado: data.estado ? 0 : 1,
      }
      this.editar(registroEditar, data.id);
    }
  }

  async eliminar(registroAEliminar: ClienteInterface) {
    const respuestaModalConfirmacion = await this._notificacionService.modalConfirmsSwal(
      'question',
      'Advertencia',
      'Está seguro de borrar el registro',
    );
    if (respuestaModalConfirmacion) {
      const promesaTieneRegistrosHijos = await (this._clienteRestService.findAll(
        `busqueda=${JSON.stringify(
          {
            where: {
              id: registroAEliminar.id,
            },
            relations: [
              'vehiculos',
              'facturas',
              'trabajoRealizado',
            ],
          }
        )}`
      ).toPromise());
      const cliente = promesaTieneRegistrosHijos[0][0];
      const vehiculos = cliente.vehiculos;
      const facturas = cliente.facturas;
      const trabajoRealizado = cliente.trabajoRealizado;
      if (!vehiculos?.length && !facturas?.length && !trabajoRealizado?.length) {
        const cliente$ = this._clienteRestService.eliminarCliente(registroAEliminar.id as number);
        cliente$.subscribe({
          next: () => {
            this._notificacionService.modalInfo(
              'success',
              'Info',
              'Registro borrado correctamente',
            );
            this.onRefreshDataGrid();
          },
          error: (err) => {
            console.error({
              mensage: 'Error al borrar registro',
              error: err,
            });
            this._notificacionService.modalInfo(
              'error',
              'Error',
              'Error al borrar el registro',
            );
          },
        });
      } else {
        await this._notificacionService.modalInfoAsync(
          'warning',
          'Advertencia',
          'El registro no puede ser borrado porque ya es usado en otros procesos del sistema',
        );
      }
    }
  }

}

export function setearClienteAEditar(cliente: ClienteInterface) {
  cliente.nombreComercial = cliente.informacionTributaria?.nombreComercial;
  cliente.razonSocial = cliente.informacionTributaria?.razonSocial;
  cliente.tipoIdentificacion = cliente.informacionTributaria?.tipoIdentificacion;
  cliente.identificacion = cliente.informacionTributaria?.identificacion;
  cliente.direccion = cliente.informacionTributaria?.direccion;
  cliente.telefono = cliente.informacionTributaria?.telefono;
  cliente.correo = cliente.informacionTributaria?.correo;
}

export function setearProveedorAEditar(proveedor: ProveedorInterface) {
  proveedor.nombreComercial = proveedor.informacionTributaria?.nombreComercial;
  proveedor.razonSocial = proveedor.informacionTributaria?.razonSocial;
  proveedor.tipoIdentificacion = proveedor.informacionTributaria?.tipoIdentificacion;
  proveedor.identificacion = proveedor.informacionTributaria?.identificacion;
  proveedor.direccion = proveedor.informacionTributaria?.direccion;
  proveedor.telefono = proveedor.informacionTributaria?.telefono;
  proveedor.correo = proveedor.informacionTributaria?.correo;
}
