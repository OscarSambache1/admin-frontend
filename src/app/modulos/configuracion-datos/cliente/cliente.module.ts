import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClienteRoutingModule } from './cliente-routing.module';
import { RutaClienteComponent } from './ruta-cliente/ruta-cliente.component';
import {IMPORTS_MODULO} from "../../imports/imports-modulo";
import {ComponentesModule} from "../../../componentes/componentes.module";


@NgModule({
  declarations: [
    RutaClienteComponent
  ],
  imports: [
    CommonModule,
    ClienteRoutingModule,
    ...IMPORTS_MODULO,
    ComponentesModule,
  ]
})
export class ClienteModule { }
