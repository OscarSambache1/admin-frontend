import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaClienteComponent} from "./ruta-cliente/ruta-cliente.component";

const routes: Routes = [
  {
    path: '',
    component: RutaClienteComponent,
    data: {
      breadcrumb: 'Cliente',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClienteRoutingModule { }
