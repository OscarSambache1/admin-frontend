import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaServicioComponent} from "./ruta-servicio/ruta-servicio.component";

const routes: Routes = [
  {
    path: '',
    component: RutaServicioComponent,
    data: {
      breadcrumb: 'Servicio',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServicioRoutingModule { }
