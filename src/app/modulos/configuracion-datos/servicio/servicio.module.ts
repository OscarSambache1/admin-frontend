import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServicioRoutingModule } from './servicio-routing.module';
import { RutaServicioComponent } from './ruta-servicio/ruta-servicio.component';
import {IMPORTS_MODULO} from "../../imports/imports-modulo";
import {ComponentesModule} from "../../../componentes/componentes.module";


@NgModule({
  declarations: [
    RutaServicioComponent
  ],
  imports: [
    CommonModule,
    ServicioRoutingModule,
    ...IMPORTS_MODULO,
    ComponentesModule,
  ]
})
export class ServicioModule { }
