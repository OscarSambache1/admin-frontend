import {ImpuestoInterface} from "./impuesto.interface";

export interface TarifaImpuestoInterface {
  id?: number;
  valor?: number;
  descripcion?: string;
  esPrincipal?: 0 | 1;
  impuesto?: ImpuestoInterface
}
