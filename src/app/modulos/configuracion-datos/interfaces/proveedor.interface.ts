import {InformacionTributariaInterface} from "./informacion-tributaria.interface";
import {ContactoInterface} from "./contacto.interface";
import {CuentaBancoInterface} from "./cuenta-banco.interface";
import {FacturaCabeceraInterface} from "../../gastos/interfaces/factura-cabecera.interface";

export interface ProveedorInterface {
  id?: number;
  responsable?: string;
  ciudad?: string;
  provincia?: string;
  pais?: string;
  whatsapp?: string;
  web?: string;
  observacion?: string;
  informacionTributaria?: InformacionTributariaInterface;
  contactos?: ContactoInterface[];
  cuentasBanco?: CuentaBancoInterface[];
  facturas?: FacturaCabeceraInterface[];
  nombreComercial?: string;
  razonSocial?: string;
  direccion?: string;
  telefono?: string;
  identificacion?: string;
  tipoIdentificacion?: any;
  correo?: string;
  label?: any;
}
