import {InformacionTributariaInterface} from "./informacion-tributaria.interface";
import {ContactoInterface} from "./contacto.interface";
import {CuentaBancoInterface} from "./cuenta-banco.interface";
import {VehiculoInterface} from "./vehiculo.interface";

export interface ClienteInterface {
  id?: number;
  ciudad?: string;
  provincia?: string;
  pais?: string;
  whatsapp?: string;
  web?: string;
  observacion?: string;
  informacionTributaria?: InformacionTributariaInterface;
  contactos?: ContactoInterface[];
  cuentasBanco?: CuentaBancoInterface[];
  nombreComercial?: string;
  razonSocial?: string;
  direccion?: string;
  telefono?: string;
  identificacion?: string;
  tipoIdentificacion?: any;
  correo?: string;
  vehiculos?: VehiculoInterface[];
  label?: string;
  facturas?: any[];
  trabajoRealizado?: any[];
}
