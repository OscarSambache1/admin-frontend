import {ArticuloInterface} from "./articulo.interface";
import {TarifaImpuestoInterface} from "./tarifa-impuesto.interface";

export interface ArticuloImpuestoInterface {
  id?: number;
  articulo?: ArticuloInterface;
  tarifaImpuesto?: TarifaImpuestoInterface;
}
