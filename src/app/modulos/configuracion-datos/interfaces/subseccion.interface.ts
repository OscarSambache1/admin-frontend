import {SeccionInterface} from "./seccion.interface";

export interface SubseccionInterface {
  id?: number;
  nombre?: string;
  seccion?: SeccionInterface;
}
