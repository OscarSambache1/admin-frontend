import {ProveedorInterface} from "./proveedor.interface";
import {BancoInterface} from "./banco.interface";

export interface CuentaBancoInterface {
  id?: number;
  tipoCuenta?: any; // AHORROS O CORRIENTE
  numero?: string;
  nombres?: string;
  identificacion?: string;
  telefono?: string;
  correo?: string;
  observacion?: string;
  banco?: BancoInterface;
  proveedor?: ProveedorInterface;
}
