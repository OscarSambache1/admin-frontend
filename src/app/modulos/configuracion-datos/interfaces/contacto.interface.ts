import {ProveedorInterface} from "./proveedor.interface";
import {BancoInterface} from "./banco.interface";

export interface ContactoInterface {
  id?: number;
  tipoContacto?: any; // Celular, Correo, Fijo
  valor?: string;
  esPrincipal?: 0 | 1;
  proveedor?: ProveedorInterface;
}
