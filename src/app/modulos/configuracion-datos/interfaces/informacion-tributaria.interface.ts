export interface InformacionTributariaInterface {
  id?: number;
  nombreComercial?: string;
  razonSocial?: string;
  direccion?: string;
  telefono?: string;
  identificacion?: string;
  tipoIdentificacion?: string;
  correo?: string;
}
