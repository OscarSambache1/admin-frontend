import {ProveedorInterface} from "./proveedor.interface";
import {ArticuloInterface} from "./articulo.interface";

export interface ProveedorArticuloInterface {
  id?: number;
  proveedor?: ProveedorInterface;
  articulo?: ArticuloInterface;
}
