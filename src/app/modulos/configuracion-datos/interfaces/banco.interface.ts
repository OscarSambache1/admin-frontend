export interface BancoInterface {
  id?: number;
  nombre?: string;
  estado?: 0 | 1;
}
