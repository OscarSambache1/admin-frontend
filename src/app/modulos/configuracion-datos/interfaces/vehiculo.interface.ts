import {ClienteInterface} from "./cliente.interface";
import {MarcaInterface} from "./marca.interface";
import {TipoVehiculoInterface} from "./tipo-vehiculo.interface";
import {TipoCambioInterface} from "./tipo-cambio.interface";
import {TipoCombustibleInterface} from "./tipo-combustible.interface";

export interface VehiculoInterface {
  id?: number;
  modelo?: string;
  color?: string;
  chasis?: string;
  motor?: string;
  placa?: string;
  anio?: number;
  cliente?: ClienteInterface;
  marca?: MarcaInterface;
  tipoVehiculo?: TipoVehiculoInterface;
  tipoCambio?: TipoCambioInterface;
  tipoCombustible?: TipoCombustibleInterface;
}
