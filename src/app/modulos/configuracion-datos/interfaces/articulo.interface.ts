import {SeccionInterface} from "./seccion.interface";
import {SubseccionInterface} from "./subseccion.interface";
import {ProveedorArticuloInterface} from "./proveedor-articulo.interface";
import {ArticuloImpuestoInterface} from "./articulo-impuesto.interface";
import {EstadoStockEnum, EstadoStockEnumLabel} from "../../../enums/tipo-contacto.enums";
import {FacturaDetalleInterface} from "../../gastos/interfaces/factura-detalle.interface";
import {OrdenTrabajoDetalleInterface} from "../../trabajos/interfaces/orden-trabajo-detalle.interface";
import {TrabajoRealizadoDetalleInterface} from "../../trabajos/interfaces/trabajo-realizado-detalle.interface";

export interface ArticuloInterface {
  id?: number;
  codigo?: string;
  nombre?: string;
  descripcion?: string;
  codigoBarras?: string;
  precioVenta?: number;
  costo?: number;
  stockMinimo?: number;
  stockActual?: number;
  observacion?: string;
  urlImagen?: string;
  esServicio?: 0 | 1;
  seccion?: SeccionInterface;
  subseccion?: SubseccionInterface;
  proveedoresArticulo?: ProveedorArticuloInterface[];
  articulosTarifaImpuesto?: ArticuloImpuestoInterface[];
  imagen?: any;
  estadoStock?: any;
  trabajoRealizadoDetalles?: TrabajoRealizadoDetalleInterface[];
  ordenTrabajoDetalles?: OrdenTrabajoDetalleInterface[];
  facturaDetalles?: FacturaDetalleInterface[];
  archivos?: any[];
}
