import {Component, OnInit, ViewChild} from '@angular/core';
import {DxDataGridComponent} from "devextreme-angular";
import {ROWS, ROWS_PER_PAGE} from "../../../../constantes/table-config";
import {TipoCombustibleInterface} from "../../interfaces/tipo-combustible.interface";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../../servicios/cargando.service";
import {TipoCombustibleRestService} from "../../servicios/tipo-combustible-rest.service";
import {NotificacionService} from "../../../../servicios/notificacion/notificacion.service";
import {ModalCrearEditarTipoCombustibleComponent} from "../../../../componentes/modales/modal-crear-editar-tipo-combustible/modal-crear-editar-tipo-combustible.component";

@Component({
  selector: 'app-ruta-tipo-combustible',
  templateUrl: './ruta-tipo-combustible.component.html',
  styleUrls: ['./ruta-tipo-combustible.component.scss']
})
export class RutaTipoCombustibleComponent implements OnInit {

  @ViewChild(DxDataGridComponent)
  dataGrid!: DxDataGridComponent;

  rows = ROWS;
  rowsPerPage = ROWS_PER_PAGE;
  datos: TipoCombustibleInterface[] = [];
  pageSize!: number;
  disabledTable!: boolean;
  pagination: number[] = [];
  paginationStart!: number;


  consulta: any = {
    relations: [],
    where: {},
    order: {
      id: 'DESC',
    },
  };

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _tipoCombustibleRestService: TipoCombustibleRestService,
    private readonly _notificacionService: NotificacionService,
  ) {}

  ngOnInit(): void {
    this.pagination = ROWS_PER_PAGE;
    this.paginationStart = ROWS;
    this.cargarDatosIniciales(this.consulta);
  }

  cargarDatosIniciales(consulta: {}): void {
    this._cargandoService.habilitar();
    const tipoCombustible$ = this._tipoCombustibleRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    tipoCombustible$.subscribe({
      next: (tipoCombustible) => {
        this.datos = tipoCombustible[0];
        this.pageSize = tipoCombustible[1];
        this.disabledTable = false;
        this._cargandoService.deshabilitar();
      },
      error: (error) => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
      },
    });
  }

  onToolbarPreparing(evento: any): void {
    evento.toolbarOptions.items.unshift(
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Agregar nuevo registro',
          onClick: this.add.bind(this, undefined),
          icon: 'plus',
        },
      },
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Recargar datos de la tabla',
          icon: 'refresh',
          onClick: this.onRefreshDataGrid.bind(this),
        },
      }
    );
  }

  private onRefreshDataGrid(): void {
    this.cargarDatosIniciales(this.consulta);
    void this.dataGrid.instance.refresh();
  }

  add(tipoCombustible?: TipoCombustibleInterface): void {
    const dialogRef = this._matDialog.open(ModalCrearEditarTipoCombustibleComponent, {
      width: '450px',
      data: {
        tipoCombustible: tipoCombustible,
      },
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          if (tipoCombustible) {
            this.editar(response, tipoCombustible.id as number);
          } else {
            this.crear(response);
          }
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

  crear(tipoCombustible: TipoCombustibleInterface): void {
    const tipoCombustible$ = this._tipoCombustibleRestService.create(tipoCombustible);

    tipoCombustible$.subscribe({
      next: () => {
        this._notificacionService.modalInfo(
          'success',
          'Info',
          'Registro guardado correctamente',
        );
        this.onRefreshDataGrid();
      },
      error: (err) => {
        console.error({
          mensage: 'Error crear registro',
          error: err,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al guardar el registro',
        );
      },
    });
  }

  editar(tipoCombustible: TipoCombustibleInterface, id: number): void {
    const tipoCombustible$ = this._tipoCombustibleRestService.updateOne(id, tipoCombustible);

    tipoCombustible$.subscribe({
      next: () => {
        this._notificacionService.modalInfo(
          'success',
          'Info',
          'Registro editado correctamente',
        );
        this.onRefreshDataGrid();
      },
      error: (err) => {
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al editar el registro',
        );
        console.error({
          mensage: 'Error editar registro',
          error: err,
        });
      },
    });
  }

  async cambiarEstado(data: any) {
    const respuestaModalConfirmacion = await this._notificacionService.modalConfirmsSwal(
      'question',
      'Advertencia',
      'Está seguro de cambiar el estado del registro',
    );
    if (respuestaModalConfirmacion) {
      const registroEditar: any = {
        estado: data.estado ? 0 : 1,
      }
      this.editar(registroEditar, data.id);
    }
  }
}
