import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaTipoCombustibleComponent} from "./ruta-tipo-combustible/ruta-tipo-combustible.component";

const routes: Routes = [
  {
    path: '',
    component: RutaTipoCombustibleComponent,
    data: {
      breadcrumb: 'Tipo Combustible',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TipoCombustibleRoutingModule { }
