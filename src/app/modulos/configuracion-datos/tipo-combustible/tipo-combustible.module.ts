import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TipoCombustibleRoutingModule } from './tipo-combustible-routing.module';
import { RutaTipoCombustibleComponent } from './ruta-tipo-combustible/ruta-tipo-combustible.component';
import {IMPORTS_MODULO} from "../../imports/imports-modulo";
import {ComponentesModule} from "../../../componentes/componentes.module";


@NgModule({
  declarations: [
    RutaTipoCombustibleComponent
  ],
  imports: [
    CommonModule,
    TipoCombustibleRoutingModule,
    ...IMPORTS_MODULO,
    ComponentesModule,
  ]
})
export class TipoCombustibleModule { }
