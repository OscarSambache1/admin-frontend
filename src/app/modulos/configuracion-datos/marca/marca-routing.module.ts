import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaMarcaComponent} from "./ruta-marca/ruta-marca.component";

const routes: Routes = [
  {
    path: '',
    component: RutaMarcaComponent,
    data: {
      breadcrumb: 'Marca',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MarcaRoutingModule { }
