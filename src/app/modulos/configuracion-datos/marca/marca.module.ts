import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MarcaRoutingModule } from './marca-routing.module';
import { RutaMarcaComponent } from './ruta-marca/ruta-marca.component';
import {IMPORTS_MODULO} from "../../imports/imports-modulo";
import {ComponentesModule} from "../../../componentes/componentes.module";


@NgModule({
  declarations: [
    RutaMarcaComponent
  ],
  imports: [
    CommonModule,
    MarcaRoutingModule,
    ...IMPORTS_MODULO,
    ComponentesModule,
  ]
})
export class MarcaModule { }
