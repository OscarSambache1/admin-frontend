import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RutaProveedorComponent } from './ruta-proveedor/ruta-proveedor.component';
import {ProveedorRoutingModule} from "./proveedor-routing.module";
import {IMPORTS_MODULO} from "../../imports/imports-modulo";
import {ComponentesModule} from "../../../componentes/componentes.module";



@NgModule({
  declarations: [
    RutaProveedorComponent
  ],
  imports: [
    CommonModule,
    ProveedorRoutingModule,
    ...IMPORTS_MODULO,
    ComponentesModule,
  ]
})
export class ProveedorModule { }
