import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaProveedorComponent} from "./ruta-proveedor/ruta-proveedor.component";

const routes: Routes = [
  {
    path: '',
    component: RutaProveedorComponent,
    data: {
      breadcrumb: 'Proveedor',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProveedorRoutingModule { }
