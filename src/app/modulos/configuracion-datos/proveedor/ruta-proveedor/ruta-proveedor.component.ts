import {Component, OnInit, ViewChild} from '@angular/core';
import {DxDataGridComponent} from "devextreme-angular";
import {ROWS, ROWS_PER_PAGE} from "../../../../constantes/table-config";
import {ProveedorInterface} from "../../interfaces/proveedor.interface";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../../servicios/cargando.service";
import {ProveedorRestService} from "../../servicios/proveedor-rest.service";
import {NotificacionService} from "../../../../servicios/notificacion/notificacion.service";
import {ModalCrearEditarProveedorComponent} from "../../../../componentes/modales/modal-crear-editar-proveedor/modal-crear-editar-proveedor.component";
import {TipoCuentaBancoEnum} from "../../../../enums/tipo-contacto.enums";
import {CuentaBancoInterface} from "../../interfaces/cuenta-banco.interface";
import {ArticuloInterface} from "../../interfaces/articulo.interface";
// import {ModalCrearEditarProveedorComponent} from "../../../../componentes/modales/modal-crear-editar-proveedor/modal-crear-editar-proveedor.component";

@Component({
  selector: 'app-ruta-proveedor',
  templateUrl: './ruta-proveedor.component.html',
  styleUrls: ['./ruta-proveedor.component.scss']
})
export class RutaProveedorComponent implements OnInit {


  @ViewChild(DxDataGridComponent)
  dataGrid!: DxDataGridComponent;

  rows = ROWS;
  rowsPerPage = ROWS_PER_PAGE;
  datos: ProveedorInterface[] = [];
  pageSize!: number;
  disabledTable!: boolean;
  pagination: number[] = [];
  paginationStart!: number;
  tipoCuentaBancoEnum = TipoCuentaBancoEnum;


  consulta: any = {
    relations: ['informacionTributaria', 'contactos', 'cuentasBanco', 'cuentasBanco.banco'],
    where: {
    },
    order: {
      id: 'DESC',
    },
  };

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _proveedorRestService: ProveedorRestService,
    private readonly _notificacionService: NotificacionService,
  ) {}

  ngOnInit(): void {
    this.pagination = ROWS_PER_PAGE;
    this.paginationStart = ROWS;
    this.cargarDatosIniciales(this.consulta);
  }

  cargarDatosIniciales(consulta: {}): void {
    this._cargandoService.habilitar();
    const proveedor$ = this._proveedorRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    proveedor$.subscribe({
      next: (proveedor) => {
        this.datos = proveedor[0];
        this.pageSize = proveedor[1];
        this.disabledTable = false;
        this._cargandoService.deshabilitar();
      },
      error: (error) => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
      },
    });
  }

  onToolbarPreparing(evento: any): void {
    evento.toolbarOptions.items.unshift(
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Agregar nuevo registro',
          onClick: this.add.bind(this, undefined, false),
          icon: 'plus',
        },
      },
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Recargar datos de la tabla',
          icon: 'refresh',
          onClick: this.onRefreshDataGrid.bind(this),
        },
      }
    );
  }

  private onRefreshDataGrid(): void {
    this.cargarDatosIniciales(this.consulta);
    void this.dataGrid.instance.refresh();
  }

  add(proveedor?: ProveedorInterface, soloVer?: boolean): void {
    if (proveedor) {
      proveedor.nombreComercial = proveedor.informacionTributaria?.nombreComercial;
      proveedor.razonSocial = proveedor.informacionTributaria?.razonSocial;
      proveedor.tipoIdentificacion = proveedor.informacionTributaria?.tipoIdentificacion;
      proveedor.identificacion = proveedor.informacionTributaria?.identificacion;
      proveedor.direccion = proveedor.informacionTributaria?.direccion;
      proveedor.telefono = proveedor.informacionTributaria?.telefono;
      proveedor.correo = proveedor.informacionTributaria?.correo;
    }
    const dialogRef = this._matDialog.open(ModalCrearEditarProveedorComponent, {
      width: '1550px',
      data: {
        proveedor,
        soloVer,
      },
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          if (proveedor) {
            response.id = proveedor.id;
            response.idInfoTributaria = proveedor.informacionTributaria?.id;
          }
          this.crearEditar(response);
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

  crearEditar(proveedor: ProveedorInterface): void {
    const proveedor$ = this._proveedorRestService.crearEditarProveedor(proveedor);

    proveedor$.subscribe({
      next: () => {
        this._notificacionService.modalInfo(
          'success',
          'Info',
          'Registro guardado correctamente',
        );
        this.onRefreshDataGrid();
      },
      error: (err) => {
        console.error({
          mensage: 'Error guardar Proveedor',
          error: err,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al guardar el registro',
        );
      },
    });
  }

  editar(proveedor: ProveedorInterface, id: number): void {
    const proveedor$ = this._proveedorRestService.updateOne(id, proveedor);

    proveedor$.subscribe({
      next: () => {
        this._notificacionService.modalInfo(
          'success',
          'Info',
          'Registro editado correctamente',
        );
        this.onRefreshDataGrid();
      },
      error: (err) => {
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al editar el registro',
        );
        console.error({
          mensage: 'Error editar Proveedor',
          error: err,
        });
      },
    });
  }

  async cambiarEstado(data: any) {
    const respuestaModalConfirmacion = await this._notificacionService.modalConfirmsSwal(
      'question',
      'Advertencia',
      'Está seguro de cambiar el estado del registro',
    );
    if (respuestaModalConfirmacion) {
      const registroEditar: any = {
        estado: data.estado ? 0 : 1,
      }
      const producto$ = this._proveedorRestService.updateOne(data.id, registroEditar);

      producto$.subscribe({
        next: () => {
          this._notificacionService.modalInfo(
            'success',
            'Info',
            'Registro guardado correctamente',
          );
          this.onRefreshDataGrid();
        },
        error: (err) => {
          console.error({
            mensage: 'Error guardar registro',
            error: err,
          });
          this._notificacionService.modalInfo(
            'error',
            'Error',
            'Error al guardar el registro',
          );
        },
      });
    }
  }

  setearCuentaBancoNumero(data: ProveedorInterface) {
    const cuentasBanco = data.cuentasBanco;
    let cuentasBancoString: any = undefined;
    cuentasBanco?.map(cuentaBanco => {
      cuentasBancoString = (cuentasBancoString as any || '') + cuentaBanco.numero + '\n';
    });
    return cuentasBancoString || 'No Tiene';
  }

  setearCuentaBancoTipo(data: ProveedorInterface) {
    const cuentasBanco = data.cuentasBanco;
    let cuentasBancoString: any = undefined;
    cuentasBanco?.map(cuentaBanco => {
      // @ts-ignore
      cuentasBancoString = (cuentasBancoString as any || '') + TipoCuentaBancoEnum[cuentaBanco.tipoCuenta as any] + '\n';
    });
    return cuentasBancoString || 'No Tiene';
  }

  setearTipoCuenta(data: CuentaBancoInterface) {
    // @ts-ignore
    return this.tipoCuentaBancoEnum[data.tipoCuenta];
  }

  setearBanco(data: ProveedorInterface) {
    const cuentasBanco = data.cuentasBanco;
    let cuentasBancoString: any = undefined;
    cuentasBanco?.map(cuentaBanco => {
      cuentasBancoString = (cuentasBancoString as any || '') + cuentaBanco.banco?.nombre + '\n';
    });
    return cuentasBancoString || 'No Tiene';
  }

  async eliminar(registroAEliminar: ProveedorInterface) {
    const respuestaModalConfirmacion = await this._notificacionService.modalConfirmsSwal(
      'question',
      'Advertencia',
      'Está seguro de borrar el registro',
    );
    if (respuestaModalConfirmacion) {
      const promesaTieneRegistrosHijos = await (this._proveedorRestService.findAll(
        `busqueda=${JSON.stringify(
          {
            where: {
              id: registroAEliminar.id,
            },
            relations: [
              'facturas',
            ],
          }
        )}`
      ).toPromise());
      const proveedor = promesaTieneRegistrosHijos[0][0];
      const facturas = proveedor.facturas;
      if (!facturas?.length) {
        const producto$ = this._proveedorRestService
          .eliminarProveedor(registroAEliminar.id as number);
        producto$.subscribe({
          next: () => {
            this._notificacionService.modalInfo(
              'success',
              'Info',
              'Registro borrado correctamente',
            );
            this.onRefreshDataGrid();
          },
          error: (err) => {
            console.error({
              mensage: 'Error al borrar registro',
              error: err,
            });
            this._notificacionService.modalInfo(
              'error',
              'Error',
              'Error al borrar el registro',
            );
          },
        });
      } else {
        await this._notificacionService.modalInfoAsync(
          'warning',
          'Advertencia',
          'El registro no puede ser borrado porque ya es usado en otros procesos del sistema',
        );
      }
    }
  }
}
