import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfiguracionDatosRoutingModule } from './configuracion-datos-routing.module';
import { MenuConfiguracionDatosComponent } from './rutas/menu-configuracion-datos/menu-configuracion-datos.component';


@NgModule({
  declarations: [
    MenuConfiguracionDatosComponent,
  ],
  imports: [
    CommonModule,
    ConfiguracionDatosRoutingModule,
  ]
})
export class ConfiguracionDatosModule { }
