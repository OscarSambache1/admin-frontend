import { Injectable } from '@angular/core';
import {ServicioPrincipalService} from "../../../servicios/servicio-principal.service";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {InformacionTributariaInterface} from "../interfaces/informacion-tributaria.interface";
import {ContactoInterface} from "../interfaces/contacto.interface";

@Injectable({
  providedIn: 'root'
})
export class ContactoRestService extends ServicioPrincipalService<ContactoInterface>  {

  constructor(protected readonly _httpClient: HttpClient) {
    super(_httpClient);
    this.url = environment.api.url;
    this.puerto = environment.api.puerto;
    this.segmento = 'contacto';
  }
}
