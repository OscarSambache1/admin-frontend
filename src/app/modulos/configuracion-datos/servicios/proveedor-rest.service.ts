import { Injectable } from '@angular/core';
import {ServicioPrincipalService} from "../../../servicios/servicio-principal.service";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {ProveedorInterface} from "../interfaces/proveedor.interface";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ProveedorRestService extends ServicioPrincipalService<ProveedorInterface>  {

  constructor(
    protected readonly _httpClient: HttpClient,
    private readonly _notificacionService: NotificacionService,
  ) {
    super(_httpClient);
    this.url = environment.api.url;
    this.puerto = environment.api.puerto;
    this.segmento = 'proveedor';
  }

  crearEditarProveedor(datos: any): Observable<any> {
    return this._httpClient.post(
      `${this.url}:${this.puerto}/${this.segmento}/crear-editar-proveedor`,
      datos,
    );
  }

  async validarIdentificacion(
    identificacion: string,
    idProveedor: number,
  ): Promise<boolean> {
    const consulta = {
      where: {
        informacionTributaria: {
          identificacion,
        }
      }
    };
    const proveedor$ = this.findAll(
      'busqueda=' + JSON.stringify(consulta),
    );

    const respuestaProveedor = await (proveedor$.toPromise());
    let proveedores = respuestaProveedor[0];
    if (idProveedor) {
      proveedores = proveedores.filter(proveedor => proveedor.id !== idProveedor);
    }
    if (!!proveedores.length) {
      this._notificacionService.modalInfo(
        'warning',
        'Advertencia',
        `Ya existe un proveedor registrado en el sistema con la identificación: ${identificacion}`
      );
      return false;
    } else {
      return true;
    }

  }

  eliminarProveedor(id: number): Observable<any> {
    return this._httpClient.put(
      `${this.url}:${this.puerto}/${this.segmento}/eliminar-proveedor/${id}`,
      {}
    );
  }
}
