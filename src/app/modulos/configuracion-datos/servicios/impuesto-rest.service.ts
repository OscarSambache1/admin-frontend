import { Injectable } from '@angular/core';
import {ServicioPrincipalService} from "../../../servicios/servicio-principal.service";
import {MarcaInterface} from "../interfaces/marca.interface";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {ModeloInterface} from "../interfaces/modelo.interface";
import {AccesorioInterface} from "../interfaces/accesorio.interface";
import {ImpuestoInterface} from "../interfaces/impuesto.interface";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ImpuestoRestService extends ServicioPrincipalService<ImpuestoInterface>  {

  constructor(protected readonly _httpClient: HttpClient) {
    super(_httpClient);
    this.url = environment.api.url;
    this.puerto = environment.api.puerto;
    this.segmento = 'impuesto';
  }

  crearImpuesto(datos: any): Observable<any> {
    return this._httpClient.post(
      `${this.url}:${this.puerto}/${this.segmento}/crear-impuesto`,
      datos,
    );
  }

  editarImpuesto(datos: any): Observable<any> {
    return this._httpClient.post(
      `${this.url}:${this.puerto}/${this.segmento}/editar-impuesto`,
      datos,
    );
  }
}
