import { Injectable } from '@angular/core';
import {ServicioPrincipalService} from "../../../servicios/servicio-principal.service";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {ClienteInterface} from "../interfaces/cliente.interface";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ClienteRestService extends ServicioPrincipalService<ClienteInterface>  {

  constructor(
    protected readonly _httpClient: HttpClient,
    private readonly _notificacionService: NotificacionService,
  ) {
    super(_httpClient);
    this.url = environment.api.url;
    this.puerto = environment.api.puerto;
    this.segmento = 'cliente';
  }

  crearEditarCliente(datos: any): Observable<any> {
    return this._httpClient.post(
      `${this.url}:${this.puerto}/${this.segmento}/crear-editar-cliente`,
      datos,
    );
  }

  eliminarCliente(id: number): Observable<any> {
    return this._httpClient.put(
      `${this.url}:${this.puerto}/${this.segmento}/eliminar-cliente/${id}`,
      {}
    );
  }
}
