import { Injectable } from '@angular/core';
import {ServicioPrincipalService} from "../../../servicios/servicio-principal.service";
import {MarcaInterface} from "../interfaces/marca.interface";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {ModeloInterface} from "../interfaces/modelo.interface";

@Injectable({
  providedIn: 'root'
})
export class ModeloRestService extends ServicioPrincipalService<ModeloInterface>  {

  constructor(protected readonly _httpClient: HttpClient) {
    super(_httpClient);
    this.url = environment.api.url;
    this.puerto = environment.api.puerto;
    this.segmento = 'modelo';
  }
}
