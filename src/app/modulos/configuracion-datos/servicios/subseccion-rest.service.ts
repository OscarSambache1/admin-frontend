import { Injectable } from '@angular/core';
import {ServicioPrincipalService} from "../../../servicios/servicio-principal.service";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {SeccionInterface} from "../interfaces/seccion.interface";
import {SubseccionInterface} from "../interfaces/subseccion.interface";

@Injectable({
  providedIn: 'root'
})
export class SubseccionRestService extends ServicioPrincipalService<SubseccionInterface>  {

  constructor(protected readonly _httpClient: HttpClient) {
    super(_httpClient);
    this.url = environment.api.url;
    this.puerto = environment.api.puerto;
    this.segmento = 'subseccion';
  }
}
