import { Injectable } from '@angular/core';
import {ServicioPrincipalService} from "../../../servicios/servicio-principal.service";
import {MarcaInterface} from "../interfaces/marca.interface";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {ModeloInterface} from "../interfaces/modelo.interface";
import {AccesorioInterface} from "../interfaces/accesorio.interface";
import {FormaPagoInterface} from "../interfaces/forma-pago.interface";
import {TipoCambioInterface} from "../interfaces/tipo-cambio.interface";

@Injectable({
  providedIn: 'root'
})
export class TipoCambioRestService extends ServicioPrincipalService<TipoCambioInterface>  {

  constructor(protected readonly _httpClient: HttpClient) {
    super(_httpClient);
    this.url = environment.api.url;
    this.puerto = environment.api.puerto;
    this.segmento = 'tipo-cambio';
  }
}
