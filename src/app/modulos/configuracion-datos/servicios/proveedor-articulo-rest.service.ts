import { Injectable } from '@angular/core';
import {ServicioPrincipalService} from "../../../servicios/servicio-principal.service";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {ArticuloInterface} from "../interfaces/articulo.interface";
import {Observable} from "rxjs";
import {ProveedorArticuloInterface} from "../interfaces/proveedor-articulo.interface";

@Injectable({
  providedIn: 'root'
})
export class ProveedorArticuloRestService extends ServicioPrincipalService<ProveedorArticuloInterface>  {

  constructor(protected readonly _httpClient: HttpClient) {
    super(_httpClient);
    this.url = environment.api.url;
    this.puerto = environment.api.puerto;
    this.segmento = 'proveedor-articulo';
  }
}
