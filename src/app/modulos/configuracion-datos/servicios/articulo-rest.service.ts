import { Injectable } from '@angular/core';
import {ServicioPrincipalService} from "../../../servicios/servicio-principal.service";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {ArticuloInterface} from "../interfaces/articulo.interface";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ArticuloRestService extends ServicioPrincipalService<ArticuloInterface>  {

  constructor(protected readonly _httpClient: HttpClient) {
    super(_httpClient);
    this.url = environment.api.url;
    this.puerto = environment.api.puerto;
    this.segmento = 'articulo';
  }

  crearEditarProductoServicio(datos: any, esServicio: 0 | 1): Observable<any> {
    return this._httpClient.post(
      `${this.url}:${this.puerto}/${this.segmento}/crear-editar-articulo/${esServicio}`,
      datos,
    );
  }

  actualizarStock(datos: any): Observable<any> {
    return this._httpClient.post(
      `${this.url}:${this.puerto}/${this.segmento}/actualizar-stock-articulo`,
      datos,
    );
  }

  eliminarArticulo(id: number): Observable<any> {
    return this._httpClient.put(
      `${this.url}:${this.puerto}/${this.segmento}/eliminar-articulo/${id}`,
      {}
    );
  }
}
