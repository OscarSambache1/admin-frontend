import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductoRoutingModule } from './producto-routing.module';
import { RutaProductoComponent } from './ruta-producto/ruta-producto.component';
import {IMPORTS_MODULO} from "../../imports/imports-modulo";
import {ComponentesModule} from "../../../componentes/componentes.module";


@NgModule({
  declarations: [
    RutaProductoComponent
  ],
  imports: [
    CommonModule,
    ProductoRoutingModule,
    ...IMPORTS_MODULO,
    ComponentesModule,
  ]
})
export class ProductoModule { }
