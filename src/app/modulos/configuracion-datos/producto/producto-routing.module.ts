import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaProductoComponent} from "./ruta-producto/ruta-producto.component";

const routes: Routes = [
  {
    path: '',
    component: RutaProductoComponent,
    data: {
      breadcrumb: 'Repuesto',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductoRoutingModule { }
