import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-menu-configuracion-datos',
  templateUrl: './menu-configuracion-datos.component.html',
  styleUrls: ['./menu-configuracion-datos.component.scss']
})
export class MenuConfiguracionDatosComponent implements OnInit {

  arregloItemsMenuInicio = [
    // {
    //   nombre: 'Accesorios',
    //   texto: 'Accesorios',
    //   imagen: '/assets/images/accesorio.png',
    //   ruta: 'accesorio'
    // },
    // {
    //   nombre: 'Repuestos',
    //   texto: 'Repuestos',
    //   imagen: '/assets/images/articulo.png',
    //   ruta: 'producto'
    // },
    {
      nombre: 'Servicios',
      texto: 'Servicios',
      imagen: '/assets/images/servicio.png',
      ruta: 'servicio'
    },
    {
      nombre: 'Proveedores',
      texto: 'Proveedores',
      imagen: '/assets/images/proveedor.png',
      ruta: 'proveedor'
    },
    {
      nombre: 'Clientes',
      texto: 'Clientes',
      imagen: '/assets/images/cliente.png',
      ruta: 'cliente'
    },
    {
      nombre: 'Vehículos',
      texto: 'Vehículos',
      imagen: '/assets/images/vehiculo.png',
      ruta: 'vehiculo'
    },
    {
      nombre: 'Bancos',
      texto: 'Bancos',
      imagen: '/assets/images/banco.png',
      ruta: 'banco'
    },
    {
      nombre: 'Formas de Pago',
      texto: 'Formas de Pago',
      imagen: '/assets/images/forma-pago.png',
      ruta: 'forma-pago'
    },
    {
      nombre: 'Impuestos',
      texto: 'Impuestos',
      imagen: '/assets/images/impuesto.png',
      ruta: 'impuesto'
    },
    {
      nombre: 'Marcas',
      texto: 'Marcas',
      imagen: '/assets/images/marca.png',
      ruta: 'marca'
    },
    // {
    //   nombre: 'Secciones',
    //   texto: 'Secciones',
    //   imagen: '/assets/images/seccion.png',
    //   ruta: 'seccion'
    // },
    // {
    //   nombre: 'Subsecciones',
    //   texto: 'Subecciones',
    //   imagen: '/assets/images/subseccion.png',
    //   ruta: 'subseccion'
    // },
    // {
    //   nombre: 'Tipos de Cambio',
    //   texto: 'Tipos de Cambio',
    //   imagen: '/assets/images/tipo-cambio.png',
    //   ruta: 'tipo-cambio'
    // },
    // {
    //   nombre: 'Tipo de Combustibles',
    //   texto: 'Tipo de Combustibles',
    //   imagen: '/assets/images/tipo-combustible.png',
    //   ruta: 'tipo-combustible'
    // },
    // {
    //   nombre: 'Tipo de Vehículos',
    //   texto: 'Tipo de Vehículos',
    //   imagen: '/assets/images/tipo-vehiculo.png',
    //   ruta: 'tipo-vehiculo'
    // },
  ];
  constructor(
    private readonly _router: Router
  ) { }

  ngOnInit(): void {
  }

  irARuta(rutaOpcion: string) {
    const ruta = ['/app', 'configuracion-datos', rutaOpcion];
    void this._router.navigate(ruta);
  }
}
