import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModeloRoutingModule } from './modelo-routing.module';
import { RutaModeloComponent } from './ruta-modelo/ruta-modelo.component';
import {IMPORTS_MODULO} from "../../imports/imports-modulo";
import {ComponentesModule} from "../../../componentes/componentes.module";


@NgModule({
  declarations: [
    RutaModeloComponent
  ],
  imports: [
    CommonModule,
    ModeloRoutingModule,
    ...IMPORTS_MODULO,
    ComponentesModule,
  ]
})
export class ModeloModule { }
