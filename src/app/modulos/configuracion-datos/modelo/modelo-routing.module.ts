import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaModeloComponent} from "./ruta-modelo/ruta-modelo.component";

const routes: Routes = [
  {
    path: '',
    component: RutaModeloComponent,
    data: {
      breadcrumb: 'Modelo',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModeloRoutingModule { }
