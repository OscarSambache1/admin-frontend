import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TipoCambioRoutingModule } from './tipo-cambio-routing.module';
import { RutaTipoCambioComponent } from './ruta-tipo-cambio/ruta-tipo-cambio.component';
import {IMPORTS_MODULO} from "../../imports/imports-modulo";
import {ComponentesModule} from "../../../componentes/componentes.module";


@NgModule({
  declarations: [
    RutaTipoCambioComponent
  ],
  imports: [
    CommonModule,
    TipoCambioRoutingModule,
    ...IMPORTS_MODULO,
    ComponentesModule,
  ]
})
export class TipoCambioModule { }
