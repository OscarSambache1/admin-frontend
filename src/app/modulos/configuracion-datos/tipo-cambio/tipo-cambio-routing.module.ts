import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaTipoCambioComponent} from "./ruta-tipo-cambio/ruta-tipo-cambio.component";

const routes: Routes = [
  {
    path: '',
    component: RutaTipoCambioComponent,
    data: {
      breadcrumb: 'Tipo Cambio',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TipoCambioRoutingModule { }
