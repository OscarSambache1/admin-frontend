import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccesorioRoutingModule } from './accesorio-routing.module';
import { RutaAccesorioComponent } from './ruta-accesorio/ruta-accesorio.component';
import {IMPORTS_MODULO} from "../../imports/imports-modulo";
import {ComponentesModule} from "../../../componentes/componentes.module";


@NgModule({
  declarations: [
    RutaAccesorioComponent
  ],
  imports: [
    CommonModule,
    AccesorioRoutingModule,
    ...IMPORTS_MODULO,
    ComponentesModule,
  ],
})
export class AccesorioModule { }
