import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaAccesorioComponent} from "./ruta-accesorio/ruta-accesorio.component";

const routes: Routes = [
  {
    path: '',
    component: RutaAccesorioComponent,
    data: {
      breadcrumb: 'Accesorio',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccesorioRoutingModule { }
