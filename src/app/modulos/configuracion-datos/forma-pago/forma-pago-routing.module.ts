import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaFormaPagoComponent} from "./ruta-forma-pago/ruta-forma-pago.component";

const routes: Routes = [
  {
    path: '',
    component: RutaFormaPagoComponent,
    data: {
      breadcrumb: 'Forma de Pago',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormaPagoRoutingModule { }
