import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormaPagoRoutingModule } from './forma-pago-routing.module';
import { RutaFormaPagoComponent } from './ruta-forma-pago/ruta-forma-pago.component';
import {IMPORTS_MODULO} from "../../imports/imports-modulo";
import {ComponentesModule} from "../../../componentes/componentes.module";


@NgModule({
  declarations: [
    RutaFormaPagoComponent
  ],
  imports: [
    CommonModule,
    FormaPagoRoutingModule,
    ...IMPORTS_MODULO,
    ComponentesModule,
  ]
})
export class FormaPagoModule { }
