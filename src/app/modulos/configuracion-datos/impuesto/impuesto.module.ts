import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ImpuestoRoutingModule } from './impuesto-routing.module';
import { RutaImpuestoComponent } from './ruta-impuesto/ruta-impuesto.component';
import {IMPORTS_MODULO} from "../../imports/imports-modulo";
import {ComponentesModule} from "../../../componentes/componentes.module";


@NgModule({
  declarations: [
    RutaImpuestoComponent
  ],
  imports: [
    CommonModule,
    ImpuestoRoutingModule,
    ...IMPORTS_MODULO,
    ComponentesModule,
  ]
})
export class ImpuestoModule { }
