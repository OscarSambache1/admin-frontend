import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaImpuestoComponent} from "./ruta-impuesto/ruta-impuesto.component";

const routes: Routes = [
  {
    path: '',
    component: RutaImpuestoComponent,
    data: {
      breadcrumb: 'Impuesto',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ImpuestoRoutingModule { }
