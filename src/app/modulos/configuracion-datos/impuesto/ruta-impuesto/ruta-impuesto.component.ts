import {Component, OnInit, ViewChild} from '@angular/core';
import {DxDataGridComponent} from "devextreme-angular";
import {ROWS, ROWS_PER_PAGE} from "../../../../constantes/table-config";
import {ImpuestoInterface} from "../../interfaces/impuesto.interface";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../../servicios/cargando.service";
import {ImpuestoRestService} from "../../servicios/impuesto-rest.service";
import {NotificacionService} from "../../../../servicios/notificacion/notificacion.service";
import {ModalCrearEditarImpuestoComponent} from "../../../../componentes/modales/modal-crear-editar-impuesto/modal-crear-editar-impuesto.component";

@Component({
  selector: 'app-ruta-impuesto',
  templateUrl: './ruta-impuesto.component.html',
  styleUrls: ['./ruta-impuesto.component.scss']
})
export class RutaImpuestoComponent implements OnInit {


  @ViewChild(DxDataGridComponent)
  dataGrid!: DxDataGridComponent;

  rows = ROWS;
  rowsPerPage = ROWS_PER_PAGE;
  datos: ImpuestoInterface[] = [];
  pageSize!: number;
  disabledTable!: boolean;
  pagination: number[] = [];
  paginationStart!: number;


  consulta: any = {
    relations: [],
    where: {},
    order: {
      id: 'DESC',
    },
  };

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _impuestoRestService: ImpuestoRestService,
    private readonly _notificacionService: NotificacionService,
  ) {}

  ngOnInit(): void {
    this.pagination = ROWS_PER_PAGE;
    this.paginationStart = ROWS;
    this.cargarDatosIniciales(this.consulta);
  }

  cargarDatosIniciales(consulta: {}): void {
    this._cargandoService.habilitar();
    const impuesto$ = this._impuestoRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    impuesto$.subscribe({
      next: (impuesto) => {
        this.datos = impuesto[0];
        this.pageSize = impuesto[1];
        this.disabledTable = false;
        this._cargandoService.deshabilitar();
      },
      error: (error) => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
      },
    });
  }

  onToolbarPreparing(evento: any): void {
    evento.toolbarOptions.items.unshift(
      // {
      //   location: 'after',
      //   widget: 'dxButton',
      //   options: {
      //     disabled: this.disabledTable,
      //     hint: 'Agregar nuevo registro',
      //     onClick: this.add.bind(this, undefined),
      //     icon: 'plus',
      //   },
      // },
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Recargar datos de la tabla',
          icon: 'refresh',
          onClick: this.onRefreshDataGrid.bind(this),
        },
      }
    );
  }

  private onRefreshDataGrid(): void {
    this.cargarDatosIniciales(this.consulta);
    void this.dataGrid.instance.refresh();
  }

  add(impuesto?: ImpuestoInterface): void {
    const dialogRef = this._matDialog.open(ModalCrearEditarImpuestoComponent, {
      width: '750px',
      data: {
        impuesto,
      },
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          if (impuesto) {
            this.editar(response, impuesto.id as number);
          } else {
            this.crear(response);
          }
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

  crear(impuesto: ImpuestoInterface): void {
    const impuesto$ = this._impuestoRestService.crearImpuesto(impuesto);

    impuesto$.subscribe({
      next: () => {
        this._notificacionService.modalInfo(
          'success',
          'Info',
          'Registro guardado correctamente',
        );
        this.onRefreshDataGrid();
      },
      error: (err) => {
        console.error({
          mensage: 'Error crear Impuesto',
          error: err,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al guardar el registro',
        );
      },
    });
  }

  editar(impuesto: ImpuestoInterface, id: number): void {
    impuesto.id = id;
    const impuesto$ = this._impuestoRestService.editarImpuesto(impuesto);

    impuesto$.subscribe({
      next: () => {
        this._notificacionService.modalInfo(
          'success',
          'Info',
          'Registro editado correctamente',
        );
        this.onRefreshDataGrid();
      },
      error: (err) => {
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al editar el registro',
        );
        console.error({
          mensage: 'Error editar Impuesto',
          error: err,
        });
      },
    });
  }

}
