import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VehiculoRoutingModule } from './vehiculo-routing.module';
import { RutaVehiculoComponent } from './ruta-vehiculo/ruta-vehiculo.component';
import {IMPORTS_MODULO} from "../../imports/imports-modulo";
import {ComponentesModule} from "../../../componentes/componentes.module";


@NgModule({
  declarations: [
    RutaVehiculoComponent
  ],
  imports: [
    CommonModule,
    VehiculoRoutingModule,
    ...IMPORTS_MODULO,
    ComponentesModule,
  ]
})
export class VehiculoModule { }
