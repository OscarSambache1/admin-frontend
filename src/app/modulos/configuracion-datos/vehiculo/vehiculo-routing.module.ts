import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaVehiculoComponent} from "./ruta-vehiculo/ruta-vehiculo.component";

const routes: Routes = [
  {
    path: '',
    component: RutaVehiculoComponent,
    data: {
      breadcrumb: 'Vehículo',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VehiculoRoutingModule { }
