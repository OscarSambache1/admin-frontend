import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TipoVehiculoRoutingModule } from './tipo-vehiculo-routing.module';
import { RutaTipoVehiculoComponent } from './ruta-tipo-vehiculo/ruta-tipo-vehiculo.component';
import {IMPORTS_MODULO} from "../../imports/imports-modulo";
import {ComponentesModule} from "../../../componentes/componentes.module";


@NgModule({
  declarations: [
    RutaTipoVehiculoComponent
  ],
  imports: [
    CommonModule,
    TipoVehiculoRoutingModule,
    ...IMPORTS_MODULO,
    ComponentesModule,
  ]
})
export class TipoVehiculoModule { }
