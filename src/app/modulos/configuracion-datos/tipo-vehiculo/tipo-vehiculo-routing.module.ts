import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaTipoVehiculoComponent} from "./ruta-tipo-vehiculo/ruta-tipo-vehiculo.component";

const routes: Routes = [
  {
    path: '',
    component: RutaTipoVehiculoComponent,
    data: {
      breadcrumb: 'Tipo Vehículo',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TipoVehiculoRoutingModule { }
