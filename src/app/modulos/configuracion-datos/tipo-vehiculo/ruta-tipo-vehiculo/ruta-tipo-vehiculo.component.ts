import {Component, OnInit, ViewChild} from '@angular/core';
import {DxDataGridComponent} from "devextreme-angular";
import {ROWS, ROWS_PER_PAGE} from "../../../../constantes/table-config";
import {TipoVehiculoInterface} from "../../interfaces/tipo-vehiculo.interface";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../../servicios/cargando.service";
import {TipoVehiculoRestService} from "../../servicios/tipo-vehiculo-rest.service";
import {NotificacionService} from "../../../../servicios/notificacion/notificacion.service";
import {ModalCrearEditarTipoVehiculoComponent} from "../../../../componentes/modales/modal-crear-editar-tipo-vehiculo/modal-crear-editar-tipo-vehiculo.component";

@Component({
  selector: 'app-ruta-tipo-vehiculo',
  templateUrl: './ruta-tipo-vehiculo.component.html',
  styleUrls: ['./ruta-tipo-vehiculo.component.scss']
})
export class RutaTipoVehiculoComponent implements OnInit {

  @ViewChild(DxDataGridComponent)
  dataGrid!: DxDataGridComponent;

  rows = ROWS;
  rowsPerPage = ROWS_PER_PAGE;
  datos: TipoVehiculoInterface[] = [];
  pageSize!: number;
  disabledTable!: boolean;
  pagination: number[] = [];
  paginationStart!: number;


  consulta: any = {
    relations: [],
    where: {},
    order: {
      id: 'DESC',
    },
  };

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _tipoVehiculoRestService: TipoVehiculoRestService,
    private readonly _notificacionService: NotificacionService,
  ) {}

  ngOnInit(): void {
    this.pagination = ROWS_PER_PAGE;
    this.paginationStart = ROWS;
    this.cargarDatosIniciales(this.consulta);
  }

  cargarDatosIniciales(consulta: {}): void {
    this._cargandoService.habilitar();
    const tipoVehiculo$ = this._tipoVehiculoRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    tipoVehiculo$.subscribe({
      next: (tipoVehiculo) => {
        this.datos = tipoVehiculo[0];
        this.pageSize = tipoVehiculo[1];
        this.disabledTable = false;
        this._cargandoService.deshabilitar();
      },
      error: (error) => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
      },
    });
  }

  onToolbarPreparing(evento: any): void {
    evento.toolbarOptions.items.unshift(
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Agregar nuevo registro',
          onClick: this.add.bind(this, undefined),
          icon: 'plus',
        },
      },
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Recargar datos de la tabla',
          icon: 'refresh',
          onClick: this.onRefreshDataGrid.bind(this),
        },
      }
    );
  }

  private onRefreshDataGrid(): void {
    this.cargarDatosIniciales(this.consulta);
    void this.dataGrid.instance.refresh();
  }

  add(tipoVehiculo?: TipoVehiculoInterface): void {
    const dialogRef = this._matDialog.open(ModalCrearEditarTipoVehiculoComponent, {
      width: '450px',
      data: {
        tipoVehiculo: tipoVehiculo,
      },
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          if (tipoVehiculo) {
            this.editar(response, tipoVehiculo.id as number);
          } else {
            this.crear(response);
          }
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

  crear(tipoVehiculo: TipoVehiculoInterface): void {
    const tipoVehiculo$ = this._tipoVehiculoRestService.create(tipoVehiculo);

    tipoVehiculo$.subscribe({
      next: () => {
        this._notificacionService.modalInfo(
          'success',
          'Info',
          'Registro guardado correctamente',
        );
        this.onRefreshDataGrid();
      },
      error: (err) => {
        console.error({
          mensage: 'Error crear registro',
          error: err,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al guardar el registro',
        );
      },
    });
  }

  editar(tipoVehiculo: TipoVehiculoInterface, id: number): void {
    const tipoVehiculo$ = this._tipoVehiculoRestService.updateOne(id, tipoVehiculo);

    tipoVehiculo$.subscribe({
      next: () => {
        this._notificacionService.modalInfo(
          'success',
          'Info',
          'Registro editado correctamente',
        );
        this.onRefreshDataGrid();
      },
      error: (err) => {
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al editar el registro',
        );
        console.error({
          mensage: 'Error editar registro',
          error: err,
        });
      },
    });
  }

  async cambiarEstado(data: any) {
    const respuestaModalConfirmacion = await this._notificacionService.modalConfirmsSwal(
      'question',
      'Advertencia',
      'Está seguro de cambiar el estado del registro',
    );
    if (respuestaModalConfirmacion) {
      const registroEditar: any = {
        estado: data.estado ? 0 : 1,
      }
      this.editar(registroEditar, data.id);
    }
  }
}
