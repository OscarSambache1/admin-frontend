import {GastoInterface} from "./gasto.interface";
import {ProveedorInterface} from "../../configuracion-datos/interfaces/proveedor.interface";
import {FacturaDetalleInterface} from "./factura-detalle.interface";
import {AbonoFacturaInterface} from "./abono-factura.interface";
import {VehiculoInterface} from "../../configuracion-datos/interfaces/vehiculo.interface";
import {ClienteInterface} from "../../configuracion-datos/interfaces/cliente.interface";
import {TrabajoInterface} from "../../trabajos/interfaces/trabajo.interface";

export interface FacturaCabeceraInterface {
  id?: number;
  tipoFactura?: string; // VENTA V, GASTO G
  fechaEmision?: string;
  numero?: string;

  identificacionProveedor?: string;
  nombreComercialProveedor?: string;
  razonSocialProveedor?: string;
  direccionProveedor?: string;
  telefonoProveedor?: string;
  correoProveedor?: string;

  identificacionCliente?: string;
  nombreComercialCliente?: string;
  razonSocialCliente?: string;
  direccionCliente?: string;
  telefonoCliente?: string;
  correoCliente?: string;

  propina?: number;
  descuento?: number;
  descuentoPorcentaje?: number;
  subtotalSinImpuestos?: number;
  subtotalIva0?: number;
  subtotalIva?: number;
  subtotalIvaExento?: number;
  subtotalIvaNoObjeto?: number;
  ivaValor?: number;
  iceValor?: number;
  total?: number;
  ivaPorcentaje?: any;

  gasto?: GastoInterface;
  proveedor?: ProveedorInterface;
  facturaDetalles?: FacturaDetalleInterface[];
  abonosFactura?: AbonoFacturaInterface[];

  archivoPdf?: any;
  nombreArchivo?: string;
  estadoAccion?: any;

  idTrabajo?: number;
  vehiculo?: VehiculoInterface;
  esConsumidorFinal?: any;

  cliente?: ClienteInterface;
  trabajo?: TrabajoInterface;
  razonSocial?: string;
  nombreComercial?: string;
  identificacion?: string,
  tipoIdentificacion?: string;
  direccion?: string;
  telefono?: string;
  correo?: string;
  //idGoogleDrive?: string;
  archivos?: any[];
}
