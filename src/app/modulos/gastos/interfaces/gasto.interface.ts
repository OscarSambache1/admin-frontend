import {FacturaCabeceraInterface} from "./factura-cabecera.interface";

export interface GastoInterface {
  id?: number;
  tipoGasto?: string; // P PRODCUTO, R RESPUESTO
  estadoGasto?: string; // PP por pagar, A abonado, P pagado
  estadoGastoAccion?: string;
  valorAPagar?: number;
  valorPagado?: number;
  facturaCabecera?: FacturaCabeceraInterface;
}
