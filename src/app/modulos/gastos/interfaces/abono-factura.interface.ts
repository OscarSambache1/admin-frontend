import {FacturaCabeceraInterface} from "./factura-cabecera.interface";
import {FormaPagoInterface} from "../../configuracion-datos/interfaces/forma-pago.interface";

export interface AbonoFacturaInterface {
  id?: number;
  fechaPago?: string;
  valorAPagar?: number;
  estadoAbono?: string; // PP por pagar, A abonado, P pagado
  facturaCabecera?: FacturaCabeceraInterface;
  formaPago?: FormaPagoInterface;
}
