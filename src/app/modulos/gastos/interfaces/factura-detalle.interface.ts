import {ArticuloInterface} from "../../configuracion-datos/interfaces/articulo.interface";
import {FacturaCabeceraInterface} from "./factura-cabecera.interface";

export interface FacturaDetalleInterface {
  id?: number;
  codigoPrincipal?: string;
  descripcion?: string;
  cantidad?: number;
  precioUnitario?: number;
  ivaPorcentaje?: number;
  ivaDescripcion?: string;
  valorIva?: number;
  valorIce?: number;
  descuento?: number;
  valorDescuento?: number;
  total?: number;
  articulo?: ArticuloInterface;
  facturaCabecera?: FacturaCabeceraInterface;
  tipo?: any;
}
