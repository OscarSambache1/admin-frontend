import { Injectable } from '@angular/core';
import {ServicioPrincipalService} from "../../../servicios/servicio-principal.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {GastoInterface} from "../interfaces/gasto.interface";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class GastoRestService extends ServicioPrincipalService<GastoInterface>  {

  constructor(protected readonly _httpClient: HttpClient) {
    super(_httpClient);
    this.url = environment.api.url;
    this.puerto = environment.api.puerto;
    this.segmento = 'gasto';
  }

  eliminarGasto(
    idGasto: number,
  ): Observable<any> {
    return this._httpClient.delete(
      `${this.url}:${this.puerto}/${this.segmento}/eliminar-gasto/${idGasto}`,
      {
        headers: new HttpHeaders({
          timeout: `${360000000}`,
        }),
      },
    );
  }
}
