import { Injectable } from '@angular/core';
import {ServicioPrincipalService} from "../../../servicios/servicio-principal.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {FacturaCabeceraInterface} from "../interfaces/factura-cabecera.interface";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class FacturaCabeceraRestService extends ServicioPrincipalService<FacturaCabeceraInterface>  {

  constructor(protected readonly _httpClient: HttpClient) {
    super(_httpClient);
    this.url = environment.api.url;
    this.puerto = environment.api.puerto;
    this.segmento = 'factura-cabecera';
  }

  crearEditarGastoFactura(
    formadata: FormData,
  ): Observable<any> {
    return this._httpClient.post(
      `${this.url}:${this.puerto}/${this.segmento}/crear-editar-gasto-factura`,
      formadata,
      {
        headers: new HttpHeaders({
          timeout: `${360000000}`,
        }),
      },
    );
  }

  agregarAbonoGasto(
    datos: any
  ): Observable<any> {
    return this._httpClient.post(
      `${this.url}:${this.puerto}/${this.segmento}/agregar-abono-pago-gasto-factura`,
      datos,
      {
        headers: new HttpHeaders({
          timeout: `${360000000}`,
        }),
      },
    );
  }

  crearEditarFactura(datos: any): Observable<any> {
    return this._httpClient.post(
      `${this.url}:${this.puerto}/${this.segmento}/crear-editar-factura`,
      datos,
    );
  }
}
