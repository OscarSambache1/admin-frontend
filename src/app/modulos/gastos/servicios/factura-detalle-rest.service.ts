import { Injectable } from '@angular/core';
import {ServicioPrincipalService} from "../../../servicios/servicio-principal.service";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {FacturaDetalleInterface} from "../interfaces/factura-detalle.interface";

@Injectable({
  providedIn: 'root'
})
export class FacturaDetalleRestService extends ServicioPrincipalService<FacturaDetalleInterface>  {

  constructor(protected readonly _httpClient: HttpClient) {
    super(_httpClient);
    this.url = environment.api.url;
    this.puerto = environment.api.puerto;
    this.segmento = 'factura-detalle';
  }
}
