import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GastosRoutingModule } from './gastos-routing.module';
import { RutaGastosComponent } from './ruta/ruta-gastos/ruta-gastos.component';
import {IMPORTS_MODULO} from "../imports/imports-modulo";
import {ComponentesModule} from "../../componentes/componentes.module";
import { RutaCrearEditarGastoComponent } from './ruta/ruta-crear-editar-gasto/ruta-crear-editar-gasto.component';


@NgModule({
  declarations: [
    RutaGastosComponent,
    RutaCrearEditarGastoComponent
  ],
  imports: [
    CommonModule,
    GastosRoutingModule,
    ...IMPORTS_MODULO,
    ComponentesModule,
  ],
  providers: [
  ]
})
export class GastosModule { }
