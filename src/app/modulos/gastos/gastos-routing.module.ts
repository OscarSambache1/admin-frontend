import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaGastosComponent} from "./ruta/ruta-gastos/ruta-gastos.component";
import {RutaCrearEditarGastoComponent} from "./ruta/ruta-crear-editar-gasto/ruta-crear-editar-gasto.component";

const routes: Routes = [
  {
    path: '',
    component: RutaGastosComponent,
    data: {
      breadcrumb: '',
    },
  },
  {
    path: 'crear-editar-gasto/:idGasto',
    component: RutaCrearEditarGastoComponent,
    data: {
      breadcrumb: 'Guardar',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GastosRoutingModule { }
