import {Component, OnInit, ViewChild} from '@angular/core';
import {DxDataGridComponent} from "devextreme-angular";
import {ROWS, ROWS_PER_PAGE} from "../../../../constantes/table-config";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../../servicios/cargando.service";
import {NotificacionService} from "../../../../servicios/notificacion/notificacion.service";
import {GastoInterface} from "../../interfaces/gasto.interface";
import {GastoRestService} from "../../servicios/gasto-rest.service";
import {EstadoGasto, EstadoGastoAccion, TipoGasto} from "../../../../enums/tipo-contacto.enums";
import {FacturaCabeceraInterface} from "../../interfaces/factura-cabecera.interface";
import {environment} from "../../../../../environments/environment";
import {ModalPagosAbonosComponent} from "../../../../componentes/modales/modal-pagos-abonos/modal-pagos-abonos.component";
import {FacturaCabeceraRestService} from "../../servicios/factura-cabecera-rest.service";
import {ActualizarPagosService} from "../../../../servicios/actualizar-pagos.service";
import {ImprimitService} from "../../../../servicios/imprimit.service";
import {DomSanitizer} from "@angular/platform-browser";
import {Buffer} from 'buffer';

@Component({
  selector: 'app-ruta-gastos',
  templateUrl: './ruta-gastos.component.html',
  styleUrls: ['./ruta-gastos.component.scss']
})
export class RutaGastosComponent implements OnInit {

  @ViewChild(DxDataGridComponent)
  dataGrid!: DxDataGridComponent;

  rows = ROWS;
  rowsPerPage = ROWS_PER_PAGE;
  datos: GastoInterface[] = [];
  pageSize!: number;
  disabledTable!: boolean;
  pagination: number[] = [];
  paginationStart!: number;
  tiposGasto = [
    {
      label: 'REPUESTO',
      value: TipoGasto.Repuesto
    },
    {
      label: 'PRODUCTO',
      value: TipoGasto.Producto
    },
  ];
  estadoGastoAccion = [
    {
      label: EstadoGastoAccion.FINALIZADO,
      value: EstadoGastoAccion.FINALIZADO
    },
    {
      label: EstadoGastoAccion.GUARDADO,
      value: EstadoGastoAccion.GUARDADO
    },
  ];

  consulta: any = {
    relations: [
      'facturaCabecera',
      'facturaCabecera.facturaDetalles',
      'facturaCabecera.facturaDetalles.articulo',
      'facturaCabecera.archivos',
      'facturaCabecera.abonosFactura',
      'facturaCabecera.abonosFactura.formaPago',
      'facturaCabecera.gasto',
      'facturaCabecera.proveedor',
      'facturaCabecera.proveedor.informacionTributaria',
      'facturaCabecera.proveedor.cuentasBanco',
      'facturaCabecera.proveedor.cuentasBanco.banco',
    ],
    where: {},
    order: {
      id: 'DESC',
    },
  };
  estadoGAtoAccionEnum = EstadoGastoAccion;

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _gastoRestService: GastoRestService,
    private readonly _facturaCabeceraRestService: FacturaCabeceraRestService,
    private readonly _notificacionService: NotificacionService,
    private readonly _actualizarPagosService: ActualizarPagosService,
    private readonly _imprimirService: ImprimitService,
    public readonly _domSanitizer: DomSanitizer,
  ) {
  }

  ngOnInit(): void {
    this.pagination = ROWS_PER_PAGE;
    this.paginationStart = ROWS;
    this.cargarDatosIniciales(this.consulta);
  }

  cargarDatosIniciales(consulta: {}): void {
    this._cargandoService.habilitar();
    const gasto$ = this._gastoRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    gasto$.subscribe({
      next: (gasto) => {
        this.datos = gasto[0];
        this.pageSize = gasto[1];
        this.disabledTable = false;
        this._cargandoService.deshabilitar();
      },
      error: (error) => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
      },
    });
  }

  onToolbarPreparing(evento: any): void {
    evento.toolbarOptions.items.unshift(
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Agregar nuevo gasto',
          onClick: this.add.bind(this, undefined),
          icon: 'plus',
        },
      },
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Recargar datos de la tabla',
          icon: 'refresh',
          onClick: this.onRefreshDataGrid.bind(this),
        },
      }
    );
  }

  private onRefreshDataGrid(): void {
    this.cargarDatosIniciales(this.consulta);
    void this.dataGrid.instance.refresh();
  }

  add(gasto?: GastoInterface) {
    const ruta = ['/app', 'gastos', 'crear-editar-gasto', (gasto?.id || -1)];
    void this._router.navigate(ruta);
  }

  descargarFactura(data: FacturaCabeceraInterface) {
    if (data.archivos?.length) {
      const archivo = data.archivos[0];
      const buffer = archivo.buffer?.data;
      const urlArchivo = `data:${archivo.mimetype};base64, ${Buffer.from(buffer)}`;
      const link = document.createElement('a');
      link.setAttribute('target', '_blank');
      link.setAttribute('href', urlArchivo);
      link.setAttribute('download', archivo.originalname as string);
      document.body.appendChild(link);
      link.click();
      link.remove();
    }

  }


  abrirModalPagosAbonos(
    gasto: GastoInterface
  ) {
    const dialogRef = this._matDialog.open(ModalPagosAbonosComponent, {
      width: '850px',
      data: {
        factura: gasto?.facturaCabecera,
        idFactura: gasto?.facturaCabecera?.id,
      },
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          const datos = {
            idFactura: gasto.facturaCabecera?.id,
            abonosPagos: response,
          };
          const gastoFactura$ = this._facturaCabeceraRestService
            .agregarAbonoGasto(datos);

          gastoFactura$
            .subscribe(
              (respuesta) => {
                this._notificacionService.modalInfo(
                  'success',
                  'Éxito',
                  `El abono/pago ha sido agregado con éxito`,
                );
                this._cargandoService.deshabilitar();
                this.cargarDatosIniciales(this.consulta);
                this._actualizarPagosService.actualizarPagos(true);
              },
              error => {
                this._notificacionService.modalInfo(
                  'error',
                  'Error',
                  `Error al agregar el abono/pago`,
                );
                this.cargarDatosIniciales(this.consulta);
                this._cargandoService.deshabilitar();
                console.error({
                  mensaje: `Error al agregar el abono/pago`,
                  error,
                });
              }
            );
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

  async imprimir(gasto: any) {
    const pdfDefinition: any = await this._imprimirService.setearObjetoGasto(gasto);
    // console.log(pdfDefinition);
    await this._imprimirService.imprimir(pdfDefinition);
  }
}
