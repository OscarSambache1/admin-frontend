import {Component, OnInit, ViewChild} from '@angular/core';
import {TipoFactura, TipoGasto, EstadoGastoAccion} from "../../../../enums/tipo-contacto.enums";
import {GastoInterface} from "../../interfaces/gasto.interface";
import {FormularioGastoComponent} from "../../../../componentes/formularios/formulario-gasto/formulario-gasto.component";
import {FacturaCabeceraInterface} from "../../interfaces/factura-cabecera.interface";
import {NotificacionService} from "../../../../servicios/notificacion/notificacion.service";
import {CargandoService} from "../../../../servicios/cargando.service";
import {FacturaCabeceraRestService} from "../../servicios/factura-cabecera-rest.service";
import {GastoRestService} from "../../servicios/gasto-rest.service";
import {ActivatedRoute, Router} from "@angular/router";
import {debounceTime, map, mergeMap, timeout} from "rxjs/operators";
import {ModalCrearEditarAbonoFacturaComponent} from "../../../../componentes/modales/modal-crear-editar-abono-factura/modal-crear-editar-abono-factura.component";
import {MatDialog} from "@angular/material/dialog";
import {ModalPagosAbonosComponent} from "../../../../componentes/modales/modal-pagos-abonos/modal-pagos-abonos.component";
import {ActualizarPagosService} from "../../../../servicios/actualizar-pagos.service";
import {FacturaComponent} from "../../../../componentes/factura/factura.component";

@Component({
  selector: 'app-ruta-crear-editar-gasto',
  templateUrl: './ruta-crear-editar-gasto.component.html',
  styleUrls: ['./ruta-crear-editar-gasto.component.scss']
})
export class RutaCrearEditarGastoComponent implements OnInit {
  @ViewChild(FormularioGastoComponent)
  formularioGasto!: FormularioGastoComponent;

  @ViewChild(FacturaComponent)
  componenteFactura!: FacturaComponent;

  tipoFactura = TipoFactura;

  formulatioGastoValido!: boolean;

  facturaValida!: boolean;

  facturaACrearEditar?: FacturaCabeceraInterface;
  gastoACrearEditar?: GastoInterface;
  tipoGasto?: string = '';
  facturaPrevia?: FacturaCabeceraInterface;
  gastoPrevio?: GastoInterface;
  estaCargando = false;
  enumsEstadoGastoAccion = EstadoGastoAccion;
  mostrarFactura!: boolean;
  constructor(
    private readonly _notificacionService: NotificacionService,
    private readonly _cargandoService: CargandoService,
    private readonly _facturaCabeceraRestService: FacturaCabeceraRestService,
    private readonly _gastoRestService: GastoRestService,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _router: Router,
    private readonly _matDialog: MatDialog,
    private readonly _actualizarPagosService: ActualizarPagosService,
  ) { }

  ngOnInit(): void {
    this.estaCargando = true;
    this._cargandoService.habilitar();
    this._activatedRoute.params
      .pipe(
        mergeMap(params => {
          const idGasto = params.idGasto;
          const consulta: any = {
            relations: [
              'facturaCabecera',
              'facturaCabecera.archivos',
              'facturaCabecera.proveedor',
              'facturaCabecera.proveedor.informacionTributaria',
              'facturaCabecera.facturaDetalles',
              'facturaCabecera.facturaDetalles.articulo',
              'facturaCabecera.abonosFactura',
              'facturaCabecera.abonosFactura.formaPago',
            ],
            where: {
              id: idGasto,
            },
            order: {
              id: 'DESC',
            },
          };
          return this._gastoRestService.findAll(
            `busqueda=${JSON.stringify(consulta)}`
          );
        })
      )
      .subscribe(respuesta => {
        if (respuesta && respuesta[0] && respuesta[0][0]) {
          this.gastoPrevio = respuesta[0][0];
          this.facturaPrevia = this.gastoPrevio.facturaCabecera;
          this.mostrarFactura = !!this.facturaPrevia;
          this.tipoGasto = this.gastoPrevio.tipoGasto;
        }
        this._cargandoService.deshabilitar();
        this.estaCargando = false;
      }, error => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
        this.estaCargando = false;
      });
  }

  setearGasto(gasto: string) {
    this.mostrarFactura = false;
    this.tipoGasto = gasto;
    setTimeout(() => {
      this.mostrarFactura = true;
    }, 500);
  }

  recibirValorTotal(valorTotal: any) {
    if (this.formularioGasto?.formulario) {
      this.formularioGasto.formulario.patchValue(
        {
          valorAPagar: valorTotal,
        }
      )
    }
  }

  recibirValorPagado(valorPagado: any) {
    if (this.formularioGasto?.formulario) {
      this.formularioGasto.formulario.patchValue(
        {
          valorPagado,
        }
      )
    }
  }

  validarBotones() {
    return this.formulatioGastoValido && this.facturaValida;
  }

  mostrarBotonFinalizar() {
    return this.gastoPrevio?.estadoGastoAccion !== EstadoGastoAccion.FINALIZADO;
  }

  mostrarBotonGuadar() {
    return this.gastoPrevio?.estadoGastoAccion !== EstadoGastoAccion.FINALIZADO;
  }

  obtenerFactura(factura: FacturaCabeceraInterface) {
    if (factura) {
      this.facturaValida = true;
      this.facturaACrearEditar = factura;
    } else {
      this.facturaValida = false;
    }
  }

  obtnerGasto(gasto: GastoInterface | boolean) {
    if (gasto) {
      this.gastoACrearEditar = gasto as GastoInterface;
      this.formulatioGastoValido = true;
    } else {
      this.formulatioGastoValido = false;
    }
  }

  async guardarFinalizarGasto(accion: string) {
    let mensaje = '';
    if (accion === 'GUARDADO') {
      mensaje = 'Esta seguro de guardar el gasto, en caso de hacerlo podra seguir editando la información del gasto y la factura hasta finalizarlo';
    }
    if (accion === 'FINALIZADO') {
      mensaje = 'Esta seguro de finalizar el gasto, tenga en cuenta que' +
        ` ${this.tipoGasto === TipoGasto.Repuesto ? ' el stock será actualizado y ' : ''}  ` +
        'ya no podra editar la información del gasto y la factura ';
    }
    const respuestaModalConfirmacion = await this._notificacionService.modalConfirmsSwal(
      'question',
      'Advertencia',
      mensaje,
    );
    if (respuestaModalConfirmacion) {
      if (this.gastoPrevio && this.gastoACrearEditar) {
        this.gastoACrearEditar.id = this.gastoPrevio.id;
      }
      if (this.facturaPrevia && this.facturaACrearEditar) {
        this.facturaACrearEditar.id = this.facturaPrevia.id;
      }
      const datos = {
        accion,
        gasto: this.gastoACrearEditar,
        factura: this.facturaACrearEditar,
      }


      this._cargandoService.habilitar();
      const formData: FormData = new FormData();
      formData.append('archivoPdf', datos.factura?.archivoPdf);
      formData.append('datosGastoFactura', JSON.stringify(datos));
      const gastoFactura$ = this._facturaCabeceraRestService
        .crearEditarGastoFactura(formData);

      gastoFactura$
        .subscribe(
          (respuesta) => {
            this._notificacionService.modalInfo(
              'success',
              'Éxito',
              `El gasto ha sido ${accion.toUpperCase()} con éxito`,
            );
            this._cargandoService.deshabilitar();
            this._actualizarPagosService.actualizarPagos(true);
            const ruta = ['/app', 'gastos'];
            void this._router.navigate(ruta);
          },
          error => {
            const accionGasto = accion === 'GUARDARDO' ? 'guardar' : 'finalizar';
            this._notificacionService.modalInfo(
              'error',
              'Error',
              `Error al ${accionGasto} el gasto`,
            );
            this._cargandoService.deshabilitar();
            console.error({
              mensaje: `Error al ${accionGasto} el gasto`,
              error,
            });
          }
        );
    }

  }

  cancelar() {
    const ruta = ['/app', 'gastos'];
    void this._router.navigate(ruta);
  }
}
