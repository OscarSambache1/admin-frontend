import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListadosRoutingModule } from './listados-routing.module';
import { RutaMenuListadosComponent } from './rutas/ruta-menu-listados/ruta-menu-listados.component';
import { RutaListadoClientesComponent } from './rutas/ruta-listado-clientes/ruta-listado-clientes.component';
import { RutaListadoProveedoresComponent } from './rutas/ruta-listado-proveedores/ruta-listado-proveedores.component';
import {IMPORTS_MODULO} from "../imports/imports-modulo";


@NgModule({
  declarations: [
    RutaMenuListadosComponent,
    RutaListadoClientesComponent,
    RutaListadoProveedoresComponent
  ],
  imports: [
    CommonModule,
    ListadosRoutingModule,
    ...IMPORTS_MODULO,
  ]
})
export class ListadosModule { }
