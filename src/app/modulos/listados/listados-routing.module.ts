import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaMenuListadosComponent} from "./rutas/ruta-menu-listados/ruta-menu-listados.component";
import {RutaListadoClientesComponent} from "./rutas/ruta-listado-clientes/ruta-listado-clientes.component";
import {RutaListadoProveedoresComponent} from "./rutas/ruta-listado-proveedores/ruta-listado-proveedores.component";

const routes: Routes = [
  {
    path: '',
    component: RutaMenuListadosComponent,
    data: {
      breadcrumb: '',
    },
  },
  {
    path: 'cliente',
    component: RutaListadoClientesComponent,
    data: {
      breadcrumb: '',
    },
  },
  {
    path: 'proveedor',
    component: RutaListadoProveedoresComponent,
    data: {
      breadcrumb: '',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListadosRoutingModule { }
