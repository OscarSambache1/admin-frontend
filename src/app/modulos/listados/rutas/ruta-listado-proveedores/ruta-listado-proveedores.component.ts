import {Component, OnInit, ViewChild} from '@angular/core';
import {DxDataGridComponent} from "devextreme-angular";
import {ROWS, ROWS_PER_PAGE} from "../../../../constantes/table-config";
import {TipoCuentaBancoEnum} from "../../../../enums/tipo-contacto.enums";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../../servicios/cargando.service";
import {NotificacionService} from "../../../../servicios/notificacion/notificacion.service";
import {CuentaBancoInterface} from "../../../configuracion-datos/interfaces/cuenta-banco.interface";
import {ProveedorInterface} from "../../../configuracion-datos/interfaces/proveedor.interface";
import {ProveedorRestService} from "../../../configuracion-datos/servicios/proveedor-rest.service";

@Component({
  selector: 'app-ruta-listado-proveedores',
  templateUrl: './ruta-listado-proveedores.component.html',
  styleUrls: ['./ruta-listado-proveedores.component.scss']
})
export class RutaListadoProveedoresComponent implements OnInit {



  @ViewChild(DxDataGridComponent)
  dataGrid!: DxDataGridComponent;

  rows = ROWS;
  rowsPerPage = ROWS_PER_PAGE;
  datos: ProveedorInterface[] = [];
  pageSize!: number;
  disabledTable!: boolean;
  pagination: number[] = [];
  paginationStart!: number;
  tipoCuentaBancoEnum = TipoCuentaBancoEnum;

  consulta: any = {
    relations: ['informacionTributaria', 'contactos', 'cuentasBanco', 'cuentasBanco.banco'],
    where: {
    },
    order: {
      id: 'DESC',
    },
  };

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _proveedorRestService: ProveedorRestService,
    private readonly _notificacionService: NotificacionService,
  ) {}

  ngOnInit(): void {
    this.pagination = ROWS_PER_PAGE;
    this.paginationStart = ROWS;
    this.cargarDatosIniciales(this.consulta);
  }

  cargarDatosIniciales(consulta: {}): void {
    this._cargandoService.habilitar();
    const proveedor$ = this._proveedorRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    proveedor$.subscribe({
      next: (proveedor) => {
        this.datos = proveedor[0];
        this.pageSize = proveedor[1];
        this.disabledTable = false;
        this._cargandoService.deshabilitar();
      },
      error: (error) => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
      },
    });
  }

  onToolbarPreparing(evento: any): void {
    evento.toolbarOptions.items.unshift(
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Recargar datos de la tabla',
          icon: 'refresh',
          onClick: this.onRefreshDataGrid.bind(this),
        },
      }
    );
  }

  private onRefreshDataGrid(): void {
    this.cargarDatosIniciales(this.consulta);
    void this.dataGrid.instance.refresh();
  }

  setearCuentaBancoNumero(data: ProveedorInterface) {
    const cuentasBanco = data.cuentasBanco;
    let cuentasBancoString: any = undefined;
    cuentasBanco?.map(cuentaBanco => {
      cuentasBancoString = (cuentasBancoString as any || '') + cuentaBanco.numero + '\n';
    });
    return cuentasBancoString || 'No Tiene';
  }

  setearCuentaBancoTipo(data: ProveedorInterface) {
    const cuentasBanco = data.cuentasBanco;
    let cuentasBancoString: any = undefined;
    cuentasBanco?.map(cuentaBanco => {
      // @ts-ignore
      cuentasBancoString = (cuentasBancoString as any || '') + TipoCuentaBancoEnum[cuentaBanco.tipoCuenta as any] + '\n';
    });
    return cuentasBancoString || 'No Tiene';
  }

  setearBanco(data: ProveedorInterface) {
    const cuentasBanco = data.cuentasBanco;
    let cuentasBancoString: any = undefined;
    cuentasBanco?.map(cuentaBanco => {
      cuentasBancoString = (cuentasBancoString as any || '') + cuentaBanco.banco?.nombre + '\n';
    });
    return cuentasBancoString || 'No Tiene';
  }

  setearTipoCuenta(data: CuentaBancoInterface) {
    // @ts-ignore
    return this.tipoCuentaBancoEnum[data.tipoCuenta];
  }

}
