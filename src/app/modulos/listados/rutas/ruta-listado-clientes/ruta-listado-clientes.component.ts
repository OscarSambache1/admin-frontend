import {Component, OnInit, ViewChild} from '@angular/core';
import {DxDataGridComponent} from "devextreme-angular";
import {ROWS, ROWS_PER_PAGE} from "../../../../constantes/table-config";
import {ClienteInterface} from "../../../configuracion-datos/interfaces/cliente.interface";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../../servicios/cargando.service";
import {ClienteRestService} from "../../../configuracion-datos/servicios/cliente-rest.service";
import {NotificacionService} from "../../../../servicios/notificacion/notificacion.service";
import {TIPOS_CUENTA_BANCO} from "../../../../constantes/tipos-identificacion";
import {TipoCuentaBancoEnum} from "../../../../enums/tipo-contacto.enums";
import {CuentaBancoInterface} from "../../../configuracion-datos/interfaces/cuenta-banco.interface";

@Component({
  selector: 'app-ruta-listado-clientes',
  templateUrl: './ruta-listado-clientes.component.html',
  styleUrls: ['./ruta-listado-clientes.component.scss']
})
export class RutaListadoClientesComponent implements OnInit {


  @ViewChild(DxDataGridComponent)
  dataGrid!: DxDataGridComponent;

  rows = ROWS;
  rowsPerPage = ROWS_PER_PAGE;
  datos: ClienteInterface[] = [];
  pageSize!: number;
  disabledTable!: boolean;
  pagination: number[] = [];
  paginationStart!: number;
  tipoCuentaBancoEnum = TipoCuentaBancoEnum;

  consulta: any = {
    relations: ['informacionTributaria', 'contactos', 'cuentasBanco', 'cuentasBanco.banco'],
    where: {
    },
    order: {
      id: 'DESC',
    },
  };

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _clienteRestService: ClienteRestService,
    private readonly _notificacionService: NotificacionService,
  ) {}

  ngOnInit(): void {
    this.pagination = ROWS_PER_PAGE;
    this.paginationStart = ROWS;
    this.cargarDatosIniciales(this.consulta);
  }

  cargarDatosIniciales(consulta: {}): void {
    this._cargandoService.habilitar();
    const cliente$ = this._clienteRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    cliente$.subscribe({
      next: (cliente) => {
        this.datos = cliente[0];
        this.pageSize = cliente[1];
        this.disabledTable = false;
        this._cargandoService.deshabilitar();
      },
      error: (error) => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
      },
    });
  }

  onToolbarPreparing(evento: any): void {
    evento.toolbarOptions.items.unshift(
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Recargar datos de la tabla',
          icon: 'refresh',
          onClick: this.onRefreshDataGrid.bind(this),
        },
      }
    );
  }

  private onRefreshDataGrid(): void {
    this.cargarDatosIniciales(this.consulta);
    void this.dataGrid.instance.refresh();
  }

  setearCuentaBancoNumero(data: ClienteInterface) {
    const cuentasBanco = data.cuentasBanco;
    let cuentasBancoString: any = undefined;
    cuentasBanco?.map(cuentaBanco => {
      cuentasBancoString = (cuentasBancoString as any || '') + cuentaBanco.numero + '\n';
    });
    return cuentasBancoString || 'No Tiene';
  }

  setearCuentaBancoTipo(data: ClienteInterface) {
    const cuentasBanco = data.cuentasBanco;
    let cuentasBancoString: any = undefined;
    cuentasBanco?.map(cuentaBanco => {
      // @ts-ignore
      cuentasBancoString = (cuentasBancoString as any || '') + TipoCuentaBancoEnum[cuentaBanco.tipoCuenta as any] + '\n';
    });
    return cuentasBancoString || 'No Tiene';
  }

  setearBanco(data: ClienteInterface) {
    const cuentasBanco = data.cuentasBanco;
    let cuentasBancoString: any = undefined;
    cuentasBanco?.map(cuentaBanco => {
      cuentasBancoString = (cuentasBancoString as any || '') + cuentaBanco.banco?.nombre + '\n';
    });
    return cuentasBancoString || 'No Tiene';
  }

  setearTipoCuenta(data: CuentaBancoInterface) {
    // @ts-ignore
    return this.tipoCuentaBancoEnum[data.tipoCuenta];
  }
}
