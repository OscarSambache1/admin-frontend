import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-ruta-menu-listados',
  templateUrl: './ruta-menu-listados.component.html',
  styleUrls: ['./ruta-menu-listados.component.scss']
})
export class RutaMenuListadosComponent implements OnInit {


  arregloItemsMenuInicio = [
    {
      nombre: 'Clientes',
      texto: 'Listados de Clientes',
      imagen: '/assets/images/cliente.png',
      ruta: 'cliente'
    },
    {
      nombre: 'Proveedores',
      texto: 'Listados de Proveedores',
      imagen: '/assets/images/proveedor.png',
      ruta: 'proveedor'
    },
  ];
  constructor(
    private readonly _router: Router
  ) { }

  ngOnInit(): void {
  }

  irARuta(rutaOpcion: string) {
    const ruta = ['/app', 'listados', rutaOpcion];
    void this._router.navigate(ruta);
  }

}
