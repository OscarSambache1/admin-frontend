import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {MenuTrabajosComponent} from "./rutas/menu-trabajos/menu-trabajos.component";

const routes: Routes = [
  {
    path: '',
    component: MenuTrabajosComponent,
    data: {
      breadcrumb: '',
    },
  },
  {
    path: 'gestion-trabajo',
    loadChildren: () =>
      import('./trabajo/trabajo.module').then((modulo) => modulo.TrabajoModule),
    data: {
      breadcrumb: '',
    },
  },
  {
    path: 'recepcion-vehiculo',
    loadChildren: () =>
      import('./recepcion-vehiculo/recepcion-vehiculo.module').then((modulo) => modulo.RecepcionVehiculoModule),
    data: {
      breadcrumb: '',
    },
  },

  {
    path: 'orden-trabajo',
    loadChildren: () =>
      import('./orden-trabajo/orden-trabajo.module').then((modulo) => modulo.OrdenTrabajoModule),
    data: {
      breadcrumb: '',
    },
  },
  {
    path: 'trabajo-realizado',
    loadChildren: () =>
      import('./trabajo-realizado/trabajo-realizado.module').then((modulo) => modulo.TrabajoRealizadoModule),
    data: {
      breadcrumb: '',
    },
  },
  {
    path: 'factura',
    loadChildren: () =>
      import('./factura/factura.module').then((modulo) => modulo.FacturaModule),
    data: {
      breadcrumb: '',
    },
  },
  {
    path: 'trabajos-cliente',
    loadChildren: () =>
      import('./trabajos-cliente/trabajos-cliente.module').then((modulo) => modulo.TrabajosClienteModule),
    data: {
      breadcrumb: '',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrabajosRoutingModule { }
