import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrdenTrabajoRoutingModule } from './orden-trabajo-routing.module';
import { RutaOrdenTrabajoComponent } from './rutas/ruta-orden-trabajo/ruta-orden-trabajo.component';
import {IMPORTS_MODULO} from "../../imports/imports-modulo";
import {ComponentesModule} from "../../../componentes/componentes.module";
import { RutaFormularioOrdenTrabajoComponent } from './rutas/ruta-formulario-orden-trabajo/ruta-formulario-orden-trabajo.component';


@NgModule({
  declarations: [
    RutaOrdenTrabajoComponent,
    RutaFormularioOrdenTrabajoComponent
  ],
  imports: [
    CommonModule,
    OrdenTrabajoRoutingModule,
    ...IMPORTS_MODULO,
    ComponentesModule,
  ]
})
export class OrdenTrabajoModule { }
