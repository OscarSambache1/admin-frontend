import { Component, OnInit } from '@angular/core';
import {NotificacionService} from "../../../../../servicios/notificacion/notificacion.service";
import {CargandoService} from "../../../../../servicios/cargando.service";
import {ActivatedRoute, Router} from "@angular/router";
import {mergeMap} from "rxjs/operators";
import {OrdenTrabajoRestService} from "../../../servicios/orden-trabajo-rest.service";
import {OrdenTrabajoInterface} from "../../../interfaces/orden-trabajo.interface";
import {TipoGasto} from "../../../../../enums/tipo-contacto.enums";

@Component({
  selector: 'app-ruta-formulario-orden-trabajo',
  templateUrl: './ruta-formulario-orden-trabajo.component.html',
  styleUrls: ['./ruta-formulario-orden-trabajo.component.scss']
})
export class RutaFormularioOrdenTrabajoComponent implements OnInit {
  estaCargando = false;
  ordenTrabajo?: OrdenTrabajoInterface;
  constructor(
    private readonly _notificacionService: NotificacionService,
    private readonly _cargandoService: CargandoService,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _router: Router,
    private readonly _ordenTrabajoRestService: OrdenTrabajoRestService,
  ) { }

  ngOnInit(): void {
    this.estaCargando = true;
    this._cargandoService.habilitar();
    this._activatedRoute.params
      .pipe(
        mergeMap(params => {
          const idOrdenTrabajo = params.idOrdenTrabajo;
          const consulta: any = {
            relations: [
              'vehiculo',
              'vehiculo.marca',
              'vehiculo.cliente',
              'vehiculo.cliente.informacionTributaria',
              'trabajo',
              'trabajo.vehiculo',
              'trabajo.vehiculo.marca',
              'trabajo.vehiculo.cliente',
              'trabajo.vehiculo.cliente.informacionTributaria',
              'trabajo.recepciones',
              'ordenTrabajoDetalles',
              'ordenTrabajoDetalles.articulo',
            ],
            where: {
              id: idOrdenTrabajo,
            },
            order: {
              id: 'DESC',
            },
          };
          return this._ordenTrabajoRestService.findAll(
            `busqueda=${JSON.stringify(consulta)}`
          );
        })
      )
      .subscribe(respuesta => {
        if (respuesta && respuesta[0] && respuesta[0][0]) {
          this.ordenTrabajo = respuesta[0][0];
          if (this.ordenTrabajo) {
            this.ordenTrabajo?.ordenTrabajoDetalles?.map(ordenTrabajoDetalle => {
              ordenTrabajoDetalle.tipo = TipoGasto.Repuesto;
            });
          }
        }
        this._cargandoService.deshabilitar();
        this.estaCargando = false;
      }, error => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
        this.estaCargando = false;
      });
  }

  cancelar() {
    const ruta = ['/app', 'trabajos', 'orden-trabajo',];
    void this._router.navigate(ruta);
  }
}
