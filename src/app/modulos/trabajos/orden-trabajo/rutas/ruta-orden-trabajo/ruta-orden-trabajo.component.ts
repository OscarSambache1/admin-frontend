import {Component, OnInit, ViewChild} from '@angular/core';
import {DxDataGridComponent} from "devextreme-angular";
import {ROWS, ROWS_PER_PAGE} from "../../../../../constantes/table-config";
import {EstadoTrabajo, EstadoTrabajoEnum} from "../../../../../enums/tipo-contacto.enums";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../../../servicios/cargando.service";
import {NotificacionService} from "../../../../../servicios/notificacion/notificacion.service";
import {OrdenTrabajoInterface} from "../../../interfaces/orden-trabajo.interface";
import {OrdenTrabajoRestService} from "../../../servicios/orden-trabajo-rest.service";
import {ImprimitService} from "../../../../../servicios/imprimit.service";

@Component({
  selector: 'app-ruta-orden-trabajo',
  templateUrl: './ruta-orden-trabajo.component.html',
  styleUrls: ['./ruta-orden-trabajo.component.scss']
})
export class RutaOrdenTrabajoComponent implements OnInit {
  @ViewChild(DxDataGridComponent)
  dataGrid!: DxDataGridComponent;

  rows = ROWS;
  rowsPerPage = ROWS_PER_PAGE;
  datos: OrdenTrabajoInterface[] = [];
  pageSize!: number;
  disabledTable!: boolean;
  pagination: number[] = [];
  paginationStart!: number;
  consulta: any = {
    relations: [
      'vehiculo',
      'vehiculo.marca',
      'vehiculo.cliente',
      'vehiculo.cliente.informacionTributaria',
      'trabajo',
      'ordenTrabajoDetalles',
      'ordenTrabajoDetalles.articulo'
    ],
    where: {},
    order: {
      id: 'DESC',
    },
  };
  estadoAccionEnum: any = EstadoTrabajoEnum;
  estadosAccion = [
    {
      value: EstadoTrabajo.Guardado,
      label: EstadoTrabajoEnum.G
    },
    {
      value: EstadoTrabajo.NoGuardado,
      label: EstadoTrabajoEnum.NG,
    },
    {
      value: EstadoTrabajo.Finalizado,
      label: EstadoTrabajoEnum.F
    },
  ];
  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _ordenTrabajoRestService: OrdenTrabajoRestService,
    private readonly _notificacionService: NotificacionService,
    private readonly _imprimirService: ImprimitService,
  ) {}

  ngOnInit(): void {
    this.pagination = ROWS_PER_PAGE;
    this.paginationStart = ROWS;
    this.cargarDatosIniciales(this.consulta);
  }

  cargarDatosIniciales(consulta: {}): void {
    this._cargandoService.habilitar();
    const ordenTrabajo$ = this._ordenTrabajoRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    ordenTrabajo$.subscribe({
      next: (ordenTrabajo) => {
        this.datos = ordenTrabajo[0];
        this.pageSize = ordenTrabajo[1];
        this.disabledTable = false;
        this._cargandoService.deshabilitar();
      },
      error: (error) => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
      },
    });
  }

  onToolbarPreparing(evento: any): void {
    evento.toolbarOptions.items.unshift(
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Recargar datos de la tabla',
          icon: 'refresh',
          onClick: this.onRefreshDataGrid.bind(this),
        },
      }
    );
  }

  private onRefreshDataGrid(): void {
    this.cargarDatosIniciales(this.consulta);
    void this.dataGrid.instance.refresh();
  }

  add(ordenTrabajo?: OrdenTrabajoInterface) {
    const ruta = ['/app', 'trabajos', 'orden-trabajo', 'formulario', ordenTrabajo?.id];
    void this._router.navigate(ruta);
  }

  setearCliente(ordenTrabajo: OrdenTrabajoInterface) {
    return ordenTrabajo?.vehiculo?.cliente?.informacionTributaria?.nombreComercial;
  }

  async imprimir(ordenTrabajo: any) {
    const pdfDefinition: any = await this._imprimirService.setearObjetoOrdenTRabajo(ordenTrabajo);
    await this._imprimirService.imprimir(pdfDefinition);
  }
}
