import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaOrdenTrabajoComponent} from "./rutas/ruta-orden-trabajo/ruta-orden-trabajo.component";
import {RutaFormularioOrdenTrabajoComponent} from "./rutas/ruta-formulario-orden-trabajo/ruta-formulario-orden-trabajo.component";

const routes: Routes = [
  {
    path: '',
    component: RutaOrdenTrabajoComponent,
    data: {
      breadcrumb: 'Orden de Trabajo',
    },
  },
  {
    path: 'formulario/:idOrdenTrabajo',
    component: RutaFormularioOrdenTrabajoComponent,
    data: {
      breadcrumb: 'Ver Información',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdenTrabajoRoutingModule { }
