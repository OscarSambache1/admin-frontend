import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FacturaRoutingModule } from './factura-routing.module';
import { RutaFacturaComponent } from './rutas/ruta-factura/ruta-factura.component';
import {IMPORTS_MODULO} from "../../imports/imports-modulo";
import {ComponentesModule} from "../../../componentes/componentes.module";
import { RutaFormularioFacturaComponent } from './rutas/ruta-formulario-factura/ruta-formulario-factura.component';


@NgModule({
  declarations: [
    RutaFacturaComponent,
    RutaFormularioFacturaComponent
  ],
  imports: [
    CommonModule,
    FacturaRoutingModule,
    ...IMPORTS_MODULO,
    ComponentesModule,
  ]
})
export class FacturaModule { }
