import { Component, OnInit } from '@angular/core';
import {NotificacionService} from "../../../../../servicios/notificacion/notificacion.service";
import {CargandoService} from "../../../../../servicios/cargando.service";
import {ActivatedRoute, Router} from "@angular/router";
import {mergeMap} from "rxjs/operators";
import {TipoGasto} from "../../../../../enums/tipo-contacto.enums";
import {FacturaCabeceraInterface} from "../../../../gastos/interfaces/factura-cabecera.interface";
import {FacturaCabeceraRestService} from "../../../../gastos/servicios/factura-cabecera-rest.service";

@Component({
  selector: 'app-ruta-formulario-factura',
  templateUrl: './ruta-formulario-factura.component.html',
  styleUrls: ['./ruta-formulario-factura.component.scss']
})
export class RutaFormularioFacturaComponent implements OnInit {

  estaCargando = false;
  factura?: FacturaCabeceraInterface;
  constructor(
    private readonly _notificacionService: NotificacionService,
    private readonly _cargandoService: CargandoService,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _router: Router,
    private readonly _facturaCabeceraRestService: FacturaCabeceraRestService,
  ) { }

  ngOnInit(): void {
    this.estaCargando = true;
    this._cargandoService.habilitar();
    this._activatedRoute.params
      .pipe(
        mergeMap(params => {
          const idFactura = params.idFactura;
          const consulta: any = {
            relations: [
              'vehiculo',
              'vehiculo.marca',
              'vehiculo.cliente',
              'vehiculo.cliente.informacionTributaria',
              'trabajo',
              'trabajo.vehiculo',
              'trabajo.vehiculo.marca',
              'trabajo.vehiculo.cliente',
              'trabajo.vehiculo.cliente.informacionTributaria',
              'trabajo.recepciones',
              'facturaDetalles',
              'facturaDetalles.articulo',
              'cliente',
              'cliente.informacionTributaria',
              'abonosFactura',
              'abonosFactura.formaPago',
            ],
            where: {
              id: idFactura,
            },
            order: {
              id: 'DESC',
            },
          };
          return this._facturaCabeceraRestService.findAll(
            `busqueda=${JSON.stringify(consulta)}`
          );
        })
      )
      .subscribe(respuesta => {
        if (respuesta && respuesta[0] && respuesta[0][0]) {
          this.factura = respuesta[0][0];
          if (this.factura) {
            this.factura?.facturaDetalles?.map(facturaDetalle => {
              facturaDetalle.tipo = TipoGasto.Repuesto;
            });
          }
        }
        this._cargandoService.deshabilitar();
        this.estaCargando = false;
      }, error => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
        this.estaCargando = false;
      });
  }

  cancelar() {
    const ruta = ['/app', 'trabajos', 'factura',];
    void this._router.navigate(ruta);
  }
}
