import {Component, OnInit, ViewChild} from '@angular/core';
import {DxDataGridComponent} from "devextreme-angular";
import {ROWS, ROWS_PER_PAGE} from "../../../../../constantes/table-config";
import {EstadoTrabajo, EstadoTrabajoEnum} from "../../../../../enums/tipo-contacto.enums";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../../../servicios/cargando.service";
import {NotificacionService} from "../../../../../servicios/notificacion/notificacion.service";
import {FacturaCabeceraInterface} from "../../../../gastos/interfaces/factura-cabecera.interface";
import {FacturaCabeceraRestService} from "../../../../gastos/servicios/factura-cabecera-rest.service";
import {ImprimitService} from "../../../../../servicios/imprimit.service";

@Component({
  selector: 'app-ruta-factura',
  templateUrl: './ruta-factura.component.html',
  styleUrls: ['./ruta-factura.component.scss']
})
export class RutaFacturaComponent implements OnInit {
  @ViewChild(DxDataGridComponent)
  dataGrid!: DxDataGridComponent;

  rows = ROWS;
  rowsPerPage = ROWS_PER_PAGE;
  datos: FacturaCabeceraInterface[] = [];
  pageSize!: number;
  disabledTable!: boolean;
  pagination: number[] = [];
  paginationStart!: number;
  consulta: any = {
    relations: [
      'vehiculo',
      'vehiculo.marca',
      'vehiculo.cliente',
      'vehiculo.cliente.informacionTributaria',
      'trabajo',
      'facturaDetalles',
      'facturaDetalles.articulo',
      'cliente',
      'cliente.informacionTributaria',
      'abonosFactura',
      'abonosFactura.formaPago',
    ],
    where: {
      tipoFactura: 'V'
    },
    order: {
      id: 'DESC',
    },
  };
  estadoAccionEnum: any = EstadoTrabajoEnum;
  estadosAccion = [
    {
      value: EstadoTrabajo.Guardado,
      label: EstadoTrabajoEnum.G
    },
    {
      value: EstadoTrabajo.NoGuardado,
      label: EstadoTrabajoEnum.NG,
    },
    {
      value: EstadoTrabajo.Finalizado,
      label: EstadoTrabajoEnum.F
    },
  ];
  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _facturaCabeceraRestService: FacturaCabeceraRestService,
    private readonly _notificacionService: NotificacionService,
    private readonly _imprimirService: ImprimitService,
  ) {}

  ngOnInit(): void {
    this.pagination = ROWS_PER_PAGE;
    this.paginationStart = ROWS;
    this.cargarDatosIniciales(this.consulta);
  }

  cargarDatosIniciales(consulta: {}): void {
    this._cargandoService.habilitar();
    const factura$ = this._facturaCabeceraRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    factura$.subscribe({
      next: (factura) => {
        this.datos = factura[0];
        this.pageSize = factura[1];
        this.disabledTable = false;
        this._cargandoService.deshabilitar();
      },
      error: (error) => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
      },
    });
  }

  onToolbarPreparing(evento: any): void {
    evento.toolbarOptions.items.unshift(
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Recargar datos de la tabla',
          icon: 'refresh',
          onClick: this.onRefreshDataGrid.bind(this),
        },
      }
    );
  }

  private onRefreshDataGrid(): void {
    this.cargarDatosIniciales(this.consulta);
    void this.dataGrid.instance.refresh();
  }

  add(factura?: FacturaCabeceraInterface) {
    const ruta = ['/app', 'trabajos', 'factura', 'formulario', factura?.id];
    void this._router.navigate(ruta);
  }

  setearCliente(factura: FacturaCabeceraInterface) {
    return factura?.vehiculo?.cliente?.informacionTributaria?.nombreComercial;
  }

  async imprimir(factura: any) {
    const pdfDefinition: any = await this._imprimirService.setearObjetoFactura(factura);
    await this._imprimirService.imprimir(pdfDefinition);
  }
}
