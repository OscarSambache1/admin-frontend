import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaFacturaComponent} from "./rutas/ruta-factura/ruta-factura.component";
import {RutaFormularioFacturaComponent} from "./rutas/ruta-formulario-factura/ruta-formulario-factura.component";

const routes: Routes = [
  {
    path: '',
    component: RutaFacturaComponent,
    data: {
      breadcrumb: 'Facturas',
    },
  },
  {
    path: 'formulario/:idFactura',
    component: RutaFormularioFacturaComponent,
    data: {
      breadcrumb: 'Ver Información',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FacturaRoutingModule { }
