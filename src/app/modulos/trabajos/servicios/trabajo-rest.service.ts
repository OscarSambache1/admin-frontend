import { Injectable } from '@angular/core';
import {ServicioPrincipalService} from "../../../servicios/servicio-principal.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {TrabajoInterface} from "../interfaces/trabajo.interface";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TrabajoRestService extends ServicioPrincipalService<TrabajoInterface>  {

  constructor(protected readonly _httpClient: HttpClient) {
    super(_httpClient);
    this.url = environment.api.url;
    this.puerto = environment.api.puerto;
    this.segmento = 'trabajo';
  }

  eliminarTrabajo(
    idTrabajo: number,
  ): Observable<any> {
    return this._httpClient.delete(
      `${this.url}:${this.puerto}/${this.segmento}/eliminar-trabajo/${idTrabajo}`,
      {
        headers: new HttpHeaders({
          timeout: `${360000000}`,
        }),
      },
    );
  }

  listarTrabajos(): Observable<[TrabajoInterface[], number]> {
    return this._httpClient.get<[TrabajoInterface[], number]>(
      `${this.url}:${this.puerto}/${this.segmento}/listar-trabajos`
    );
  }

  obtenerTrabajo(id: number): Observable<TrabajoInterface> {
    return this._httpClient.get<TrabajoInterface>(
      `${this.url}:${this.puerto}/${this.segmento}/obtener-trabajo/${id}`
    );
  }
}
