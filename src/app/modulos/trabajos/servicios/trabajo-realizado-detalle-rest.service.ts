import { Injectable } from '@angular/core';
import {ServicioPrincipalService} from "../../../servicios/servicio-principal.service";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {OrdenTrabajoDetalleInterface} from "../interfaces/orden-trabajo-detalle.interface";
import {TrabajoRealizadoDetalleInterface} from "../interfaces/trabajo-realizado-detalle.interface";

@Injectable({
  providedIn: 'root'
})
export class TrabajoRealizadoDetalleRestService extends ServicioPrincipalService<TrabajoRealizadoDetalleInterface>  {

  constructor(protected readonly _httpClient: HttpClient) {
    super(_httpClient);
    this.url = environment.api.url;
    this.puerto = environment.api.puerto;
    this.segmento = 'trabajo-realizado-detalle';
  }
}
