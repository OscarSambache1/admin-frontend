import { Injectable } from '@angular/core';
import {ServicioPrincipalService} from "../../../servicios/servicio-principal.service";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {RecepcionInterface} from "../interfaces/recepcion.interface";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class RecepcionRestService extends ServicioPrincipalService<RecepcionInterface>  {

  constructor(protected readonly _httpClient: HttpClient) {
    super(_httpClient);
    this.url = environment.api.url;
    this.puerto = environment.api.puerto;
    this.segmento = 'recepcion';
  }

  crearEditarTrabajoRecepcion(datos: any): Observable<any> {
    return this._httpClient.post(
      `${this.url}:${this.puerto}/${this.segmento}/crear-editar-trabajo-recepcion`,
      datos,
    );
  }
}
