import { Injectable } from '@angular/core';
import {ServicioPrincipalService} from "../../../servicios/servicio-principal.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {RecepcionInterface} from "../interfaces/recepcion.interface";
import {Observable} from "rxjs";
import {OrdenTrabajoInterface} from "../interfaces/orden-trabajo.interface";
import {TrabajoRealizadoInterface} from "../interfaces/trabajo-realizado.interface";

@Injectable({
  providedIn: 'root'
})
export class TrabajoRealizadoRestService extends ServicioPrincipalService<TrabajoRealizadoInterface>  {

  constructor(protected readonly _httpClient: HttpClient) {
    super(_httpClient);
    this.url = environment.api.url;
    this.puerto = environment.api.puerto;
    this.segmento = 'trabajo-realizado';
  }

  crearEditarTrabajoRealizado(datos: any): Observable<any> {
    return this._httpClient.post(
      `${this.url}:${this.puerto}/${this.segmento}/crear-editar-trabajo-realizado`,
      datos,
    );
  }

  eliminarTrabajo(
    idTrabajo: number,
  ): Observable<any> {
    return this._httpClient.delete(
      `${this.url}:${this.puerto}/${this.segmento}/eliminar-trabajo-realizado/${idTrabajo}`,
      {
        headers: new HttpHeaders({
          timeout: `${360000000}`,
        }),
      },
    );
  }
}
