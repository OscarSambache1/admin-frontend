import { Injectable } from '@angular/core';
import {ServicioPrincipalService} from "../../../servicios/servicio-principal.service";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {OrdenTrabajoDetalleInterface} from "../interfaces/orden-trabajo-detalle.interface";

@Injectable({
  providedIn: 'root'
})
export class OrdenTrabajoDetalleRestService extends ServicioPrincipalService<OrdenTrabajoDetalleInterface>  {

  constructor(protected readonly _httpClient: HttpClient) {
    super(_httpClient);
    this.url = environment.api.url;
    this.puerto = environment.api.puerto;
    this.segmento = 'orden-trabajo-detalle';
  }
}
