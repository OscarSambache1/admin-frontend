import {Component, OnInit, ViewChild} from '@angular/core';
import {TrabajoClienteInterface, TrabajoInterface} from "../../interfaces/trabajo.interface";
import {ActivatedRoute, Router} from "@angular/router";
import {CargandoService} from "../../../../servicios/cargando.service";
import {TrabajoRestService} from "../../servicios/trabajo-rest.service";
import {NotificacionService} from "../../../../servicios/notificacion/notificacion.service";
import {FacturaCabeceraRestService} from "../../../gastos/servicios/factura-cabecera-rest.service";
import {ClienteInterface} from "../../../configuracion-datos/interfaces/cliente.interface";
import {setearLabelInformacionTributaria} from "../../../../componentes/formularios/formulario-trabajo/formulario-trabajo.component";
import {ClienteRestService} from "../../../configuracion-datos/servicios/cliente-rest.service";
import {MatAccordion} from "@angular/material/expansion";
import {EstadoTrabajo, TipoGasto} from "../../../../enums/tipo-contacto.enums";
import * as moment from 'moment-timezone';
import {
  FORMATO_RANGO_FECHA_FILTROS,
  setearStringFechasActuales, setearStringFechasConsulta
} from "../../../../constantes/setear-string-fechas-actuales";

@Component({
  selector: 'app-trabajos-cliente',
  templateUrl: './trabajos-cliente.component.html',
  styleUrls: ['./trabajos-cliente.component.scss']
})
export class TrabajosClienteComponent implements OnInit {
  @ViewChild(MatAccordion) accordion!: MatAccordion;

  datos: TrabajoClienteInterface[] = [];
  fechaInicio: any;
  fechaFin: any;
  clientes: ClienteInterface[] = [];
  cliente!: any;
  tipoGasto = TipoGasto.Repuesto;
  ordenFechas = 1;
  estadoAccion = EstadoTrabajo.Finalizado;
  todoAbierto = true;
  fechaInicioFormato!: any;
  fechaFinFormato!: any;
  displayedColumns: string[] = ['item', 'cost'];
  transactions: any[] = [
    {item: 'Beach ball', cost: 4},
    {item: 'Towel', cost: 5},
    {item: 'Frisbee', cost: 2},
    {item: 'Sunscreen', cost: 4},
    {item: 'Cooler', cost: 25},
    {item: 'Swim suit', cost: 15},
  ];

  relations = [
    'vehiculo',
    'vehiculo.marca',
    'vehiculo.cliente',
    'vehiculo.cliente.informacionTributaria',
    'recepciones',
    'ordenesTrabajo',
    'ordenesTrabajo.ordenTrabajoDetalles',
    'ordenesTrabajo.ordenTrabajoDetalles.articulo',
    'trabajosRealizados',
    'trabajosRealizados.trabajoRealizadoDetalles',
    'trabajosRealizados.trabajoRealizadoDetalles.articulo',
    'facturas',
    'facturas.trabajo',
    'facturas.facturaDetalles',
    'facturas.facturaDetalles.articulo',
    'facturas.cliente',
    'facturas.cliente.informacionTributaria',
    'facturas.abonosFactura',
  ];

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _cargandoService: CargandoService,
    private readonly _trabajoRestService: TrabajoRestService,
    private readonly _notificacionService: NotificacionService,
    private readonly _facturaCabeceraRestService: FacturaCabeceraRestService,
    private readonly _clienteRestService: ClienteRestService,
  ) {
  }

  ngOnInit(): void {
  }

  cargarDatosIniciales(): void {
    this.datos = [];
    if (this.cliente) {
      let consulta: any = {
        where: {},
        order: {
          fechaCreacion: this.ordenFechas === 1 ? 'ASC' : 'DESC',
        },
        relations: this.relations,
      };
      consulta.where.vehiculo = {
        cliente: this.cliente.id,
      }
      this._cargandoService.habilitar();
      const gasto$ = this._trabajoRestService.findAll(
        `busqueda=${JSON.stringify(consulta)}`
      );
      gasto$.subscribe({
        next: (gasto: any) => {
          this.datos = gasto[0]
            .filter((trabajoAux2: any) => {
              if (this.fechaInicioFormato && this.fechaFinFormato) {
                return moment(trabajoAux2.fechaCreacion).isBetween(this.fechaInicioFormato, this.fechaFinFormato);
              } else {
                if (this.fechaInicioFormato) {
                  return moment(trabajoAux2.fechaCreacion).isSameOrAfter(this.fechaInicioFormato);
                }
                if (this.fechaFinFormato) {
                  return moment(trabajoAux2.fechaCreacion).isSameOrAfter(this.fechaFinFormato);
                }
                return true;
              }
            })
            .filter((trabajo: any) => {
            if (trabajo.trabajosRealizados && trabajo.trabajosRealizados.length) {
              const trabajoRealizado = trabajo.trabajosRealizados[0];
              return !trabajoRealizado.esProforma;
            } else {
              return true;
            }
          })
            .map((trabajoFil: any) => {
              trabajoFil.kmEntrada = trabajoFil.recepciones[0]?.kmEntrada ?? 'NO TIENE';
              trabajoFil.fechaRecepcion = trabajoFil.recepciones[0]?.fechaRecepcion ?? 'NO TIENE';
              trabajoFil.placa = trabajoFil.vehiculo?.placa ?? 'NO TIENE';
              const trabajoRealizado = trabajoFil.trabajosRealizados[0];
              const ordenTrabajo = trabajoFil.ordenesTrabajo[0];
              const recepcion = trabajoFil.recepciones[0];
              trabajoFil.observacionRecepcion = recepcion?.observacionesCliente ?? '';
              trabajoFil.observacionOrden = ordenTrabajo?.observacion ?? '';
              trabajoFil.observacionTrabajoRealizado = trabajoRealizado?.observacion ?? '';
              const detalles = trabajoRealizado ? trabajoRealizado?.trabajoRealizadoDetalles : ordenTrabajo?.ordenTrabajoDetalles;
              trabajoFil.detalles = detalles?.map((detalle: any) => {
                return {
                  articulo: detalle?.articulo,
                  descripcion: detalle?.descripcion,
                  cantidad: detalle?.cantidad,
                  tipo: detalle?.tipo,
                  precioUnitario: detalle.precioUnitario ?? 0,
                  ivaPorcentaje: detalle.ivaPorcentaje ?? 0,
                  ivaDescripcion: detalle.ivaDescripcion,
                  descuento: detalle.descuento ?? 0,
                  valorDescuento: detalle.valorDescuento ?? 0,
                  valorIva: detalle.valorIva ?? 0,
                  valorIce: detalle.valorIce ?? 0,
                  total: detalle.total ?? 0,
                }
              });
              return trabajoFil;
            });
          this._cargandoService.deshabilitar();
        },
        error: (error) => {
          console.error({
            mensage: 'Error al traer registros',
            error: error,
          });
          this._notificacionService.modalInfo(
            'error',
            'Error',
            'Error al traer registros',
          );
          this._cargandoService.deshabilitar();
        },
      });
    }

  }

  buscarClientes(evento: any) {
    this._cargandoService.habilitar();
    let consulta = {
      where: {
        estado: 1
      },
      relations: ['informacionTributaria']
    };

    const cliente$ = this._clienteRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    cliente$.subscribe({
      next: (respuesta: [any[], number]) => {
        this.clientes = respuesta[0].filter(cliente => {
          const informacionTributaria = cliente.informacionTributaria;
          cliente.label = setearLabelInformacionTributaria(informacionTributaria);
          const buscarRazonSocial = (informacionTributaria.razonSocial || '').toUpperCase().search(evento.query.toUpperCase()) !== -1;
          const buscarNombreComercial = (informacionTributaria.nombreComercial || '').toUpperCase().search(evento.query.toUpperCase()) !== -1;
          const buscarIdentificacion = (informacionTributaria.identificacion || '').toUpperCase().search(evento.query.toUpperCase()) !== -1;
          return buscarRazonSocial || buscarNombreComercial || buscarIdentificacion;
        });
        this._cargandoService.deshabilitar();
      },
      error: (err) => {
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        console.error({
          mensage: 'Error en autocomplete',
          error: err,
        });
        this._cargandoService.deshabilitar();
      },
    });
  }

  seleccionoClienteAutoncomplete(evento: any) {
    if (evento.id) {
    } else {
      this.cliente = null;
    }
    this.cargarDatosIniciales();
  }

  setearTotal(detalles: any[] | undefined): number {
    return (detalles ?? []).map((t: any) => t.total).reduce((acc, value) => acc + value, 0) ?? 0;
  }

  abrirCerrarTodo() {
    if (this.todoAbierto) {
      this.accordion.openAll()
    } else {
      this.accordion.closeAll()
    }
    this.todoAbierto = !this.todoAbierto;
  }

  cambioFechaInicio(event: Date | number | string) {
    if (event) {
      this.fechaInicioFormato = moment(event).format(FORMATO_RANGO_FECHA_FILTROS);
    } else {
      this.fechaInicioFormato = undefined;
    }
    this.cargarDatosIniciales();
  }

  cambioFechaFin(event: Date | number | string) {
    if (event) {
      this.fechaFinFormato = moment(event).format(FORMATO_RANGO_FECHA_FILTROS);
    } else {
      this.fechaFinFormato = undefined;
    }
    this.cargarDatosIniciales();
  }

  ordenar() {
    if (this.ordenFechas === 1) {
      this.ordenFechas = -1;
    } else {
      this.ordenFechas = 1;
    }
    this.cargarDatosIniciales();
  }
}
