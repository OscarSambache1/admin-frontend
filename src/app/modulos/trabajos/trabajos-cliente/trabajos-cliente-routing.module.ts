import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {TrabajosClienteComponent} from "./ruta/trabajos-cliente.component";

const routes: Routes = [
  {
    path: '',
    component: TrabajosClienteComponent,
    data: {
      breadcrumb: 'Trabajos Clientes',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrabajosClienteRoutingModule { }
