import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TrabajosClienteRoutingModule } from './trabajos-cliente-routing.module';
import { TrabajosClienteComponent } from './ruta/trabajos-cliente.component';
import {IMPORTS_MODULO} from "../../imports/imports-modulo";
import {DataViewModule} from "primeng/dataview";
import {DxDateBoxModule} from "devextreme-angular";
import {CardModule} from "primeng/card";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatIconModule} from "@angular/material/icon";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatInputModule} from "@angular/material/input";
import {MatTableModule} from "@angular/material/table";
import {ComponentesModule} from "../../../componentes/componentes.module";


@NgModule({
  declarations: [
    TrabajosClienteComponent
  ],
  imports: [
    CommonModule,
    TrabajosClienteRoutingModule,
    ...IMPORTS_MODULO,
    DxDateBoxModule,
    CardModule,
    MatExpansionModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    ComponentesModule,
  ]
})
export class TrabajosClienteModule { }
