import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TrabajosRoutingModule } from './trabajos-routing.module';
import {MenuTrabajosComponent} from "./rutas/menu-trabajos/menu-trabajos.component";


@NgModule({
  declarations: [
    MenuTrabajosComponent,
  ],
  imports: [
    CommonModule,
    TrabajosRoutingModule
  ]
})
export class TrabajosModule { }
