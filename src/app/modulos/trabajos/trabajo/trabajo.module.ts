import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TrabajoRoutingModule } from './trabajo-routing.module';
import { RutaTrabajoComponent } from './ruta/ruta-trabajo/ruta-trabajo.component';
import {IMPORTS_MODULO} from "../../imports/imports-modulo";
import {ComponentesModule} from "../../../componentes/componentes.module";
import { RutaCrearEditarTrabajoComponent } from './ruta/ruta-crear-editar-trabajo/ruta-crear-editar-trabajo.component';
import {MatTabsModule} from "@angular/material/tabs";
import {MatIconModule} from "@angular/material/icon";


@NgModule({
  declarations: [
    RutaTrabajoComponent,
    RutaCrearEditarTrabajoComponent,
  ],
  imports: [
    CommonModule,
    TrabajoRoutingModule,
    ...IMPORTS_MODULO,
    ComponentesModule,
    MatTabsModule,
    MatIconModule,
  ]
})
export class TrabajoModule { }
