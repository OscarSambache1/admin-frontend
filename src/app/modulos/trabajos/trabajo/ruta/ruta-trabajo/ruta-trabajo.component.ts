import {Component, OnInit, ViewChild} from '@angular/core';
import {DxDataGridComponent} from "devextreme-angular";
import {ROWS, ROWS_PER_PAGE} from "../../../../../constantes/table-config";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../../../servicios/cargando.service";
import {NotificacionService} from "../../../../../servicios/notificacion/notificacion.service";
import {TrabajoInterface} from "../../../interfaces/trabajo.interface";
import {TrabajoRestService} from "../../../servicios/trabajo-rest.service";
import {EstadoAbono, EstadoStockEnum, EstadoTrabajo, EstadoTrabajoEnum} from "../../../../../enums/tipo-contacto.enums";
import {ModalPagosAbonosComponent} from "../../../../../componentes/modales/modal-pagos-abonos/modal-pagos-abonos.component";
import {FacturaCabeceraRestService} from "../../../../gastos/servicios/factura-cabecera-rest.service";
import {ActualizarPagosService} from "../../../../../servicios/actualizar-pagos.service";

@Component({
  selector: 'app-ruta-trabajo',
  templateUrl: './ruta-trabajo.component.html',
  styleUrls: ['./ruta-trabajo.component.scss']
})
export class RutaTrabajoComponent implements OnInit {


  @ViewChild(DxDataGridComponent)
  dataGrid!: DxDataGridComponent;

  rows = ROWS;
  rowsPerPage = ROWS_PER_PAGE;
  datos: TrabajoInterface[] = [];
  pageSize!: number;
  disabledTable!: boolean;
  pagination: number[] = [];
  paginationStart!: number;
  consulta: any = {
    relations: [
      'vehiculo',
      'vehiculo.marca',
      'vehiculo.cliente',
      'vehiculo.cliente.informacionTributaria',
      'recepciones',
      'ordenesTrabajo',
      'ordenesTrabajo.ordenTrabajoDetalles',
      'ordenesTrabajo.ordenTrabajoDetalles.articulo',
      'trabajosRealizados',
      'trabajosRealizados.trabajoRealizadoDetalles',
      'trabajosRealizados.trabajoRealizadoDetalles.articulo',
      'facturas',
      'facturas.trabajo',
      'facturas.facturaDetalles',
      'facturas.facturaDetalles.articulo',
      'facturas.cliente',
      'facturas.cliente.informacionTributaria',
      'facturas.abonosFactura',
    ],
    where: {},
    order: {
      id: 'DESC',
    },
  };
  estadoAccionEnum: any = EstadoTrabajoEnum;

  estadosAccion = [
    {
      value: EstadoTrabajo.Guardado,
      label: EstadoTrabajoEnum.G
    },
    {
      value: EstadoTrabajo.NoGuardado,
      label: EstadoTrabajoEnum.NG,
    },
    {
      value: EstadoTrabajo.Finalizado,
      label: EstadoTrabajoEnum.F
    },
  ];
  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _trabajoRestService: TrabajoRestService,
    private readonly _notificacionService: NotificacionService,
    private readonly _facturaCabeceraRestService: FacturaCabeceraRestService,
    private readonly _actualizarPagosService: ActualizarPagosService,
  ) {}

  ngOnInit(): void {
    this.pagination = ROWS_PER_PAGE;
    this.paginationStart = ROWS;
    this.cargarDatosIniciales(this.consulta);
  }

  cargarDatosIniciales(consulta: {}): void {
    this._cargandoService.habilitar();
    const gasto$ = this._trabajoRestService.listarTrabajos();
    gasto$.subscribe({
      next: (gasto) => {
        this.datos = gasto[0].filter(trabajo => {
          if (trabajo.trabajosRealizados && trabajo.trabajosRealizados.length) {
            const trabajoRealizado = trabajo.trabajosRealizados[0];
            return !trabajoRealizado.esProforma;
          } else {
            return true;
          }
        });
        this.pageSize = gasto[1];
        this.disabledTable = false;
        this._cargandoService.deshabilitar();
      },
      error: (error) => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
      },
    });
  }

  onToolbarPreparing(evento: any): void {
    evento.toolbarOptions.items.unshift(
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Agregar nuevo trabajo',
          onClick: this.add.bind(this, undefined, 0),
          icon: 'plus',
        },
      },
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Recargar datos de la tabla',
          icon: 'refresh',
          onClick: this.onRefreshDataGrid.bind(this),
        },
      }
    );
  }

  private onRefreshDataGrid(): void {
    this.cargarDatosIniciales(this.consulta);
    void this.dataGrid.instance.refresh();
  }

  add(trabajo?: TrabajoInterface, soloVer?: 0| 1) {
    const ruta = ['/app', 'trabajos', 'gestion-trabajo', 'crear-editar-trabajo', (trabajo?.id || -1), soloVer];
    void this._router.navigate(ruta);
  }

  setearCliente(trabajo: TrabajoInterface) {
    return trabajo.vehiculo?.cliente?.informacionTributaria?.nombreComercial;
  }

  setearEstadoRecepcion(data: TrabajoInterface) {
    const recepcion = data.recepciones ? data.recepciones[0] : null;
    if (recepcion) {
      return recepcion.estadoAccion;
    } else {
      return EstadoTrabajo.NoGuardado;
    }
  }

  setearEstadoOrden(data: TrabajoInterface) {
    const orden = data.ordenesTrabajo ? data.ordenesTrabajo[0] : null;
    if (orden) {
      return orden.estadoAccion;
    } else {
      return EstadoTrabajo.NoGuardado;
    }
  }

  setearEstadoTrabajo(data: TrabajoInterface) {
    const trabajo = data.trabajosRealizados ? data.trabajosRealizados[0] : null;
    if (trabajo) {
      return trabajo.estadoAccion;
    } else {
      return EstadoTrabajo.NoGuardado;
    }
  }

  setearEstadoFactura(data: TrabajoInterface) {
    const factura = data.facturas ? data.facturas[0] : null;
    if (factura) {
      return factura.estadoAccion;
    } else {
      return EstadoTrabajo.NoGuardado;
    }
  }

  setearNumeroFactura(data: TrabajoInterface) {
    const factura = data.facturas ? data.facturas[0] : null;
    if (factura) {
      return factura.numero;
    } else {
      return '-';
    }
  }

  abrirModalPagosAbonos(data: TrabajoInterface) {
    if (data.facturas?.length) {
      const factura = data.facturas[0];
      const dialogRef = this._matDialog.open(ModalPagosAbonosComponent, {
        width: '850px',
        data: {
          idFactura: factura.id,
        },
        disableClose: true,
      });
      dialogRef.afterClosed().subscribe({
        next: (response) => {
          if (response) {
            const datos = {
              idFactura: factura.id,
              abonosPagos: response,
            };
            console.log(datos);
            const gastoFactura$ = this._facturaCabeceraRestService
              .agregarAbonoGasto(datos);

            gastoFactura$
              .subscribe(
                (respuesta) => {
                  this._notificacionService.modalInfo(
                    'success',
                    'Éxito',
                    `El abono/pago ha sido agregado con éxito`,
                  );
                  this._cargandoService.deshabilitar();
                  this.cargarDatosIniciales(this.consulta);
                  this._actualizarPagosService.actualizarPagos(true);
                },
                error => {
                  this._notificacionService.modalInfo(
                    'error',
                    'Error',
                    `Error al agregar el abono/pago`,
                  );
                  this.cargarDatosIniciales(this.consulta);
                  this._cargandoService.deshabilitar();
                  console.error({
                    mensaje: `Error al agregar el abono/pago`,
                    error,
                  });
                }
              );
          }
        },
        error: (err) => {
          // eslint-disable-next-line no-console
          console.error(err);
        },
      });
    }

  }

  setearPagosPendientes(trabajo: TrabajoInterface) {
    if (trabajo.facturas?.length) {
      const factura = trabajo.facturas[0];
      const pagosPorPagar = factura.abonosFactura?.filter(abono => abono.estadoAbono === EstadoAbono.PorPagar);
      return pagosPorPagar?.length || 0;
    } else {
      return 0;
    }
  }

  async eliminar(data: any) {
    const respuestaModalConfirmacion = await this._notificacionService.modalConfirmsSwal(
      'question',
      'Advertencia',
      `¿Esta seguro de eliminar el trabajo con su respectiva recepción, orden de trabajo, trabajo realizado y factura?`,
    );
    if (respuestaModalConfirmacion) {
      const trabajo$ = this._trabajoRestService
        .eliminarTrabajo(data?.id);

      trabajo$
        .subscribe(
          (respuesta) => {
            this._notificacionService.modalInfo(
              'success',
              'Éxito',
              `El registro ha sido eliminado con éxito`,
            );
            this._cargandoService.deshabilitar();
            this.cargarDatosIniciales(this.consulta);
            this._actualizarPagosService.actualizarPagos(true);
          },
          error => {
            this._notificacionService.modalInfo(
              'error',
              'Error',
              `Error al eliminar`,
            );
            this.cargarDatosIniciales(this.consulta);
            this._cargandoService.deshabilitar();
            console.error({
              mensaje: `Error al eliminar`,
              error,
            });
          }
        );
    }
  }
}
