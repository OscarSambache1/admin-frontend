import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {RecepcionInterface} from "../../../interfaces/recepcion.interface";
import {TrabajoInterface} from "../../../interfaces/trabajo.interface";
import {
  EstadoGasto,
  EstadoGastoAccion,
  EstadoTrabajo,
  TipoGasto,
  TipoGastoEnum
} from "../../../../../enums/tipo-contacto.enums";
import {NotificacionService} from "../../../../../servicios/notificacion/notificacion.service";
import {CargandoService} from "../../../../../servicios/cargando.service";
import {RecepcionRestService} from "../../../servicios/recepcion-rest.service";
import {ActivatedRoute, Router} from "@angular/router";
import {TrabajoRestService} from "../../../servicios/trabajo-rest.service";
import {mergeMap} from "rxjs/operators";
import {OrdenTrabajoInterface} from "../../../interfaces/orden-trabajo.interface";
import {OrdenTrabajoRestService} from "../../../servicios/orden-trabajo-rest.service";
import {TrabajoRealizadoInterface} from "../../../interfaces/trabajo-realizado.interface";
import {TrabajoRealizadoRestService} from "../../../servicios/trabajo-realizado-rest.service";
import {FacturaCabeceraInterface} from "../../../../gastos/interfaces/factura-cabecera.interface";
import {FacturaCabeceraRestService} from "../../../../gastos/servicios/factura-cabecera-rest.service";
import {VehiculoInterface} from "../../../../configuracion-datos/interfaces/vehiculo.interface";
import {ActualizarPagosService} from "../../../../../servicios/actualizar-pagos.service";
import {ModalCrearEditarGastoComponent} from "../../../../../componentes/modales/modal-crear-editar-gasto/modal-crear-editar-gasto.component";
import {MatDialog} from "@angular/material/dialog";
import {GastoInterface} from "../../../../gastos/interfaces/gasto.interface";
import {ROWS, ROWS_PER_PAGE} from "../../../../../constantes/table-config";
import {ModalPagosAbonosComponent} from "../../../../../componentes/modales/modal-pagos-abonos/modal-pagos-abonos.component";
import {DxDataGridComponent} from "devextreme-angular";
import {GastoRestService} from "../../../../gastos/servicios/gasto-rest.service";

@Component({
  selector: 'app-ruta-crear-editar-trabajo',
  templateUrl: './ruta-crear-editar-trabajo.component.html',
  styleUrls: ['./ruta-crear-editar-trabajo.component.scss']
})
export class RutaCrearEditarTrabajoComponent implements OnInit {
  @ViewChild(DxDataGridComponent)
  dataGrid!: DxDataGridComponent;

  formularioRecepcionValido!: boolean;
  formularioOrdenTrabajoValido!: boolean;
  formularioTrabajoRealizadoValido!: boolean;
  formularioFacturaValido!: boolean;

  recepcionFormulario!: RecepcionInterface | undefined;
  ordenTrabajoFormulario!: OrdenTrabajoInterface | undefined;
  trabajoRealizadoFormulario!: TrabajoRealizadoInterface | undefined;
  facturaFormulario!: FacturaCabeceraInterface | undefined;

  recepcionPrevia!: RecepcionInterface;
  ordenTrabajoPrevio!: OrdenTrabajoInterface;
  trabajoRealizadoPrevio!: TrabajoRealizadoInterface;
  facturaPrevio!: FacturaCabeceraInterface;
  gastos: GastoInterface[] = [];
  trabajoPrevio!: TrabajoInterface;

  estaCargando = false;

  estadosTrabajo = EstadoTrabajo;

  vehiculo?: VehiculoInterface;

  afectarInventario = false;

  estadoGastoAccionEnum = EstadoGastoAccion;
  tipoGastoEnum: any = TipoGastoEnum;
  rows = ROWS;
  rowsPerPage = ROWS_PER_PAGE;
  datos: GastoInterface[] = [];
  pageSize!: number;
  disabledTable!: boolean;
  pagination: number[] = [];
  paginationStart!: number;
  columns = [
    {
      field: 'tipoGasto',
      header: 'Tipo',
      width: '10%',
    },
    {
      field: 'valorAPagar',
      header: 'Valor a Pagar',
      width: '7%',
    },
    {
      field: 'valorPagado',
      header: 'Valor Pagado',
      width: '7%',
    },
    {
      field: 'numero',
      header: 'N° Factura',
      width: '10%',
    },
    {
      field: 'fechaEmision',
      header: 'Fecha Emisión',
      width: '10%',
    },
    {
      field: 'proveedor',
      header: 'Proveedor',
      width: '10%',
    },
    {
      field: 'estado',
      header: 'Estado',
      width: '10%',
    },
    {
      field: 'acciones',
      header: 'Acciones',
      width: '10%',
    },
  ];
  soloVer = false;
  constructor(
    private readonly _notificacionService: NotificacionService,
    private readonly _cargandoService: CargandoService,
    private readonly _recepcionRestService: RecepcionRestService,
    private readonly _ordenTrabajoRestService: OrdenTrabajoRestService,
    private readonly _trabajoRealizadoRestService: TrabajoRealizadoRestService,
    private readonly _facturaCabeceraRestService: FacturaCabeceraRestService,
    private readonly _trabajoRestService: TrabajoRestService,
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _actualizarPagosService: ActualizarPagosService,
    private readonly _matDialog: MatDialog,
    private readonly _gastoRestService: GastoRestService,
  ) {
  }

  ngOnInit(): void {
    this.pagination = ROWS_PER_PAGE;
    this.paginationStart = ROWS;
    this.estaCargando = true;
    this._cargandoService.habilitar();
    this._activatedRoute.params
      .pipe(
        mergeMap(params => {
          const soloVer = +params.soloVer;
          this.soloVer = !!soloVer;
          const idTrabajo = params.idTrabajo;
          return this._trabajoRestService.obtenerTrabajo(idTrabajo);
        })
      )
      .subscribe(respuesta => {
        if (respuesta) {
          this.trabajoPrevio = respuesta;
          if (this.trabajoPrevio?.recepciones) {
            this.recepcionPrevia = this.trabajoPrevio?.recepciones[0];
          }
          if (this.trabajoPrevio?.ordenesTrabajo) {
            this.ordenTrabajoPrevio = this.trabajoPrevio?.ordenesTrabajo[0];
            this.ordenTrabajoPrevio?.ordenTrabajoDetalles?.map(ordenTrabajoDetalle => {
              ordenTrabajoDetalle.tipo = TipoGasto.Repuesto;
            });
          }
          if (this.trabajoPrevio?.trabajosRealizados) {
            this.trabajoRealizadoPrevio = this.trabajoPrevio?.trabajosRealizados[0];
            this.trabajoRealizadoPrevio?.trabajoRealizadoDetalles?.map(trabajoRealizadoDetalle => {
              trabajoRealizadoDetalle.tipo = TipoGasto.Repuesto;
            });
          }
          if (this.trabajoPrevio?.facturas) {
            this.facturaPrevio = this.trabajoPrevio?.facturas[0];
            this.facturaPrevio?.facturaDetalles?.map(facturas => {
              facturas.tipo = TipoGasto.Repuesto;
            });
          }
          if (this.trabajoPrevio?.gastos) {
            this.gastos = this.trabajoPrevio.gastos;
          }
        }
        this._cargandoService.deshabilitar();
        this.estaCargando = false;
      }, error => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
        this.estaCargando = false;
      });
  }

  async guardarRecepcionVehiculo(accion: string) {
    let mensaje = '';
    if (accion === EstadoTrabajo.Guardado) {
      mensaje = 'Esta seguro de guardar la recepción del vehículo, en caso de hacerlo podra seguir editando la información hasta finalizarlo';
    }
    if (accion === EstadoTrabajo.Finalizado) {
      mensaje = 'Esta seguro de finalizar la recepción del vehículo, tenga en cuenta que ya no podra editar la información';
    }
    const respuestaModalConfirmacion = await this._notificacionService.modalConfirmsSwal(
      'question',
      'Advertencia',
      mensaje,
    );
    if (respuestaModalConfirmacion) {
      if (this.recepcionFormulario) {
        this.recepcionFormulario.estadoAccion = accion;
        this.recepcionFormulario.vehiculo = this.vehiculo;
      }
      if (this.recepcionPrevia && this.recepcionFormulario) {
        this.recepcionFormulario.id = this.recepcionPrevia.id;
      }
      if (this.trabajoPrevio && this.recepcionFormulario) {
        this.recepcionFormulario.idTrabajo = this.trabajoPrevio.id;
      }
      const datos = this.recepcionFormulario;

      this._cargandoService.habilitar();
      const recepcion$ = this._recepcionRestService
        .crearEditarTrabajoRecepcion(datos);

      recepcion$
        .subscribe(
          (respuesta) => {
            this.estaCargando = true;
            let mensajeExito = '';
            if (accion === EstadoTrabajo.Guardado) {
              mensajeExito = `La Recepción ha sido guardada con éxito`
            }
            if (accion === EstadoTrabajo.Finalizado) {
              mensajeExito = `La Recepción ha sido finalizada con éxito`
            }

            this._notificacionService.modalInfo(
              'success',
              'Éxito',
              mensajeExito,
            );
            this._cargandoService.deshabilitar();
            this.estaCargando = false;
            this.trabajoPrevio = respuesta;
            this.recepcionPrevia = respuesta.recepciones[0];
            const ruta = ['/app', 'trabajos', 'gestion-trabajo', 'crear-editar-trabajo', (respuesta.id), 0];
            void this._router.navigate(ruta);
          },
          error => {
            let mensajeError = '';
            if (accion === EstadoTrabajo.Guardado) {
              mensajeError = `Error al guardar la recepción`
            }
            if (accion === EstadoTrabajo.Finalizado) {
              mensajeError = `Error al finalizar la recepción`
            }
            this._notificacionService.modalInfo(
              'error',
              'Error',
              mensajeError,
            );
            this._cargandoService.deshabilitar();
            console.error({
              mensaje: mensajeError,
              error,
            });
          }
        );
    }
  }

  obtenerRecepcion(recepcion: RecepcionInterface | boolean) {
    if (recepcion) {
      this.recepcionFormulario = recepcion as RecepcionInterface;
      this.formularioRecepcionValido = true;
    } else {
      this.formularioRecepcionValido = false;
    }
  }

  obtenerOrdenTrabajo(ordenTrabajo: OrdenTrabajoInterface | boolean) {
    if (ordenTrabajo) {
      this.ordenTrabajoFormulario = ordenTrabajo as OrdenTrabajoInterface;
      this.formularioOrdenTrabajoValido = true;
    } else {
      this.formularioOrdenTrabajoValido = false;
    }
  }

  async guardarOrdenTrabajo(accion: EstadoTrabajo) {
    let mensaje = '';
    if (accion === EstadoTrabajo.Guardado) {
      mensaje = 'Esta seguro de guardar la orden de trabajo, en caso de hacerlo podra seguir editando la información hasta finalizarlo';
    }
    if (accion === EstadoTrabajo.Finalizado) {
      mensaje = 'Esta seguro de finalizar la orden de trabajo, tenga en cuenta que ya no podra editar la información';
    }
    const respuestaModalConfirmacion = await this._notificacionService.modalConfirmsSwal(
      'question',
      'Advertencia',
      mensaje,
    );
    if (respuestaModalConfirmacion) {
      if (this.ordenTrabajoFormulario) {
        this.ordenTrabajoFormulario.estadoAccion = accion;
        this.ordenTrabajoFormulario.vehiculo = this.vehiculo;
      }
      if (this.ordenTrabajoPrevio && this.ordenTrabajoFormulario) {
        this.ordenTrabajoFormulario.id = this.ordenTrabajoPrevio.id;
      }
      if (this.trabajoPrevio && this.ordenTrabajoFormulario) {
        this.ordenTrabajoFormulario.idTrabajo = this.trabajoPrevio.id;
      }
      const datos = this.ordenTrabajoFormulario;

      this._cargandoService.habilitar();
      const recepcion$ = this._ordenTrabajoRestService
        .crearEditarOrdenTrabajo(datos);

      recepcion$
        .subscribe(
          (respuesta) => {
            this.estaCargando = true;
            let mensajeExito = '';
            if (accion === EstadoTrabajo.Guardado) {
              mensajeExito = `La Orden de Trabajo ha sido guardada con éxito`
            }
            if (accion === EstadoTrabajo.Finalizado) {
              mensajeExito = `La Orden de Trabajo ha sido finalizada con éxito`
            }

            this._notificacionService.modalInfo(
              'success',
              'Éxito',
              mensajeExito,
            );
            this._cargandoService.deshabilitar();
            this.estaCargando = false;
            this.trabajoPrevio = respuesta;
            this.ordenTrabajoPrevio = respuesta.ordenesTrabajo[0];
          },
          error => {
            let mensajeError = '';
            if (accion === EstadoTrabajo.Guardado) {
              mensajeError = `Error al guardar la Orden de Trabajo`
            }
            if (accion === EstadoTrabajo.Finalizado) {
              mensajeError = `Error al finalizar la Orden de Trabajo`
            }
            this._notificacionService.modalInfo(
              'error',
              'Error',
              mensajeError,
            );
            this._cargandoService.deshabilitar();
            console.error({
              mensaje: mensajeError,
              error,
            });
          }
        );
    }
  }

  obtenerTrabajo(trabajo: TrabajoInterface | boolean) {
    if (trabajo) {
      this.vehiculo = (trabajo as TrabajoInterface).vehiculo;
    }
  }

  obtenerTrabajoRealizado(trabajoRealizado: TrabajoRealizadoInterface | boolean) {
    if (trabajoRealizado) {
      this.trabajoRealizadoFormulario = trabajoRealizado as TrabajoRealizadoInterface;
      this.formularioTrabajoRealizadoValido = true;
    } else {
      this.formularioTrabajoRealizadoValido = false;
    }
  }

  async guardarTrabajoRealizado(accion: EstadoTrabajo) {
    let mensaje = '';
    if (accion === EstadoTrabajo.Guardado) {
      mensaje = 'Esta seguro de guardar el trabajo realizado, en caso de hacerlo podrá seguir editando la información hasta finalizarlo';
    }
    if (accion === EstadoTrabajo.Finalizado) {
      mensaje = `Esta seguro de finalizar el trabajo realizado, tenga en cuenta que ${this.afectarInventario ? 'el inventario sera afectado y ya' : ''} no podrá editar la información`;
    }
    const respuestaModalConfirmacion = await this._notificacionService.modalConfirmsSwal(
      'question',
      'Advertencia',
      mensaje,
    );
    if (respuestaModalConfirmacion) {
      if (this.trabajoRealizadoFormulario) {
        this.trabajoRealizadoFormulario.estadoAccion = accion;
        this.trabajoRealizadoFormulario.vehiculo = this.vehiculo;
      }
      if (this.trabajoRealizadoPrevio && this.trabajoRealizadoFormulario) {
        this.trabajoRealizadoFormulario.id = this.trabajoRealizadoPrevio.id;
      }
      if (this.trabajoPrevio && this.trabajoRealizadoFormulario) {
        this.trabajoRealizadoFormulario.idTrabajo = this.trabajoPrevio.id;
      }
      const datos: any = this.trabajoRealizadoFormulario;
      datos.afectarInventario = this.afectarInventario;

      this._cargandoService.habilitar();
      const trabajoRealizado$ = this._trabajoRealizadoRestService
        .crearEditarTrabajoRealizado(datos);

      trabajoRealizado$
        .subscribe(
          (respuesta: TrabajoInterface) => {
            this.estaCargando = true;
            let mensajeExito = '';
            if (accion === EstadoTrabajo.Guardado) {
              mensajeExito = `El Trabajo Realizado ha sido guardada con éxito`
            }
            if (accion === EstadoTrabajo.Finalizado) {
              mensajeExito = `El Trabajo Realizado ha sido finalizado con éxito`
            }

            this._notificacionService.modalInfo(
              'success',
              'Éxito',
              mensajeExito,
            );
            this._cargandoService.deshabilitar();
            this.estaCargando = false;
            this.trabajoPrevio = respuesta;
            if (respuesta && respuesta.trabajosRealizados) {
              this.trabajoRealizadoPrevio = respuesta.trabajosRealizados[0];
            }
          },
          error => {
            let mensajeError = '';
            if (accion === EstadoTrabajo.Guardado) {
              mensajeError = `Error al guardar El Trabajo Realizado `
            }
            if (accion === EstadoTrabajo.Finalizado) {
              mensajeError = `Error al finalizar El Trabajo Realizado`
            }
            this._notificacionService.modalInfo(
              'error',
              'Error',
              mensajeError,
            );
            this._cargandoService.deshabilitar();
            console.error({
              mensaje: mensajeError,
              error,
            });
          }
        );
    }
  }

  obtenerFactura(factura: FacturaCabeceraInterface | boolean) {
    if (factura) {
      this.facturaFormulario = factura as FacturaCabeceraInterface;
      this.formularioFacturaValido = true;
    } else {
      this.formularioFacturaValido = false;
    }
  }

  async guardarFactura(accion: EstadoTrabajo) {
    let mensaje = '';
    if (accion === EstadoTrabajo.Guardado) {
      mensaje = 'Esta seguro de guardar la factura, en caso de hacerlo podrá seguir editando la información hasta finalizarla';
    }
    if (accion === EstadoTrabajo.Finalizado) {
      mensaje = 'Esta seguro de finalizar la factura, tenga en cuenta que ya no podrá editar la información';
    }
    const respuestaModalConfirmacion = await this._notificacionService.modalConfirmsSwal(
      'question',
      'Advertencia',
      mensaje,
    );
    if (respuestaModalConfirmacion) {
      if (this.facturaFormulario) {
        this.facturaFormulario.estadoAccion = accion;
        this.facturaFormulario.vehiculo = this.vehiculo;
      }
      if (this.facturaPrevio && this.facturaFormulario) {
        this.facturaFormulario.id = this.facturaPrevio.id;
      }
      if (this.trabajoPrevio && this.facturaFormulario) {
        this.facturaFormulario.idTrabajo = this.trabajoPrevio.id;
      }
      const datos = this.facturaFormulario;

      this._cargandoService.habilitar();
      const factura$ = this._facturaCabeceraRestService
        .crearEditarFactura(datos);

      factura$
        .subscribe(
          (respuesta: TrabajoInterface) => {
            this.estaCargando = true;
            let mensajeExito = '';
            if (accion === EstadoTrabajo.Guardado) {
              mensajeExito = `La factura ha sido guardada con éxito`
            }
            if (accion === EstadoTrabajo.Finalizado) {
              mensajeExito = `La Factura ha sido finalizada con éxito`
            }

            this._notificacionService.modalInfo(
              'success',
              'Éxito',
              mensajeExito,
            );
            this._cargandoService.deshabilitar();
            this.estaCargando = false;
            this.trabajoPrevio = respuesta;
            if (respuesta && respuesta.facturas) {
              this.facturaPrevio = respuesta.facturas[0];
            }
            this._actualizarPagosService.actualizarPagos(true);
          },
          error => {
            let mensajeError = '';
            if (accion === EstadoTrabajo.Guardado) {
              mensajeError = `Error al guardar la factura `
            }
            if (accion === EstadoTrabajo.Finalizado) {
              mensajeError = `Error al finalizar la factura`
            }
            this._notificacionService.modalInfo(
              'error',
              'Error',
              mensajeError,
            );
            this._cargandoService.deshabilitar();
            console.error({
              mensaje: mensajeError,
              error,
            });
          }
        );
    }
  }

  setearSoloVer() {
    return false;
    // return this.facturaPrevio?.estadoAccion === EstadoTrabajo.Finalizado;
  }

  agregarGasto(
    gastoPrevio?: GastoInterface,
    facturaPrevia?: FacturaCabeceraInterface,
    soloVer?: boolean
  ) {
    const dialogRef = this._matDialog.open(ModalCrearEditarGastoComponent, {
      width: '1550px',
      data: {
        gasto: gastoPrevio,
        factura: facturaPrevia,
        soloVer
      },
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          const facturaACrearEditar = response.facturaACrearEditar;
          const gastoACrearEditar = response.gastoACrearEditar;
          if (gastoPrevio && gastoACrearEditar) {
            gastoACrearEditar.id = gastoPrevio.id;
          }
          if (facturaPrevia && facturaACrearEditar) {
            facturaACrearEditar.id = facturaPrevia.id;
          }
          const accion = response.accion;
          const datos = {
            accion,
            gasto: gastoACrearEditar,
            factura: facturaACrearEditar,
            trabajo: this.trabajoPrevio.id,
          };


          this._cargandoService.habilitar();
          const formData: FormData = new FormData();
          formData.append('archivoPdf', datos.factura?.archivoPdf);
          formData.append('datosGastoFactura', JSON.stringify(datos));
          const gastoFactura$ = this._facturaCabeceraRestService
            .crearEditarGastoFactura(formData);

          gastoFactura$
            .subscribe(
              (respuesta) => {
                this._notificacionService.modalInfo(
                  'success',
                  'Éxito',
                  `El gasto ha sido ${accion.toUpperCase()} con éxito`,
                );
                this._cargandoService.deshabilitar();
                this._actualizarPagosService.actualizarPagos(true);
                this.buscarGastos();
              },
              error => {
                const accionGasto = accion === 'GUARDARDO' ? 'guardar' : 'finalizar';
                this._notificacionService.modalInfo(
                  'error',
                  'Error',
                  `Error al ${accionGasto} el gasto`,
                );
                this._cargandoService.deshabilitar();
                console.error({
                  mensaje: `Error al ${accionGasto} el gasto`,
                  error,
                });
              }
            );
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

  abrirModalPagosAbonos(
    gasto: GastoInterface
  ) {
    const dialogRef = this._matDialog.open(ModalPagosAbonosComponent, {
      width: '850px',
      data: {
        factura: gasto?.facturaCabecera,
        idFactura: gasto?.facturaCabecera?.id,
      },
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          const datos = {
            idFactura: gasto.facturaCabecera?.id,
            abonosPagos: response,
          };
          const gastoFactura$ = this._facturaCabeceraRestService
            .agregarAbonoGasto(datos);

          gastoFactura$
            .subscribe(
              (respuesta) => {
                this._notificacionService.modalInfo(
                  'success',
                  'Éxito',
                  `El abono/pago ha sido agregado con éxito`,
                );
                this._cargandoService.deshabilitar();
                const ruta = ['/app', 'trabajos', 'gestion-trabajo', 'crear-editar-trabajo', (this.trabajoPrevio?.id || -1), 0];
                void this._router.navigate(ruta);
                this._actualizarPagosService.actualizarPagos(true);
              },
              error => {
                this._notificacionService.modalInfo(
                  'error',
                  'Error',
                  `Error al agregar el abono/pago`,
                );
                this._cargandoService.deshabilitar();
                console.error({
                  mensaje: `Error al agregar el abono/pago`,
                  error,
                });
              }
            );
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

  buscarGastos() {
    const consulta: any = {
      relations: [
        'facturaCabecera',
        'facturaCabecera.archivos',
        'facturaCabecera.proveedor',
        'facturaCabecera.proveedor.informacionTributaria',
        'facturaCabecera.facturaDetalles',
        'facturaCabecera.facturaDetalles.articulo',
        'facturaCabecera.abonosFactura',
        'facturaCabecera.abonosFactura.formaPago',
      ],
      where: {
        trabajo: this.trabajoPrevio?.id,
      },
      order: {
        id: 'DESC',
      },
    };
    this._cargandoService.habilitar();
    this._gastoRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    )      .subscribe(respuesta => {
      if (respuesta && respuesta[0] && respuesta[0][0]) {
        if (this.trabajoPrevio) {
          this.trabajoPrevio.gastos = respuesta[0];
        }
        this.gastos = respuesta[0];
      }
      this._cargandoService.deshabilitar();
      this.estaCargando = false;
    }, error => {
      console.error({
        mensage: 'Error al traer registros',
        error: error,
      });
      this._notificacionService.modalInfo(
        'error',
        'Error',
        'Error al traer registros',
      );
      this._cargandoService.deshabilitar();
      this.estaCargando = false;
    });
  }

  async eliminar(data: any) {
    const respuestaModalConfirmacion = await this._notificacionService.modalConfirmsSwal(
      'question',
      'Advertencia',
      `¿Esta seguro de eliminar el gasto con su respectiva factura?`,
    );
    if (respuestaModalConfirmacion) {
      const gasto$ = this._gastoRestService
        .eliminarGasto(data?.id);

      gasto$
        .subscribe(
          (respuesta) => {
            this._notificacionService.modalInfo(
              'success',
              'Éxito',
              `El registro ha sido eliminado con éxito`,
            );
            this._cargandoService.deshabilitar();
            this.buscarGastos();
            this._actualizarPagosService.actualizarPagos(true);
          },
          error => {
            this._notificacionService.modalInfo(
              'error',
              'Error',
              `Error al eliminar`,
            );
            this._cargandoService.deshabilitar();
            this._cargandoService.deshabilitar();
            console.error({
              mensaje: `Error al eliminar`,
              error,
            });
          }
        );
    }
  }

}
