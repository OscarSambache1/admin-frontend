import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaVehiculoComponent} from "../../configuracion-datos/vehiculo/ruta-vehiculo/ruta-vehiculo.component";
import {RutaTrabajoComponent} from "./ruta/ruta-trabajo/ruta-trabajo.component";
import {RutaCrearEditarGastoComponent} from "../../gastos/ruta/ruta-crear-editar-gasto/ruta-crear-editar-gasto.component";
import {RutaCrearEditarTrabajoComponent} from "./ruta/ruta-crear-editar-trabajo/ruta-crear-editar-trabajo.component";

const routes: Routes = [
  {
    path: '',
    component: RutaTrabajoComponent,
    data: {
      breadcrumb: 'Listar Trabajos',
    },
  },
  {
    path: 'crear-editar-trabajo/:idTrabajo/:soloVer',
    component: RutaCrearEditarTrabajoComponent,
    data: {
      breadcrumb: 'Registrar Trabajo',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrabajoRoutingModule { }
