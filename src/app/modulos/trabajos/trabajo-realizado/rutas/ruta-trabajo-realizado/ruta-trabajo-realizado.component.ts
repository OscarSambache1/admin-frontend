import {Component, OnInit, ViewChild} from '@angular/core';
import {DxDataGridComponent} from "devextreme-angular";
import {ROWS, ROWS_PER_PAGE} from "../../../../../constantes/table-config";
import {RecepcionInterface} from "../../../interfaces/recepcion.interface";
import {EstadoTrabajo, EstadoTrabajoEnum} from "../../../../../enums/tipo-contacto.enums";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../../../servicios/cargando.service";
import {NotificacionService} from "../../../../../servicios/notificacion/notificacion.service";
import {TrabajoRealizadoInterface} from "../../../interfaces/trabajo-realizado.interface";
import {TrabajoRealizadoRestService} from "../../../servicios/trabajo-realizado-rest.service";
import {ImprimitService} from "../../../../../servicios/imprimit.service";

@Component({
  selector: 'app-ruta-trabajo-realizado',
  templateUrl: './ruta-trabajo-realizado.component.html',
  styleUrls: ['./ruta-trabajo-realizado.component.scss']
})
export class RutaTrabajoRealizadoComponent implements OnInit {

  @ViewChild(DxDataGridComponent)
  dataGrid!: DxDataGridComponent;

  rows = ROWS;
  rowsPerPage = ROWS_PER_PAGE;
  datos: TrabajoRealizadoInterface[] = [];
  pageSize!: number;
  disabledTable!: boolean;
  pagination: number[] = [];
  paginationStart!: number;
  consulta: any = {
    relations: [
      'vehiculo',
      'vehiculo.marca',
      'vehiculo.cliente',
      'vehiculo.cliente.informacionTributaria',
      'trabajo',
      'trabajoRealizadoDetalles',
      'trabajoRealizadoDetalles.articulo'
    ],
    where: {},
    order: {
      id: 'DESC',
    },
  };
  estadoAccionEnum: any = EstadoTrabajoEnum;
  estadosAccion = [
    {
      value: EstadoTrabajo.Guardado,
      label: EstadoTrabajoEnum.G
    },
    {
      value: EstadoTrabajo.NoGuardado,
      label: EstadoTrabajoEnum.NG,
    },
    {
      value: EstadoTrabajo.Finalizado,
      label: EstadoTrabajoEnum.F
    },
  ];
  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _trabajoRealizadoRestService: TrabajoRealizadoRestService,
    private readonly _notificacionService: NotificacionService,
    private readonly _imprimirService: ImprimitService,
  ) {}

  ngOnInit(): void {
    this.pagination = ROWS_PER_PAGE;
    this.paginationStart = ROWS;
    this.cargarDatosIniciales(this.consulta);
  }

  cargarDatosIniciales(consulta: {}): void {
    this._cargandoService.habilitar();
    const trabajoRealizado$ = this._trabajoRealizadoRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    trabajoRealizado$.subscribe({
      next: (trabajoRealizado) => {
        this.datos = trabajoRealizado[0].filter(trabajoR => trabajoR.trabajo);
        this.pageSize = this.datos.length;
        this.disabledTable = false;
        this._cargandoService.deshabilitar();
      },
      error: (error) => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
      },
    });
  }

  onToolbarPreparing(evento: any): void {
    evento.toolbarOptions.items.unshift(
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Recargar datos de la tabla',
          icon: 'refresh',
          onClick: this.onRefreshDataGrid.bind(this),
        },
      }
    );
  }

  private onRefreshDataGrid(): void {
    this.cargarDatosIniciales(this.consulta);
    void this.dataGrid.instance.refresh();
  }

  add(trabajoRealizado?: TrabajoRealizadoInterface) {
    const ruta = ['/app', 'trabajos', 'trabajo-realizado', 'formulario', trabajoRealizado?.id];
    void this._router.navigate(ruta);
  }

  setearCliente(trabajoRealizado?: TrabajoRealizadoInterface) {
    return trabajoRealizado?.vehiculo?.cliente?.informacionTributaria?.nombreComercial;
  }

  async imprimir(trabajoRealizado: any) {
    const pdfDefinition: any = await this._imprimirService.setearObjetoTrabajoRealizado(trabajoRealizado);
    await this._imprimirService.imprimir(pdfDefinition);
  }
}
