import { Component, OnInit } from '@angular/core';
import {NotificacionService} from "../../../../../servicios/notificacion/notificacion.service";
import {CargandoService} from "../../../../../servicios/cargando.service";
import {ActivatedRoute, Router} from "@angular/router";
import {mergeMap} from "rxjs/operators";
import {TrabajoRealizadoInterface} from "../../../interfaces/trabajo-realizado.interface";
import {TrabajoRealizadoRestService} from "../../../servicios/trabajo-realizado-rest.service";
import {TipoGasto} from "../../../../../enums/tipo-contacto.enums";

@Component({
  selector: 'app-ruta-formulario-trabajo-realizado',
  templateUrl: './ruta-formulario-trabajo-realizado.component.html',
  styleUrls: ['./ruta-formulario-trabajo-realizado.component.scss']
})
export class RutaFormularioTrabajoRealizadoComponent implements OnInit {
  estaCargando = false;
  trabajoRealizado?: TrabajoRealizadoInterface;
  constructor(
    private readonly _notificacionService: NotificacionService,
    private readonly _cargandoService: CargandoService,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _router: Router,
    private readonly _trabajoRealizadoRestService: TrabajoRealizadoRestService,
  ) { }

  ngOnInit(): void {
    this.estaCargando = true;
    this._cargandoService.habilitar();
    this._activatedRoute.params
      .pipe(
        mergeMap(params => {
          const idTrabajoRealizado = params.idTrabajoRealizado;
          const consulta: any = {
            relations: [
              'vehiculo',
              'vehiculo.marca',
              'vehiculo.cliente',
              'vehiculo.cliente.informacionTributaria',
              'trabajo',
              'trabajo.vehiculo',
              'trabajo.vehiculo.marca',
              'trabajo.vehiculo.cliente',
              'trabajo.vehiculo.cliente.informacionTributaria',
              'trabajo.recepciones',
              'trabajoRealizadoDetalles',
              'trabajoRealizadoDetalles.articulo',
            ],
            where: {
              id: idTrabajoRealizado,
            },
            order: {
              id: 'DESC',
            },
          };
          return this._trabajoRealizadoRestService.findAll(
            `busqueda=${JSON.stringify(consulta)}`
          );
        })
      )
      .subscribe(respuesta => {
        if (respuesta && respuesta[0] && respuesta[0][0]) {
          this.trabajoRealizado = respuesta[0][0];
          if (this.trabajoRealizado) {
            this.trabajoRealizado?.trabajoRealizadoDetalles?.map(trabajoRealizadoDetalle => {
              trabajoRealizadoDetalle.tipo = TipoGasto.Repuesto;
            });
          }
        }
        this._cargandoService.deshabilitar();
        this.estaCargando = false;
      }, error => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
        this.estaCargando = false;
      });
  }

  cancelar() {
    const ruta = ['/app', 'trabajos', 'trabajo-realizado',];
    void this._router.navigate(ruta);
  }
}
