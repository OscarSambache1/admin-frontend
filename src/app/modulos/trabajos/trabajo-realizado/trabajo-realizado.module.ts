import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TrabajoRealizadoRoutingModule } from './trabajo-realizado-routing.module';
import { RutaTrabajoRealizadoComponent } from './rutas/ruta-trabajo-realizado/ruta-trabajo-realizado.component';
import {IMPORTS_MODULO} from "../../imports/imports-modulo";
import {ComponentesModule} from "../../../componentes/componentes.module";
import { RutaFormularioTrabajoRealizadoComponent } from './rutas/ruta-formulario-trabajo-realizado/ruta-formulario-trabajo-realizado.component';


@NgModule({
  declarations: [

    RutaTrabajoRealizadoComponent,
     RutaFormularioTrabajoRealizadoComponent
  ],
  imports: [
    CommonModule,
    TrabajoRealizadoRoutingModule,
    ...IMPORTS_MODULO,
    ComponentesModule,
  ]
})
export class TrabajoRealizadoModule { }
