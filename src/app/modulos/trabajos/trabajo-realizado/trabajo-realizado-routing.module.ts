import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaTrabajoRealizadoComponent} from "./rutas/ruta-trabajo-realizado/ruta-trabajo-realizado.component";
import {RutaFormularioTrabajoRealizadoComponent} from "./rutas/ruta-formulario-trabajo-realizado/ruta-formulario-trabajo-realizado.component";

const routes: Routes = [
  {
    path: '',
    component: RutaTrabajoRealizadoComponent,
    data: {
      breadcrumb: 'Trabajos Realizados',
    },
  },
  {
    path: 'formulario/:idTrabajoRealizado',
    component: RutaFormularioTrabajoRealizadoComponent,
    data: {
      breadcrumb: 'Ver Información',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrabajoRealizadoRoutingModule { }
