import {VehiculoInterface} from "../../configuracion-datos/interfaces/vehiculo.interface";
import {TrabajoInterface} from "./trabajo.interface";
import {EstadoTrabajo} from "../../../enums/tipo-contacto.enums";
import {TrabajoRealizadoDetalleInterface} from "./trabajo-realizado-detalle.interface";
import {ClienteInterface} from "../../configuracion-datos/interfaces/cliente.interface";

export interface TrabajoRealizadoInterface {
  id?: number;
  fechaTrabajoRealizado?: string;
  vehiculo?: VehiculoInterface;
  trabajo?: TrabajoInterface;
  trabajoRealizadoDetalles?: TrabajoRealizadoDetalleInterface[];
  estadoAccion?: EstadoTrabajo | any;
  idTrabajo?: number;
  observacion?: string;

  propina?: number;
  descuento?: number;
  descuentoPorcentaje?: number;
  subtotalIva0?: number;
  subtotalIva?: number;
  subtotalIvaExento?: number;
  subtotalIvaNoObjeto?: number;
  ivaPorcentaje?: string;
  ivaValor?: number;
  iceValor?: number;
  total?: number;
  subtotalSinImpuestos?: number;

  cliente?: ClienteInterface;
  esConsumidorFinal?: any;

  esProforma?: 0 | 1;
}
