import {ArticuloInterface} from "../../configuracion-datos/interfaces/articulo.interface";
import {OrdenTrabajoInterface} from "./orden-trabajo.interface";

export interface OrdenTrabajoDetalleInterface {
  id?: number;
  cantidad?: number;
  articulo?: ArticuloInterface;
  ordenTrabajo?: OrdenTrabajoInterface;
  descripcion?: string;
  tipo?: any;
}
