import {VehiculoInterface} from "../../configuracion-datos/interfaces/vehiculo.interface";
import {RecepcionInterface} from "./recepcion.interface";
import {OrdenTrabajoInterface} from "./orden-trabajo.interface";
import {TrabajoRealizadoInterface} from "./trabajo-realizado.interface";
import {FacturaCabeceraInterface} from "../../gastos/interfaces/factura-cabecera.interface";
import {GastoInterface} from "../../gastos/interfaces/gasto.interface";

export interface TrabajoInterface {
  id?: number;
  numero?: string;
  vehiculo?: VehiculoInterface;
  recepciones?: RecepcionInterface[];
  ordenesTrabajo?: OrdenTrabajoInterface[];
  trabajosRealizados?: TrabajoRealizadoInterface[];
  facturas?: FacturaCabeceraInterface[],
  gastos?: GastoInterface[];
  placa?: string,
  chasis?: string,
  anio?: number,
  color?: string,
  marca?: string,
  modelo?: string,
  motor?: string,
  razonSocial?: string,
  nombreComercial?: string,
  identificacion?: string,
  tipoIdentificacion?: string,
  direccion?: string,
  telefono?: string,
  correo?: string,
}

export interface TrabajoClienteInterface {
  id?: number;
  numero?: string;
  kmEntrada?: string;
  fechaRecepcion?: string;
  detalles?: any[];
  observacionRecepcion?: string;
  observacionOrden?: string;
  observacionTrabajoRealizado?: string;
  placa?: string,
  chasis?: string,
  anio?: number,
  color?: string,
  marca?: string,
  modelo?: string,
  motor?: string,
  razonSocial?: string,
  nombreComercial?: string,
  identificacion?: string,
  tipoIdentificacion?: string,
  direccion?: string,
  telefono?: string,
  correo?: string,
}

