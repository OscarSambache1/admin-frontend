import {VehiculoInterface} from "../../configuracion-datos/interfaces/vehiculo.interface";
import {TrabajoInterface} from "./trabajo.interface";
import {OrdenTrabajoDetalleInterface} from "./orden-trabajo-detalle.interface";
import {EstadoTrabajo} from "../../../enums/tipo-contacto.enums";

export interface OrdenTrabajoInterface {
  id?: number;
  fechaOrdenTrabajo?: string;
  vehiculo?: VehiculoInterface;
  trabajo?: TrabajoInterface;
  ordenTrabajoDetalles?: OrdenTrabajoDetalleInterface[];
  estadoAccion?: EstadoTrabajo | any;
  idTrabajo?: number;
  observacion?: string;
}
