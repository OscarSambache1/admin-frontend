import {VehiculoInterface} from "../../configuracion-datos/interfaces/vehiculo.interface";
import {TrabajoInterface} from "./trabajo.interface";
import {EstadoTrabajo} from "../../../enums/tipo-contacto.enums";

export interface RecepcionInterface {
  id?: number;
  observacionesTaller?: string;
  observacionesCliente?: string;
  fechaRecepcion?: string;
  kmEntrada?: number;
  gasolinaEntrada?: number;
  piezasRecambio?: 0 | 1;
  enGarantia?: 0 | 1;
  siniestro?: 0 | 1;
  grua?: 0 | 1;
  vehiculo?: VehiculoInterface;
  trabajo?: TrabajoInterface;
  idTrabajo?: number;
  estadoAccion?: EstadoTrabajo | any;
}
