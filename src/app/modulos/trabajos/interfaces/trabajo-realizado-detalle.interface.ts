import {ArticuloInterface} from "../../configuracion-datos/interfaces/articulo.interface";
import {TrabajoRealizadoInterface} from "./trabajo-realizado.interface";

export interface TrabajoRealizadoDetalleInterface {
  id?: number;
  cantidad?: number;
  precioUnitario?: number;
  ivaPorcentaje?: number;
  ivaDescripcion?: string;
  descuento?: number;
  valorDescuento?: number;
  valorIva?: number;
  valorIce?: number;
  total?: number;
  articulo?: ArticuloInterface;
  trabajoRealizado?: TrabajoRealizadoInterface;
  descripcion?: any;
  tipo?: any;
}
