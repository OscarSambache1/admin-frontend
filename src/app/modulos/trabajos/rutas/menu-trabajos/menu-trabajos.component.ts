import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-menu-configuracion-datos',
  templateUrl: './menu-trabajos.component.html',
  styleUrls: ['./menu-trabajos.component.scss']
})
export class MenuTrabajosComponent implements OnInit {

  arregloItemsMenuInicio = [
    {
      nombre: 'Listar / Registrar Trabajos',
      texto: 'Listar / Registrar Trabajos',
      imagen: '/assets/images/gestion-trabajos.png',
      ruta: 'gestion-trabajo'
    },
    {
      nombre: 'Trabajos por Cliente',
      texto: 'Trabajos por Cliente',
      imagen: '/assets/images/trabajos-cliente.png',
      ruta: 'trabajos-cliente'
    },
    {
      nombre: 'Recepciones de Vehículo',
      texto: 'Recepciones de Vehículo',
      imagen: '/assets/images/recepcion-vehiculo.png',
      ruta: 'recepcion-vehiculo'
    },
    {
      nombre: 'Ordenes de Trabajo',
      texto: 'Ordenes de Trabajo',
      imagen: '/assets/images/orden-trabajo.png',
      ruta: 'orden-trabajo'
    },
    {
      nombre: 'Trabajos Realizados',
      texto: 'Trabajos Realizados',
      imagen: '/assets/images/trabajo-realizado.png',
      ruta: 'trabajo-realizado'
    },
    {
      nombre: 'Facturas',
      texto: 'Facturas',
      imagen: '/assets/images/factura.png',
      ruta: 'factura'
    },
  ];
  constructor(
    private readonly _router: Router
  ) { }

  ngOnInit(): void {
  }

  irARuta(rutaOpcion: string) {
    const ruta = ['/app', 'trabajos', rutaOpcion];
    void this._router.navigate(ruta);
  }
}
