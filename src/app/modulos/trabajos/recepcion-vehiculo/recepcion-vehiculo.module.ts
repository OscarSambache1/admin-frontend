import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecepcionVehiculoRoutingModule } from './recepcion-vehiculo-routing.module';
import {IMPORTS_MODULO} from "../../imports/imports-modulo";
import { RutaRecepcionVehiculoComponent } from './ruta/ruta-recepcion-vehiculo/ruta-recepcion-vehiculo.component';
import { RutaFormularioRecepcionVehiculoComponent } from './ruta/ruta-formulario-recepcion-vehiculo/ruta-formulario-recepcion-vehiculo.component';
import {ComponentesModule} from "../../../componentes/componentes.module";


@NgModule({
  declarations: [
    RutaRecepcionVehiculoComponent,
    RutaFormularioRecepcionVehiculoComponent
  ],
  imports: [
    CommonModule,
    RecepcionVehiculoRoutingModule,
    ...IMPORTS_MODULO,
    ComponentesModule,
  ]
})
export class RecepcionVehiculoModule { }
