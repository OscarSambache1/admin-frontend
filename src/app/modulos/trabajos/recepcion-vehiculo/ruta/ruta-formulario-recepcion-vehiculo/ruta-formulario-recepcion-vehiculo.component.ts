import { Component, OnInit } from '@angular/core';
import {NotificacionService} from "../../../../../servicios/notificacion/notificacion.service";
import {CargandoService} from "../../../../../servicios/cargando.service";
import {ActivatedRoute, Router} from "@angular/router";
import {RecepcionRestService} from "../../../servicios/recepcion-rest.service";
import {mergeMap} from "rxjs/operators";
import {RecepcionInterface} from "../../../interfaces/recepcion.interface";

@Component({
  selector: 'app-ruta-formulario-recepcion-vehiculo',
  templateUrl: './ruta-formulario-recepcion-vehiculo.component.html',
  styleUrls: ['./ruta-formulario-recepcion-vehiculo.component.scss']
})
export class RutaFormularioRecepcionVehiculoComponent implements OnInit {
  estaCargando = false;
  recepcion?: RecepcionInterface;
  constructor(
    private readonly _notificacionService: NotificacionService,
    private readonly _cargandoService: CargandoService,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _router: Router,
    private readonly _recepcionRestService: RecepcionRestService,
  ) { }

  ngOnInit(): void {
    this.estaCargando = true;
    this._cargandoService.habilitar();
    this._activatedRoute.params
      .pipe(
        mergeMap(params => {
          const idRecepcion = params.idRecepcion;
          const consulta: any = {
            relations: [
              'vehiculo',
              'vehiculo.marca',
              'vehiculo.cliente',
              'vehiculo.cliente.informacionTributaria',
              'trabajo',
              'trabajo.vehiculo',
              'trabajo.vehiculo.marca',
              'trabajo.vehiculo.cliente',
              'trabajo.vehiculo.cliente.informacionTributaria',
              'trabajo.recepciones',
            ],
            where: {
              id: idRecepcion,
            },
            order: {
              id: 'DESC',
            },
          };
          return this._recepcionRestService.findAll(
            `busqueda=${JSON.stringify(consulta)}`
          );
        })
      )
      .subscribe(respuesta => {
        if (respuesta && respuesta[0] && respuesta[0][0]) {
          this.recepcion = respuesta[0][0];
        }
        this._cargandoService.deshabilitar();
        this.estaCargando = false;
      }, error => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
        this.estaCargando = false;
      });
  }

  cancelar() {
    const ruta = ['/app', 'trabajos', 'recepcion-vehiculo',];
    void this._router.navigate(ruta);
  }
}
