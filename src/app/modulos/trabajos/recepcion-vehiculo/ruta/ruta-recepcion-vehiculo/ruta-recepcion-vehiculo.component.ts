import {Component, OnInit, ViewChild} from '@angular/core';
import {DxDataGridComponent} from "devextreme-angular";
import {ROWS, ROWS_PER_PAGE} from "../../../../../constantes/table-config";
import {TrabajoInterface} from "../../../interfaces/trabajo.interface";
import {EstadoGasto, EstadoTrabajo, EstadoTrabajoEnum} from "../../../../../enums/tipo-contacto.enums";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../../../servicios/cargando.service";
import {TrabajoRestService} from "../../../servicios/trabajo-rest.service";
import {NotificacionService} from "../../../../../servicios/notificacion/notificacion.service";
import {RecepcionInterface} from "../../../interfaces/recepcion.interface";
import {RecepcionRestService} from "../../../servicios/recepcion-rest.service";
import {ImprimitService} from "../../../../../servicios/imprimit.service";

@Component({
  selector: 'app-ruta-recepcion-vehiculo',
  templateUrl: './ruta-recepcion-vehiculo.component.html',
  styleUrls: ['./ruta-recepcion-vehiculo.component.scss']
})
export class RutaRecepcionVehiculoComponent implements OnInit {

  @ViewChild(DxDataGridComponent)
  dataGrid!: DxDataGridComponent;

  rows = ROWS;
  rowsPerPage = ROWS_PER_PAGE;
  datos: RecepcionInterface[] = [];
  pageSize!: number;
  disabledTable!: boolean;
  pagination: number[] = [];
  paginationStart!: number;
  consulta: any = {
    relations: [
      'vehiculo',
      'vehiculo.marca',
      'vehiculo.cliente',
      'vehiculo.cliente.informacionTributaria',
      'trabajo',
    ],
    where: {},
    order: {
      id: 'DESC',
    },
  };
  estadoAccionEnum: any = EstadoTrabajoEnum;
  estadosAccion = [
    {
      value: EstadoTrabajo.Guardado,
      label: EstadoTrabajoEnum.G
    },
    {
      value: EstadoTrabajo.NoGuardado,
      label: EstadoTrabajoEnum.NG,
    },
    {
      value: EstadoTrabajo.Finalizado,
      label: EstadoTrabajoEnum.F
    },
  ];
  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _recepcionRestService: RecepcionRestService,
    private readonly _notificacionService: NotificacionService,
    private readonly _imprimirService: ImprimitService,
  ) {}

  ngOnInit(): void {
    this.pagination = ROWS_PER_PAGE;
    this.paginationStart = ROWS;
    this.cargarDatosIniciales(this.consulta);
  }

  cargarDatosIniciales(consulta: {}): void {
    this._cargandoService.habilitar();
    const recepcion$ = this._recepcionRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    recepcion$.subscribe({
      next: (recepcion) => {
        this.datos = recepcion[0];
        this.pageSize = recepcion[1];
        this.disabledTable = false;
        this._cargandoService.deshabilitar();
      },
      error: (error) => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
      },
    });
  }

  onToolbarPreparing(evento: any): void {
    evento.toolbarOptions.items.unshift(
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Recargar datos de la tabla',
          icon: 'refresh',
          onClick: this.onRefreshDataGrid.bind(this),
        },
      }
    );
  }

  private onRefreshDataGrid(): void {
    this.cargarDatosIniciales(this.consulta);
    void this.dataGrid.instance.refresh();
  }

  add(recepcion?: RecepcionInterface) {
    const ruta = ['/app', 'trabajos', 'recepcion-vehiculo', 'formulario', recepcion?.id];
    void this._router.navigate(ruta);
  }

  setearCliente(recepcion: RecepcionInterface) {
    return recepcion?.vehiculo?.cliente?.informacionTributaria?.nombreComercial;
  }

  async imprimir(recepcion: any) {
    console.log(recepcion);
    const pdfDefinition: any = await this._imprimirService.setearObjetoRecepcion(recepcion);
    await this._imprimirService.imprimir(pdfDefinition);
  }
}
