import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaRecepcionVehiculoComponent} from "./ruta/ruta-recepcion-vehiculo/ruta-recepcion-vehiculo.component";
import {RutaFormularioRecepcionVehiculoComponent} from "./ruta/ruta-formulario-recepcion-vehiculo/ruta-formulario-recepcion-vehiculo.component";

const routes: Routes = [
  {
    path: '',
    component: RutaRecepcionVehiculoComponent,
    data: {
      breadcrumb: 'Recepciones de Vehículo',
    },
  },
  {
    path: 'formulario/:idRecepcion',
    component: RutaFormularioRecepcionVehiculoComponent,
    data: {
      breadcrumb: 'Ver Información',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecepcionVehiculoRoutingModule { }
