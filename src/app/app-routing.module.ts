import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RutaNoEncontradaComponent} from "./rutas/ruta-no-encontrada/ruta-no-encontrada.component";
import {TieneConfiguracionGuardService} from "./servicios/guards/tiene-configuracion";

const routes: Routes = [
  {
    path: 'app',
    loadChildren: () =>
      import('./modulos/modulos.module').then((modulo) => modulo.ModulosModule),
      data: {
        breadcrumb: 'Inicio',
      },
  },
  // {
  //   path: '',
  //   component: RutaMenuInicioComponent,
  //   data: {
  //     breadcrumb: 'Inicio',
  //   },
  // },
  {
    path: 'no-encontrada',
    component: RutaNoEncontradaComponent,
  },
  {
    path: '',
    redirectTo: 'app',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
