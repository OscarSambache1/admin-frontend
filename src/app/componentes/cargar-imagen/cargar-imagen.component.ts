import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder} from "@angular/forms";

@Component({
  selector: 'app-cargar-imagen',
  templateUrl: './cargar-imagen.component.html',
  styleUrls: ['./cargar-imagen.component.scss']
})
export class CargarImagenComponent implements OnInit {

  imagen: any;

  @Input()
  pathImagen!: any;

  @Input()
  width = '100';

  @Input()
  height = 'auto';

  @Input()
  ocultarInput?: boolean;

  @Output()
  emitirImagen: EventEmitter<any> = new EventEmitter<any>();

  src: any;
  ngOnInit(): void {
    this.src = this.pathImagen;
  }

  constructor(
    private readonly _formBuilder: FormBuilder,
  ) {
  }

  onBasicUploadAuto(eventoImagen: any) {
    const fileList: FileList = eventoImagen.target.files;
    if (eventoImagen.target.files && eventoImagen.target.files[0]) {
      this.imagen = fileList[0];
      const reader = new FileReader();
      reader.onload = (event: any) => {
        this.src = event.target.result;
      };
      reader.readAsDataURL(eventoImagen.target.files[0]);
    } else {
      this.src = undefined;
      this.imagen = null;
    }
    this.emitirImagen.emit(this.imagen);
  }

}
