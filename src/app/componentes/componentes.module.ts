import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormularioMarcaComponent} from './formularios/formulario-marca/formulario-marca.component';
import {IMPORTS_MODULO} from "../modulos/imports/imports-modulo";
import {ModalCrearEditarMarcaComponent} from './modales/modal-crear-editar-marca/modal-crear-editar-marca.component';
import {FormularioModeloComponent} from './formularios/formulario-modelo/formulario-modelo.component';
import {ModalCrearEditarModeloComponent} from './modales/modal-crear-editar-modelo/modal-crear-editar-modelo.component';
import {FormularioAccesorioComponent} from './formularios/formulario-accesorio/formulario-accesorio.component';
import {ModalCrearEditarAccesorioComponent} from './modales/modal-crear-editar-accesorio/modal-crear-editar-accesorio.component';
import {FormularioBancoComponent} from './formularios/formulario-banco/formulario-banco.component';
import {ModalCrearEditarBancoComponent} from './modales/modal-crear-editar-banco/modal-crear-editar-banco.component';
import {FormularioFormaPagoComponent} from './formularios/formulario-forma-pago/formulario-forma-pago.component';
import {ModalCrearEditarFormaPagoComponent} from './modales/modal-crear-editar-forma-pago/modal-crear-editar-forma-pago.component';
import {FormularioTipoCambioComponent} from './formularios/formulario-tipo-cambio/formulario-tipo-cambio.component';
import {ModalCrearEditarTipoCambioComponent} from './modales/modal-crear-editar-tipo-cambio/modal-crear-editar-tipo-cambio.component';
import {FormularioTipoCombustibleComponent} from './formularios/formulario-tipo-combustible/formulario-tipo-combustible.component';
import {ModalCrearEditarTipoCombustibleComponent} from './modales/modal-crear-editar-tipo-combustible/modal-crear-editar-tipo-combustible.component';
import {FormularioTipoVehiculoComponent} from './formularios/formulario-tipo-vehiculo/formulario-tipo-vehiculo.component';
import {ModalCrearEditarTipoVehiculoComponent} from './modales/modal-crear-editar-tipo-vehiculo/modal-crear-editar-tipo-vehiculo.component';
import {FormularioImpuestoComponent} from './formularios/formulario-impuesto/formulario-impuesto.component';
import {ModalCrearEditarImpuestoComponent} from './modales/modal-crear-editar-impuesto/modal-crear-editar-impuesto.component';
import {FormularioSeccionComponent} from './formularios/formulario-seccion/formulario-seccion.component';
import {ModalCrearEditarSeccionComponent} from './modales/modal-crear-editar-seccion/modal-crear-editar-seccion.component';
import {TablaTarifaImpuestoComponent} from './tablas/tabla-tarifa-impuesto/tabla-tarifa-impuesto.component';
import {FormularioTarifaImpuestoComponent} from './formularios/formulario-tarifa-impuesto/formulario-tarifa-impuesto.component';
import {ModalCrearEditarTarifaImpuestoComponent} from './modales/modal-crear-editar-tarifa-impuesto/modal-crear-editar-tarifa-impuesto.component';
import {FormularioSubseccionComponent} from './formularios/formulario-subseccion/formulario-subseccion.component';
import {ModalCrearEditarSubseccionComponent} from './modales/modal-crear-editar-subseccion/modal-crear-editar-subseccion.component';
import {FormularioProveedorComponent} from './formularios/formulario-proveedor/formulario-proveedor.component';
import {ModalCrearEditarProveedorComponent} from './modales/modal-crear-editar-proveedor/modal-crear-editar-proveedor.component';
import {FormularioAgregarContactosComponent} from './formularios/formulario-agregar-contactos/formulario-agregar-contactos.component';
import {TablaCuentaBancoComponent} from './tablas/tabla-cuenta-banco/tabla-cuenta-banco.component';
import {FormularioCuentaBancoComponent} from './formularios/formulario-cuenta-banco/formulario-cuenta-banco.component';
import {ModalCrearEditarCuentaBancoComponent} from './modales/modal-crear-editar-cuenta-banco/modal-crear-editar-cuenta-banco.component';
import {TablaContactosComponent} from './tablas/tabla-contactos/tabla-contactos.component';
import {ModalCrearEditarContactoComponent} from './modales/modal-crear-editar-contacto/modal-crear-editar-contacto.component';
import {FormularioContactoComponent} from './formularios/formulario-contacto/formulario-contacto.component';
import {FormularioProductoComponent} from './formularios/formulario-producto/formulario-producto.component';
import {ModalCrearEditarProductoComponent} from './modales/modal-crear-editar-producto/modal-crear-editar-producto.component';
import {TablaProveedorArticuloComponent} from './tablas/tabla-proveedor-articulo/tabla-proveedor-articulo.component';
import {FormularioProveedorArticuloComponent} from './formularios/formulario-proveedor-articulo/formulario-proveedor-articulo.component';
import {ModalCrearEditarProveedorArticuloComponent} from './modales/modal-crear-editar-proveedor-articulo/modal-crear-editar-proveedor-articulo.component';
import {TablaArticuloImpuestoComponent} from './tablas/tabla-articulo-impuesto/tabla-articulo-impuesto.component';
import {FormularioArticuloImpuestoComponent} from './formularios/formulario-articulo-impuesto/formulario-articulo-impuesto.component';
import {ModalCrearEditarArticuloImpuestoComponent} from './modales/modal-crear-editar-articulo-impuesto/modal-crear-editar-articulo-impuesto.component';
import {CargarImagenComponent} from "./cargar-imagen/cargar-imagen.component";
import {FormularioServicioComponent} from './formularios/formulario-servicio/formulario-servicio.component';
import {ModalCrearEditarServicioComponent} from './modales/modal-crear-editar-servicio/modal-crear-editar-servicio.component';
import {FormularioGastoComponent} from './formularios/formulario-gasto/formulario-gasto.component';
import {FacturaComponent} from './factura/factura.component';
import {FormularioFacturaProveedorComponent} from './formularios/formulario-factura-proveedor/formulario-factura-proveedor.component';
import {TablaDetalleFacturaComponent} from './tablas/tabla-detalle-factura/tabla-detalle-factura.component';
import {ModalSeleccionarArticulosComponent} from './modales/modal-seleccionar-articulos/modal-seleccionar-articulos.component';
import {TablaTotalesFacturaComponent} from './tablas/tabla-totales-factura/tabla-totales-factura.component';
import {FormularioFacturaCabeceraComponent} from './formularios/formulario-factura-cabecera/formulario-factura-cabecera.component';
import {TablaAbonosFacturaComponent} from './tablas/tabla-abonos-factura/tabla-abonos-factura.component';
import {FormularioAbonoFacturaComponent} from './formularios/formulario-abono-factura/formulario-abono-factura.component';
import {ModalCrearEditarAbonoFacturaComponent} from './modales/modal-crear-editar-abono-factura/modal-crear-editar-abono-factura.component';
import {VerPdfComponent} from './ver-pdf/ver-pdf.component';
import {ModalStockBodegaComponent} from './modales/modal-stock-bodega/modal-stock-bodega.component';
import {FormularioStockBodegaComponent} from './formularios/formulario-stock-bodega/formulario-stock-bodega.component';
import {FormularioClienteComponent} from './formularios/formulario-cliente/formulario-cliente.component';
import {ModalCrearEditarClienteComponent} from './modales/modal-crear-editar-cliente/modal-crear-editar-cliente.component';
import {FormularioVehiculoComponent} from './formularios/formulario-vehiculo/formulario-vehiculo.component';
import {ModalCrearEditarVehiculoComponent} from './modales/modal-crear-editar-vehiculo/modal-crear-editar-vehiculo.component';
import {TablaVehiculoComponent} from './tablas/tabla-vehiculo/tabla-vehiculo.component';
import {FormularioConfiguracionComponent} from './formularios/formulario-configuracion/formulario-configuracion.component';
import {ModalPagosAbonosComponent} from './modales/modal-pagos-abonos/modal-pagos-abonos.component';
import {FormularioOrdenTrabajoComponent} from './formularios/formulario-orden-trabajo/formulario-orden-trabajo.component';
import {FormularioPresupuestoComponent} from './formularios/formulario-presupuesto/formulario-presupuesto.component';
import {FormularioFacturaComponent} from './formularios/formulario-factura/formulario-factura.component';
import {FormularioRecepcionComponent} from './formularios/formulario-recepcion/formulario-recepcion.component';
import {FormularioTrabajoRealizadoComponent} from './formularios/formulario-trabajo-realizado/formulario-trabajo-realizado.component';
import {TablaDetallesTrabajoComponent} from './tablas/tabla-detalles-trabajo/tabla-detalles-trabajo.component';
import {FormularioTrabajoComponent} from './formularios/formulario-trabajo/formulario-trabajo.component';
import {FormularioProformaComponent} from './formularios/formulario-proforma/formulario-proforma.component';
import {FormularioGastoFacturaComponent} from "./formularios/formulario-gasto-factura/formulario-gasto-factura.component";
import {ModalCrearEditarGastoComponent} from "./modales/modal-crear-editar-gasto/modal-crear-editar-gasto.component";
import { FormularioProformaRapidaComponent } from './formularios/formulario-proforma-rapida/formulario-proforma-rapida.component';

@NgModule({
  declarations: [
    FormularioMarcaComponent,
    ModalCrearEditarMarcaComponent,
    FormularioModeloComponent,
    ModalCrearEditarModeloComponent,
    FormularioAccesorioComponent,
    ModalCrearEditarAccesorioComponent,
    FormularioBancoComponent,
    ModalCrearEditarBancoComponent,
    FormularioFormaPagoComponent,
    ModalCrearEditarFormaPagoComponent,
    FormularioTipoCambioComponent,
    ModalCrearEditarTipoCambioComponent,
    FormularioTipoCombustibleComponent,
    ModalCrearEditarTipoCombustibleComponent,
    FormularioTipoVehiculoComponent,
    ModalCrearEditarTipoVehiculoComponent,
    FormularioImpuestoComponent,
    ModalCrearEditarImpuestoComponent,
    FormularioSeccionComponent,
    ModalCrearEditarSeccionComponent,
    TablaTarifaImpuestoComponent,
    FormularioTarifaImpuestoComponent,
    ModalCrearEditarTarifaImpuestoComponent,
    FormularioSubseccionComponent,
    ModalCrearEditarSubseccionComponent,
    FormularioProveedorComponent,
    ModalCrearEditarProveedorComponent,
    FormularioAgregarContactosComponent,
    TablaCuentaBancoComponent,
    FormularioCuentaBancoComponent,
    ModalCrearEditarCuentaBancoComponent,
    TablaContactosComponent,
    ModalCrearEditarContactoComponent,
    FormularioContactoComponent,
    FormularioProductoComponent,
    ModalCrearEditarProductoComponent,
    TablaProveedorArticuloComponent,
    FormularioProveedorArticuloComponent,
    ModalCrearEditarProveedorArticuloComponent,
    TablaArticuloImpuestoComponent,
    FormularioArticuloImpuestoComponent,
    ModalCrearEditarArticuloImpuestoComponent,
    CargarImagenComponent,
    FormularioServicioComponent,
    ModalCrearEditarServicioComponent,
    FormularioGastoComponent,
    FacturaComponent,
    FormularioFacturaProveedorComponent,
    TablaDetalleFacturaComponent,
    ModalSeleccionarArticulosComponent,
    TablaTotalesFacturaComponent,
    FormularioFacturaCabeceraComponent,
    TablaAbonosFacturaComponent,
    FormularioAbonoFacturaComponent,
    ModalCrearEditarAbonoFacturaComponent,
    VerPdfComponent,
    ModalStockBodegaComponent,
    FormularioStockBodegaComponent,
    FormularioClienteComponent,
    ModalCrearEditarClienteComponent,
    FormularioVehiculoComponent,
    ModalCrearEditarVehiculoComponent,
    TablaVehiculoComponent,
    FormularioConfiguracionComponent,
    ModalPagosAbonosComponent,
    FormularioOrdenTrabajoComponent,
    FormularioPresupuestoComponent,
    FormularioFacturaComponent,
    FormularioRecepcionComponent,
    FormularioTrabajoRealizadoComponent,
    TablaDetallesTrabajoComponent,
    FormularioTrabajoComponent,
    FormularioProformaComponent,
    FormularioGastoFacturaComponent,
    ModalCrearEditarGastoComponent,
    FormularioProformaRapidaComponent,
  ],
  imports: [
    CommonModule,
    ...IMPORTS_MODULO,
  ],
  exports: [
    FormularioMarcaComponent,
    ModalCrearEditarMarcaComponent,
    FormularioModeloComponent,
    ModalCrearEditarModeloComponent,
    FormularioAccesorioComponent,
    ModalCrearEditarAccesorioComponent,
    FormularioGastoComponent,
    FacturaComponent,
    FormularioConfiguracionComponent,
    FormularioRecepcionComponent,
    FormularioPresupuestoComponent,
    FormularioOrdenTrabajoComponent,
    FormularioFacturaComponent,
    FormularioTrabajoRealizadoComponent,
    FormularioTrabajoComponent,
    FormularioProformaComponent,
    TablaDetalleFacturaComponent,
    FormularioGastoFacturaComponent,
    ModalCrearEditarGastoComponent,
    FormularioProformaRapidaComponent,
  ]
})
export class ComponentesModule {
}
