import {Component, Inject, OnInit} from '@angular/core';
import {TipoCombustibleInterface} from "../../../modulos/configuracion-datos/interfaces/tipo-combustible.interface";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-modal-crear-editar-tipo-combustible',
  templateUrl: './modal-crear-editar-tipo-combustible.component.html',
  styleUrls: ['./modal-crear-editar-tipo-combustible.component.scss']
})
export class ModalCrearEditarTipoCombustibleComponent implements OnInit {
  tipoCombustibleACrear: TipoCombustibleInterface | undefined;

  constructor(
    public dialogRef: MatDialogRef<ModalCrearEditarTipoCombustibleComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { tipoCombustible: TipoCombustibleInterface }
  ) {}

  ngOnInit(): void {
    if (this.data) {
      this.tipoCombustibleACrear = this.data.tipoCombustible as TipoCombustibleInterface;
    }
  }

  crearEditar(dato: TipoCombustibleInterface | boolean): void {
    !dato
      ? (this.tipoCombustibleACrear = undefined)
      : (this.tipoCombustibleACrear = dato as TipoCombustibleInterface);
  }

  enviarDatosFormulario(): void {
    this.dialogRef.close(this.tipoCombustibleACrear);
  }

  cancelarModal(): void {
    this.dialogRef.close();
  }

}
