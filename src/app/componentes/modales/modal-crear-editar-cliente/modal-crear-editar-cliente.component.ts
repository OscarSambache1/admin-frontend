import {Component, Inject, OnInit} from '@angular/core';
import {ClienteInterface} from "../../../modulos/configuracion-datos/interfaces/cliente.interface";
import {ContactoInterface} from "../../../modulos/configuracion-datos/interfaces/contacto.interface";
import {CuentaBancoInterface} from "../../../modulos/configuracion-datos/interfaces/cuenta-banco.interface";
import {ClienteRestService} from "../../../modulos/configuracion-datos/servicios/cliente-rest.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {VehiculoInterface} from "../../../modulos/configuracion-datos/interfaces/vehiculo.interface";

@Component({
  selector: 'app-modal-crear-editar-cliente',
  templateUrl: './modal-crear-editar-cliente.component.html',
  styleUrls: ['./modal-crear-editar-cliente.component.scss']
})
export class ModalCrearEditarClienteComponent implements OnInit {

  clienteACrear: ClienteInterface | undefined;
  contactos?: ContactoInterface[] = [];
  vehiculos?: VehiculoInterface[] = [];
  cuentasBancos?: CuentaBancoInterface[] = [];
  constructor(
    private readonly _clienteRestService: ClienteRestService,
    public dialogRef: MatDialogRef<ModalCrearEditarClienteComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { cliente: ClienteInterface, soloVer: boolean }
  ) {}

  ngOnInit(): void {
    if (this.data && this.data.cliente) {
      this.clienteACrear = this.data.cliente as ClienteInterface;
      this.vehiculos = this.data.cliente.vehiculos;
      this.contactos = this.data.cliente.contactos;
      this.cuentasBancos = this.data.cliente.cuentasBanco;
    }
  }

  crearEditar(dato: ClienteInterface | boolean): void {
    !dato
      ? (this.clienteACrear = undefined)
      : (this.clienteACrear = dato as ClienteInterface);
  }

  enviarDatosFormulario(): void {
    if (this.clienteACrear) {
      this.clienteACrear.contactos = this.contactos;
      this.clienteACrear.cuentasBanco = this.cuentasBancos;
      this.clienteACrear.vehiculos = this.vehiculos;
    }
    this.dialogRef.close(this.clienteACrear);
  }

  cancelarModal(): void {
    this.dialogRef.close();
  }

  setearCuentasBancos(cuentasBanco: CuentaBancoInterface[]) {
    this.cuentasBancos = cuentasBanco;
  }

  setearContactos(contactos: ContactoInterface[]) {
    this.contactos = contactos;
  }

  setearVehiculos(vehiculos: VehiculoInterface[]) {
    this.vehiculos = vehiculos;
  }
}
