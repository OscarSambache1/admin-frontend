import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {ArticuloInterface} from "../../../modulos/configuracion-datos/interfaces/articulo.interface";
import {ProveedorArticuloInterface} from "../../../modulos/configuracion-datos/interfaces/proveedor-articulo.interface";
import {ArticuloImpuestoInterface} from "../../../modulos/configuracion-datos/interfaces/articulo-impuesto.interface";
import {environment} from "../../../../environments/environment";
import {ArticuloRestService} from "../../../modulos/configuracion-datos/servicios/articulo-rest.service";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import { Buffer } from 'buffer';
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-modal-crear-editar-producto',
  templateUrl: './modal-crear-editar-producto.component.html',
  styleUrls: ['./modal-crear-editar-producto.component.scss']
})
export class ModalCrearEditarProductoComponent implements OnInit {

  productoACrear: ArticuloInterface | undefined;
  proveedoresArticulo?: ProveedorArticuloInterface[] = [];
  articulosTarifaImpuesto?: ArticuloImpuestoInterface[] = [];
  imagen: any;
  constructor(
    private readonly _articuloRestService : ArticuloRestService,
    private readonly _notificacionService: NotificacionService,
    public dialogRef: MatDialogRef<ModalCrearEditarProductoComponent>,
    public readonly _domSanitizer: DomSanitizer,
    @Inject(MAT_DIALOG_DATA)
    public data: { producto: ArticuloInterface, soloVer: boolean }
  ) {}

  ngOnInit(): void {
    if (this.data) {
      this.productoACrear = this.data.producto as ArticuloInterface;
    }
  }

  crearEditar(dato: ArticuloInterface | boolean): void {
    !dato
      ? (this.productoACrear = undefined)
      : (this.productoACrear = dato as ArticuloInterface);
  }

  async enviarDatosFormulario() {
    if (this.productoACrear) {
      this.productoACrear.articulosTarifaImpuesto = this.articulosTarifaImpuesto;
      this.productoACrear.proveedoresArticulo = this.proveedoresArticulo;
      this.productoACrear.imagen = this.imagen;
    }
    let codigoBarrasProductoValido = true;
    if (this.productoACrear?.codigoBarras) {
      codigoBarrasProductoValido = await this.validarCodigoBarrasProducto();
    }
    const codigoProductoValido = await this.validarCodigoProducto();
    if (codigoProductoValido && codigoBarrasProductoValido) {
      this.dialogRef.close(this.productoACrear);
    }
  }

  cancelarModal(): void {
    this.dialogRef.close();
  }

  setearArticulosImpuestos(articulosTarifaImpuesto: ArticuloImpuestoInterface[]) {
    this.articulosTarifaImpuesto = articulosTarifaImpuesto;
  }

  setearProveedoresArticulo(proveedoresArticulo: ProveedorArticuloInterface[]) {
    this.proveedoresArticulo = proveedoresArticulo;
  }

  cargarImagen(imagen: any) {
    this.imagen = imagen;
  }

  setearUrlImagen() {
    if (this.data.producto) {
      if (this.data?.producto?.archivos?.length) {
        const imagen = this.data?.producto?.archivos[0];
        const buffer = imagen.buffer?.data;
        return this._domSanitizer.bypassSecurityTrustUrl(`data:${imagen.mimetype};base64, ${Buffer.from(buffer)}`) as any;
      }
      return ``;
    } else {
      return null;
    }
  }

  async validarCodigoProducto(): Promise<boolean> {
    const codigo = this.productoACrear?.codigo;
    const consulta = {
      where: {
        codigo,
        esServicio: 0,
      }
    };
    const impuesto$ = this._articuloRestService.findAll(
      'busqueda=' + JSON.stringify(consulta),
    );

    const respuestaProducto = await (impuesto$.toPromise());
    let productos = respuestaProducto[0];
    if (this.data?.producto) {
      productos = productos.filter(producto => producto.codigo !== this.data.producto.codigo);
    }
    if (!!productos.length) {
      this._notificacionService.modalInfo(
        'warning',
        'Advertencia',
        `Ya existe un repuesto registrado en el sistema con el código: ${codigo}`
      );
      return false;
    } else {
      return true;
    }

  }

  async validarCodigoBarrasProducto(): Promise<boolean> {
    const codigoBarras = this.productoACrear?.codigoBarras;
    const consulta = {
      where: {
        codigoBarras,
        esServicio: 0,
      }
    };
    const impuesto$ = this._articuloRestService.findAll(
      'busqueda=' + JSON.stringify(consulta),
    );

    const respuestaProducto = await (impuesto$.toPromise());
    let productos = respuestaProducto[0];
    if (this.data?.producto) {
      productos = productos.filter(producto => producto.codigoBarras !== this.data.producto.codigoBarras);
    }
    if (!!productos.length) {
      this._notificacionService.modalInfo(
        'warning',
        'Advertencia',
        `Ya existe un repuesto registrado en el sistema con el código dde barras: ${codigoBarras}`
      );
      return false;
    } else {
      return true;
    }

  }

}
