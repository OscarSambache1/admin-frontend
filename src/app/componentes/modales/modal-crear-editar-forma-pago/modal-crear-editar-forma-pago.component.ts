import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FormaPagoInterface} from "../../../modulos/configuracion-datos/interfaces/forma-pago.interface";

@Component({
  selector: 'app-modal-crear-editar-forma-pago',
  templateUrl: './modal-crear-editar-forma-pago.component.html',
  styleUrls: ['./modal-crear-editar-forma-pago.component.scss']
})
export class ModalCrearEditarFormaPagoComponent implements OnInit {

  formaPagoACrear: FormaPagoInterface | undefined;

  constructor(
    public dialogRef: MatDialogRef<ModalCrearEditarFormaPagoComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { formaPago: FormaPagoInterface, soloVer?: boolean }
  ) {}

  ngOnInit(): void {
    if (this.data) {
      this.formaPagoACrear = this.data.formaPago as FormaPagoInterface;
    }
  }

  crearEditar(dato: FormaPagoInterface | boolean): void {
    !dato
      ? (this.formaPagoACrear = undefined)
      : (this.formaPagoACrear = dato as FormaPagoInterface);
  }

  enviarDatosFormulario(): void {
    this.dialogRef.close(this.formaPagoACrear);
  }

  cancelarModal(): void {
    this.dialogRef.close();
  }

}
