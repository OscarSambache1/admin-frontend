import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {CuentaBancoInterface} from "../../../modulos/configuracion-datos/interfaces/cuenta-banco.interface";

@Component({
  selector: 'app-modal-crear-editar-cuenta-banco',
  templateUrl: './modal-crear-editar-cuenta-banco.component.html',
  styleUrls: ['./modal-crear-editar-cuenta-banco.component.scss']
})
export class ModalCrearEditarCuentaBancoComponent implements OnInit {


  cuentaBancoACrear: CuentaBancoInterface | undefined;

  constructor(
    public dialogRef: MatDialogRef<ModalCrearEditarCuentaBancoComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { cuentaBanco: CuentaBancoInterface }
  ) {}

  ngOnInit(): void {
    if (this.data) {
      this.cuentaBancoACrear = this.data.cuentaBanco as CuentaBancoInterface;
    }
  }

  crearEditar(dato: CuentaBancoInterface | boolean): void {
    !dato
      ? (this.cuentaBancoACrear = undefined)
      : (this.cuentaBancoACrear = dato as CuentaBancoInterface);
  }

  enviarDatosFormulario(): void {
    this.dialogRef.close(this.cuentaBancoACrear);
  }

  cancelarModal(): void {
    this.dialogRef.close();
  }

}
