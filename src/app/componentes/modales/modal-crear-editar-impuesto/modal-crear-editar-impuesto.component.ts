import {Component, Inject, OnInit} from '@angular/core';
import {ImpuestoInterface} from "../../../modulos/configuracion-datos/interfaces/impuesto.interface";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {ImpuestoRestService} from "../../../modulos/configuracion-datos/servicios/impuesto-rest.service";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {TarifaImpuestoInterface} from "../../../modulos/configuracion-datos/interfaces/tarifa-impuesto.interface";

@Component({
  selector: 'app-modal-crear-editar-impuesto',
  templateUrl: './modal-crear-editar-impuesto.component.html',
  styleUrls: ['./modal-crear-editar-impuesto.component.scss']
})
export class ModalCrearEditarImpuestoComponent implements OnInit {

  impuestoACrear: ImpuestoInterface | undefined;
  tarifasImpuesto?: TarifaImpuestoInterface[] = [];
  constructor(
    public dialogRef: MatDialogRef<ModalCrearEditarImpuestoComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { impuesto: ImpuestoInterface },
    private readonly _impuestoRestService: ImpuestoRestService,
    private readonly _notificacionService: NotificacionService,
  ) {}

  ngOnInit(): void {
    if (this.data) {
      this.impuestoACrear = this.data.impuesto as ImpuestoInterface;
    }
  }

  crearEditar(dato: ImpuestoInterface | boolean): void {
    !dato
      ? (this.impuestoACrear = undefined)
      : (this.impuestoACrear = dato as ImpuestoInterface);
  }

  async enviarDatosFormulario(): Promise<void> {
    const codigoImpuestoValido = await this.validarCodigoImpuesto();
    if (codigoImpuestoValido) {
      if (this.impuestoACrear){
        this.impuestoACrear.tarifasImpuestos = this.tarifasImpuesto;
        this.dialogRef.close(this.impuestoACrear);
      }
    }
  }

  cancelarModal(): void {
    this.dialogRef.close();
  }

  async validarCodigoImpuesto(): Promise<boolean> {
    const codigo = this.impuestoACrear?.codigo;
    const consulta = {
      where: {
        codigo,
      }
    };
    const impuesto$ = this._impuestoRestService.findAll(
      'busqueda=' + JSON.stringify(consulta),
    );

    const respuestaImpuesto = await (impuesto$.toPromise());
    let impuestos = respuestaImpuesto[0];
    if (this.data?.impuesto) {
      impuestos = impuestos.filter(impuesto => impuesto.codigo !== this.data.impuesto.codigo);
    }
    if (!!impuestos.length) {
      this._notificacionService.modalInfo(
        'warning',
        'Advertencia',
        `Ya existe un impuesto registrado en el sistema con el código: ${codigo}`
      );
      return false;
    } else {
      return true;
    }

  }

  setearTarifas(tarifasImpuesto: TarifaImpuestoInterface[]) {
    this.tarifasImpuesto = tarifasImpuesto;
  }
}
