import {Component, Inject, OnInit} from '@angular/core';
import {ArticuloImpuestoInterface} from "../../../modulos/configuracion-datos/interfaces/articulo-impuesto.interface";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-modal-crear-editar-articulo-impuesto',
  templateUrl: './modal-crear-editar-articulo-impuesto.component.html',
  styleUrls: ['./modal-crear-editar-articulo-impuesto.component.scss']
})
export class ModalCrearEditarArticuloImpuestoComponent implements OnInit {

  articuloImpuestoACrear: ArticuloImpuestoInterface | undefined;

  constructor(
    public dialogRef: MatDialogRef<ModalCrearEditarArticuloImpuestoComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { articuloImpuesto: ArticuloImpuestoInterface }
  ) {}

  ngOnInit(): void {
    if (this.data) {
      this.articuloImpuestoACrear = this.data.articuloImpuesto as ArticuloImpuestoInterface;
    }
  }

  crearEditar(dato: ArticuloImpuestoInterface | boolean): void {
    !dato
      ? (this.articuloImpuestoACrear = undefined)
      : (this.articuloImpuestoACrear = dato as ArticuloImpuestoInterface);
  }

  enviarDatosFormulario(): void {
    this.dialogRef.close(this.articuloImpuestoACrear);
  }

  cancelarModal(): void {
    this.dialogRef.close();
  }
}
