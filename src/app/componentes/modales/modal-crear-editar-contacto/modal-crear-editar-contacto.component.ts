import {Component, Inject, OnInit} from '@angular/core';
import {ContactoInterface} from "../../../modulos/configuracion-datos/interfaces/contacto.interface";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-modal-crear-editar-contacto',
  templateUrl: './modal-crear-editar-contacto.component.html',
  styleUrls: ['./modal-crear-editar-contacto.component.scss']
})
export class ModalCrearEditarContactoComponent implements OnInit {

  contactoACrear: ContactoInterface | undefined;

  constructor(
    public dialogRef: MatDialogRef<ModalCrearEditarContactoComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { contacto: ContactoInterface }
  ) {}

  ngOnInit(): void {
    if (this.data) {
      this.contactoACrear = this.data.contacto as ContactoInterface;
    }
  }

  crearEditar(dato: ContactoInterface | boolean): void {
    !dato
      ? (this.contactoACrear = undefined)
      : (this.contactoACrear = dato as ContactoInterface);
  }

  enviarDatosFormulario(): void {
    this.dialogRef.close(this.contactoACrear);
  }

  cancelarModal(): void {
    this.dialogRef.close();
  }

}
