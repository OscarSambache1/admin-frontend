import {Component, Inject, OnInit} from '@angular/core';
import {SubseccionInterface} from "../../../modulos/configuracion-datos/interfaces/subseccion.interface";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-modal-crear-editar-subseccion',
  templateUrl: './modal-crear-editar-subseccion.component.html',
  styleUrls: ['./modal-crear-editar-subseccion.component.scss']
})
export class ModalCrearEditarSubseccionComponent implements OnInit {

  subseccionACrear: SubseccionInterface | undefined;

  constructor(
    public dialogRef: MatDialogRef<ModalCrearEditarSubseccionComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { subseccion: SubseccionInterface }
  ) {}

  ngOnInit(): void {
    if (this.data) {
      this.subseccionACrear = this.data.subseccion as SubseccionInterface;
    }
  }

  crearEditar(dato: SubseccionInterface | boolean): void {
    !dato
      ? (this.subseccionACrear = undefined)
      : (this.subseccionACrear = dato as SubseccionInterface);
  }

  enviarDatosFormulario(): void {
    if (this.subseccionACrear?.seccion?.id) {
      this.subseccionACrear.seccion = this.subseccionACrear?.seccion.id as any;
    }
    this.dialogRef.close(this.subseccionACrear);
  }

  cancelarModal(): void {
    this.dialogRef.close();
  }
}
