import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {AccesorioInterface} from "../../../modulos/configuracion-datos/interfaces/accesorio.interface";

@Component({
  selector: 'app-modal-crear-editar-accesorio',
  templateUrl: './modal-crear-editar-accesorio.component.html',
  styleUrls: ['./modal-crear-editar-accesorio.component.scss']
})
export class ModalCrearEditarAccesorioComponent implements OnInit {
  accesorioACrear: AccesorioInterface | undefined;

  constructor(
    public dialogRef: MatDialogRef<ModalCrearEditarAccesorioComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { accesorio: AccesorioInterface }
  ) {}

  ngOnInit(): void {
    if (this.data) {
      this.accesorioACrear = this.data.accesorio as AccesorioInterface;
    }
  }

  crearEditar(dato: AccesorioInterface | boolean): void {
    !dato
      ? (this.accesorioACrear = undefined)
      : (this.accesorioACrear = dato as AccesorioInterface);
  }

  enviarDatosFormulario(): void {
    this.dialogRef.close(this.accesorioACrear);
  }

  cancelarModal(): void {
    this.dialogRef.close();
  }
}
