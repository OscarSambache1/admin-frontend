import {Component, Inject, OnInit} from '@angular/core';
import {TipoVehiculoInterface} from "../../../modulos/configuracion-datos/interfaces/tipo-vehiculo.interface";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-modal-crear-editar-tipo-vehiculo',
  templateUrl: './modal-crear-editar-tipo-vehiculo.component.html',
  styleUrls: ['./modal-crear-editar-tipo-vehiculo.component.scss']
})
export class ModalCrearEditarTipoVehiculoComponent implements OnInit {

  tipoVehiculoACrear: TipoVehiculoInterface | undefined;

  constructor(
    public dialogRef: MatDialogRef<ModalCrearEditarTipoVehiculoComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { tipoVehiculo: TipoVehiculoInterface }
  ) {}

  ngOnInit(): void {
    if (this.data) {
      this.tipoVehiculoACrear = this.data.tipoVehiculo as TipoVehiculoInterface;
    }
  }

  crearEditar(dato: TipoVehiculoInterface | boolean): void {
    !dato
      ? (this.tipoVehiculoACrear = undefined)
      : (this.tipoVehiculoACrear = dato as TipoVehiculoInterface);
  }

  enviarDatosFormulario(): void {
    this.dialogRef.close(this.tipoVehiculoACrear);
  }

  cancelarModal(): void {
    this.dialogRef.close();
  }

}
