import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {DxDataGridComponent} from "devextreme-angular";
import {ROWS, ROWS_PER_PAGE} from "../../../constantes/table-config";
import {ArticuloInterface} from "../../../modulos/configuracion-datos/interfaces/articulo.interface";
import {CargandoService} from "../../../servicios/cargando.service";
import {ArticuloRestService} from "../../../modulos/configuracion-datos/servicios/articulo-rest.service";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {DomSanitizer} from "@angular/platform-browser";
import { Buffer } from 'buffer';
import {ModalCrearEditarProductoComponent} from "../modal-crear-editar-producto/modal-crear-editar-producto.component";
import {ModalCrearEditarServicioComponent} from "../modal-crear-editar-servicio/modal-crear-editar-servicio.component";

@Component({
  selector: 'app-modal-seleccionar-articulos',
  templateUrl: './modal-seleccionar-articulos.component.html',
  styleUrls: ['./modal-seleccionar-articulos.component.scss']
})
export class ModalSeleccionarArticulosComponent implements OnInit {
  @ViewChild(DxDataGridComponent)
  dataGrid!: DxDataGridComponent;

  rows = ROWS;
  rowsPerPage = ROWS_PER_PAGE;
  datos: ArticuloInterface[] = [];
  pageSize!: number;
  disabledTable!: boolean;
  pagination: number[] = [];
  paginationStart!: number;


  consulta: any = {
    relations: [
      'seccion',
      'subseccion',
      'proveedoresArticulo',
      'proveedoresArticulo.proveedor',
      'articulosTarifaImpuesto',
      'articulosTarifaImpuesto.tarifaImpuesto',
      'articulosTarifaImpuesto.tarifaImpuesto.impuesto',
      'archivos'
    ],
    where: {
      esServicio: 0,
      estado: 1,
    },
    order: {
      id: 'DESC',
    },
  };
  constructor(
    private readonly _cargandoService: CargandoService,
    private readonly _articuloRestService: ArticuloRestService,
    private readonly _notificacionService: NotificacionService,
    public readonly _domSanitizer: DomSanitizer,
    private readonly _matDialog: MatDialog,
    public dialogRef: MatDialogRef<ModalSeleccionarArticulosComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      esServicio: 0 | 1,
      esOrdenTrabajo: 0 | 1,
      esTrabajoRealizado: 0 | 1,
      esFactura: 0 | 1,
      idsRepuestos: number[],
    }
  ) { }

  ngOnInit(): void {
    this.pagination = ROWS_PER_PAGE;
    this.paginationStart = ROWS;
    this.cargarDatosIniciales(this.consulta);
  }

  cargarDatosIniciales(consulta: {}): void {
    this._cargandoService.habilitar();
    this.consulta.where.esServicio = this.data.esServicio;
    const producto$ = this._articuloRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    producto$.subscribe({
      next: (producto) => {
        this.datos = producto[0].filter(producto => {
          return !this.data.idsRepuestos?.find(idRepuesto => idRepuesto === producto.id);
        });
        this.pageSize = this.datos.length;
        this.disabledTable = false;
        this._cargandoService.deshabilitar();
      },
      error: (error) => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
      },
    });
  }

  onToolbarPreparing(evento: any): void {
    evento.toolbarOptions.items.unshift(
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Agregar nuevo registro',
          onClick: this.add.bind(this, undefined),
          icon: 'plus',
        },
      },
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Recargar datos de la tabla',
          icon: 'refresh',
          onClick: this.onRefreshDataGrid.bind(this),
        },
      }
    );
  }

  private onRefreshDataGrid(): void {
    this.cargarDatosIniciales(this.consulta);
    void this.dataGrid.instance.refresh();
  }

  setearUrlImagen(producto: ArticuloInterface) {
    if (producto) {
      if (producto.archivos?.length) {
        const imagen = producto.archivos[0];
        const buffer = imagen.buffer?.data;
        return this._domSanitizer.bypassSecurityTrustUrl(`data:${imagen.mimetype};base64, ${Buffer.from(buffer)}`) as any;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }
  enviarRepuestosSeleccionados() {
    if (this.data.esOrdenTrabajo || this.data.esTrabajoRealizado || this.data.esFactura) {
      if (!this.data.esServicio) {
        const registroStockCero = this.dataGrid.selectedRowKeys.some(registro => !registro.stockActual);
        if (!registroStockCero) {
          this.dialogRef.close(this.dataGrid.selectedRowKeys);
        } else {
          this._notificacionService.modalInfo(
            'warning',
            'Advertencia',
            'Seleccione repuestos con stock mayor a cero',
          );
        }
      } else {
        this.dialogRef.close(this.dataGrid.selectedRowKeys);
      }
    } else {
      this.dialogRef.close(this.dataGrid.selectedRowKeys);
    }
  }

  cancelarModal() {
    this.dialogRef.close();
  }

  revisarRegistrosSeleccionados() {
    return !this.dataGrid?.selectedRowKeys?.length;
  }

  add(producto?: ArticuloInterface): void {
    const modal = this.data.esServicio ? ModalCrearEditarServicioComponent : ModalCrearEditarProductoComponent;
    const dialogRef = this._matDialog.open((modal as any), {
      width: '1550px',
      data: {
        producto,
      },
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          if (producto) {
            response.id = producto.id;
          }
          if (this.data.esServicio) {
            this.crearEditarServicio(response);
          } else {
            this.crearEditarRepuesto(response);
          }
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

  crearEditarRepuesto(producto: ArticuloInterface): void {
    const formData: FormData = new FormData();
    formData.append('imagen', producto.imagen);
    formData.append('datos', JSON.stringify(producto));
    const producto$ = this._articuloRestService.crearEditarProductoServicio(formData, 0);

    producto$.subscribe({
      next: () => {
        this._notificacionService.modalInfo(
          'success',
          'Info',
          'Registro guardado correctamente',
        );
        this.onRefreshDataGrid();
      },
      error: (err) => {
        console.error({
          mensage: 'Error guardar registro',
          error: err,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al guardar el registro',
        );
      },
    });
  }

  crearEditarServicio(servicio: ArticuloInterface): void {
    const formData: FormData = new FormData();
    formData.append('imagen', servicio.imagen);
    formData.append('datos', JSON.stringify(servicio));
    const servicio$ = this._articuloRestService.crearEditarProductoServicio(formData, 1);

    servicio$.subscribe({
      next: () => {
        this._notificacionService.modalInfo(
          'success',
          'Info',
          'Registro guardado correctamente',
        );
        this.onRefreshDataGrid();
      },
      error: (err) => {
        console.error({
          mensage: 'Error guardar registro',
          error: err,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al guardar el registro',
        );
      },
    });
  }

}
