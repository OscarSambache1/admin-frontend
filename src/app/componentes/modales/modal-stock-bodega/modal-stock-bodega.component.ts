import {Component, Inject, OnInit} from '@angular/core';
import {ArticuloInterface} from "../../../modulos/configuracion-datos/interfaces/articulo.interface";
import {ArticuloRestService} from "../../../modulos/configuracion-datos/servicios/articulo-rest.service";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-modal-stock-bodega',
  templateUrl: './modal-stock-bodega.component.html',
  styleUrls: ['./modal-stock-bodega.component.scss']
})
export class ModalStockBodegaComponent implements OnInit {
  productoACrear: ArticuloInterface | undefined;
  constructor(
    private readonly _articuloRestService : ArticuloRestService,
    private readonly _notificacionService: NotificacionService,
    public dialogRef: MatDialogRef<ModalStockBodegaComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { producto: ArticuloInterface }
  ) {}

  ngOnInit(): void {
    if (this.data) {
      this.productoACrear = this.data.producto as ArticuloInterface;
    }
  }

  crearEditar(dato: ArticuloInterface | boolean): void {
    !dato
      ? (this.productoACrear = undefined)
      : (this.productoACrear = dato as ArticuloInterface);
  }

  async enviarDatosFormulario() {
    this.dialogRef.close(this.productoACrear);
  }

  cancelarModal(): void {
    this.dialogRef.close();
  }
}
