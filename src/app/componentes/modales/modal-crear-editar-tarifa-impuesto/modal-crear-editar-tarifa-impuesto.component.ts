import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {TarifaImpuestoInterface} from "../../../modulos/configuracion-datos/interfaces/tarifa-impuesto.interface";

@Component({
  selector: 'app-modal-crear-editar-tarifa-impuesto',
  templateUrl: './modal-crear-editar-tarifa-impuesto.component.html',
  styleUrls: ['./modal-crear-editar-tarifa-impuesto.component.scss']
})
export class ModalCrearEditarTarifaImpuestoComponent implements OnInit {

  tarifaImpuestoACrear: TarifaImpuestoInterface | undefined;

  constructor(
    public dialogRef: MatDialogRef<ModalCrearEditarTarifaImpuestoComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { tarifaImpuesto: TarifaImpuestoInterface }
  ) {}

  ngOnInit(): void {
    if (this.data) {
      this.tarifaImpuestoACrear = this.data.tarifaImpuesto as TarifaImpuestoInterface;
    }
  }

  crearEditar(dato: TarifaImpuestoInterface | boolean): void {
    !dato
      ? (this.tarifaImpuestoACrear = undefined)
      : (this.tarifaImpuestoACrear = dato as TarifaImpuestoInterface);
  }

  enviarDatosFormulario(): void {
    this.dialogRef.close(this.tarifaImpuestoACrear);
  }

  cancelarModal(): void {
    this.dialogRef.close();
  }

}
