import {Component, Inject, OnInit} from '@angular/core';
import {CargandoService} from "../../../servicios/cargando.service";
import {ArticuloRestService} from "../../../modulos/configuracion-datos/servicios/articulo-rest.service";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {AbonoFacturaRestService} from "../../../modulos/gastos/servicios/abono-factura-rest.service";
import {GastoInterface} from "../../../modulos/gastos/interfaces/gasto.interface";
import {AbonoFacturaInterface} from "../../../modulos/gastos/interfaces/abono-factura.interface";
import {FacturaCabeceraInterface} from "../../../modulos/gastos/interfaces/factura-cabecera.interface";
import {EstadoGasto, EstadoTrabajo, TipoGasto} from "../../../enums/tipo-contacto.enums";
import {FacturaCabeceraRestService} from "../../../modulos/gastos/servicios/factura-cabecera-rest.service";

@Component({
  selector: 'app-modal-pagos-abonos',
  templateUrl: './modal-pagos-abonos.component.html',
  styleUrls: ['./modal-pagos-abonos.component.scss']
})
export class ModalPagosAbonosComponent implements OnInit {

  abonosPagos: AbonoFacturaInterface[] = [];
  tipoGasto!: TipoGasto;
  tablaValida = false;
  estadoAccion = EstadoTrabajo;
  factura!: FacturaCabeceraInterface;
  constructor(
    private readonly _cargandoService: CargandoService,
    private readonly _notificacionService: NotificacionService,
    public dialogRef: MatDialogRef<ModalPagosAbonosComponent>,
    private readonly _abonoFacturaRestService: AbonoFacturaRestService,
    private readonly _facturaCabeceraRestService: FacturaCabeceraRestService,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      idFactura: number
    }
  ) { }

  ngOnInit(): void {
    this.buscarFactura();
  }

  cargarDatosIniciales(): void {
    const consulta = {
      where: {
        facturaCabecera: this.data.idFactura ?? -1
      },
      relations: ['formaPago']
    }
    this._cargandoService.habilitar();
    const gasto$ = this._abonoFacturaRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    gasto$.subscribe({
      next: (gasto) => {
        this.abonosPagos = gasto[0];
        this._cargandoService.deshabilitar();
      },
      error: (error) => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
      },
    });
  }

  cancelarModal() {
    this.dialogRef.close();
  }

  enviarAbonosPagos() {
    this.dialogRef.close(this.abonosPagos);
  }

  obtenerAbonosPago(abonos: AbonoFacturaInterface[]) {
    this.tablaValida = !!abonos;
    // this.abonosPagos = abonos || [];
  }

  buscarFactura() {
    this._cargandoService.habilitar();
    const consulta: any = {
      relations: [
        'trabajo',
        'facturaDetalles',
        'facturaDetalles.articulo',
        'abonosFactura',
        'abonosFactura.formaPago',
        'gasto',
        'proveedor',
        'proveedor.cuentasBanco'
      ],
      where: {
        id: this.data?.idFactura,
      },
      order: {
        id: 'DESC',
      },
    };
    this._facturaCabeceraRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    ).subscribe({
      next: (facutra) => {
        this.factura = facutra[0][0];
        if (this.factura?.gasto) {
          const gasto = this.factura?.gasto;
          this.tipoGasto = gasto?.tipoGasto as TipoGasto;
        }
        if (this.factura?.trabajo) {
          this.tipoGasto = TipoGasto.Repuesto;
        }
        this.abonosPagos = this.factura.abonosFactura as any;
      },
        error: (error) => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
      },
      complete: () => {
        this._cargandoService.deshabilitar();
      }
    });
  }
}
