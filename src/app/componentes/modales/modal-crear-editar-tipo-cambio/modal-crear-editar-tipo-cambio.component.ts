import {Component, Inject, OnInit} from '@angular/core';
import {TipoCambioInterface} from "../../../modulos/configuracion-datos/interfaces/tipo-cambio.interface";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-modal-crear-editar-tipo-cambio',
  templateUrl: './modal-crear-editar-tipo-cambio.component.html',
  styleUrls: ['./modal-crear-editar-tipo-cambio.component.scss']
})
export class ModalCrearEditarTipoCambioComponent implements OnInit {

  tipoCambioACrear: TipoCambioInterface | undefined;

  constructor(
    public dialogRef: MatDialogRef<ModalCrearEditarTipoCambioComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { tipoCambio: TipoCambioInterface }
  ) {}

  ngOnInit(): void {
    if (this.data) {
      this.tipoCambioACrear = this.data.tipoCambio as TipoCambioInterface;
    }
  }

  crearEditar(dato: TipoCambioInterface | boolean): void {
    !dato
      ? (this.tipoCambioACrear = undefined)
      : (this.tipoCambioACrear = dato as TipoCambioInterface);
  }

  enviarDatosFormulario(): void {
    this.dialogRef.close(this.tipoCambioACrear);
  }

  cancelarModal(): void {
    this.dialogRef.close();
  }

}
