import {Component, Inject, OnInit} from '@angular/core';
import {ArticuloInterface} from "../../../modulos/configuracion-datos/interfaces/articulo.interface";
import {ProveedorArticuloInterface} from "../../../modulos/configuracion-datos/interfaces/proveedor-articulo.interface";
import {ArticuloImpuestoInterface} from "../../../modulos/configuracion-datos/interfaces/articulo-impuesto.interface";
import {ArticuloRestService} from "../../../modulos/configuracion-datos/servicios/articulo-rest.service";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {environment} from "../../../../environments/environment";

@Component({
  selector: 'app-modal-crear-editar-servicio',
  templateUrl: './modal-crear-editar-servicio.component.html',
  styleUrls: ['./modal-crear-editar-servicio.component.scss']
})
export class ModalCrearEditarServicioComponent implements OnInit {

  servicioACrear: ArticuloInterface | undefined;
  articulosTarifaImpuesto?: ArticuloImpuestoInterface[] = [];
  imagen: any;

  constructor(
    private readonly _articuloRestService: ArticuloRestService,
    private readonly _notificacionService: NotificacionService,
    public dialogRef: MatDialogRef<ModalCrearEditarServicioComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { servicio: ArticuloInterface, soloVer: boolean }
  ) {
  }

  ngOnInit(): void {
    if (this.data) {
      this.servicioACrear = this.data.servicio as ArticuloInterface;
    }
  }

  crearEditar(dato: ArticuloInterface | boolean): void {
    !dato
      ? (this.servicioACrear = undefined)
      : (this.servicioACrear = dato as ArticuloInterface);
  }

  async enviarDatosFormulario() {
    if (this.servicioACrear) {
      this.servicioACrear.articulosTarifaImpuesto = this.articulosTarifaImpuesto;
      this.servicioACrear.imagen = this.imagen;
    }
    const codigoServicioValido = await this.validarCodigoServicio();
    if (codigoServicioValido) {
      this.dialogRef.close(this.servicioACrear);
    }
  }

  cancelarModal(): void {
    this.dialogRef.close();
  }

  setearArticulosImpuestos(articulosTarifaImpuesto: ArticuloImpuestoInterface[]) {
    this.articulosTarifaImpuesto = articulosTarifaImpuesto;
  }

  async validarCodigoServicio(): Promise<boolean> {
    const codigo = this.servicioACrear?.codigo;
    const consulta = {
      where: {
        codigo,
        esServicio: 1,
      }
    };
    const impuesto$ = this._articuloRestService.findAll(
      'busqueda=' + JSON.stringify(consulta),
    );

    const respuestaServicio = await (impuesto$.toPromise());
    let servicios = respuestaServicio[0];
    if (this.data?.servicio) {
      servicios = servicios.filter(servicio => servicio.codigo !== this.data.servicio.codigo);
    }
    if (!!servicios.length) {
      this._notificacionService.modalInfo(
        'warning',
        'Advertencia',
        `Ya existe un servicio registrado en el sistema con el código: ${codigo}`
      );
      return false;
    } else {
      return true;
    }

  }
}
