import {Component, Inject, OnInit} from '@angular/core';
import {ProveedorInterface} from "../../../modulos/configuracion-datos/interfaces/proveedor.interface";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {CuentaBancoInterface} from "../../../modulos/configuracion-datos/interfaces/cuenta-banco.interface";
import {ContactoInterface} from "../../../modulos/configuracion-datos/interfaces/contacto.interface";
import {ProveedorRestService} from "../../../modulos/configuracion-datos/servicios/proveedor-rest.service";

@Component({
  selector: 'app-modal-crear-editar-proveedor',
  templateUrl: './modal-crear-editar-proveedor.component.html',
  styleUrls: ['./modal-crear-editar-proveedor.component.scss']
})
export class ModalCrearEditarProveedorComponent implements OnInit {

  proveedorACrear: ProveedorInterface | undefined;
  contactos?: ContactoInterface[] = [];
  cuentasBancos?: CuentaBancoInterface[] = [];
  constructor(
    private readonly _proveedorRestService: ProveedorRestService,
    public dialogRef: MatDialogRef<ModalCrearEditarProveedorComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { proveedor: ProveedorInterface, soloVer: boolean }
  ) {}

  ngOnInit(): void {
    if (this.data) {
      this.proveedorACrear = this.data.proveedor as ProveedorInterface;
    }
  }

  crearEditar(dato: ProveedorInterface | boolean): void {
    !dato
      ? (this.proveedorACrear = undefined)
      : (this.proveedorACrear = dato as ProveedorInterface);
  }

  enviarDatosFormulario(): void {
    if (this.proveedorACrear) {
      this.proveedorACrear.contactos = this.contactos;
      this.proveedorACrear.cuentasBanco = this.cuentasBancos;
    }
    this.dialogRef.close(this.proveedorACrear);
  }

  cancelarModal(): void {
    this.dialogRef.close();
  }

  setearCuentasBancos(cuentasBanco: CuentaBancoInterface[]) {
    this.cuentasBancos = cuentasBanco;
  }

  setearContactos(contactos: ContactoInterface[]) {
    this.contactos = contactos;
  }
}
