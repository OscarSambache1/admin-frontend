import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {GastoInterface} from "../../../modulos/gastos/interfaces/gasto.interface";
import {EstadoGastoAccion} from "../../../enums/tipo-contacto.enums";
import {FacturaCabeceraInterface} from "../../../modulos/gastos/interfaces/factura-cabecera.interface";

@Component({
  selector: 'app-modal-crear-editar-gasto',
  templateUrl: './modal-crear-editar-gasto.component.html',
  styleUrls: ['./modal-crear-editar-gasto.component.scss']
})
export class ModalCrearEditarGastoComponent implements OnInit {
  formulatioGastoValido!: boolean;
  facturaValida!: boolean;
  facturaACrearEditar?: FacturaCabeceraInterface;
  gastoACrearEditar?: GastoInterface;
  enumsEstadoGastoAccion = EstadoGastoAccion;
  gastoPrevio!: GastoInterface;
  facturaPrevia!: FacturaCabeceraInterface;
  constructor(
    public dialogRef: MatDialogRef<ModalCrearEditarGastoComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      soloVer: boolean,
      gasto: GastoInterface,
      factura: FacturaCabeceraInterface,
    }
  ) { }

  ngOnInit(): void {
    this.gastoPrevio = this.data?.gasto;
    this.facturaPrevia = this.data?.factura;
  }

  cancelar() {
    this.dialogRef.close();
  }

  guardarFinalizarGasto(accion: string) {
    this.dialogRef.close(
      {
        facturaACrearEditar: this.facturaACrearEditar,
        gastoACrearEditar: this.gastoACrearEditar,
        accion,
      }
    );
  }

  validarBotones() {
    return this.formulatioGastoValido && this.facturaValida;
  }

  obtenerFormularioGasto(gasto: any) {
    if (gasto) {
      this.gastoACrearEditar = gasto as GastoInterface;
      this.formulatioGastoValido = true;
    } else {
      this.formulatioGastoValido = false;
    }
  }

  obtenerFormularioFactura(factura: any) {
    if (factura) {
      this.facturaValida = true;
      this.facturaACrearEditar = factura;
    } else {
      this.facturaValida = false;
    }
  }
}
