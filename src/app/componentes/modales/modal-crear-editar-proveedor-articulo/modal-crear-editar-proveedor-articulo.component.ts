import {Component, Inject, OnInit} from '@angular/core';
import {ProveedorArticuloInterface} from "../../../modulos/configuracion-datos/interfaces/proveedor-articulo.interface";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-modal-crear-editar-proveedor-articulo',
  templateUrl: './modal-crear-editar-proveedor-articulo.component.html',
  styleUrls: ['./modal-crear-editar-proveedor-articulo.component.scss']
})
export class ModalCrearEditarProveedorArticuloComponent implements OnInit {
  proveedorArticuloACrear: ProveedorArticuloInterface | undefined;

  constructor(
    public dialogRef: MatDialogRef<ModalCrearEditarProveedorArticuloComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { proveedorArticulo: ProveedorArticuloInterface }
  ) {}

  ngOnInit(): void {
    if (this.data) {
      this.proveedorArticuloACrear = this.data.proveedorArticulo as ProveedorArticuloInterface;
    }
  }

  crearEditar(dato: ProveedorArticuloInterface | boolean): void {
    !dato
      ? (this.proveedorArticuloACrear = undefined)
      : (this.proveedorArticuloACrear = dato as ProveedorArticuloInterface);
  }

  enviarDatosFormulario(): void {
    this.dialogRef.close(this.proveedorArticuloACrear);
  }

  cancelarModal(): void {
    this.dialogRef.close();
  }

}
