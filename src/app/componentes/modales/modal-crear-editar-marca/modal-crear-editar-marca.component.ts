import {Component, Inject, OnInit} from '@angular/core';
import {MarcaInterface} from "../../../modulos/configuracion-datos/interfaces/marca.interface";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-modal-crear-editar-marca',
  templateUrl: './modal-crear-editar-marca.component.html',
  styleUrls: ['./modal-crear-editar-marca.component.scss']
})
export class ModalCrearEditarMarcaComponent implements OnInit {

  marcaACrear: MarcaInterface | undefined;

  constructor(
    public dialogRef: MatDialogRef<ModalCrearEditarMarcaComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { marca: MarcaInterface, soloVer: boolean }
  ) {}

  ngOnInit(): void {
    if (this.data) {
      this.marcaACrear = this.data.marca as MarcaInterface;
    }
  }

  crearEditar(dato: MarcaInterface | boolean): void {
    !dato
      ? (this.marcaACrear = undefined)
      : (this.marcaACrear = dato as MarcaInterface);
  }

  enviarDatosFormulario(): void {
    this.dialogRef.close(this.marcaACrear);
  }

  cancelarModal(): void {
    this.dialogRef.close();
  }
}
