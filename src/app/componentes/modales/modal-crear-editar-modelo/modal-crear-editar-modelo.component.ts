import {Component, Inject, OnInit} from '@angular/core';
import {ModeloInterface} from "../../../modulos/configuracion-datos/interfaces/modelo.interface";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-modal-crear-editar-modelo',
  templateUrl: './modal-crear-editar-modelo.component.html',
  styleUrls: ['./modal-crear-editar-modelo.component.scss']
})
export class ModalCrearEditarModeloComponent implements OnInit {

  modeloACrear: ModeloInterface | undefined;

  constructor(
    public dialogRef: MatDialogRef<ModalCrearEditarModeloComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { modelo: ModeloInterface }
  ) {}

  ngOnInit(): void {
    if (this.data) {
      this.modeloACrear = this.data.modelo as ModeloInterface;
    }
  }

  crearEditar(dato: ModeloInterface | boolean): void {
    !dato
      ? (this.modeloACrear = undefined)
      : (this.modeloACrear = dato as ModeloInterface);
  }

  enviarDatosFormulario(): void {
    this.dialogRef.close(this.modeloACrear);
  }

  cancelarModal(): void {
    this.dialogRef.close();
  }

}
