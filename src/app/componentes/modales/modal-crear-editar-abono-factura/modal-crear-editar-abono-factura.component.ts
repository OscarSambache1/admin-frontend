import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {AbonoFacturaInterface} from "../../../modulos/gastos/interfaces/abono-factura.interface";

@Component({
  selector: 'app-modal-crear-editar-abono-factura',
  templateUrl: './modal-crear-editar-abono-factura.component.html',
  styleUrls: ['./modal-crear-editar-abono-factura.component.scss']
})
export class ModalCrearEditarAbonoFacturaComponent implements OnInit {


  abonoFacturaACrear: AbonoFacturaInterface | undefined;

  constructor(
    public dialogRef: MatDialogRef<ModalCrearEditarAbonoFacturaComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { abonoFactura: AbonoFacturaInterface }
  ) {}

  ngOnInit(): void {
    if (this.data) {
      this.abonoFacturaACrear = this.data.abonoFactura as AbonoFacturaInterface;
    }
  }

  crearEditar(dato: AbonoFacturaInterface | boolean): void {
    !dato
      ? (this.abonoFacturaACrear = undefined)
      : (this.abonoFacturaACrear = dato as AbonoFacturaInterface);
  }

  enviarDatosFormulario(): void {
    this.dialogRef.close(this.abonoFacturaACrear);
  }

  cancelarModal(): void {
    this.dialogRef.close();
  }

}
