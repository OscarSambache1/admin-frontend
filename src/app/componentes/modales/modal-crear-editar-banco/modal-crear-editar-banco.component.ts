import {Component, Inject, OnInit} from '@angular/core';
import {BancoInterface} from "../../../modulos/configuracion-datos/interfaces/banco.interface";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-modal-crear-editar-banco',
  templateUrl: './modal-crear-editar-banco.component.html',
  styleUrls: ['./modal-crear-editar-banco.component.scss']
})
export class ModalCrearEditarBancoComponent implements OnInit {

  bancoACrear: BancoInterface | undefined;

  constructor(
    public dialogRef: MatDialogRef<ModalCrearEditarBancoComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { banco: BancoInterface, soloVer: boolean }
  ) {}

  ngOnInit(): void {
    if (this.data) {
      this.bancoACrear = this.data.banco as BancoInterface;
    }
  }

  crearEditar(dato: BancoInterface | boolean): void {
    !dato
      ? (this.bancoACrear = undefined)
      : (this.bancoACrear = dato as BancoInterface);
  }

  enviarDatosFormulario(): void {
    this.dialogRef.close(this.bancoACrear);
  }

  cancelarModal(): void {
    this.dialogRef.close();
  }

}
