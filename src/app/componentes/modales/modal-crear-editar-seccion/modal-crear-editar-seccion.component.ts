import {Component, Inject, OnInit} from '@angular/core';
import {SeccionInterface} from "../../../modulos/configuracion-datos/interfaces/seccion.interface";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-modal-crear-editar-seccion',
  templateUrl: './modal-crear-editar-seccion.component.html',
  styleUrls: ['./modal-crear-editar-seccion.component.scss']
})
export class ModalCrearEditarSeccionComponent implements OnInit {
  seccionACrear: SeccionInterface | undefined;

  constructor(
    public dialogRef: MatDialogRef<ModalCrearEditarSeccionComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { seccion: SeccionInterface }
  ) {}

  ngOnInit(): void {
    if (this.data) {
      this.seccionACrear = this.data.seccion as SeccionInterface;
    }
  }

  crearEditar(dato: SeccionInterface | boolean): void {
    !dato
      ? (this.seccionACrear = undefined)
      : (this.seccionACrear = dato as SeccionInterface);
  }

  enviarDatosFormulario(): void {
    this.dialogRef.close(this.seccionACrear);
  }

  cancelarModal(): void {
    this.dialogRef.close();
  }

}
