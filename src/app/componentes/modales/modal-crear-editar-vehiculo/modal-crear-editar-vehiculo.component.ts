import {Component, Inject, OnInit} from '@angular/core';
import {VehiculoInterface} from "../../../modulos/configuracion-datos/interfaces/vehiculo.interface";
import {VehiculoRestService} from "../../../modulos/configuracion-datos/servicios/vehiculo-rest.service";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {ClienteInterface} from "../../../modulos/configuracion-datos/interfaces/cliente.interface";

@Component({
  selector: 'app-modal-crear-editar-vehiculo',
  templateUrl: './modal-crear-editar-vehiculo.component.html',
  styleUrls: ['./modal-crear-editar-vehiculo.component.scss']
})
export class ModalCrearEditarVehiculoComponent implements OnInit {

  vehiculoACrear: VehiculoInterface | undefined;
  constructor(
    private readonly _vehiculoRestService : VehiculoRestService,
    private readonly _notificacionService: NotificacionService,
    public dialogRef: MatDialogRef<ModalCrearEditarVehiculoComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      cliente: ClienteInterface,
      vehiculo: VehiculoInterface,
      esModalCliente: boolean,
      soloVer: boolean,
    }
  ) {}

  ngOnInit(): void {
    if (this.data) {
      this.vehiculoACrear = this.data.vehiculo as VehiculoInterface;
    }
  }

  crearEditar(dato: VehiculoInterface | boolean): void {
    !dato
      ? (this.vehiculoACrear = undefined)
      : (this.vehiculoACrear = dato as VehiculoInterface);
  }

  async enviarDatosFormulario() {
    const placaValido = await this.validarPlaca();
    if (placaValido) {
      this.dialogRef.close(this.vehiculoACrear);
    }
  }

  cancelarModal(): void {
    this.dialogRef.close();
  }

  async validarPlaca(): Promise<boolean> {
    const placa = this.vehiculoACrear?.placa;
    const consulta = {
      where: {
        placa
      }
    };
    const vehiculo$ = this._vehiculoRestService.findAll(
      'busqueda=' + JSON.stringify(consulta),
    );

    const respuestaVehiculo = await (vehiculo$.toPromise());
    let vehiculos = respuestaVehiculo[0];
    if (this.data?.vehiculo) {
      vehiculos = vehiculos.filter(vehiculo => vehiculo.placa !== this.data.vehiculo.placa);
    }
    if (!!vehiculos.length) {
      this._notificacionService.modalInfo(
        'warning',
        'Advertencia',
        `Ya existe un vehículo con la placa: ${placa}`
      );
      return false;
    } else {
      return true;
    }

  }

}
