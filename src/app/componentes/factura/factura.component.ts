import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {
  EstadoAbono,
  TarfifasIvaEnum,
  TARIFAS_NO_TOTALES,
  TipoFactura,
  TipoGasto
} from "../../enums/tipo-contacto.enums";
import {ProveedorInterface} from "../../modulos/configuracion-datos/interfaces/proveedor.interface";
import {FacturaDetalleInterface} from "../../modulos/gastos/interfaces/factura-detalle.interface";
import {TarifaImpuestoRestService} from "../../modulos/configuracion-datos/servicios/tarifa-impuesto-rest.service";
import {TarifaImpuestoInterface} from "../../modulos/configuracion-datos/interfaces/tarifa-impuesto.interface";
import {FacturaCabeceraInterface} from "../../modulos/gastos/interfaces/factura-cabecera.interface";
import {AbonoFacturaInterface} from "../../modulos/gastos/interfaces/abono-factura.interface";
import {TablaDetalleFacturaComponent} from "../tablas/tabla-detalle-factura/tabla-detalle-factura.component";
import {CargandoService} from "../../servicios/cargando.service";

@Component({
  selector: 'app-factura',
  templateUrl: './factura.component.html',
  styleUrls: ['./factura.component.scss']
})
export class FacturaComponent implements OnInit {
  @ViewChild(TablaDetalleFacturaComponent)
  tablaDetalleFactura!: TablaDetalleFacturaComponent;
  @Input()
  tipoFactura?: string;

  @Output() tipoGastoChange = new EventEmitter<string>();

  facturaACrear: FacturaCabeceraInterface = {};

  @Output() datosFacturaChange = new EventEmitter<string>();
  @Output() valorTotalCambia = new EventEmitter<any>();
  @Output() valorTotalAbonosPagado = new EventEmitter<any>();
  @Output() facturaEmitir = new EventEmitter<FacturaCabeceraInterface>();

  @Input()
  tipoGasto?: string;

  @Input()
  facturaPrevia?: FacturaCabeceraInterface;

  formularioProveedorValido!: boolean;
  tablaDetalleFacturaValida!: boolean;
  formularioFacturaCabeceraValido!: boolean;
  tablaAbonosFacturaValida!: boolean;
  proveedorFacturaFormulario?: ProveedorInterface;
  tipoFacturaEnum = TipoFactura;
  detalles?: FacturaDetalleInterface[] = [];
  abonosFactura?: AbonoFacturaInterface[] = [];
  arreglototalesFactura: any[] = [];
  tarifas: TarifaImpuestoInterface[] = [];
  facturaCabceraFormulario?: FacturaCabeceraInterface;
  valorTotalFactura?: number;
  tarifaSeleccionadaTotales = TarfifasIvaEnum.Iva12;
  detallesPrevios?: FacturaDetalleInterface[] = [];
  abonosPrevios?: AbonoFacturaInterface[] = [];
  mostrarAbonos!: boolean;
  descuentoGeneral = 0;
  constructor(
    private readonly _tarifaImpuestoRestService: TarifaImpuestoRestService,
    private readonly _cargandoService: CargandoService,
  ) {
  }

  async ngOnInit() {
    await this.obtenerTarifas();
    if (this.facturaPrevia) {
      this.detallesPrevios = this.facturaPrevia.facturaDetalles;
      this.detalles = this.detallesPrevios;
      this.abonosPrevios = this.facturaPrevia.abonosFactura;
      this.abonosFactura = this.abonosPrevios;
      this.descuentoGeneral = this.facturaPrevia.descuentoPorcentaje as number;
    }
    this.setearArregloTotales();
    this.calcularTotalesAbonos();
    this.validarTablaDetalle();
  }

  async obtenerTarifas() {
    const consulta = {
      where: {},
      relations: ['impuesto']
    };
    const tarifaImpuesto$ = this._tarifaImpuestoRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    const respuesta = await (tarifaImpuesto$.toPromise());
    this.tarifas = respuesta[0]
      .filter(tarifaAux => tarifaAux.impuesto?.nombre === 'IVA');
  }

  obtenerProveedorFormulario(proveedor: ProveedorInterface | boolean) {
    if (proveedor) {
      this.proveedorFacturaFormulario = proveedor as ProveedorInterface;
      this.facturaACrear.proveedor = this.proveedorFacturaFormulario;
      this.facturaACrear.nombreComercialProveedor = this.proveedorFacturaFormulario?.nombreComercial;
      this.facturaACrear.razonSocialProveedor = this.proveedorFacturaFormulario?.razonSocial;
      this.facturaACrear.identificacionProveedor = this.proveedorFacturaFormulario?.identificacion;
      this.facturaACrear.telefonoProveedor = this.proveedorFacturaFormulario?.telefono;
      this.facturaACrear.direccionProveedor = this.proveedorFacturaFormulario?.direccion;
      this.facturaACrear.correoProveedor = this.proveedorFacturaFormulario?.correo;
      this.formularioProveedorValido = true;
    } else {
      this.formularioProveedorValido = false;
    }
    this.setearFactura();
  }

  obtenerTablaDetalles(respuesta: any) {
    this._cargandoService.habilitar();
    this.mostrarAbonos = false;
    this.tablaDetalleFacturaValida = respuesta.tablaDetalleValida;
    if (respuesta?.detalles?.length) {
      this.detalles = respuesta?.detalles;
      this.facturaACrear.facturaDetalles = this.detalles;
      this.setearArregloTotales();
    } else {
    }
    this.setearFactura();
    setTimeout(() => {
      this.mostrarAbonos = true;
      this._cargandoService.deshabilitar();
    }, 250);
  }

  setearArregloTotales() {
    const arregloTotales: any[] = [];
    const arregloSubtotalesTarifa: any[] = [];
    const arregloTarifasValidas = [...TARIFAS_NO_TOTALES, this.tarifaSeleccionadaTotales];
    this.tarifas.map(tarifa => {
      if (arregloTarifasValidas.includes(tarifa.descripcion as any)) {
        const detallesTarifas = this.detalles?.filter(detalle => detalle.ivaDescripcion === tarifa.descripcion);
        const sumaDetallesValorTotalTarifa = detallesTarifas?.reduce((acum: any, detalle) => {
          return +(detalle.total as any) + acum;
        }, 0) || 0;
        const sumaDetallesValorTarifa = detallesTarifas?.reduce((acum: any, detalle) => {
          return +(detalle.valorIva as any) + acum;
        }, 0) || 0;
        arregloSubtotalesTarifa.push(
          {
            sumaDetallesValorTotalTarifa,
            sumaDetallesValorTarifa,
            tarifa: tarifa.descripcion
          }
        );
      }
    });

    const sumaDetallesValorTotal = this.detalles?.reduce((acum: any, detalle) => {
      return +(detalle.total as any) + acum;
    }, 0) || 0;

    arregloTotales.push(
      {
        label: 'Subtotal sin impuestos',
        valor: sumaDetallesValorTotal
      }
    );
    const arregloTarifas = [...TARIFAS_NO_TOTALES, this.tarifaSeleccionadaTotales];
    arregloSubtotalesTarifa.map(datosTarifa => {
      if (arregloTarifas.includes(datosTarifa.tarifa)) {
        arregloTotales.push(
          {
            label: `Subtotal ${datosTarifa.tarifa}`,
            valor: datosTarifa.sumaDetallesValorTotalTarifa,
          }
        )
      }
    });


    // const sumaDetallesDescuentos = this.detalles?.reduce((acum: any, detalle) => {
    //   return +(detalle.valorDescuento as any) + acum;
    // }, 0) || 0;


    const sumaDetallesValorIce = this.detalles?.reduce((acum: any, detalle) => {
      return +(detalle.valorIce as number) + acum;
    }, 0) || 0;
    // arregloTotales.push(
    //   {
    //     label: 'Valor ICE',
    //     valor: sumaDetallesValorIce
    //   }
    // );
    const tarifaIva = arregloSubtotalesTarifa.find(datosTarifa => datosTarifa.tarifa === this.tarifaSeleccionadaTotales);
    if (tarifaIva) {
      arregloTotales.push(
        {
          label: `Iva ${tarifaIva.tarifa}`,
          valor: tarifaIva.sumaDetallesValorTarifa,
          esTarifa: 1,
        }
      );
    }

    const sumaValoresIva = arregloSubtotalesTarifa.reduce((acum, tarifa) => {
      return tarifa.sumaDetallesValorTarifa + acum;
    }, 0)

    // arregloTotales.push(
    //   {
    //     label: `Propina`,
    //     valor: 0,
    //   }
    // );
    // arregloTotales.push(
    //   {
    //     label: 'Descuento General',
    //     // valor: sumaDetallesDescuentos
    //     valor: this.descuentoGeneral || 0,
    //   }
    // );
    // console.log(sumaDetallesValorTotal, 'sumaDetallesValorTotal');
    // console.log(sumaValoresIva, 'sumaValoresIva');
    // console.log(sumaDetallesValorIce, 'sumaDetallesValorIce');
    // console.log(sumaDetallesDescuentos, 'sumaDetallesDescuentos');
    const subtotalSinDescuentoGeneral = sumaDetallesValorTotal + sumaValoresIva + sumaDetallesValorIce;
    const descuento = this.descuentoGeneral / 100 * subtotalSinDescuentoGeneral;
    const valorAPagar = subtotalSinDescuentoGeneral - descuento;
    arregloTotales.push(
      {
        label: `Valor a pagar`,
        valor: valorAPagar,
      }
    );
    this.arreglototalesFactura = arregloTotales;
    this.valorTotalFactura = valorAPagar;

    this.facturaACrear.subtotalIva0 = arregloTotales.find(total => total.label === TarfifasIvaEnum.Iva0)?.valor || 0;
    this.facturaACrear.subtotalIva = arregloTotales.find(total => total.esTarifa)?.valor || 0;
    this.facturaACrear.subtotalIvaExento = arregloTotales.find(total => total.label === TarfifasIvaEnum.IvaExtento)?.valor || 0;
    this.facturaACrear.subtotalIvaNoObjeto = arregloTotales.find(total => total.label === TarfifasIvaEnum.IvaNoObjeto)?.valor || 0;
    this.facturaACrear.subtotalSinImpuestos = sumaDetallesValorTotal;
    this.facturaACrear.descuento = descuento;
    this.facturaACrear.descuentoPorcentaje = this.descuentoGeneral;
    this.facturaACrear.ivaValor = sumaValoresIva;
    this.facturaACrear.ivaPorcentaje = this.tarifaSeleccionadaTotales;
    this.facturaACrear.iceValor = sumaDetallesValorIce;
    this.facturaACrear.total = valorAPagar;

    this.valorTotalCambia.emit(valorAPagar);
  }

  validarTablaDetalle() {
    if (this.tipoGasto === TipoGasto.Producto) {
      if (this.detalles?.length) {
      } else {
        this.tablaDetalleFacturaValida = true;
      }
    }
  }

  recibirValorTotalAPagar(objetValorAPagar: any) {
    this.mostrarAbonos = false;
    // this.facturaACrear.propina = objetValorAPagar.propina;
    this.facturaACrear.total = objetValorAPagar.valorAPagar;
    this.descuentoGeneral = objetValorAPagar.porcentajeDescuento || this.descuentoGeneral;
    this.facturaACrear.descuentoPorcentaje = this.descuentoGeneral;
    this.facturaACrear.descuento = objetValorAPagar.descuentos;

    if (objetValorAPagar.ingresoManual) {
      const arregloTotales = objetValorAPagar.arreglototalesFactura as any[];
      const ivaPorcentaje = this.facturaACrear.ivaPorcentaje || '12 %';
      this.facturaACrear.subtotalIva = arregloTotales.find(tarifa => (tarifa.label === `Subtotal ${ivaPorcentaje}`))?.valor || 0;
      this.facturaACrear.subtotalIva0 = arregloTotales.find(tarifa => tarifa.label === 'Subtotal 0 %').valor || 0;
      this.facturaACrear.subtotalIvaExento = arregloTotales.find(tarifa => tarifa.label === 'Subtotal EXENTO IVA').valor || 0;
      this.facturaACrear.subtotalIvaNoObjeto = arregloTotales.find(tarifa => tarifa.label === 'Subtotal NO OBJETO').valor || 0;
      this.facturaACrear.subtotalSinImpuestos = objetValorAPagar.subtotalSinImpuesto;
      this.facturaACrear.descuento = objetValorAPagar.descuentos;
      this.facturaACrear.iceValor = objetValorAPagar.valorIce;
      this.facturaACrear.ivaValor = objetValorAPagar.valoresIva;
      this.valorTotalFactura = objetValorAPagar.valorAPagar;
    }
    this.valorTotalCambia.emit(objetValorAPagar.valorAPagar);
    setTimeout(() => {
      this.mostrarAbonos = true;
    }, 250)
  }

  obtenerFacturaCabeceraFormulario(facturaCabecera: FacturaCabeceraInterface | boolean) {
    if (facturaCabecera) {
      this.facturaCabceraFormulario = facturaCabecera as FacturaCabeceraInterface;
      this.facturaACrear.fechaEmision = this.facturaCabceraFormulario?.fechaEmision;
      this.facturaACrear.numero = this.facturaCabceraFormulario?.numero;
      this.facturaACrear.archivoPdf = this.facturaCabceraFormulario?.archivoPdf;
      this.formularioFacturaCabeceraValido = true;
    } else {
      this.formularioFacturaCabeceraValido = false;
    }
    this.setearFactura();
  }

  obtenerAbonosFactura(abonos: AbonoFacturaInterface[]) {
    if (abonos) {
      this.tablaAbonosFacturaValida = true;
      this.abonosFactura = abonos;
      this.calcularTotalesAbonos();
      this.facturaACrear.abonosFactura = abonos;
    } else {
      this.tablaAbonosFacturaValida = false;
    }
    this.setearFactura();
  }

  calcularTotalesAbonos() {
    const totalAbonosPagados = this.abonosFactura?.filter(abonoFil => abonoFil.estadoAbono === EstadoAbono.Pagado)
      .reduce((acum: any, abono) => {
      return (abono.valorAPagar as number) + acum;
    }, 0);
    this.valorTotalAbonosPagado.emit(totalAbonosPagados);
  }

  private setearFactura() {
    if (this.formularioFacturaCabeceraValido && this.formularioProveedorValido && this.tablaDetalleFacturaValida && this.tablaAbonosFacturaValida) {
      this.facturaEmitir.emit(this.facturaACrear);
    } else {
      this.facturaEmitir.emit(undefined);
    }
  }

  async recibirTarifaIva(tarifaDescripcion: any) {
    this.tarifaSeleccionadaTotales = tarifaDescripcion;
    this.tablaDetalleFactura.tarifaSeleccionadaTotales = this.tarifaSeleccionadaTotales;
    await this.tablaDetalleFactura.obtenerTarifasIva();
    const tarifa = this.tarifas.find(tarifa => tarifa.descripcion === tarifaDescripcion);
    this.tablaDetalleFactura.datos.map(detalle => {
      if (!TARIFAS_NO_TOTALES.includes(detalle.ivaDescripcion as any)) {
        detalle.ivaDescripcion = tarifaDescripcion;
        detalle.ivaPorcentaje = tarifa?.valor;
        this.tablaDetalleFactura.calcularValores(detalle);
      }
    });
    this.tablaDetalleFactura.datos = [...this.tablaDetalleFactura.datos];
    this.tablaDetalleFactura.emitirTablaDetalle();
    this.detalles = this.tablaDetalleFactura.datos;
    this.setearArregloTotales();
  }
}
