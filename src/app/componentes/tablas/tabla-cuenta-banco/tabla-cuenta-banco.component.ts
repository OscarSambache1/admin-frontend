import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {DxDataGridComponent} from "devextreme-angular";
import {ROWS, ROWS_PER_PAGE} from "../../../constantes/table-config";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../servicios/cargando.service";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {CuentaBancoInterface} from "../../../modulos/configuracion-datos/interfaces/cuenta-banco.interface";
import {CuentaBancoRestService} from "../../../modulos/configuracion-datos/servicios/cuenta-banco-rest.service";
import {ModalCrearEditarCuentaBancoComponent} from "../../modales/modal-crear-editar-cuenta-banco/modal-crear-editar-cuenta-banco.component";
import {EstadoGastoAccion, TipoCuentaBancoEnum} from "../../../enums/tipo-contacto.enums";
import {TIPOS_CUENTA_BANCO} from "../../../constantes/tipos-identificacion";

@Component({
  selector: 'app-tabla-cuenta-banco',
  templateUrl: './tabla-cuenta-banco.component.html',
  styleUrls: ['./tabla-cuenta-banco.component.scss']
})
export class TablaCuentaBancoComponent implements OnInit {

  @Input()
  soloVer!: any;
  @Input()
  idProveedor?: number;

  @Input()
  idCliente?: number;

  @Output()
  cuentasBanco: EventEmitter<CuentaBancoInterface[]> = new EventEmitter<CuentaBancoInterface[]>();

  @ViewChild(DxDataGridComponent)
  dataGrid!: DxDataGridComponent;

  rows = ROWS;
  rowsPerPage = ROWS_PER_PAGE;
  datos: CuentaBancoInterface[] = [];
  pageSize!: number;
  disabledTable!: boolean;
  pagination: number[] = [];
  paginationStart!: number;

  consulta: any = {
    relations: ['banco'],
    where: {},
    order: {
      id: 'DESC',
    },
  };
  tiposCuentaBanco = [...TIPOS_CUENTA_BANCO];

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _cuentaBancoRestService: CuentaBancoRestService,
    private readonly _notificacionService: NotificacionService,
  ) {
  }

  ngOnInit(): void {
    this.pagination = ROWS_PER_PAGE;
    this.paginationStart = ROWS;
    this.cargarDatosIniciales(this.consulta);
  }

  cargarDatosIniciales(consulta: any): void {
    if (this.idProveedor) {
      consulta.where = {
        proveedor: this.idProveedor
      };
    } else {
      if (this.idCliente) {
        consulta.where = {
          cliente: this.idCliente
        };
      } else {
        consulta.where = {
          cliente: -1
        };
      }
    }

    this._cargandoService.habilitar();
    const cuentaBanco$ = this._cuentaBancoRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    cuentaBanco$.subscribe({
      next: (cuentaBanco) => {
        this.datos = [...cuentaBanco[0]];
        this.pageSize = cuentaBanco[1];
        this.disabledTable = false;
        this._cargandoService.deshabilitar();
        this.cuentasBanco.emit([...cuentaBanco[0]]);
      },
      error: (error) => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
      },
    });
  }

  onToolbarPreparing(evento: any): void {
    evento.toolbarOptions.items.unshift(
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Agregar nuevo registro',
          onClick: this.add.bind(this, undefined, -1),
          icon: 'plus',
        },
      },
      // {
      //   location: 'after',
      //   widget: 'dxButton',
      //   options: {
      //     disabled: this.disabledTable,
      //     hint: 'Recargar datos de la tabla',
      //     icon: 'refresh',
      //     onClick: this.onRefreshDataGrid.bind(this),
      //   },
      // }
    );
  }

  private onRefreshDataGrid(): void {
    this.cargarDatosIniciales(this.consulta);
    void this.dataGrid.instance.refresh();
  }

  add(cuentaBanco?: CuentaBancoInterface, indice?: number): void {
    const dialogRef = this._matDialog.open(ModalCrearEditarCuentaBancoComponent, {
      width: '1050px',
      data: {
        cuentaBanco: cuentaBanco,
      },
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          if (!cuentaBanco) {
            this.datos.push(response);
          } else {
            this.datos[indice as number] = response;
          }
          this.cuentasBanco.emit(this.datos);
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

  quitar(rowIndex: number) {
    this.datos.splice(rowIndex, 1);
    this.cuentasBanco.emit(this.datos);
  }

}
