import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ProveedorArticuloInterface} from "../../../modulos/configuracion-datos/interfaces/proveedor-articulo.interface";
import {DxDataGridComponent} from "devextreme-angular";
import {ROWS, ROWS_PER_PAGE} from "../../../constantes/table-config";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../servicios/cargando.service";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {ModalCrearEditarProveedorArticuloComponent} from "../../modales/modal-crear-editar-proveedor-articulo/modal-crear-editar-proveedor-articulo.component";
import {ProveedorArticuloRestService} from "../../../modulos/configuracion-datos/servicios/proveedor-articulo-rest.service";

@Component({
  selector: 'app-tabla-proveedor-articulo',
  templateUrl: './tabla-proveedor-articulo.component.html',
  styleUrls: ['./tabla-proveedor-articulo.component.scss']
})
export class TablaProveedorArticuloComponent implements OnInit {

  @Input()
  idArticulo?: number;

  @Output()
  proveedoresArticulo: EventEmitter<ProveedorArticuloInterface[]> = new EventEmitter<ProveedorArticuloInterface[]>();

  @ViewChild(DxDataGridComponent)
  dataGrid!: DxDataGridComponent;

  rows = ROWS;
  rowsPerPage = ROWS_PER_PAGE;
  datos: ProveedorArticuloInterface[] = [];
  pageSize!: number;
  disabledTable!: boolean;
  pagination: number[] = [];
  paginationStart!: number;

  consulta: any = {
    relations: ['proveedor', 'articulo', 'proveedor.informacionTributaria'],
    where: {},
    order: {
      id: 'DESC',
    },
  };

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _proveedorArticuloRestService: ProveedorArticuloRestService,
    private readonly _notificacionService: NotificacionService,
  ) {
  }

  ngOnInit(): void {
    this.pagination = ROWS_PER_PAGE;
    this.paginationStart = ROWS;
    this.cargarDatosIniciales(this.consulta);
  }

  cargarDatosIniciales(consulta: any): void {
    consulta.where = {
      articulo: this.idArticulo
    };

    this._cargandoService.habilitar();
    const proveedorArticulo$ = this._proveedorArticuloRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    proveedorArticulo$.subscribe({
      next: (proveedorArticulo) => {
        this.datos = [...proveedorArticulo[0]];
        this.pageSize = proveedorArticulo[1];
        this.disabledTable = false;
        this._cargandoService.deshabilitar();
        this.proveedoresArticulo.emit([...proveedorArticulo[0]]);
      },
      error: (error) => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
      },
    });
  }

  onToolbarPreparing(evento: any): void {
    evento.toolbarOptions.items.unshift(
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Agregar nuevo registro',
          onClick: this.add.bind(this, undefined, -1),
          icon: 'plus',
        },
      },
      // {
      //   location: 'after',
      //   widget: 'dxButton',
      //   options: {
      //     disabled: this.disabledTable,
      //     hint: 'Recargar datos de la tabla',
      //     icon: 'refresh',
      //     onClick: this.onRefreshDataGrid.bind(this),
      //   },
      // }
    );
  }

  private onRefreshDataGrid(): void {
    this.cargarDatosIniciales(this.consulta);
    void this.dataGrid.instance.refresh();
  }

  add(proveedorArticulo?: ProveedorArticuloInterface, indice?: number): void {
    const dialogRef = this._matDialog.open(ModalCrearEditarProveedorArticuloComponent, {
      width: '550px',
      data: {
        proveedorArticulo: proveedorArticulo,
      },
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          if (!proveedorArticulo) {
            this.datos.push(response);
          } else {
            this.datos[indice as number] = response;
          }
          this.proveedoresArticulo.emit(this.datos);
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

  quitar(rowIndex: number) {
    this.datos.splice(rowIndex, 1);
    this.proveedoresArticulo.emit(this.datos);
  }
}
