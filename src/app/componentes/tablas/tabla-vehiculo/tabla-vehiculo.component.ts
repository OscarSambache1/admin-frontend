import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {DxDataGridComponent} from "devextreme-angular";
import {ROWS, ROWS_PER_PAGE} from "../../../constantes/table-config";
import {VehiculoInterface} from "../../../modulos/configuracion-datos/interfaces/vehiculo.interface";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../servicios/cargando.service";
import {VehiculoRestService} from "../../../modulos/configuracion-datos/servicios/vehiculo-rest.service";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {ModalCrearEditarVehiculoComponent} from "../../modales/modal-crear-editar-vehiculo/modal-crear-editar-vehiculo.component";

@Component({
  selector: 'app-tabla-vehiculo',
  templateUrl: './tabla-vehiculo.component.html',
  styleUrls: ['./tabla-vehiculo.component.scss']
})
export class TablaVehiculoComponent implements OnInit {
  @Input()
  idCliente?: number;

  @Output()
  vehiculos: EventEmitter<VehiculoInterface[]> = new EventEmitter<VehiculoInterface[]>();

  @ViewChild(DxDataGridComponent)
  dataGrid!: DxDataGridComponent;

  rows = ROWS;
  rowsPerPage = ROWS_PER_PAGE;
  datos: VehiculoInterface[] = [];
  pageSize!: number;
  disabledTable!: boolean;
  pagination: number[] = [];
  paginationStart!: number;


  consulta: any = {
    relations: [
      'cliente',
      'cliente.informacionTributaria',
      'marca',
      'tipoVehiculo',
      'tipoCambio',
      'tipoCombustible',
    ],
    where: {
    },
    order: {
      id: 'DESC',
    },
  };

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _vehiculoRestService: VehiculoRestService,
    private readonly _notificacionService: NotificacionService,
  ) {}

  ngOnInit(): void {
    this.pagination = ROWS_PER_PAGE;
    this.paginationStart = ROWS;
    this.cargarDatosIniciales(this.consulta);
  }

  cargarDatosIniciales(consulta: any): void {
    if (this.idCliente) {
      consulta.where = {
        cliente: this.idCliente
      }
    } else {
      consulta.where = {
        cliente: -1
      }
    }
    this._cargandoService.habilitar();
    const vehiculo$ = this._vehiculoRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    vehiculo$.subscribe({
      next: (vehiculo) => {
        this.datos = vehiculo[0];
        this.pageSize = vehiculo[1];
        this.disabledTable = false;
        this._cargandoService.deshabilitar();
      },
      error: (error) => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
      },
    });
  }

  onToolbarPreparing(evento: any): void {
    evento.toolbarOptions.items.unshift(
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Agregar nuevo registro',
          onClick: this.add.bind(this, undefined, -1),
          icon: 'plus',
        },
      },
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Recargar datos de la tabla',
          icon: 'refresh',
          onClick: this.onRefreshDataGrid.bind(this),
        },
      }
    );
  }

  private onRefreshDataGrid(): void {
    this.cargarDatosIniciales(this.consulta);
    void this.dataGrid.instance.refresh();
  }

  add(vehiculo?: VehiculoInterface, indice?: number): void {
    const dialogRef = this._matDialog.open(ModalCrearEditarVehiculoComponent, {
      width: '1050px',
      data: {
        vehiculo,
        esModalCliente: true,
      },
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          if (!vehiculo) {
            this.datos.push(response);
          } else {
            this.datos[indice as number] = response;
          }
          this.vehiculos.emit(this.datos);
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }


  async cambiarEstado(data: any) {
    const respuestaModalConfirmacion = await this._notificacionService.modalConfirmsSwal(
      'question',
      'Advertencia',
      'Está seguro de cambiar el estado del registro',
    );
    if (respuestaModalConfirmacion) {
        data.estado = data.estado ? 0 : 1;
    }
  }
}
