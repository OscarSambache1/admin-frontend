import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {
  EstadoTrabajo,
  TarfifasIvaEnum,
  TARIFAS_NO_TOTALES,
  TipoFactura,
  TipoGasto
} from "../../../enums/tipo-contacto.enums";
import {TarifaImpuestoRestService} from "../../../modulos/configuracion-datos/servicios/tarifa-impuesto-rest.service";

@Component({
  selector: 'app-tabla-totales-factura',
  templateUrl: './tabla-totales-factura.component.html',
  styleUrls: ['./tabla-totales-factura.component.scss']
})
export class TablaTotalesFacturaComponent implements OnInit {

  @Input()
  arreglototalesFactura: any[] = [];

  @Input()
  soloVer = false;

  @Input()
  estadoAccion!: string;

  @Input()
  tipoGasto: any;

  @Output() valorTotalCambia = new EventEmitter<any>();
  @Output() valorIvaCambia = new EventEmitter<any>();

  tipoGastoEnum = TipoGasto;
  tarifas: any[] = [];

  columns: any[] = [
    {
      field: 'label',
      header: '',
      width: '50%',
    },
    {
      field: 'valor',
      header: '',
      width: '50%',
    },
  ];
  ingresoManual: boolean = false;

  constructor(
    private readonly _tarifaImpuestoRestService: TarifaImpuestoRestService,

  ) { }

  async ngOnInit() {
    await this.obtenerTarifasIva();
    const valorAPagar = this.arreglototalesFactura.find(tarifa => tarifa.label === 'Valor a pagar');
    // const descuentos = this.arreglototalesFactura.find(tarifa => tarifa.label === 'Descuento General');
    if (valorAPagar) {
      this.valorTotalCambia.emit(
          {
            valorAPagar: valorAPagar.valor || 0,
          }
      );
    }
  }

  async obtenerTarifasIva(){
    const consulta = {
      where: {},
      relations: ['impuesto']
    };
    const tarifaImpuesto$ = this._tarifaImpuestoRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    const respuesta = await (tarifaImpuesto$.toPromise());
    this.tarifas = respuesta[0]
      .filter(tarifaAux => !TARIFAS_NO_TOTALES.includes(tarifaAux.descripcion as any))
      .map(tarifa => {
        return  {
          label: `${tarifa.descripcion}`,
          value: +(tarifa.valor as any),
        };
      });
  }

  puedeEditar(detalle: any) {
    const estadoFinalizado = this.estadoAccion && this.estadoAccion === EstadoTrabajo.Finalizado;
    if (this.soloVer || estadoFinalizado) {
      return false;
    } else {
      // if (detalle.label === 'Propina' || detalle.label === 'Descuento General') {
      //   return true;
      // } else {
        return this.ingresoManual;
      // }
    }

  }

  puedeEditarLabel(detalle: any) {
    const estadoFinalizado = this.estadoAccion && this.estadoAccion === EstadoTrabajo.Finalizado;
    if (estadoFinalizado || this.soloVer) {
      return false;
    } else {
      return !!detalle.esTarifa;
    }
  }

  efectuarCalculos(datos: any) {
    let valorAPagar = 0;
    const subtotalSinImpuesto = this.arreglototalesFactura.find(tarifa => tarifa.label === 'Subtotal sin impuestos').valor || 0;
    // const porcentajeDescuento = this.arreglototalesFactura.find(tarifa => tarifa.label === 'Descuento General').valor || 0;
    // const valorIce = this.arreglototalesFactura.find(tarifa => tarifa.label === 'Valor ICE').valor || 0;
    const valorIce = 0;
    const indiceValorAPagar = this.arreglototalesFactura.findIndex(tarifa => tarifa.label === 'Valor a pagar');
    const valoresIva = this.arreglototalesFactura
      .filter(registroFiltrado => registroFiltrado.esTarifa)
      .reduce((acum, registro) => {
        return registro.valor + acum;
      }, 0);
    // const propina = this.arreglototalesFactura.find(tarifa => tarifa.label === 'Propina').valor || 0;
    let descuentos = 0;
    if (!this.ingresoManual) {
      const subtotalSinDEscuentoGeneral = subtotalSinImpuesto + valoresIva + valorIce;
      // descuentos = porcentajeDescuento / 100 * subtotalSinDEscuentoGeneral;
      valorAPagar = subtotalSinDEscuentoGeneral - descuentos;
      this.arreglototalesFactura[indiceValorAPagar].valor = valorAPagar;
    } else {
      valorAPagar = this.arreglototalesFactura.find(tarifa => tarifa.label === 'Valor a pagar').valor || 0;
    }
    this.valorTotalCambia.emit({
      valorAPagar: valorAPagar || 0,
      subtotalSinImpuesto,
      descuentos,
      // porcentajeDescuento,
      valorIce,
      ingresoManual: this.ingresoManual,
      valoresIva,
      arreglototalesFactura: this.arreglototalesFactura,
    });
  }

  cambiarTarifa(detalle: any) {
    const tarifaSeleccionada = detalle.label + '';
    detalle.label = 'Iva ' + detalle.label;
    this.valorIvaCambia.emit(tarifaSeleccionada);
  }

  setearMaximo(detalle: any) {
    return 1000000;
  }
}
