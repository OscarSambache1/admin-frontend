import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {EstadoAbono, EstadoAbonoEnum, EstadoGasto, TipoGasto} from "../../../enums/tipo-contacto.enums";
import {AbonoFacturaInterface} from "../../../modulos/gastos/interfaces/abono-factura.interface";
import {MatDialog} from "@angular/material/dialog";
import {ModalCrearEditarAbonoFacturaComponent} from "../../modales/modal-crear-editar-abono-factura/modal-crear-editar-abono-factura.component";
import {FacturaCabeceraInterface} from "../../../modulos/gastos/interfaces/factura-cabecera.interface";

@Component({
  selector: 'app-tabla-abonos-factura',
  templateUrl: './tabla-abonos-factura.component.html',
  styleUrls: ['./tabla-abonos-factura.component.scss']
})
export class TablaAbonosFacturaComponent implements OnInit {

  @Output() valorTotalCambia = new EventEmitter<any>();

  @Output() arregloAbonosFacturaCambia = new EventEmitter<AbonoFacturaInterface[]>();

  @Input()
  tipoGasto: any;

  @Input()
  factura?: FacturaCabeceraInterface;

  @Input()
  abonosPrevios?: AbonoFacturaInterface[] = [];

  @Input()
  abonosFactura: AbonoFacturaInterface[] = [];

  @Input()
  valorTotalFactura?: number;

  @Input()
  esModal = false;

  @Input()
  soloVer = false;
  columns: any[] = [
    {
      field: 'formaPago',
      header: 'Forma de Pago',
      width: '35%',
    },
    {
      field: 'valorAPagar',
      header: 'Valor',
      width: '15%',
    },
    {
      field: 'fechaPago',
      header: 'Fecha de Pago',
      width: '20%',
    },
    {
      field: 'estadoAbono',
      header: 'Estado',
      width: '15%',
    },
    {
      field: 'id',
      header: 'Acciones',
      width: '10%',
    },
  ];
  estadoAbonoEnum: any = EstadoAbonoEnum;
  constructor(
    private readonly _matDialog: MatDialog,
  ) { }

  ngOnInit(): void {
    if (this.abonosPrevios) {
      this.abonosFactura = this.abonosPrevios;
    } else {
      this.abonosFactura = [];
    }
    this.emitirTablaAbonos();
  }

  agregarAbonoFactura(abonoFactura?: AbonoFacturaInterface, indice?: number) {
    const dialogRef = this._matDialog.open(ModalCrearEditarAbonoFacturaComponent, {
      width: '650px',
      data: {
        abonoFactura
      },
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          if (abonoFactura) {
            this.abonosFactura[indice as number] = response;
          } else {
            this.abonosFactura.unshift(response);
          }
          this.emitirTablaAbonos();
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

  quitar(rowIndex: any) {
    this.abonosFactura.splice(rowIndex, 1);
    this.emitirTablaAbonos();
  }

  get totalAbonosFacturaPagados(): number {
    return this.abonosFactura
      .reduce((acum: any, abono: any) => {
        return +(abono.valorAPagar as number) + acum;
      }, 0);
  }

  validarValores() {
    return true;
    // return +(this.totalAbonosFacturaPagados.toFixed(2)) <= +(this.valorTotalFactura as number).toFixed(2);
  }

  emitirTablaAbonos() {
    const totalAbonosPagados = this.validarValores();
    if (totalAbonosPagados) {
      this.arregloAbonosFacturaCambia.emit(this.abonosFactura);
    } else {
      this.arregloAbonosFacturaCambia.emit(undefined);
    }
  }

  setearEstadoGasto() {
    return !this.abonosFactura.length;
  }
}
