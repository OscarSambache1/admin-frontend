import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {DxDataGridComponent} from "devextreme-angular";
import {ROWS, ROWS_PER_PAGE} from "../../../constantes/table-config";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../servicios/cargando.service";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {ContactoInterface} from "../../../modulos/configuracion-datos/interfaces/contacto.interface";
import {ContactoRestService} from "../../../modulos/configuracion-datos/servicios/contacto-rest.service";
import {ModalCrearEditarContactoComponent} from "../../modales/modal-crear-editar-contacto/modal-crear-editar-contacto.component";
import {TipoContactoEnums} from "../../../enums/tipo-contacto.enums";

@Component({
  selector: 'app-tabla-contactos',
  templateUrl: './tabla-contactos.component.html',
  styleUrls: ['./tabla-contactos.component.scss']
})
export class TablaContactosComponent implements OnInit {


  @Input()
  idProveedor?: number;

  @Input()
  idCliente?: number;

  @Output()
  contactos: EventEmitter<ContactoInterface[]> = new EventEmitter<ContactoInterface[]>();

  @ViewChild(DxDataGridComponent)
  dataGrid!: DxDataGridComponent;

  rows = ROWS;
  rowsPerPage = ROWS_PER_PAGE;
  datos: ContactoInterface[] = [];
  pageSize!: number;
  disabledTable!: boolean;
  pagination: number[] = [];
  paginationStart!: number;
  tipoContactoEnum = TipoContactoEnums;
  consulta: any = {
    where: {},
    order: {
      id: 'DESC',
    },
  };

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _contactoRestService: ContactoRestService,
    private readonly _notificacionService: NotificacionService,
  ) {
  }

  ngOnInit(): void {
    this.pagination = ROWS_PER_PAGE;
    this.paginationStart = ROWS;
    this.cargarDatosIniciales(this.consulta);
  }

  cargarDatosIniciales(consulta: any): void {
    if (this.idProveedor) {
      consulta.where = {
        proveedor: this.idProveedor
      };
    } else {
      if (this.idCliente) {
        consulta.where = {
          cliente: this.idCliente
        };
      } else {
        consulta.where = {
          cliente: -1
        };
      }
    }

    this._cargandoService.habilitar();
    const contacto$ = this._contactoRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    contacto$.subscribe({
      next: (contacto) => {
        this.datos = [...contacto[0]];
        this.pageSize = contacto[1];
        this.disabledTable = false;
        this._cargandoService.deshabilitar();
        this.contactos.emit([...contacto[0]]);
      },
      error: (error) => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
      },
    });
  }

  onToolbarPreparing(evento: any): void {
    evento.toolbarOptions.items.unshift(
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Agregar nuevo registro',
          onClick: this.add.bind(this, undefined, -1),
          icon: 'plus',
        },
      },
      // {
      //   location: 'after',
      //   widget: 'dxButton',
      //   options: {
      //     disabled: this.disabledTable,
      //     hint: 'Recargar datos de la tabla',
      //     icon: 'refresh',
      //     onClick: this.onRefreshDataGrid.bind(this),
      //   },
      // }
    );
  }

  private onRefreshDataGrid(): void {
    this.cargarDatosIniciales(this.consulta);
    void this.dataGrid.instance.refresh();
  }

  add(contacto?: ContactoInterface, indice?: number): void {
    const dialogRef = this._matDialog.open(ModalCrearEditarContactoComponent, {
      width: '650px',
      data: {
        contacto: contacto,
      },
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          if (!contacto) {
            this.datos.push(response);
          } else {
            this.datos[indice as number] = response;
          }
          this.contactos.emit(this.datos);
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

  quitar(rowIndex: number) {
    this.datos.splice(rowIndex, 1);
    this.contactos.emit(this.datos);
  }

  setearTipoContacto(contacto: ContactoInterface) {
    // @ts-ignore
    return TipoContactoEnums[contacto.tipoContacto] || 'Sin Tipo';
  }
}
