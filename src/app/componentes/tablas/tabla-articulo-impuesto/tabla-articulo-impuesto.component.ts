import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ArticuloImpuestoInterface} from "../../../modulos/configuracion-datos/interfaces/articulo-impuesto.interface";
import {DxDataGridComponent} from "devextreme-angular";
import {ROWS, ROWS_PER_PAGE} from "../../../constantes/table-config";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../servicios/cargando.service";
import {ArticuloImpuestoRestService} from "../../../modulos/configuracion-datos/servicios/articulo-impuesto-rest.service";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {ModalCrearEditarArticuloImpuestoComponent} from "../../modales/modal-crear-editar-articulo-impuesto/modal-crear-editar-articulo-impuesto.component";

@Component({
  selector: 'app-tabla-articulo-impuesto',
  templateUrl: './tabla-articulo-impuesto.component.html',
  styleUrls: ['./tabla-articulo-impuesto.component.scss']
})
export class TablaArticuloImpuestoComponent implements OnInit {

  @Input()
  idArticulo?: number;

  @Output()
  articulosImpuesto: EventEmitter<ArticuloImpuestoInterface[]> = new EventEmitter<ArticuloImpuestoInterface[]>();

  @ViewChild(DxDataGridComponent)
  dataGrid!: DxDataGridComponent;

  rows = ROWS;
  rowsPerPage = ROWS_PER_PAGE;
  datos: ArticuloImpuestoInterface[] = [];
  pageSize!: number;
  disabledTable!: boolean;
  pagination: number[] = [];
  paginationStart!: number;

  consulta: any = {
    relations: ['tarifaImpuesto', 'articulo', 'tarifaImpuesto.impuesto'],
    where: {},
    order: {
      id: 'DESC',
    },
  };

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _articuloImpuestoRestService: ArticuloImpuestoRestService,
    private readonly _notificacionService: NotificacionService,
  ) {
  }

  ngOnInit(): void {
    this.pagination = ROWS_PER_PAGE;
    this.paginationStart = ROWS;
    this.cargarDatosIniciales(this.consulta);
  }

  cargarDatosIniciales(consulta: any): void {
    consulta.where = {
      articulo: this.idArticulo
    };

    this._cargandoService.habilitar();
    const articuloImpuesto$ = this._articuloImpuestoRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    articuloImpuesto$.subscribe({
      next: (articuloImpuesto) => {
        this.datos = [...articuloImpuesto[0]];
        this.pageSize = articuloImpuesto[1];
        this.disabledTable = false;
        this._cargandoService.deshabilitar();
        this.articulosImpuesto.emit([...articuloImpuesto[0]]);
      },
      error: (error) => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
      },
    });
  }

  onToolbarPreparing(evento: any): void {
    evento.toolbarOptions.items.unshift(
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Agregar nuevo registro',
          onClick: this.add.bind(this, undefined, -1),
          icon: 'plus',
        },
      },
      // {
      //   location: 'after',
      //   widget: 'dxButton',
      //   options: {
      //     disabled: this.disabledTable,
      //     hint: 'Recargar datos de la tabla',
      //     icon: 'refresh',
      //     onClick: this.onRefreshDataGrid.bind(this),
      //   },
      // }
    );
  }

  private onRefreshDataGrid(): void {
    this.cargarDatosIniciales(this.consulta);
    void this.dataGrid.instance.refresh();
  }

  add(articuloImpuesto?: ArticuloImpuestoInterface, indice?: number): void {
    const dialogRef = this._matDialog.open(ModalCrearEditarArticuloImpuestoComponent, {
      width: '550px',
      data: {
        articuloImpuesto: articuloImpuesto,
      },
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          if (!articuloImpuesto) {
            this.datos.push(response);
          } else {
            this.datos[indice as number] = response;
          }
          this.articulosImpuesto.emit(this.datos);
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

  quitar(rowIndex: number) {
    this.datos.splice(rowIndex, 1);
    this.articulosImpuesto.emit(this.datos);
  }

}
