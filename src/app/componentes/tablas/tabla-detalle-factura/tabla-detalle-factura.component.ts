import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FacturaDetalleInterface} from "../../../modulos/gastos/interfaces/factura-detalle.interface";
import {DxDataGridComponent} from "devextreme-angular";
import {ROWS, ROWS_PER_PAGE} from "../../../constantes/table-config";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../servicios/cargando.service";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {FacturaDetalleRestService} from "../../../modulos/gastos/servicios/factura-detalle-rest.service";
import {TarifaImpuestoRestService} from "../../../modulos/configuracion-datos/servicios/tarifa-impuesto-rest.service";
import {EstadoTrabajo, TarfifasIvaEnum, TARIFAS_NO_TOTALES, TipoGasto} from "../../../enums/tipo-contacto.enums";
import {ArticuloInterface} from "../../../modulos/configuracion-datos/interfaces/articulo.interface";
import {ArticuloRestService} from "../../../modulos/configuracion-datos/servicios/articulo-rest.service";
import {ModalSeleccionarArticulosComponent} from "../../modales/modal-seleccionar-articulos/modal-seleccionar-articulos.component";

@Component({
  selector: 'app-tabla-detalle-factura',
  templateUrl: './tabla-detalle-factura.component.html',
  styleUrls: ['./tabla-detalle-factura.component.scss']
})
export class TablaDetalleFacturaComponent implements OnInit {
  @ViewChild(DxDataGridComponent)
  dataGrid!: DxDataGridComponent;

  @Input()
  idFactura?: number;

  @Input()
  tipoGasto?: string;

  @Input()
  detallesPrevios?: FacturaDetalleInterface[] = [];

  @Input()
  esOrdenTrabajo!: boolean;

  @Input()
  esTrabajoRealizado!: boolean;

  @Input()
  esFactura!: boolean;

  @Input()
  estadoOrdenTrabajo?: string;

  @Input()
  estadoTrabajoRealizado?: string;

  @Input()
  estadoFactura?: string;

  @Output()
  facturaDetalle: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  soloVer = false;

  rows = ROWS;
  rowsPerPage = ROWS_PER_PAGE;
  datos: FacturaDetalleInterface[] = [];
  pageSize!: number;
  disabledTable!: boolean;
  pagination: number[] = [];
  paginationStart!: number;
  tarifaSeleccionadaTotales = TarfifasIvaEnum.Iva12;

  consulta: any = {
    relations: ['articulo'],
    where: {},
    order: {
      id: 'DESC',
    },
  };
  tarifas: any[] = [];
  tipoGastoEnum = TipoGasto;
  columns = [
    {
      field: 'cantidad',
      header: 'Cantidad',
      width: '8%',
    },
    {
      field: 'descripcion',
      header: 'Descripción',
      width: '25%',
    },
    {
      field: 'precioUnitario',
      header: 'Precio unitario',
      width: '12%',
    },
    {
      field: 'ivaPorcentaje',
      header: 'IVA %',
      width: '12%',
    },
    {
      field: 'valorIva',
      header: 'Valor IVA',
      width: '10%',
    },
    {
      field: 'descuento',
      header: 'Descuentos %',
      width: '10%',
    },
    {
      field: 'total',
      header: 'Total',
      width: '10%',
    },
    // {
    //   field: 'valorIce',
    //   header: 'Valor ICE',
    //   width: '12%',
    // },
    {
      field: 'id',
      header: 'Acciones',
      width: '8%',
    },
  ]
  repuestos: ArticuloInterface[] = [];

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _facturaDetalleRestService: FacturaDetalleRestService,
    private readonly _tarifaImpuestoRestService: TarifaImpuestoRestService,
    private readonly _articuloRestService: ArticuloRestService,
    private readonly _notificacionService: NotificacionService,
  ) {
  }

  quitarColumanAccion() {
    const ordenFinalizada = this.estadoOrdenTrabajo === EstadoTrabajo.Finalizado;
    const trabajoRealizadoFinalizada = this.estadoTrabajoRealizado === EstadoTrabajo.Finalizado;
    if (ordenFinalizada || trabajoRealizadoFinalizada || this.esFactura || this.soloVer) {
      const indiceAcciones = this.columns.findIndex(columna => columna.field ==='id');
      this.columns.splice(indiceAcciones, 1);
    }
  }

  async ngOnInit() {
    if (this.esOrdenTrabajo) {
      this.columns = [
        {
          field: 'cantidad',
          header: 'Cantidad',
          width: '8%',
        },
        {
          field: 'descripcion',
          header: 'Descripción',
          width: '25%',
        },
        {
          field: 'id',
          header: 'Acciones',
          width: '8%',
        },
      ]
    }
    this.quitarColumanAccion();
    await this.obtenerTarifasIva();
    if (this.detallesPrevios) {
      this.datos = this.detallesPrevios;
      this.emitirTablaDetalle();
    }
    this.pagination = ROWS_PER_PAGE;
    this.paginationStart = ROWS;
    // this.cargarDatosIniciales(this.consulta);
  }

  async obtenerTarifasIva() {
    const consulta = {
      where: {},
      relations: ['impuesto']
    };
    const tarifaImpuesto$ = this._tarifaImpuestoRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    const respuesta = await (tarifaImpuesto$.toPromise());
    const arregloTarifas = [...TARIFAS_NO_TOTALES, this.tarifaSeleccionadaTotales];
    this.tarifas = respuesta[0]
      .filter(tarifaAux => arregloTarifas.includes(tarifaAux.descripcion as any))
      .map(tarifa => {
        return {
          label: `${tarifa.descripcion}`,
          value: +(tarifa.valor as any),
        };
      });
  }


  quitar(rowIndex: number) {
    this.datos.splice(rowIndex, 1);
    this.emitirTablaDetalle();
  }

  seEditoColumna(evento: any) {
    const data = evento.data;
    this.calcularValores(data);
    this.emitirTablaDetalle();
  }

  calcularValores(
    data: any
  ) {
    const cantidad = data.cantidad || 0;
    const precioUnitario = data.precioUnitario || 0;
    const cantidadXPrecio = cantidad * precioUnitario;
    const ivaPorcentaje = data.ivaPorcentaje || 0;
    const descuento = data.descuento || 0;
    const total = data.total || 0;
    data.valorIva = (cantidadXPrecio * (ivaPorcentaje / 100)) || 0;
    data.valorDescuento = descuento / 100 * cantidadXPrecio;
    data.total = cantidadXPrecio - data.valorDescuento;
    if ((descuento as number) > 100) {
      this._notificacionService.modalInfo(
        'warning',
        'Advertencia',
        `El descuento (${descuento}) debe ser menor o igual a 100%`
      )
    }
  }

  setearValorMaximoDescuento(detalle: any) {
    // const cantidad = detalle.cantidad || 0;
    // const precioUnitario = detalle.precioUnitario || 0;
    // const cantidadXPrecio = cantidad * precioUnitario;
    // return cantidadXPrecio + detalle.valorIva;
    return 100;
  }

  agregarDetalles(esServicio: 0 | 1, esExtra?: 0 | 1) {
    if (this.tipoGasto === TipoGasto.Repuesto && !esExtra) {
      const idsRepuestos = this.datos.map(detalle => detalle.articulo?.id);
      const dialogRef = this._matDialog.open(ModalSeleccionarArticulosComponent, {
        width: '1050px',
        data: {
          esServicio: (this.esOrdenTrabajo || this.esTrabajoRealizado || this.esFactura) ? esServicio : 0,
          idsRepuestos,
          esOrdenTrabajo: this.esOrdenTrabajo,
          esTrabajoRealizado: this.esTrabajoRealizado,
          esFactura: this.esFactura,
        },
        disableClose: true,
      });
      dialogRef.afterClosed().subscribe({
        next: (response) => {
          if (response) {
            const detalles = response.map((articulo: ArticuloInterface) => {
              const total = (articulo?.precioVenta ?? 0);
              return {
                cantidad: 1,
                precioUnitario: articulo.precioVenta ?? 0,
                valorIce: 0,
                valorIva: 0,
                descuento: 0,
                ivaPorcentaje: 0,
                ivaDescripcion: '0 %',
                articulo,
                descripcion: articulo.nombre,
                codigoPrincipal: articulo.codigo,
                tipo: this.tipoGasto,
                total,
              };
            });
            this.datos.push(...detalles);
            this.emitirTablaDetalle();
          }
        },
        error: (err) => {
          // eslint-disable-next-line no-console
          console.error(err);
        },
      });
    } else {
      this.datos.unshift(
        {
          total: 0,
          cantidad: 0,
          precioUnitario: 0,
          valorIce: 0,
          valorIva: 0,
          descuento: 0,
          ivaPorcentaje: 0,
          ivaDescripcion: '0 %',
          tipo: this.tipoGasto
        }
      );
      this.emitirTablaDetalle();
    }
  }

  validarTipoDetalle() {
    if (this.datos.length) {
      return (this.datos.every(detalle => detalle.tipo === this.tipoGasto));
    } else {
      return true;
    }
  }

  validarTotalDetalle() {
    if (this.tipoGasto === this.tipoGastoEnum.Repuesto || this.tipoGasto === this.tipoGastoEnum.Producto) {
      if (this.datos.length) {
        if (this.esOrdenTrabajo) {
          return true;
        } else {
          return (this.datos.every(detalle => (detalle.total as number) > 0));
        }
      } else {
        return true;
      }
    } else {
      return !this.datos.length;
    }
  }

  validarCantidadDetalles() {
    if (this.tipoGasto === this.tipoGastoEnum.Repuesto || this.tipoGasto === this.tipoGastoEnum.Producto) {
      if (this.tipoGasto === this.tipoGastoEnum.Repuesto) {
        return !!this.datos.length;
      } else {
        return this.tipoGasto === this.tipoGastoEnum.Producto;
      }
    } else {
      return false;
    }
  }

  validarDescripcion() {
    if (this.tipoGasto === this.tipoGastoEnum.Repuesto || this.tipoGasto === this.tipoGastoEnum.Producto) {
      if (this.datos.length) {
        return this.datos.every(detalle => detalle.descripcion && detalle.descripcion.trim() !== '');
      } else {
        return true;
      }
    } else {
      return false;
    }
  }

  validarCantidad() {
    if (!this.esOrdenTrabajo) {
      return true;
    } else {
      return (this.datos.every(detalle => {
        if (detalle.articulo) {
          if (detalle.articulo?.esServicio) {
            return (detalle.cantidad as number) > 0;
          } else {
            return ((detalle.cantidad as number) > 0) && ((detalle?.cantidad as number) <= (detalle?.articulo?.stockActual as number));
          }
        } else {
          return true;
        }

      }));
    }
  }

  emitirTablaDetalle() {
    const tablaDetalleValidaRegistros = this.validarCantidadDetalles() && this.validarTotalDetalle() && this.validarDescripcion() && this.validarCantidad();
    let tablaDetalleValida;
    if (this.tipoGasto === TipoGasto.Producto) {
      if (this.datos?.length) {
        tablaDetalleValida = tablaDetalleValidaRegistros;
      } else {
        tablaDetalleValida = true;
      }
    } else {
      tablaDetalleValida = tablaDetalleValidaRegistros;
    }
    this.facturaDetalle.emit(
      {
        detalles: this.datos,
        tablaDetalleValida,
      }
    );
  }

  cambiarTarifa(data: FacturaDetalleInterface) {
    const tarifa = this.tarifas.find(tarifa => tarifa.label === data.ivaDescripcion);
    data.ivaPorcentaje = tarifa.value;
  }

  ingresoDescuento(detalle: FacturaDetalleInterface) {
    if (detalle && (detalle.descuento as number) > 100) {
      this._notificacionService.modalInfo(
        'warning',
        'Advertencia',
        `El descuento (${detalle.descuento}) debe ser menor o igual a 100%`
      )
    }
  }

  validarDescuento(detalle: any) {
    return detalle.descuento <= 100;
  }

  setearValorMaximoCantidad(detalle: any) {
    if (this.esOrdenTrabajo) {
      if (!detalle.articulo.esServicio) {
        return detalle.articulo.stockActual || 0;
      } else {
        return 100000;
      }
    } else {
      return 100000;
    }
  }

  ingresoCantidad(detalle: any) {
    if (this.esOrdenTrabajo || this.esTrabajoRealizado || this.esFactura) {
      if (!detalle?.articulo?.esServicio && detalle.articulo) {
        if (detalle && (detalle.cantidad as number) > (detalle.articulo?.stockActual as number)) {
          this._notificacionService.modalInfo(
            'warning',
            'Advertencia',
            `La cantidad (${detalle.cantidad}) debe ser menor o igual al stock actual (${detalle.articulo.stockActual})`
          )
        }
      }
    }
  }

  validarCantidadIngresada(detalle: any): boolean {
    if (this.esOrdenTrabajo || this.esTrabajoRealizado || this.esFactura) {
      if (!detalle?.articulo?.esServicio && detalle.articulo) {
        return !(detalle && (detalle.cantidad as number) > (detalle.articulo?.stockActual as number));
      } else {
        return true;
      }
    } else {
      return true;
    }
  }

  validarEstadoTrabajo() {
    if (this.soloVer) {
      return false;
    } else {
      if (this.esOrdenTrabajo) {
        return this.estadoOrdenTrabajo !== EstadoTrabajo.Finalizado;
      } else {
        if (this.esTrabajoRealizado) {
          return this.estadoTrabajoRealizado !== EstadoTrabajo.Finalizado;
        } else {
          if (this.esFactura) {
            return this.estadoFactura !== EstadoTrabajo.Finalizado;
          } else {
            return true;
          }
        }
      }
    }
  }

  mostrarCellEditorCantidad() {
    if (this.esOrdenTrabajo || this.esTrabajoRealizado) {
      return this.validarEstadoTrabajo();
    } else {
      if (this.esFactura) {
        return false;
      } else {
        return true;
      }
    }
  }
}
