import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {DxDataGridComponent} from "devextreme-angular";
import {ROWS, ROWS_PER_PAGE} from "../../../constantes/table-config";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../servicios/cargando.service";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {TarifaImpuestoInterface} from "../../../modulos/configuracion-datos/interfaces/tarifa-impuesto.interface";
import {TarifaImpuestoRestService} from "../../../modulos/configuracion-datos/servicios/tarifa-impuesto-rest.service";
import {ModalCrearEditarTarifaImpuestoComponent} from "../../modales/modal-crear-editar-tarifa-impuesto/modal-crear-editar-tarifa-impuesto.component";

@Component({
  selector: 'app-tabla-tarifa-impuesto',
  templateUrl: './tabla-tarifa-impuesto.component.html',
  styleUrls: ['./tabla-tarifa-impuesto.component.scss']
})
export class TablaTarifaImpuestoComponent implements OnInit {

  @Input()
  idImpuesto?: number;

  @Output()
  tarifasImpuesto: EventEmitter<TarifaImpuestoInterface[]> = new EventEmitter<TarifaImpuestoInterface[]>();

  @ViewChild(DxDataGridComponent)
  dataGrid!: DxDataGridComponent;

  rows = ROWS;
  rowsPerPage = ROWS_PER_PAGE;
  datos: TarifaImpuestoInterface[] = [];
  pageSize!: number;
  disabledTable!: boolean;
  pagination: number[] = [];
  paginationStart!: number;

  consulta: any = {
    relations: [],
    where: {},
    order: {
      id: 'DESC',
    },
  };

  constructor(
    private readonly _router: Router,
    private readonly _activatedRoute: ActivatedRoute,
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _tarifaImpuestoRestService: TarifaImpuestoRestService,
    private readonly _notificacionService: NotificacionService,
  ) {
  }

  ngOnInit(): void {
    this.pagination = ROWS_PER_PAGE;
    this.paginationStart = ROWS;
    this.cargarDatosIniciales(this.consulta);
  }

  cargarDatosIniciales(consulta: any): void {
    consulta.where = {
      impuesto: this.idImpuesto
    };

    this._cargandoService.habilitar();
    const tarifaImpuesto$ = this._tarifaImpuestoRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    tarifaImpuesto$.subscribe({
      next: (tarifa) => {
        this.datos = tarifa[0];
        this.pageSize = tarifa[1];
        this.disabledTable = false;
        this._cargandoService.deshabilitar();
        this.tarifasImpuesto.emit(this.datos);
      },
      error: (error) => {
        console.error({
          mensage: 'Error al traer registros',
          error: error,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        this._cargandoService.deshabilitar();
      },
    });
  }

  onToolbarPreparing(evento: any): void {
    evento.toolbarOptions.items.unshift(
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          disabled: this.disabledTable,
          hint: 'Agregar nuevo registro',
          onClick: this.add.bind(this, undefined, -1),
          icon: 'plus',
        },
      },
      // {
      //   location: 'after',
      //   widget: 'dxButton',
      //   options: {
      //     disabled: this.disabledTable,
      //     hint: 'Recargar datos de la tabla',
      //     icon: 'refresh',
      //     onClick: this.onRefreshDataGrid.bind(this),
      //   },
      // }
    );
  }

  private onRefreshDataGrid(): void {
    this.cargarDatosIniciales(this.consulta);
    void this.dataGrid.instance.refresh();
  }

  add(tarifaImpuesto?: TarifaImpuestoInterface, indice?: number): void {
    const dialogRef = this._matDialog.open(ModalCrearEditarTarifaImpuestoComponent, {
      width: '650px',
      data: {
        tarifaImpuesto: tarifaImpuesto,
      },
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          if (!tarifaImpuesto) {
            this.datos.unshift(response);
          } else {
            this.datos[indice as number] = response;
          }
          this.tarifasImpuesto.emit(this.datos);
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

  private setarEsPrincipal() {
    this.datos = this.datos.map((registro, index) => {
      registro.esPrincipal = 0;
      return registro;
    });
  }

  quitar(rowIndex: number) {
    this.datos.splice(rowIndex, 1);
    this.tarifasImpuesto.emit(this.datos);
  }
}
