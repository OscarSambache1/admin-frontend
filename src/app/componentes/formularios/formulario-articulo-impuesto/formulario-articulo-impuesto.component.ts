import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ArticuloImpuestoInterface} from "../../../modulos/configuracion-datos/interfaces/articulo-impuesto.interface";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {TIPOS_CUENTA_BANCO} from "../../../constantes/tipos-identificacion";
import {CargandoService} from "../../../servicios/cargando.service";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {MatDialog} from "@angular/material/dialog";
import {cargarDatosDeInicioFormulario, escucharCampo} from "../../../utils/validar-campo-reactive-form.util";
import {ContactoInterface} from "../../../modulos/configuracion-datos/interfaces/contacto.interface";
import {debounceTime} from "rxjs/operators";
import {TarifaImpuestoInterface} from "../../../modulos/configuracion-datos/interfaces/tarifa-impuesto.interface";
import {TarifaImpuestoRestService} from "../../../modulos/configuracion-datos/servicios/tarifa-impuesto-rest.service";
import {ModalCrearEditarTarifaImpuestoComponent} from "../../modales/modal-crear-editar-tarifa-impuesto/modal-crear-editar-tarifa-impuesto.component";
import {ImpuestoRestService} from "../../../modulos/configuracion-datos/servicios/impuesto-rest.service";
import {ImpuestoInterface} from "../../../modulos/configuracion-datos/interfaces/impuesto.interface";
import {ModalCrearEditarImpuestoComponent} from "../../modales/modal-crear-editar-impuesto/modal-crear-editar-impuesto.component";

@Component({
  selector: 'app-formulario-articulo-impuesto',
  templateUrl: './formulario-articulo-impuesto.component.html',
  styleUrls: ['./formulario-articulo-impuesto.component.scss']
})
export class FormularioArticuloImpuestoComponent implements OnInit {
  @Input()
  data: ArticuloImpuestoInterface | undefined;

  @Output()
  datosFormulario: EventEmitter<ArticuloImpuestoInterface | boolean> = new EventEmitter<ArticuloImpuestoInterface | boolean>();

  formularioValido = false;
  formulario!: FormGroup;

  MENSAJES_DE_ERROR = {
    impuesto: {
      required: 'El campo impuesto es requerido',
    },
    tarifaImpuesto: {
      required: 'El campo tarifa es requerido',
    },
  };
  estructuraErrores = {
    impuesto: [],
    tarifaImpuesto: [],
  };
  tarifasImpuestos: TarifaImpuestoInterface[] = [];
  impuestos: ImpuestoInterface[] = [];
  constructor(
    private readonly _cargandoService: CargandoService,
    private readonly _tarifaImpuestoRestService: TarifaImpuestoRestService,
    private readonly _ImpuestoRestService: ImpuestoRestService,
    private readonly _notificacionService: NotificacionService,
    private readonly _matDialog: MatDialog,
  ) {
    this.construirFormulario();
  }

  construirFormulario(): void {
    this.formulario = new FormGroup({
      id: new FormControl(null, []),
      impuesto: new FormControl(null, [
        Validators.required,
      ]),
      tarifaImpuesto: new FormControl(null, [
        Validators.required,
      ]),
    });
  }

  ngOnInit(): void {
    this.escucharCamposDeFormulario();
    this.escucharCambiosEnFormulario();
    if (this.data) {
      cargarDatosDeInicioFormulario<ContactoInterface>(
        this.formulario,
        this.data
      );
    }
  }

  escucharCamposDeFormulario(): void {
    const campos = { ...this.estructuraErrores };
    Object.keys(campos).forEach((key) => {
      escucharCampo(
        key,
        this.formulario,
        this.estructuraErrores,
        this.MENSAJES_DE_ERROR
      );
    });
  }

  escucharCambiosEnFormulario(): void {
    const formulario$ = this.formulario.valueChanges;
    formulario$.pipe(debounceTime(250)).subscribe(async (valoresForm) => {
      this.formularioValido = !this.formulario.invalid;
      if (this.formularioValido) {
        const values = {
          ...valoresForm,
        };
        this.datosFormulario.emit(values);
      } else {
        this.datosFormulario.emit(false);
      }
    });
  }

  enviarFormulario(): void {
    this.formulario.markAllAsTouched();
    this.escucharCamposDeFormulario();
  }

  buscarTarifaImpuestos(evento: { query: string }) {
    this._cargandoService.habilitar();
    let consulta: any = {};
    const impuesto = this.formulario.get('impuesto')?.value;
    consulta = {
      where: {
        valor: `Like(\"%25${evento.query.trim()}%25\")`,
      },
      relations: ['impuesto']
    };
    if (impuesto) {
      consulta.where.impuesto = impuesto.id;
    }
    const impuesto$ = this._tarifaImpuestoRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    impuesto$.subscribe({
      next: (respuesta: [any[], number]) => {
        this.tarifasImpuestos = respuesta[0];
        this._cargandoService.deshabilitar();
      },
      error: (err) => {
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        console.error({
          mensage: 'Error en autocomplete',
          error: err,
        });
        this._cargandoService.deshabilitar();
      },
    });
  }


  buscarImpuestos(evento: { query: string }) {
    this._cargandoService.habilitar();
    let consulta = {};
    consulta = {
      where: {
        nombre: `Like(\"%25${evento.query.trim()}%25\")`,
      },
    };

    const impuesto$ = this._ImpuestoRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    impuesto$.subscribe({
      next: (respuesta: [any[], number]) => {
        this.impuestos = respuesta[0];
        this._cargandoService.deshabilitar();
      },
      error: (err) => {
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        console.error({
          mensage: 'Error en autocomplete',
          error: err,
        });
        this._cargandoService.deshabilitar();
      },
    });
  }
}
