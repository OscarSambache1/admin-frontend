import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ArticuloInterface} from "../../../modulos/configuracion-datos/interfaces/articulo.interface";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {SeccionInterface} from "../../../modulos/configuracion-datos/interfaces/seccion.interface";
import {SubseccionInterface} from "../../../modulos/configuracion-datos/interfaces/subseccion.interface";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../servicios/cargando.service";
import {SeccionRestService} from "../../../modulos/configuracion-datos/servicios/seccion-rest.service";
import {SubseccionRestService} from "../../../modulos/configuracion-datos/servicios/subseccion-rest.service";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {cargarDatosDeInicioFormulario, escucharCampo} from "../../../utils/validar-campo-reactive-form.util";
import {AccesorioInterface} from "../../../modulos/configuracion-datos/interfaces/accesorio.interface";
import {debounceTime} from "rxjs/operators";
import {ModalCrearEditarSeccionComponent} from "../../modales/modal-crear-editar-seccion/modal-crear-editar-seccion.component";
import {ModalCrearEditarSubseccionComponent} from "../../modales/modal-crear-editar-subseccion/modal-crear-editar-subseccion.component";

@Component({
  selector: 'app-formulario-stock-bodega',
  templateUrl: './formulario-stock-bodega.component.html',
  styleUrls: ['./formulario-stock-bodega.component.scss']
})
export class FormularioStockBodegaComponent implements OnInit {
  @Input()
  data: ArticuloInterface | undefined;

  @Output()
  datosFormulario: EventEmitter<ArticuloInterface | boolean> = new EventEmitter<ArticuloInterface | boolean>();

  formularioValido = false;
  formulario!: FormGroup;

  MENSAJES_DE_ERROR = {
    codigo: {
      required: 'El campo código es requerido',
      minLength: 'El campo código debe tener al menos un caracter',
      maxlength:
        'El campo código debe tener un máximo de 50 caracteres',
    },
    nombre: {
      required: 'El campo nombre es requerido',
      minLength: 'El campo nombre debe tener al menos un caracter',
      maxlength:
        'El campo nombre debe tener un máximo de 255 caracteres',
    },
    stockMinimo: {
      required: 'El campo stock Mínimo es requerido',
      min: 'El campo stock Mínimo debe ser mayor o igual a 1',
    },
    stockActual: {
      required: 'El campo stock Actual es requerido',
      min: 'El campo stock Actual debe ser mayor o igual a 1',
    },
  };
  estructuraErrores = {
    stockMinimo: [],
    stockActual: [],
    codigo: [],
    nombre: [],
  };
  constructor(
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _seccionRestService: SeccionRestService,
    private readonly _subseccionRestService: SubseccionRestService,
    private readonly _notificacionService: NotificacionService,
  ) {
    this.construirFormulario();
  }

  construirFormulario(): void {
    this.formulario = new FormGroup({
      stockMinimo: new FormControl('', [
        Validators.required,
        Validators.min(1),
      ]),
      stockActual: new FormControl('', [
        Validators.required,
        Validators.min(1),
      ]),
      codigo: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      nombre: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
    });
  }
  ngOnInit(): void {
    this.escucharCamposDeFormulario();
    this.escucharCambiosEnFormulario();
    if (this.data) {
      cargarDatosDeInicioFormulario<AccesorioInterface>(
        this.formulario,
        this.data
      );
    }
  }

  escucharCamposDeFormulario(): void {
    const campos = { ...this.estructuraErrores };
    Object.keys(campos).forEach((key) => {
      escucharCampo(
        key,
        this.formulario,
        this.estructuraErrores,
        this.MENSAJES_DE_ERROR
      );
    });
  }

  escucharCambiosEnFormulario(): void {
    const formulario$ = this.formulario.valueChanges;
    formulario$.pipe(debounceTime(250)).subscribe(async (valoresForm) => {
      this.formularioValido = !this.formulario.invalid;
      if (this.formularioValido) {
        const values = {
          ...valoresForm,
        };
        this.datosFormulario.emit(values);
      } else {
        this.datosFormulario.emit(false);
      }
    });
  }

  enviarFormulario(): void {
    this.formulario.markAllAsTouched();
    this.escucharCamposDeFormulario();
  }
}
