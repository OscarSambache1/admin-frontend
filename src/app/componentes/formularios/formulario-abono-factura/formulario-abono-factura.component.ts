import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {cargarDatosDeInicioFormulario, escucharCampo} from "../../../utils/validar-campo-reactive-form.util";
import {debounceTime} from "rxjs/operators";
import {AbonoFacturaInterface} from "../../../modulos/gastos/interfaces/abono-factura.interface";
import {CargandoService} from "../../../servicios/cargando.service";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {FormaPagoRestService} from "../../../modulos/configuracion-datos/servicios/forma-pago-rest.service";
import {MatDialog} from "@angular/material/dialog";
import {FormaPagoInterface} from "../../../modulos/configuracion-datos/interfaces/forma-pago.interface";
import {ModalCrearEditarFormaPagoComponent} from "../../modales/modal-crear-editar-forma-pago/modal-crear-editar-forma-pago.component";
import {ESTADOS_ABONO, ESTADOS_GASTO} from "../../../constantes/tipos-identificacion";
import * as moment from "moment";

@Component({
  selector: 'app-formulario-abono-factura',
  templateUrl: './formulario-abono-factura.component.html',
  styleUrls: ['./formulario-abono-factura.component.scss']
})
export class FormularioAbonoFacturaComponent implements OnInit {

  @Input()
  data: AbonoFacturaInterface | undefined;

  @Output()
  datosFormulario: EventEmitter<AbonoFacturaInterface | boolean> = new EventEmitter<AbonoFacturaInterface | boolean>();

  formularioValido = false;
  formulario!: FormGroup;

  MENSAJES_DE_ERROR = {
    valorAPagar: {
      required: 'El campo valor es requerido',
      min: 'El campo valor debe ser mayor a cero'
    },
    fechaPago: {
      required: 'El campo valor es requerido',
    },
    estadoAbono: {
      required: 'El campo estado es requerido',
    },
    formaPago: {
      required: 'El campo forma de pago es requerido',
    }
  };
  estructuraErrores = {
    valorAPagar: [],
    fechaPago: [],
    estadoAbono: [],
    formaPago: [],
  };

  formaPagos: FormaPagoInterface[] = [];
  estadosAbono = ESTADOS_ABONO;

  constructor(
    private readonly _cargandoService: CargandoService,
    private readonly _notificacionService: NotificacionService,
    private readonly _formaPagoRestService: FormaPagoRestService,
    private readonly _matDialog: MatDialog,
  ) {
    this.construirFormulario();
  }

  construirFormulario(): void {
    this.formulario = new FormGroup({
      valorAPagar: new FormControl(null, [
        Validators.required,
        Validators.min(1),
      ]),
      fechaPago: new FormControl(null, [
        Validators.required,
      ]),
      estadoAbono: new FormControl(null, [
        Validators.required,
      ]),
      formaPago: new FormControl(null, [
        // Validators.required,
      ]),
      id: new FormControl(null, [])
    });
  }

  ngOnInit(): void {
    this.escucharCamposDeFormulario();
    this.escucharCambiosEnFormulario();
    if (this.data) {
      cargarDatosDeInicioFormulario<AbonoFacturaInterface>(
        this.formulario,
        this.data
      );
    } else {
      this.formulario.patchValue(
        {
          fechaPago: moment().format('YYYY-MM-DD'),
        }
      );
    }
  }

  escucharCamposDeFormulario(): void {
    const campos = {...this.estructuraErrores};
    Object.keys(campos).forEach((key) => {
      escucharCampo(
        key,
        this.formulario,
        this.estructuraErrores,
        this.MENSAJES_DE_ERROR
      );
    });
  }

  escucharCambiosEnFormulario(): void {
    const formulario$ = this.formulario.valueChanges;
    formulario$.pipe(debounceTime(250)).subscribe(async (valoresForm) => {
      this.formularioValido = !this.formulario.invalid;
      if (this.formularioValido) {
        const values = {
          ...valoresForm,
        };
        this.datosFormulario.emit(values);
      } else {
        this.datosFormulario.emit(false);
      }
    });
  }

  enviarFormulario(): void {
    this.formulario.markAllAsTouched();
    this.escucharCamposDeFormulario();
  }

  buscarFormaPagos(evento: { query: string }) {
    this._cargandoService.habilitar();
    let consulta = {
      where: {
        nombre: `Like(\"%25${evento.query.trim()}%25\")`,
        estado: 1,
      },
    };
    const banco$ = this._formaPagoRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    banco$.subscribe({
      next: (respuesta: [any[], number]) => {
        this.formaPagos = respuesta[0];
        this._cargandoService.deshabilitar();
      },
      error: (err) => {
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        console.error({
          mensage: 'Error en autocomplete',
          error: err,
        });
        this._cargandoService.deshabilitar();
      },
    });
  }

  agregarFormaPago() {
    const dialogRef = this._matDialog.open(ModalCrearEditarFormaPagoComponent, {
      width: '450px',
      data: {},
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          const formaPago$ = this._formaPagoRestService.create(response);

          formaPago$.subscribe({
            next: (formaPagoCreada: FormaPagoInterface) => {
              this._notificacionService.modalInfo(
                'success',
                'Info',
                'Registro guardado correctamente',
              );
              this.buscarFormaPagos(
                {
                  query: ''
                }
              );
              this.formulario.patchValue(
                {
                  formaPago: formaPagoCreada,
                }
              );
            },
            error: (err) => {
              console.error({
                mensage: 'Error crear Forma Pago',
                error: err,
              });
              this._notificacionService.modalInfo(
                'error',
                'Error',
                'Error al guardar el registro',
              );
            },
          });
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

}
