import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ArticuloInterface} from "../../../modulos/configuracion-datos/interfaces/articulo.interface";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {SeccionInterface} from "../../../modulos/configuracion-datos/interfaces/seccion.interface";
import {SubseccionInterface} from "../../../modulos/configuracion-datos/interfaces/subseccion.interface";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../servicios/cargando.service";
import {SeccionRestService} from "../../../modulos/configuracion-datos/servicios/seccion-rest.service";
import {SubseccionRestService} from "../../../modulos/configuracion-datos/servicios/subseccion-rest.service";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {cargarDatosDeInicioFormulario, escucharCampo} from "../../../utils/validar-campo-reactive-form.util";
import {AccesorioInterface} from "../../../modulos/configuracion-datos/interfaces/accesorio.interface";
import {debounceTime} from "rxjs/operators";
import {ModalCrearEditarSeccionComponent} from "../../modales/modal-crear-editar-seccion/modal-crear-editar-seccion.component";
import {ModalCrearEditarSubseccionComponent} from "../../modales/modal-crear-editar-subseccion/modal-crear-editar-subseccion.component";

@Component({
  selector: 'app-formulario-servicio',
  templateUrl: './formulario-servicio.component.html',
  styleUrls: ['./formulario-servicio.component.scss']
})
export class FormularioServicioComponent implements OnInit {
  @Input()
  data: ArticuloInterface | undefined;

  @Input()
  soloVer? = false;

  @Output()
  datosFormulario: EventEmitter<ArticuloInterface | boolean> = new EventEmitter<ArticuloInterface | boolean>();

  formularioValido = false;
  formulario!: FormGroup;
  secciones: SeccionInterface[] = [];

  MENSAJES_DE_ERROR = {
    codigo: {
      required: 'El campo código es requerido',
      minLength: 'El campo código debe tener al menos un caracter',
      maxlength:
        'El campo código debe tener un máximo de 50 caracteres',
    },
    nombre: {
      required: 'El campo nombre es requerido',
      minLength: 'El campo nombre debe tener al menos un caracter',
      maxlength:
        'El campo nombre debe tener un máximo de 255 caracteres',
    },
    descripcion: {
      required: 'El campo descripción es requerido',
      minLength: 'El campo descripción debe tener al menos un caracter',
      maxlength:
        'El campo descripción debe tener un máximo de 500 caracteres',
    },
    precioVenta: {
      required: 'El campo precio de venta es requerido',
      min: 'El campo stock Mínimo debe ser mayor o igual a 0',
    },
    costo: {
      required: 'El campo costo es requerido',
      min: 'El campo stock Mínimo debe ser mayor o igual a 0',
    },
    observacion: {
      required: 'El campo observación es requerido',
      minLength: 'El campo observación debe tener al menos un caracter',
      maxlength:
        'El campo observación debe tener un máximo de 500 caracteres',
    },
    seccion: {
      required: 'El campo seccion es requerido',
    },
  };
  estructuraErrores = {
    codigo: [],
    nombre: [],
    descripcion: [],
    precioVenta: [],
    costo: [],
    observacion: [],
    seccion: [],
  };
  constructor(
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _seccionRestService: SeccionRestService,
    private readonly _subseccionRestService: SubseccionRestService,
    private readonly _notificacionService: NotificacionService,
  ) {
    this.construirFormulario();
  }

  construirFormulario(): void {
    this.formulario = new FormGroup({
      codigo: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      nombre: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      descripcion: new FormControl(null, [
        Validators.minLength(1),
        Validators.maxLength(500),
      ]),
      precioVenta: new FormControl(null, [
        Validators.min(0),
        Validators.required,
      ]),
      costo: new FormControl(null, [
        Validators.min(0),
        Validators.required,
      ]),
      observacion: new FormControl(null, [
        Validators.minLength(1),
        Validators.maxLength(500),
      ]),
      seccion: new FormControl(null, [
        // Validators.required,
      ]),
    });
  }
  ngOnInit(): void {
    this.escucharCamposDeFormulario();
    this.escucharCambiosEnFormulario();
    if (this.data) {
      cargarDatosDeInicioFormulario<AccesorioInterface>(
        this.formulario,
        this.data
      );
    }
  }

  escucharCamposDeFormulario(): void {
    const campos = { ...this.estructuraErrores };
    Object.keys(campos).forEach((key) => {
      escucharCampo(
        key,
        this.formulario,
        this.estructuraErrores,
        this.MENSAJES_DE_ERROR
      );
    });
  }

  escucharCambiosEnFormulario(): void {
    const formulario$ = this.formulario.valueChanges;
    formulario$.pipe(debounceTime(250)).subscribe(async (valoresForm) => {
      this.formularioValido = !this.formulario.invalid;
      if (this.formularioValido) {
        const values = {
          ...valoresForm,
        };
        this.datosFormulario.emit(values);
      } else {
        this.datosFormulario.emit(false);
      }
    });
  }

  enviarFormulario(): void {
    this.formulario.markAllAsTouched();
    this.escucharCamposDeFormulario();
  }

  buscarSecciones(evento: { query: string }) {
    this._cargandoService.habilitar();
    let consulta = {
      where: {
        nombre: `Like(\"%25${evento.query.trim()}%25\")`,
        estado: 1,
      },
    };
    const seccion$ = this._seccionRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    seccion$.subscribe({
      next: (respuesta: [any[], number]) => {
        this.secciones = respuesta[0];
        this._cargandoService.deshabilitar();
      },
      error: (err) => {
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        console.error({
          mensage: 'Error en autocomplete tipos de servicios',
          error: err,
        });
        this._cargandoService.deshabilitar();
      },
    });
  }

  agregarSeccion() {
    const dialogRef = this._matDialog.open(ModalCrearEditarSeccionComponent, {
      width: '450px',
      data: {},
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          const seccion$ = this._seccionRestService.create(response);

          seccion$.subscribe({
            next: (seccionCreada: SeccionInterface) => {
              this._notificacionService.modalInfo(
                'success',
                'Info',
                'Registro guardado correctamente',
              );
              this.buscarSecciones(
                {
                  query: ''
                }
              );
              this.formulario.patchValue(
                {
                  seccion: seccionCreada,
                }
              );
            },
            error: (err) => {
              console.error({
                mensage: 'Error crear Seccion',
                error: err,
              });
              this._notificacionService.modalInfo(
                'error',
                'Error',
                'Error al guardar el registro',
              );
            },
          });
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }
}
