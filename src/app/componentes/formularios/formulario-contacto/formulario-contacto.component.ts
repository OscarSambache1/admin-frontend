import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ContactoInterface} from "../../../modulos/configuracion-datos/interfaces/contacto.interface";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {cargarDatosDeInicioFormulario, escucharCampo} from "../../../utils/validar-campo-reactive-form.util";
import {debounceTime} from "rxjs/operators";
import {TIPOS_CONTACTO, TIPOS_CUENTA_BANCO} from "../../../constantes/tipos-identificacion";

@Component({
  selector: 'app-formulario-contacto',
  templateUrl: './formulario-contacto.component.html',
  styleUrls: ['./formulario-contacto.component.scss']
})
export class FormularioContactoComponent implements OnInit {


  @Input()
  data: ContactoInterface | undefined;

  @Output()
  datosFormulario: EventEmitter<ContactoInterface | boolean> = new EventEmitter<
    ContactoInterface | boolean
    >();

  formularioValido = false;
  formulario!: FormGroup;

  MENSAJES_DE_ERROR = {
    valor: {
      required: 'El campo valor es requerido',
      minLength: 'El campo valor debe tener al menos un caracter',
      maxlength:
        'El campo valor debe tener un máximo de 255 caracteres',
    },
    tipoContacto: {
      required: 'El campo tipo de contacto es requerido',
    },
  };
  estructuraErrores = {
    valor: [],
    tipoContacto: [],
  };
  tiposContactos = TIPOS_CONTACTO;

  constructor(
  ) {
    this.construirFormulario();
  }

  construirFormulario(): void {
    this.formulario = new FormGroup({
      id: new FormControl(null, []),
      valor: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      tipoContacto: new FormControl(null, [
        Validators.required,
      ]),
    });
  }

  ngOnInit(): void {
    this.escucharCamposDeFormulario();
    this.escucharCambiosEnFormulario();
    if (this.data) {
      cargarDatosDeInicioFormulario<ContactoInterface>(
        this.formulario,
        this.data
      );
    }
  }

  escucharCamposDeFormulario(): void {
    const campos = { ...this.estructuraErrores };
    Object.keys(campos).forEach((key) => {
      escucharCampo(
        key,
        this.formulario,
        this.estructuraErrores,
        this.MENSAJES_DE_ERROR
      );
    });
  }

  escucharCambiosEnFormulario(): void {
    const formulario$ = this.formulario.valueChanges;
    formulario$.pipe(debounceTime(250)).subscribe(async (valoresForm) => {
      this.formularioValido = !this.formulario.invalid;
      if (this.formularioValido) {
        const values = {
          ...valoresForm,
        };
        this.datosFormulario.emit(values);
      } else {
        this.datosFormulario.emit(false);
      }
    });
  }

  enviarFormulario(): void {
    this.formulario.markAllAsTouched();
    this.escucharCamposDeFormulario();
  }
}
