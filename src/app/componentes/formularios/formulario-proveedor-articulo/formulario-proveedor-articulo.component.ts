import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ProveedorArticuloInterface} from "../../../modulos/configuracion-datos/interfaces/proveedor-articulo.interface";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {TIPOS_CUENTA_BANCO} from "../../../constantes/tipos-identificacion";
import {ProveedorInterface} from "../../../modulos/configuracion-datos/interfaces/proveedor.interface";
import {CargandoService} from "../../../servicios/cargando.service";
import {ProveedorRestService} from "../../../modulos/configuracion-datos/servicios/proveedor-rest.service";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {MatDialog} from "@angular/material/dialog";
import {cargarDatosDeInicioFormulario, escucharCampo} from "../../../utils/validar-campo-reactive-form.util";
import {ContactoInterface} from "../../../modulos/configuracion-datos/interfaces/contacto.interface";
import {debounceTime} from "rxjs/operators";
import {ModalCrearEditarProveedorComponent} from "../../modales/modal-crear-editar-proveedor/modal-crear-editar-proveedor.component";

@Component({
  selector: 'app-formulario-proveedor-articulo',
  templateUrl: './formulario-proveedor-articulo.component.html',
  styleUrls: ['./formulario-proveedor-articulo.component.scss']
})
export class FormularioProveedorArticuloComponent implements OnInit {

  @Input()
  data: ProveedorArticuloInterface | undefined;

  @Output()
  datosFormulario: EventEmitter<ProveedorArticuloInterface | boolean> = new EventEmitter<ProveedorArticuloInterface | boolean>();

  formularioValido = false;
  formulario!: FormGroup;

  tiposCuentaProveedor = TIPOS_CUENTA_BANCO;
  MENSAJES_DE_ERROR = {
    proveedor: {
      required: 'El campo proveedor es requerido',
    },
  };
  estructuraErrores = {
    proveedor: [],
  };
  proveedores: ProveedorInterface[] = [];
  constructor(
    private readonly _cargandoService: CargandoService,
    private readonly _proveedorRestService: ProveedorRestService,
    private readonly _notificacionService: NotificacionService,
    private readonly _matDialog: MatDialog,
  ) {
    this.construirFormulario();
  }

  construirFormulario(): void {
    this.formulario = new FormGroup({
      id: new FormControl(null, []),
      proveedor: new FormControl(null, [
        Validators.required,
      ]),
    });
  }

  ngOnInit(): void {
    this.escucharCamposDeFormulario();
    this.escucharCambiosEnFormulario();
    if (this.data) {
      cargarDatosDeInicioFormulario<ContactoInterface>(
        this.formulario,
        this.data
      );
    }
  }

  escucharCamposDeFormulario(): void {
    const campos = { ...this.estructuraErrores };
    Object.keys(campos).forEach((key) => {
      escucharCampo(
        key,
        this.formulario,
        this.estructuraErrores,
        this.MENSAJES_DE_ERROR
      );
    });
  }

  escucharCambiosEnFormulario(): void {
    const formulario$ = this.formulario.valueChanges;
    formulario$.pipe(debounceTime(250)).subscribe(async (valoresForm) => {
      this.formularioValido = !this.formulario.invalid;
      if (this.formularioValido) {
        const values = {
          ...valoresForm,
        };
        this.datosFormulario.emit(values);
      } else {
        this.datosFormulario.emit(false);
      }
    });
  }

  enviarFormulario(): void {
    this.formulario.markAllAsTouched();
    this.escucharCamposDeFormulario();
  }

  buscarProveedores(evento: { query: string }) {
    this._cargandoService.habilitar();
    let consulta = {};
      consulta = {
        where: {
          estado: 1,
          // informacionTributaria: {
          //   razonSocial: `Like(\"%25${evento.query.trim()}%25\")`,
          // }
        },
        relations: ['informacionTributaria']
      };

    const proveedor$ = this._proveedorRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    proveedor$.subscribe({
      next: (respuesta: [ProveedorInterface[], number]) => {
        this.proveedores = respuesta[0].filter(proveedor => {
          const coincidencia = proveedor.informacionTributaria?.nombreComercial?.toLowerCase().search(evento.query.toLowerCase().trim());
          return coincidencia !== -1;
        });
        this._cargandoService.deshabilitar();
      },
      error: (err) => {
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        console.error({
          mensage: 'Error en autocomplete',
          error: err,
        });
        this._cargandoService.deshabilitar();
      },
    });
  }

  agregarProveedor() {
    const dialogRef = this._matDialog.open(ModalCrearEditarProveedorComponent, {
      width: '1550px',
      data: {},
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          const proveedor$ = this._proveedorRestService.create(response);

          proveedor$.subscribe({
            next: (proveedorCreado: ProveedorInterface) => {
              this._notificacionService.modalInfo(
                'success',
                'Info',
                'Registro guardado correctamente',
              );
              this.buscarProveedores(
                {
                  query: ''
                }
              );
              this.formulario.patchValue(
                {
                  proveedor: proveedorCreado,
                }
              );
            },
            error: (err) => {
              console.error({
                mensage: 'Error crear Proveedor',
                error: err,
              });
              this._notificacionService.modalInfo(
                'error',
                'Error',
                'Error al guardar el registro',
              );
            },
          });
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

}
