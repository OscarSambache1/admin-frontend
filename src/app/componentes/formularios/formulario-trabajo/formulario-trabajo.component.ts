import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TrabajoInterface} from "../../../modulos/trabajos/interfaces/trabajo.interface";
import {RecepcionInterface} from "../../../modulos/trabajos/interfaces/recepcion.interface";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ESTADOS_TRABAJO, TIPOS_IDENTIFICACION} from "../../../constantes/tipos-identificacion";
import {VehiculoInterface} from "../../../modulos/configuracion-datos/interfaces/vehiculo.interface";
import {ClienteInterface} from "../../../modulos/configuracion-datos/interfaces/cliente.interface";
import {EstadoTrabajo} from "../../../enums/tipo-contacto.enums";
import {MatDialog} from "@angular/material/dialog";
import {VehiculoRestService} from "../../../modulos/configuracion-datos/servicios/vehiculo-rest.service";
import {ClienteRestService} from "../../../modulos/configuracion-datos/servicios/cliente-rest.service";
import {CargandoService} from "../../../servicios/cargando.service";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {escucharCampo} from "../../../utils/validar-campo-reactive-form.util";
import {debounceTime} from "rxjs/operators";
import {ModalCrearEditarVehiculoComponent} from "../../modales/modal-crear-editar-vehiculo/modal-crear-editar-vehiculo.component";
import {ModalCrearEditarClienteComponent} from "../../modales/modal-crear-editar-cliente/modal-crear-editar-cliente.component";
import {setearClienteAEditar} from "../../../modulos/configuracion-datos/cliente/ruta-cliente/ruta-cliente.component";
import {InformacionTributariaInterface} from "../../../modulos/configuracion-datos/interfaces/informacion-tributaria.interface";

@Component({
  selector: 'app-formulario-trabajo',
  templateUrl: './formulario-trabajo.component.html',
  styleUrls: ['./formulario-trabajo.component.scss']
})
export class FormularioTrabajoComponent implements OnInit {

  @Input()
  data: TrabajoInterface | undefined;

  @Input()
  soloVer = false;

  recepcion: any | RecepcionInterface;
  @Output()
  datosFormulario: EventEmitter<TrabajoInterface | boolean> = new EventEmitter<
    TrabajoInterface | boolean
    >();

  formularioValido = false;
  formulario!: FormGroup;

  estados = ESTADOS_TRABAJO;

  MENSAJES_DE_ERROR = {
    vehiculo: {
      required: 'La placa del vehículo es requerido',
    },
    cliente: {
      required: 'El cliente es requerido',
    },
    nombreComercial: {
      required: 'El campo nombre comercial es requerido',
      minLength: 'El campo nombre comercial debe tener al menos un caracter',
      maxlength:
        'El campo nombre comercial debe tener un máximo de 255 caracteres',
    },
    razonSocial: {
      required: 'El campo razón social es requerido',
      minLength: 'El campo razón social debe tener al menos un caracter',
      maxlength:
        'El campo razón social debe tener un máximo de 255 caracteres',
    },
    tipoIdentificacion: {
      required: 'El campo tipo de identificación es requerido',
    },
    identificacion: {
      required: 'El campo identificación es requerido',
      minLength: 'El campo identificación debe tener al menos un caracter',
      maxlength:
        'El campo identificación debe tener un máximo de 50 caracteres',
    },
    direccion: {
      required: 'El campo dirección es requerido',
      minLength: 'El campo dirección debe tener al menos un caracter',
      maxlength:
        'El campo dirección debe tener un máximo de 255 caracteres',
    },
    telefono: {
      required: 'El campo teléfono es requerido',
      minLength: 'El campo teléfono debe tener al menos un caracter',
      maxlength:
        'El campo teléfono debe tener un máximo de 15 caracteres',
    },
    correo: {
      required: 'El campo correo es requerido',
      minLength: 'El campo correo debe tener al menos un caracter',
      maxlength:
        'El campo correo debe tener un máximo de 150 caracteres',
      pattern: 'El campo correo no es válido',
    },


    marca: {
      required: 'El campo marca es requerido',
    },
    modelo: {
      required: 'El campo marca es requerido',
    },
    color: {
      required: 'El campo color es requerido',
      minLength: 'El campo color debe tener al menos un caracter',
      maxlength:
        'El campo color debe tener un máximo de 150 caracteres',
    },
    chasis: {
      required: 'El campo chasis es requerido',
      minLength: 'El campo chasis debe tener al menos un caracter',
      maxlength:
        'El campo chasis debe tener un máximo de 150 caracteres',
    },
    motor: {
      required: 'El campo motor es requerido',
      minLength: 'El campo motor debe tener al menos un caracter',
      maxlength:
        'El campo motor debe tener un máximo de 150 caracteres',
    },
    anio: {
      required: 'El campo anio es requerido',
    },
  };
  estructuraErrores = {
    vehiculo: [],
    modelo: [],
    color: [],
    chasis: [],
    motor: [],
    anio: [],
    marca: [],

    cliente: [],
    identificacion: [],
    direccion: [],
    correo: [],
    telefono: [],
    nombreComercial: [],
    razonSocial: [],
    tipoIdentificacion: [],
  };
  vehiculos: VehiculoInterface[] = [];
  clientes: ClienteInterface[] = [];
  estadosTrabajo = EstadoTrabajo;
  tiposIdentificacion = TIPOS_IDENTIFICACION;

  constructor(
    private readonly _matDialog: MatDialog,
    private readonly _vehiculoRestService: VehiculoRestService,
    private readonly _clienteRestService: ClienteRestService,
    private readonly _cargandoService: CargandoService,
    private readonly _notificacionService: NotificacionService,
  ) {
    this.construirFormulario();
  }

  construirFormulario(): void {
    this.formulario = new FormGroup({
      id: new FormControl(null, []),
      vehiculo: new FormControl(null, [
        Validators.required
      ]),
      cliente: new FormControl(null, [
        Validators.required
      ]),
      nombreComercial: new FormControl(null, []),
      razonSocial: new FormControl(null, []),
      tipoIdentificacion: new FormControl(null, []),
      identificacion: new FormControl(null, []),
      direccion: new FormControl(null, []),
      telefono: new FormControl(null, []),
      correo: new FormControl(null, []),
      modelo: new FormControl(null, []),
      marca: new FormControl(null, []),
      color: new FormControl(null, []),
      chasis: new FormControl(null, []),
      motor: new FormControl(null, []),
      anio: new FormControl(null, []),
    });
  }

  ngOnInit(): void {
    this.escucharCamposDeFormulario();
    this.escucharCambiosEnFormulario();
    this.cargarDatosDeInicioFormulario();
  }

  escucharCamposDeFormulario(): void {
    const campos = { ...this.estructuraErrores };
    Object.keys(campos).forEach((key) => {
      escucharCampo(
        key,
        this.formulario,
        this.estructuraErrores,
        this.MENSAJES_DE_ERROR
      );
    });
  }

  escucharCambiosEnFormulario(): void {
    const formulario$ = this.formulario.valueChanges;
    formulario$.pipe(debounceTime(250)).subscribe(async (valoresForm) => {
      this.formularioValido = !this.formulario.invalid;
      if (this.formularioValido) {
        this.datosFormulario.emit(valoresForm);
      } else {
        this.datosFormulario.emit(false);
      }
    });
  }

  enviarFormulario(): void {
    this.formulario.markAllAsTouched();
    this.escucharCamposDeFormulario();
  }

  cargarDatosDeInicioFormulario() {
    if (this.data) {
      console.log(this.data);
      const vehiculo = this.data?.vehiculo;
      const cliente = vehiculo?.cliente;
      if (cliente) {
        cliente.label = setearLabelInformacionTributaria(cliente?.informacionTributaria as InformacionTributariaInterface);
      }
      this.formulario.patchValue(
        {
          id: this.data?.id,
          vehiculo: vehiculo,
          cliente,
          placa: this.data.placa,
          chasis: this.data.chasis,
          anio: this.data.anio,
          color: this.data.color,
          marca: this.data.marca,
          modelo: this.data.modelo,
          motor: this.data.motor,
          razonSocial: this.data.razonSocial,
          nombreComercial: this.data.nombreComercial,
          identificacion: this.data.identificacion,
          tipoIdentificacion: this.data.tipoIdentificacion,
          direccion: this.data.direccion,
          telefono: this.data.telefono,
          correo: this.data.correo,
        }
      );
      if (this.data.recepciones) {
        this.recepcion = this.data.recepciones[0];
      }
    }
  }

  buscarVehiculos(evento: { query: string }) {
    this._cargandoService.habilitar();
    const cliente = this.formulario.get('cliente')?.value;
    let consulta = {
      where: {
        placa: `Like(\"%25${evento.query.trim()}%25\")`,
        estado: 1,
        cliente: cliente.id,
      },
      relations: ['marca']
    };
    const vehiculo$ = this._vehiculoRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    vehiculo$.subscribe({
      next: (respuesta: [any[], number]) => {
        this.vehiculos = respuesta[0];
        this._cargandoService.deshabilitar();
      },
      error: (err) => {
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        console.error({
          mensage: 'Error en autocomplete',
          error: err,
        });
        this._cargandoService.deshabilitar();
      },
    });
  }

  agregarVehiculo(vehiculo?: VehiculoInterface) {
    const cliente = this.formulario.get('cliente')?.value;
    const dialogRef = this._matDialog.open(ModalCrearEditarVehiculoComponent, {
      width: '1050px',
      data: {
        esModalCliente: true,
        cliente,
        vehiculo
      },
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          const vehiculo$ = this._vehiculoRestService.create(response);

          vehiculo$.subscribe({
            next: (vehiculoBase: VehiculoInterface) => {
              this._notificacionService.modalInfo(
                'success',
                'Info',
                'Registro guardado correctamente',
              );
              this.buscarVehiculos(
                {
                  query: ''
                }
              );
              this.formulario.patchValue(
                {
                  vehiculo: vehiculoBase,
                  marca: vehiculoBase.marca?.nombre,
                  modelo: vehiculoBase.modelo,
                  anio: vehiculoBase.anio,
                  chasis: vehiculoBase.chasis,
                  color: vehiculoBase.color,
                  motor: vehiculoBase.motor,
                }
              );
            },
            error: (err) => {
              console.error({
                mensage: 'Error crear Vehiculo',
                error: err,
              });
              this._notificacionService.modalInfo(
                'error',
                'Error',
                'Error al guardar el registro',
              );
            },
          });
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

  buscarClientes(evento: any) {
    this._cargandoService.habilitar();
    let consulta = {
      where: {
        estado: 1
      },
      relations: ['informacionTributaria']
    };

    const cliente$ = this._clienteRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    cliente$.subscribe({
      next: (respuesta: [any[], number]) => {
        this.clientes = respuesta[0].filter(cliente => {
          const informacionTributaria = cliente.informacionTributaria;
          cliente.label = setearLabelInformacionTributaria(informacionTributaria);
          const buscarRazonSocial = (informacionTributaria.razonSocial || '').toUpperCase().search(evento.query.toUpperCase()) !== -1;
          const buscarNombreComercial = (informacionTributaria.nombreComercial || '').toUpperCase().search(evento.query.toUpperCase()) !== -1;
          const buscarIdentificacion = (informacionTributaria.identificacion || '').toUpperCase().search(evento.query.toUpperCase()) !== -1;
          return buscarRazonSocial || buscarNombreComercial || buscarIdentificacion;
        });
        this._cargandoService.deshabilitar();
      },
      error: (err) => {
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        console.error({
          mensage: 'Error en autocomplete',
          error: err,
        });
        this._cargandoService.deshabilitar();
      },
    });
  }

  editarCliente() {
    const cliente = this.formulario.get('cliente')?.value;
    if (cliente) {
      this.agregarCliente(cliente);
    } else {
      this._notificacionService.modalInfoAsync(
        'question',
        'Advertencia',
        'Busque y Seleccione un cliente/propietario para cambiar la información',
      );
    }
  }


  editarVehiculo() {
    const vehiculo = this.formulario.get('vehiculo')?.value;
    if (vehiculo) {
      this.agregarVehiculo(vehiculo);
    } else {
      this._notificacionService.modalInfoAsync(
        'question',
        'Advertencia',
        'Busque y Seleccione un vehículo para cambiar la información',
      );
    }
  }

  agregarCliente(cliente?: ClienteInterface) {
    if (cliente) {
      setearClienteAEditar(cliente);
    }
    const dialogRef = this._matDialog.open(ModalCrearEditarClienteComponent, {
      width: '1750px',
      data: {
        cliente
      },
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          if (cliente) {
            response.id = cliente.id;
            response.idInfoTributaria = cliente.informacionTributaria?.id;
          }
          const cliente$ = this._clienteRestService.crearEditarCliente(response);

          cliente$.subscribe({
            next: (clienteCreada: ClienteInterface) => {
              this._notificacionService.modalInfo(
                'success',
                'Info',
                'Registro guardado correctamente',
              );
              this.buscarClientes(
                {
                  query: ''
                }
              );
              clienteCreada.label = setearLabelInformacionTributaria(clienteCreada?.informacionTributaria as InformacionTributariaInterface);
              const informacionTributaria = clienteCreada.informacionTributaria;
              this.formulario.patchValue(
                {
                  cliente: clienteCreada,
                  identificacion: informacionTributaria?.identificacion,
                  direccion: informacionTributaria?.direccion,
                  correo: informacionTributaria?.correo,
                  telefono: informacionTributaria?.telefono,
                  nombreComercial: informacionTributaria?.nombreComercial,
                  razonSocial: informacionTributaria?.razonSocial,
                  placa: null,
                  vehiculo: null,
                  marca: null,
                  modelo: null,
                  anio: null,
                  chasis: null,
                  color: null,
                  motor: null,
                }
              );
            },
            error: (err) => {
              console.error({
                mensage: 'Error crear Cliente',
                error: err,
              });
              this._notificacionService.modalInfo(
                'error',
                'Error',
                'Error al guardar el registro',
              );
            },
          });
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

  seleccionoCliente() {
    return !!this.formulario?.get('cliente')?.value;
  }

  seleccionoClienteAutoncomplete() {
    const cliente = this.formulario.get('cliente')?.value;
    const informacionTriobutaria = cliente?.informacionTributaria;
    this.formulario.patchValue(
      {
        identificacion: informacionTriobutaria?.identificacion,
        direccion: informacionTriobutaria?.direccion,
        correo: informacionTriobutaria?.correo,
        telefono: informacionTriobutaria?.telefono,
        nombreComercial: informacionTriobutaria?.nombreComercial,
        razonSocial: informacionTriobutaria?.razonSocial,
        marca: null,
        modelo: null,
        vehiculo: null,
        anio: null,
        chasis: null,
        color: null,
        motor: null,
      }
    );
  }

  seleccionoVehiculoAutoncomplete() {
    const vehiculo = this.formulario.get('vehiculo')?.value;
    this.formulario.patchValue(
      {
        marca: vehiculo.marca?.nombre,
        modelo: vehiculo.modelo,
        vehiculo: vehiculo,
        anio: vehiculo.anio,
        chasis: vehiculo.chasis,
        color: vehiculo.color,
        motor: vehiculo.motor,
      }
    );
  }
}

export function setearLabelInformacionTributaria (informacionTributaria: InformacionTributariaInterface) {
  return informacionTributaria.nombreComercial + ' - ' + informacionTributaria.identificacion;
}
