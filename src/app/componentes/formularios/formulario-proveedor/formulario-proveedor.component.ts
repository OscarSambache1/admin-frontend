import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ProveedorInterface} from "../../../modulos/configuracion-datos/interfaces/proveedor.interface";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {cargarDatosDeInicioFormulario, escucharCampo} from "../../../utils/validar-campo-reactive-form.util";
import {AccesorioInterface} from "../../../modulos/configuracion-datos/interfaces/accesorio.interface";
import {debounceTime} from "rxjs/operators";
import {TIPOS_IDENTIFICACION} from "../../../constantes/tipos-identificacion";

@Component({
  selector: 'app-formulario-proveedor',
  templateUrl: './formulario-proveedor.component.html',
  styleUrls: ['./formulario-proveedor.component.scss']
})
export class FormularioProveedorComponent implements OnInit {

  @Input()
  data: ProveedorInterface | undefined;

  @Input()
  soloVer!: any;

  @Output()
  datosFormulario: EventEmitter<ProveedorInterface | boolean> = new EventEmitter<ProveedorInterface | boolean>();

  formularioValido = false;
  formulario!: FormGroup;

  tiposIdentificacion = TIPOS_IDENTIFICACION;
  MENSAJES_DE_ERROR = {
    nombreComercial: {
      required: 'El campo nombre comercial es requerido',
      minLength: 'El campo nombre comercial debe tener al menos un caracter',
      maxlength:
        'El campo nombre comercial debe tener un máximo de 255 caracteres',
    },
    razonSocial: {
      required: 'El campo razón social es requerido',
      minLength: 'El campo razón social debe tener al menos un caracter',
      maxlength:
        'El campo razón social debe tener un máximo de 255 caracteres',
    },
    tipoIdentificacion: {
      required: 'El campo tipo de identificación es requerido',
    },
    identificacion: {
      required: 'El campo identificación es requerido',
      minLength: 'El campo identificación debe tener al menos un caracter',
      maxlength:
        'El campo identificación debe tener un máximo de 50 caracteres',
    },
    direccion: {
      required: 'El campo dirección es requerido',
      minLength: 'El campo dirección debe tener al menos un caracter',
      maxlength:
        'El campo dirección debe tener un máximo de 255 caracteres',
    },
    telefono: {
      required: 'El campo teléfono es requerido',
      minLength: 'El campo teléfono debe tener al menos un caracter',
      maxlength:
        'El campo teléfono debe tener un máximo de 15 caracteres',
    },
    correo: {
      required: 'El campo correo es requerido',
      minLength: 'El campo correo debe tener al menos un caracter',
      maxlength:
        'El campo correo debe tener un máximo de 150 caracteres',
      pattern: 'El campo correo no es válido',
    },
    responsable: {
      required: 'El campo responsable es requerido',
      minLength: 'El campo responsable debe tener al menos un caracter',
      maxlength:
        'El campo responsable debe tener un máximo de 255 caracteres',
    },
    ciudad: {
      required: 'El campo ciudad es requerido',
      minLength: 'El campo ciudad debe tener al menos un caracter',
      maxlength:
        'El campo ciudad debe tener un máximo de 255 caracteres',
    },
    provincia: {
      required: 'El campo provincia es requerido',
      minLength: 'El campo provincia debe tener al menos un caracter',
      maxlength:
        'El campo provincia debe tener un máximo de 255 caracteres',
    },
    pais: {
      required: 'El campo pais es requerido',
      minLength: 'El campo pais debe tener al menos un caracter',
      maxlength:
        'El campo pais debe tener un máximo de 255 caracteres',
    },
    whatsapp: {
      required: 'El campo whatsapp es requerido',
      minLength: 'El campo whatsapp debe tener al menos un caracter',
      maxlength:
        'El campo whatsapp debe tener un máximo de 20 caracteres',
    },
    web: {
      required: 'El campo web es requerido',
      minLength: 'El campo web debe tener al menos un caracter',
      maxlength:
        'El campo web debe tener un máximo de 255 caracteres',
    },
    observacion: {
      required: 'El campo observación es requerido',
      minLength: 'El campo observación debe tener al menos un caracter',
      maxlength:
        'El campo observación debe tener un máximo de 500 caracteres',
    },
  };
  estructuraErrores = {
    nombreComercial: [],
    razonSocial: [],
    tipoIdentificacion: [],
    identificacion: [],
    direccion: [],
    telefono: [],
    responsable: [],
    ciudad: [],
    provincia: [],
    pais: [],
    whatsapp: [],
    web: [],
    observacion: [],
    correo: [],
  };
  constructor() {
    this.construirFormulario();
  }

  construirFormulario(): void {
    this.formulario = new FormGroup({
      nombreComercial: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      razonSocial: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      tipoIdentificacion: new FormControl(null, [
        Validators.required,
      ]),
      identificacion: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(50),
      ]),
      direccion: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      telefono: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(15),
      ]),
      responsable: new FormControl(null, [
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      ciudad: new FormControl(null, [
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      provincia: new FormControl(null, [
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      pais: new FormControl(null, [
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      whatsapp: new FormControl(null, [
        Validators.minLength(1),
        Validators.maxLength(20),
      ]),
      web: new FormControl(null, [
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      observacion: new FormControl(null, [
        Validators.minLength(1),
        Validators.maxLength(500),
      ]),
      correo: new FormControl(null, [
        Validators.minLength(1),
        Validators.maxLength(100),
        Validators.pattern(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/),
      ]),
    });
  }

  ngOnInit(): void {
    this.escucharCamposDeFormulario();
    this.escucharCambiosEnFormulario();
    if (this.data) {
      cargarDatosDeInicioFormulario<AccesorioInterface>(
        this.formulario,
        this.data
      );
    }
  }

  escucharCamposDeFormulario(): void {
    const campos = { ...this.estructuraErrores };
    Object.keys(campos).forEach((key) => {
      escucharCampo(
        key,
        this.formulario,
        this.estructuraErrores,
        this.MENSAJES_DE_ERROR
      );
    });
  }

  escucharCambiosEnFormulario(): void {
    const formulario$ = this.formulario.valueChanges;
    formulario$.pipe(debounceTime(250)).subscribe(async (valoresForm) => {
      this.formularioValido = !this.formulario.invalid;
      if (this.formularioValido) {
        const values = {
          ...valoresForm,
        };
        this.datosFormulario.emit(values);
      } else {
        this.datosFormulario.emit(false);
      }
    });
  }

  enviarFormulario(): void {
    this.formulario.markAllAsTouched();
    this.escucharCamposDeFormulario();
  }
}
