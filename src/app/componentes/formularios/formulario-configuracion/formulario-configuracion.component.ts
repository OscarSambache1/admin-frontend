import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {TIPOS_IDENTIFICACION} from "../../../constantes/tipos-identificacion";
import {cargarDatosDeInicioFormulario, escucharCampo} from "../../../utils/validar-campo-reactive-form.util";
import {AccesorioInterface} from "../../../modulos/configuracion-datos/interfaces/accesorio.interface";
import {debounceTime} from "rxjs/operators";
import {ConfiguracionInterface} from "../../../interfaces/configuracion.interface";
import {DomSanitizer} from "@angular/platform-browser";
import { Buffer } from 'buffer';

@Component({
  selector: 'app-formulario-configuracion',
  templateUrl: './formulario-configuracion.component.html',
  styleUrls: ['./formulario-configuracion.component.scss']
})
export class FormularioConfiguracionComponent implements OnInit {

  @Input()
  data: ConfiguracionInterface | undefined;

  @Output()
  datosFormulario: EventEmitter<ConfiguracionInterface | boolean> = new EventEmitter<ConfiguracionInterface | boolean>();

  formularioValido = false;
  formulario!: FormGroup;

  tiposIdentificacion = TIPOS_IDENTIFICACION;
  MENSAJES_DE_ERROR = {
    nombreComercial: {
      required: 'El campo nombre comercial es requerido',
      minLength: 'El campo nombre comercial debe tener al menos un caracter',
      maxlength:
        'El campo nombre comercial debe tener un máximo de 255 caracteres',
    },
    razonSocial: {
      required: 'El campo razón social es requerido',
      minLength: 'El campo razón social debe tener al menos un caracter',
      maxlength:
        'El campo razón social debe tener un máximo de 255 caracteres',
    },
    direccion: {
      required: 'El campo dirección es requerido',
      minLength: 'El campo dirección debe tener al menos un caracter',
      maxlength:
        'El campo dirección debe tener un máximo de 255 caracteres',
    },
    telefono: {
      required: 'El campo teléfono es requerido',
      minLength: 'El campo teléfono debe tener al menos un caracter',
      maxlength:
        'El campo teléfono debe tener un máximo de 15 caracteres',
    },
    correo: {
      required: 'El campo correo es requerido',
      minLength: 'El campo correo debe tener al menos un caracter',
      maxlength:
        'El campo correo debe tener un máximo de 150 caracteres',
      pattern: 'El campo correo no es válido',
    },
    identificacion: {
      required: 'El campo identificación es requerido',
      minLength: 'El campo identificación debe tener al menos un caracter',
      maxlength:
        'El campo identificación debe tener un máximo de 50 caracteres',
    },
  };
  estructuraErrores = {
    nombreComercial: [],
    razonSocial: [],
    direccion: [],
    telefono: [],
    correo: [],
    identificacion: [],
  };

  imagen?: any;

  constructor(
    public readonly _domSanitizer: DomSanitizer,

  ) {
    this.construirFormulario();
  }

  construirFormulario(): void {
    this.formulario = new FormGroup({
      nombreComercial: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      razonSocial: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      direccion: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      telefono: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(15),
      ]),
      correo: new FormControl(null, [
        Validators.minLength(1),
        Validators.maxLength(100),
        Validators.pattern(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/),
      ]),
      identificacion: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(50),
      ]),
    });
  }

  ngOnInit(): void {
    this.escucharCamposDeFormulario();
    this.escucharCambiosEnFormulario();
    if (this.data) {
      cargarDatosDeInicioFormulario<AccesorioInterface>(
        this.formulario,
        this.data
      );
    }
  }

  escucharCamposDeFormulario(): void {
    const campos = {...this.estructuraErrores};
    Object.keys(campos).forEach((key) => {
      escucharCampo(
        key,
        this.formulario,
        this.estructuraErrores,
        this.MENSAJES_DE_ERROR
      );
    });
  }

  escucharCambiosEnFormulario(): void {
    const formulario$ = this.formulario.valueChanges;
    formulario$.pipe(debounceTime(250)).subscribe(async (valoresForm) => {
      this.formularioValido = !this.formulario.invalid;
      if (this.formularioValido) {
        const values = {
          ...valoresForm,
          imagen: this.imagen
        };
        this.datosFormulario.emit(values);
      } else {
        this.datosFormulario.emit(false);
      }
    });
  }

  enviarFormulario(): void {
    this.formulario.markAllAsTouched();
    this.escucharCamposDeFormulario();
  }


  setearUrlImagen() {
    if (this.data) {
      if (this.data.archivos?.length) {
        const imagen = this.data.archivos[0];
        const buffer = imagen.buffer?.data;
        return this._domSanitizer.bypassSecurityTrustUrl(`data:${imagen.mimetype};base64, ${Buffer.from(buffer)}`) as any;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }

  cargarImagen(imagen: any) {
    this.imagen = imagen;
    this.formularioValido = !this.formulario.invalid;
    if (this.formularioValido) {
      const values = {
        ...this.formulario.value,
        imagen: this.imagen
      };
      this.datosFormulario.emit(values);
    } else {
      this.datosFormulario.emit(false);
    }
  }
}
