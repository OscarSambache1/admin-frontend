import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TipoCombustibleInterface} from "../../../modulos/configuracion-datos/interfaces/tipo-combustible.interface";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {cargarDatosDeInicioFormulario, escucharCampo} from "../../../utils/validar-campo-reactive-form.util";
import {debounceTime} from "rxjs/operators";

@Component({
  selector: 'app-formulario-tipo-combustible',
  templateUrl: './formulario-tipo-combustible.component.html',
  styleUrls: ['./formulario-tipo-combustible.component.scss']
})
export class FormularioTipoCombustibleComponent implements OnInit {

  @Input()
  data: TipoCombustibleInterface | undefined;

  @Output()
  datosFormulario: EventEmitter<TipoCombustibleInterface | boolean> = new EventEmitter<
    TipoCombustibleInterface | boolean
    >();

  formularioValido = false;
  formulario!: FormGroup;

  MENSAJES_DE_ERROR = {
    nombre: {
      required: 'El campo nombre es requerido',
      minLength: 'El campo nombre debe tener al menos un caracter',
      maxlength:
        'El campo nombre debe tener un máximo de 255 caracteres',
    }
  };
  estructuraErrores = {
    nombre: [],
  };

  constructor(
  ) {
    this.construirFormulario();
  }

  construirFormulario(): void {
    this.formulario = new FormGroup({
      nombre: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
    });
  }

  ngOnInit(): void {
    this.escucharCamposDeFormulario();
    this.escucharCombustiblesEnFormulario();
    if (this.data) {
      cargarDatosDeInicioFormulario<TipoCombustibleInterface>(
        this.formulario,
        this.data
      );
    }
  }

  escucharCamposDeFormulario(): void {
    const campos = { ...this.estructuraErrores };
    Object.keys(campos).forEach((key) => {
      escucharCampo(
        key,
        this.formulario,
        this.estructuraErrores,
        this.MENSAJES_DE_ERROR
      );
    });
  }

  escucharCombustiblesEnFormulario(): void {
    const formulario$ = this.formulario.valueChanges;
    formulario$.pipe(debounceTime(250)).subscribe(async (valoresForm) => {
      this.formularioValido = !this.formulario.invalid;
      if (this.formularioValido) {
        const values = {
          ...valoresForm,
        };
        this.datosFormulario.emit(values);
      } else {
        this.datosFormulario.emit(false);
      }
    });
  }

  enviarFormulario(): void {
    this.formulario.markAllAsTouched();
    this.escucharCamposDeFormulario();
  }
}
