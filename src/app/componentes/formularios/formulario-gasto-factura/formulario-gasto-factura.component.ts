import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {EstadoGastoAccion, TipoFactura} from "../../../enums/tipo-contacto.enums";
import {GastoInterface} from "../../../modulos/gastos/interfaces/gasto.interface";
import {FacturaCabeceraInterface} from "../../../modulos/gastos/interfaces/factura-cabecera.interface";
import {FormularioGastoComponent} from "../formulario-gasto/formulario-gasto.component";

@Component({
  selector: 'app-formulario-gasto-factura',
  templateUrl: './formulario-gasto-factura.component.html',
  styleUrls: ['./formulario-gasto-factura.component.scss']
})
export class FormularioGastoFacturaComponent implements OnInit {
  @ViewChild(FormularioGastoComponent)
  formularioGasto!: FormularioGastoComponent;

  @Output() eventoFormularioGasto = new EventEmitter<any>();
  @Output() eventoFormularioFactura = new EventEmitter<any>();

  estaCargando = false;
  enumsEstadoGastoAccion = EstadoGastoAccion;
  mostrarFactura = true;
  tipoGasto?: string = '';

  @Input()
  gastoPrevio?: GastoInterface;

  @Input()
  soloVer?: boolean;

  @Input()
  facturaPrevia?: FacturaCabeceraInterface;
  tipoFactura = TipoFactura;


  constructor() { }

  ngOnInit(): void {
    if (this.gastoPrevio?.tipoGasto) {
      this.tipoGasto = this.gastoPrevio?.tipoGasto;
    }
  }

  setearGasto(gasto: string) {
    this.mostrarFactura = false;
    this.tipoGasto = gasto;
    setTimeout(() => {
      this.mostrarFactura = true;
    }, 500);
  }

  obtnerGasto(gasto: GastoInterface | boolean) {
    this.eventoFormularioGasto.emit(gasto);
  }

  recibirValorTotal(valorTotal: any) {
    if (this.formularioGasto?.formulario) {
      this.formularioGasto.formulario.patchValue(
        {
          valorAPagar: valorTotal,
        }
      )
    }
  }

  recibirValorPagado(valorPagado: any) {
    if (this.formularioGasto?.formulario) {
      this.formularioGasto.formulario.patchValue(
        {
          valorPagado,
        }
      )
    }
  }

  obtenerFactura(factura: FacturaCabeceraInterface) {
    this.eventoFormularioFactura.emit(factura);
  }
}
