import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SubseccionInterface} from "../../../modulos/configuracion-datos/interfaces/subseccion.interface";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {cargarDatosDeInicioFormulario, escucharCampo} from "../../../utils/validar-campo-reactive-form.util";
import {debounceTime} from "rxjs/operators";
import {SeccionInterface} from "../../../modulos/configuracion-datos/interfaces/seccion.interface";
import {CargandoService} from "../../../servicios/cargando.service";
import {SeccionRestService} from "../../../modulos/configuracion-datos/servicios/seccion-rest.service";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {ModalCrearEditarSeccionComponent} from "../../modales/modal-crear-editar-seccion/modal-crear-editar-seccion.component";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-formulario-subseccion',
  templateUrl: './formulario-subseccion.component.html',
  styleUrls: ['./formulario-subseccion.component.scss']
})
export class FormularioSubseccionComponent implements OnInit {



  @Input()
  data: SubseccionInterface | undefined;

  @Output()
  datosFormulario: EventEmitter<SubseccionInterface | boolean> = new EventEmitter<
    SubseccionInterface | boolean
    >();

  formularioValido = false;
  formulario!: FormGroup;
  secciones: SeccionInterface[] = [];
  MENSAJES_DE_ERROR = {
    nombre: {
      required: 'El campo nombre es requerido',
      minLength: 'El campo nombre debe tener al menos un caracter',
      maxlength:
        'El campo nombre debe tener un máximo de 255 caracteres',
    },
    seccion: {
      required: 'El campo sección es requerido',
    }
  };
  estructuraErrores = {
    nombre: [],
    seccion: [],
  };

  constructor(
    private readonly _cargandoService: CargandoService,
    private readonly _seccionRestService: SeccionRestService,
    private readonly _notificacionService: NotificacionService,
    private readonly _matDialog: MatDialog,
  ) {
    this.construirFormulario();
  }

  construirFormulario(): void {
    this.formulario = new FormGroup({
      nombre: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      seccion: new FormControl(null, [
        Validators.required,
      ]),
    });
  }

  ngOnInit(): void {
    this.escucharCamposDeFormulario();
    this.escucharCambiosEnFormulario();
    if (this.data) {
      cargarDatosDeInicioFormulario<SubseccionInterface>(
        this.formulario,
        this.data
      );
    }
  }

  escucharCamposDeFormulario(): void {
    const campos = { ...this.estructuraErrores };
    Object.keys(campos).forEach((key) => {
      escucharCampo(
        key,
        this.formulario,
        this.estructuraErrores,
        this.MENSAJES_DE_ERROR
      );
    });
  }

  escucharCambiosEnFormulario(): void {
    const formulario$ = this.formulario.valueChanges;
    formulario$.pipe(debounceTime(250)).subscribe(async (valoresForm) => {
      this.formularioValido = !this.formulario.invalid;
      if (this.formularioValido) {
        const values = {
          ...valoresForm,
        };
        this.datosFormulario.emit(values);
      } else {
        this.datosFormulario.emit(false);
      }
    });
  }

  enviarFormulario(): void {
    this.formulario.markAllAsTouched();
    this.escucharCamposDeFormulario();
  }

  buscarSecciones(evento: { query: string }) {
    this._cargandoService.habilitar();
    let consulta = {};
    if (evento.query === '') {
      consulta = {
        where: {},
      };
    } else {
      consulta = {
        where: {
          nombre: `Like(\"%25${evento.query.trim()}%25\")`,
        },
      };
    }
    const seccion$ = this._seccionRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    seccion$.subscribe({
      next: (respuesta: [any[], number]) => {
        this.secciones = respuesta[0];
        this._cargandoService.deshabilitar();
      },
      error: (err) => {
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        console.error({
          mensage: 'Error en autocomplete tipos de productos',
          error: err,
        });
        this._cargandoService.deshabilitar();
      },
    });
  }

  agregarSeccion() {
    const dialogRef = this._matDialog.open(ModalCrearEditarSeccionComponent, {
      width: '450px',
      data: {},
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          const seccion$ = this._seccionRestService.create(response);

          seccion$.subscribe({
            next: (seccionCreada: SeccionInterface) => {
              this._notificacionService.modalInfo(
                'success',
                'Info',
                'Registro guardado correctamente',
              );
              this.buscarSecciones(
                {
                  query: ''
                }
              );
              this.formulario.patchValue(
                {
                  seccion: seccionCreada,
                }
              );
            },
            error: (err) => {
              console.error({
                mensage: 'Error crear Seccion',
                error: err,
              });
              this._notificacionService.modalInfo(
                'error',
                'Error',
                'Error al guardar el registro',
              );
            },
          });
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }
}
