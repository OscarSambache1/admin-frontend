import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {cargarDatosDeInicioFormulario, escucharCampo} from "../../../utils/validar-campo-reactive-form.util";
import {AccesorioInterface} from "../../../modulos/configuracion-datos/interfaces/accesorio.interface";
import {debounceTime} from "rxjs/operators";
import {ModalCrearEditarProveedorComponent} from "../../modales/modal-crear-editar-proveedor/modal-crear-editar-proveedor.component";
import {ProveedorInterface} from "../../../modulos/configuracion-datos/interfaces/proveedor.interface";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../servicios/cargando.service";
import {ProveedorRestService} from "../../../modulos/configuracion-datos/servicios/proveedor-rest.service";
import {FacturaCabeceraInterface} from "../../../modulos/gastos/interfaces/factura-cabecera.interface";
import {setearLabelInformacionTributaria} from "../formulario-trabajo/formulario-trabajo.component";
import {InformacionTributariaInterface} from "../../../modulos/configuracion-datos/interfaces/informacion-tributaria.interface";
import {setearProveedorAEditar} from "../../../modulos/configuracion-datos/cliente/ruta-cliente/ruta-cliente.component";

@Component({
  selector: 'app-formulario-factura-proveedor',
  templateUrl: './formulario-factura-proveedor.component.html',
  styleUrls: ['./formulario-factura-proveedor.component.scss']
})
export class FormularioFacturaProveedorComponent implements OnInit {
  @Input()
  data: ProveedorInterface | undefined;

  @Output()
  datosFormulario: EventEmitter<ProveedorInterface | boolean> = new EventEmitter<ProveedorInterface | boolean>();

  formularioValido = false;
  formulario!: FormGroup;

  MENSAJES_DE_ERROR = {
    nombreComercial: {
      required: 'El campo nombre comercial es requerido',
      minLength: 'El campo nombre comercial debe tener al menos un caracter',
      maxlength:
        'El campo nombre comercial debe tener un máximo de 255 caracteres',
    },
    razonSocial: {
      required: 'El campo razón social es requerido',
      minLength: 'El campo razón social debe tener al menos un caracter',
      maxlength:
        'El campo razón social debe tener un máximo de 255 caracteres',
    },
    identificacion: {
      required: 'El campo identificación es requerido',
      minLength: 'El campo identificación debe tener al menos un caracter',
      maxlength:
        'El campo identificación debe tener un máximo de 50 caracteres',
    },
    direccion: {
      required: 'El campo dirección es requerido',
      minLength: 'El campo dirección debe tener al menos un caracter',
      maxlength:
        'El campo dirección debe tener un máximo de 255 caracteres',
    },
    telefono: {
      required: 'El campo teléfono es requerido',
      minLength: 'El campo teléfono debe tener al menos un caracter',
      maxlength:
        'El campo teléfono debe tener un máximo de 15 caracteres',
    },
    correo: {
      required: 'El campo correo es requerido',
      minLength: 'El campo correo debe tener al menos un caracter',
      maxlength:
        'El campo correo debe tener un máximo de 150 caracteres',
      pattern: 'El campo correo no es válido',
    },
    proveedor: {
      required: 'El campo proveedor es requerido',
    },
  };
  estructuraErrores = {
    nombreComercial: [],
    razonSocial: [],
    identificacion: [],
    direccion: [],
    telefono: [],
    correo: [],
    proveedor: [],
  };
  proveedores: ProveedorInterface[] = [];
  constructor(
    private readonly _notificacionService: NotificacionService,
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _proveedorRestService: ProveedorRestService,
  ) {
    this.construirFormulario();
  }

  construirFormulario(): void {
    this.formulario = new FormGroup({
      nombreComercial: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      razonSocial: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      identificacion: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(50),
      ]),
      direccion: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      telefono: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(15),
      ]),
      correo: new FormControl(null, [
        Validators.minLength(1),
        Validators.maxLength(100),
        Validators.pattern(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/),
      ]),
      proveedor: new FormControl(null, [
        Validators.required,
      ]),
    });
  }

  ngOnInit(): void {
    this.escucharCamposDeFormulario();
    this.escucharCambiosEnFormulario();
    if (this.data) {
      this.cargarDatosDeInicioFormulario();
    }
  }
  cargarDatosDeInicioFormulario() {
    if (this.data) {
      const factura = this.data as FacturaCabeceraInterface;
      this.formulario.patchValue(
        {
          nombreComercial: factura.nombreComercialProveedor,
          razonSocial: factura.razonSocialProveedor,
          identificacion: factura.identificacionProveedor,
          direccion: factura.direccionProveedor,
          telefono: factura.telefonoProveedor,
          correo: factura.correoProveedor,
          proveedor: factura.proveedor,
        }
      );
    }
  }
  escucharCamposDeFormulario(): void {
    const campos = { ...this.estructuraErrores };
    Object.keys(campos).forEach((key) => {
      escucharCampo(
        key,
        this.formulario,
        this.estructuraErrores,
        this.MENSAJES_DE_ERROR
      );
    });
  }

  escucharCambiosEnFormulario(): void {
    const formulario$ = this.formulario.valueChanges;
    formulario$.pipe(debounceTime(250)).subscribe(async (valoresForm) => {
      this.formularioValido = !this.formulario.invalid;
      if (this.formularioValido) {
        const values = {
          ...valoresForm,
        };
        this.datosFormulario.emit(values);
      } else {
        this.datosFormulario.emit(false);
      }
    });
  }

  enviarFormulario(): void {
    this.formulario.markAllAsTouched();
    this.escucharCamposDeFormulario();
  }

  buscarProveedores(evento: { query: string }) {
    this._cargandoService.habilitar();
    let consulta = {};
    consulta = {
      where: {
        estado: 1,
        // informacionTributaria: {
        //   razonSocial: `Like(\"%25${evento.query.trim()}%25\")`,
        // }
      },
      relations: ['informacionTributaria']
    };

    const proveedor$ = this._proveedorRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    proveedor$.subscribe({
      next: (respuesta: [ProveedorInterface[], number]) => {
        // this.proveedores = respuesta[0].filter(proveedor => {
        //   const coincidencia = proveedor.informacionTributaria?.nombreComercial?.toLowerCase().search(evento.query.toLowerCase().trim());
        //   return coincidencia !== -1;
        // });
        this.proveedores = respuesta[0].filter(proveedor => {
          const informacionTributaria = proveedor.informacionTributaria as InformacionTributariaInterface;
          proveedor.label = setearLabelInformacionTributaria(informacionTributaria);
          const buscarRazonSocial = (informacionTributaria.razonSocial || '').toUpperCase().search(evento.query.toUpperCase()) !== -1;
          const buscarNombreComercial = (informacionTributaria.nombreComercial || '').toUpperCase().search(evento.query.toUpperCase()) !== -1;
          const buscarIdentificacion = (informacionTributaria.identificacion || '').toUpperCase().search(evento.query.toUpperCase()) !== -1;
          return buscarRazonSocial || buscarNombreComercial || buscarIdentificacion;
        });
        this._cargandoService.deshabilitar();
      },
      error: (err) => {
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        console.error({
          mensage: 'Error en autocomplete',
          error: err,
        });
        this._cargandoService.deshabilitar();
      },
    });
  }

  agregarProveedor(proveedor?: ProveedorInterface) {
    if (proveedor) {
      setearProveedorAEditar(proveedor);
    }
    const dialogRef = this._matDialog.open(ModalCrearEditarProveedorComponent, {
      width: '1550px',
      data: {
        proveedor,
      },
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          const proveedor$ = this._proveedorRestService.crearEditarProveedor(response);

          proveedor$.subscribe({
            next: (proveedorCreado: ProveedorInterface) => {
              this._notificacionService.modalInfo(
                'success',
                'Info',
                'Registro guardado correctamente',
              );
              proveedorCreado.label = setearLabelInformacionTributaria(proveedorCreado?.informacionTributaria as InformacionTributariaInterface);
              this.formulario.patchValue(
                {
                  proveedor: proveedorCreado,
                }
              );
              this.setearFormulario(proveedorCreado);
            },
            error: (err) => {
              console.error({
                mensage: 'Error crear Proveedor',
                error: err,
              });
              this._notificacionService.modalInfo(
                'error',
                'Error',
                'Error al guardar el registro',
              );
            },
          });
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

  editarProveedor() {
    const proveedor = this.formulario.get('proveedor')?.value;
    if (proveedor) {
      this.agregarProveedor(proveedor);
    } else {
      this._notificacionService.modalInfoAsync(
        'question',
        'Advertencia',
        'Busque y Seleccione un proveedor para cambiar la información',
      );
    }
  }

  setearFormulario(proveedor: ProveedorInterface) {
    this.formulario.patchValue(
      {
        nombreComercial: proveedor.informacionTributaria?.nombreComercial,
        razonSocial: proveedor.informacionTributaria?.razonSocial,
        identificacion: proveedor.informacionTributaria?.identificacion,
        direccion: proveedor.informacionTributaria?.direccion,
        telefono: proveedor.informacionTributaria?.telefono,
        correo: proveedor.informacionTributaria?.correo,
      }
    );
  }
}
