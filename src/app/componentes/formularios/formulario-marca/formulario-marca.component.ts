import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {debounceTime} from "rxjs/operators";
import {MarcaInterface} from "../../../modulos/configuracion-datos/interfaces/marca.interface";
import {cargarDatosDeInicioFormulario, escucharCampo} from "../../../utils/validar-campo-reactive-form.util";

@Component({
  selector: 'app-formulario-marca',
  templateUrl: './formulario-marca.component.html',
  styleUrls: ['./formulario-marca.component.scss']
})
export class FormularioMarcaComponent implements OnInit {

  @Input()
  data: MarcaInterface | undefined;

  @Input()
  soloVer?: any;

  @Output()
  datosFormulario: EventEmitter<MarcaInterface | boolean> = new EventEmitter<
    MarcaInterface | boolean
    >();

  formularioValido = false;
  formulario!: FormGroup;

  MENSAJES_DE_ERROR = {
    nombre: {
      required: 'El campo nombre es requerido',
      minLength: 'El campo nombre debe tener al menos un caracter',
      maxlength:
        'El campo nombre debe tener un máximo de 255 caracteres',
    }
  };
  estructuraErrores = {
    nombre: [],
  };

  constructor(
  ) {
    this.construirFormulario();
  }

  construirFormulario(): void {
    this.formulario = new FormGroup({
      nombre: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
    });
  }

  ngOnInit(): void {
    this.escucharCamposDeFormulario();
    this.escucharCambiosEnFormulario();
    if (this.data) {
      cargarDatosDeInicioFormulario<MarcaInterface>(
        this.formulario,
        this.data
      );
    }
  }

  escucharCamposDeFormulario(): void {
    const campos = { ...this.estructuraErrores };
    Object.keys(campos).forEach((key) => {
      escucharCampo(
        key,
        this.formulario,
        this.estructuraErrores,
        this.MENSAJES_DE_ERROR
      );
    });
  }

  escucharCambiosEnFormulario(): void {
    const formulario$ = this.formulario.valueChanges;
    formulario$.pipe(debounceTime(250)).subscribe(async (valoresForm) => {
      this.formularioValido = !this.formulario.invalid;
      if (this.formularioValido) {
        const values = {
          ...valoresForm,
        };
        this.datosFormulario.emit(values);
      } else {
        this.datosFormulario.emit(false);
      }
    });
  }

  enviarFormulario(): void {
    this.formulario.markAllAsTouched();
    this.escucharCamposDeFormulario();
  }
}

