import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {TrabajoInterface} from "../../../modulos/trabajos/interfaces/trabajo.interface";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {EstadoTrabajo, TarfifasIvaEnum, TARIFAS_NO_TOTALES, TipoGasto} from "../../../enums/tipo-contacto.enums";
import {ESTADOS_TRABAJO} from "../../../constantes/tipos-identificacion";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../servicios/cargando.service";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {escucharCampo} from "../../../utils/validar-campo-reactive-form.util";
import {debounceTime} from "rxjs/operators";
import * as moment from "moment";
import {TrabajoRealizadoInterface} from "../../../modulos/trabajos/interfaces/trabajo-realizado.interface";
import {TrabajoRealizadoDetalleInterface} from "../../../modulos/trabajos/interfaces/trabajo-realizado-detalle.interface";
import {TarifaImpuestoRestService} from "../../../modulos/configuracion-datos/servicios/tarifa-impuesto-rest.service";
import {TarifaImpuestoInterface} from "../../../modulos/configuracion-datos/interfaces/tarifa-impuesto.interface";
import {TablaDetalleFacturaComponent} from "../../tablas/tabla-detalle-factura/tabla-detalle-factura.component";

@Component({
  selector: 'app-formulario-trabajo-realizado',
  templateUrl: './formulario-trabajo-realizado.component.html',
  styleUrls: ['./formulario-trabajo-realizado.component.scss']
})
export class FormularioTrabajoRealizadoComponent implements OnInit {
  @ViewChild(TablaDetalleFacturaComponent)
  tablaDetalleFactura!: TablaDetalleFacturaComponent;

  @Input()
  data: TrabajoRealizadoInterface | undefined;

  @Input()
  esProforma = false

  @Input()
  soloVer = false;

  @Input()
  trabajo: TrabajoInterface | undefined;

  trabajoRealizadoDetalles?: TrabajoRealizadoDetalleInterface[];

  @Output()
  datosFormulario: EventEmitter<TrabajoRealizadoInterface | boolean> = new EventEmitter<
    TrabajoRealizadoInterface | boolean
    >();
  tarifas: TarifaImpuestoInterface[] = [];

  formularioValido = false;
  formulario!: FormGroup;
  arreglototalesFactura: any[] = [];

  MENSAJES_DE_ERROR = {
    fechaTrabajoRealizado: {
      required: 'El campo fecha de orden e trabajo es requerido',
    },
    trabajoRealizadoDetalles: {
      required: 'El campo es requerido',
    },
    estadoAccion: {
      required: 'El campo estado es requerido',
    },
    observacion: {
      required: 'El campo es requerido',
    }
  };
  estructuraErrores = {
    fechaTrabajoRealizado: [],
    trabajoRealizadoDetalles: [],
    estadoAccion: [],
    observacion: [],
  };
  tipoGasto = TipoGasto.Repuesto;
  tablaDetalleTrabajoRealizadoValida!: boolean;
  estados = ESTADOS_TRABAJO;
  estadosTrabajo = EstadoTrabajo;
  tarifaSeleccionadaTotales = TarfifasIvaEnum.Iva12;
  descuentoGeneral = 0;

  constructor(
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _notificacionService: NotificacionService,
    private readonly _tarifaImpuestoRestService: TarifaImpuestoRestService,
  ) {
    this.construirFormulario();
  }

  construirFormulario(): void {
    this.formulario = new FormGroup({
      id: new FormControl(null, []),
      fechaTrabajoRealizado: new FormControl(new Date(), [
        Validators.required
      ]),
      trabajoRealizadoDetalles: new FormControl([], [
        Validators.required
      ]),
      estadoAccion: new FormControl('NG', [
        Validators.required,
      ]),
      propina: new FormControl(null, []),
      descuento: new FormControl(null, []),
      descuentoPorcentaje: new FormControl(null, []),
      subtotalIva0: new FormControl(null, []),
      subtotalIva: new FormControl(null, []),
      subtotalIvaExento: new FormControl(null, []),
      subtotalIvaNoObjeto: new FormControl(null, []),
      ivaPorcentaje: new FormControl(null, []),
      ivaValor: new FormControl(null, []),
      iceValor: new FormControl(null, []),
      total: new FormControl(null, []),
      subtotalSinImpuestos: new FormControl(null, []),
      observacion: new FormControl(null, []),
    });
  }

  async ngOnInit() {
    await this.obtenerTarifas();
    this.escucharCamposDeFormulario();
    this.escucharCambiosEnFormulario();
    this.cargarDatosDeInicioFormulario();
  }

  escucharCamposDeFormulario(): void {
    const campos = { ...this.estructuraErrores };
    Object.keys(campos).forEach((key) => {
      escucharCampo(
        key,
        this.formulario,
        this.estructuraErrores,
        this.MENSAJES_DE_ERROR
      );
    });
  }

  escucharCambiosEnFormulario(): void {
    const formulario$ = this.formulario.valueChanges;
    formulario$.pipe(debounceTime(250)).subscribe(async (valoresForm) => {
      this.formularioValido = !this.formulario.invalid;
      if (this.formularioValido) {
        this.datosFormulario.emit(valoresForm);
      } else {
        this.datosFormulario.emit(false);
      }
    });
  }

  enviarFormulario(): void {
    this.formulario.markAllAsTouched();
    this.escucharCamposDeFormulario();
  }

  cargarDatosDeInicioFormulario() {
    if (this.data) {
      this.formulario.patchValue(
        {
          id: this.data?.id,
          fechaTrabajoRealizado: this.data?.fechaTrabajoRealizado,
          trabajoRealizadoDetalles: this.data.trabajoRealizadoDetalles,
          estadoAccion: this.data.estadoAccion,
          observacion: this.data.observacion,
        }
      );
      this.trabajoRealizadoDetalles = this.data.trabajoRealizadoDetalles;
    } else {
      if (this.trabajo?.ordenesTrabajo) {
        const ordenTrabajo = this.trabajo.ordenesTrabajo[0];
        if (ordenTrabajo) {
          this.trabajoRealizadoDetalles = ordenTrabajo.ordenTrabajoDetalles?.map(ordenTrabajoDetalle => {
            return {
              articulo: ordenTrabajoDetalle.articulo,
              descripcion: ordenTrabajoDetalle.descripcion,
              cantidad: ordenTrabajoDetalle.cantidad,
              tipo: TipoGasto.Repuesto,
              precioUnitario: ordenTrabajoDetalle.articulo?.precioVenta ?? 0,
              ivaPorcentaje: 0,
              ivaDescripcion: '0 %',
              descuento: 0,
              valorDescuento: 0,
              valorIva: 0,
              valorIce: 0,
              total: (ordenTrabajoDetalle?.cantidad ?? 0) * (ordenTrabajoDetalle.articulo?.precioVenta ?? 0),
            }
          });
        }
      }
      this.formulario.patchValue(
        {
          fechaTrabajoRealizado: moment().format('YYYY-MM-DD'),
        }
      )
    }
    if (this.trabajoRealizadoDetalles?.length) {
      this.setearArregloTotales();
    }
  }

  obtenerTablaDetalles(respuesta: any) {
    this.tablaDetalleTrabajoRealizadoValida = respuesta.tablaDetalleValida;
    if (this.tablaDetalleTrabajoRealizadoValida) {
      this.formulario.patchValue(
        {
          trabajoRealizadoDetalles: respuesta.detalles,
        }
      );

      if (respuesta?.detalles?.length) {
        this.trabajoRealizadoDetalles = respuesta?.detalles;
        this.setearArregloTotales();
      }
      // this.setearFactura();

    } else {
      this.formulario.patchValue(
        {
          trabajoRealizadoDetalles: null,
        }
      );
    }

  }

  recibirValorTotalAPagar(objetValorAPagar: any) {
    this.descuentoGeneral = objetValorAPagar.porcentajeDescuento || this.descuentoGeneral;
    this.formulario.patchValue(
      {
        // propina: objetValorAPagar.propina,
        total: objetValorAPagar.valorAPagar,
        descuentoPorcentaje: this.descuentoGeneral,
        descuento: objetValorAPagar.descuentos,
      }
    );
  }

  async recibirTarifaIva(tarifaDescripcion: any) {
    this.tarifaSeleccionadaTotales = tarifaDescripcion;
    this.tablaDetalleFactura.tarifaSeleccionadaTotales = this.tarifaSeleccionadaTotales;
    await this.tablaDetalleFactura.obtenerTarifasIva();
    const tarifa = this.tarifas.find(tarifa => tarifa.descripcion === tarifaDescripcion);
    this.tablaDetalleFactura.datos.map(detalle => {
      if (!TARIFAS_NO_TOTALES.includes(detalle.ivaDescripcion as any)) {
        detalle.ivaDescripcion = tarifaDescripcion;
        detalle.ivaPorcentaje = tarifa?.valor;
        this.tablaDetalleFactura.calcularValores(detalle);
      }
    });
    this.tablaDetalleFactura.datos = [...this.tablaDetalleFactura.datos];
    this.tablaDetalleFactura.emitirTablaDetalle();
    this.trabajoRealizadoDetalles = this.tablaDetalleFactura.datos;
    this.setearArregloTotales();
  }

  async obtenerTarifas() {
    const consulta = {
      where: {},
      relations: ['impuesto']
    };
    const tarifaImpuesto$ = this._tarifaImpuestoRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    const respuesta = await (tarifaImpuesto$.toPromise());
    this.tarifas = respuesta[0]
      .filter(tarifaAux => tarifaAux.impuesto?.nombre === 'IVA');
  }

  setearArregloTotales() {
    const arregloTotales: any[] = [];
    const arregloSubtotalesTarifa: any[] = [];
    const arregloTarifasValidas = [...TARIFAS_NO_TOTALES, this.tarifaSeleccionadaTotales];
    this.tarifas.map(tarifa => {
      if (arregloTarifasValidas.includes(tarifa.descripcion as any)) {
        const detallesTarifas = this.trabajoRealizadoDetalles?.filter(detalle => detalle.ivaDescripcion === tarifa.descripcion);
        const sumaDetallesValorTotalTarifa = detallesTarifas?.reduce((acum: any, detalle) => {
          return +(detalle.total as any) + acum;
        }, 0) || 0;
        const sumaDetallesValorTarifa = detallesTarifas?.reduce((acum: any, detalle) => {
          return +(detalle.valorIva as any) + acum;
        }, 0) || 0;
        arregloSubtotalesTarifa.push(
          {
            sumaDetallesValorTotalTarifa,
            sumaDetallesValorTarifa,
            tarifa: tarifa.descripcion
          }
        );
      }
    });

    const sumaDetallesValorTotal = this.trabajoRealizadoDetalles?.reduce((acum: any, detalle) => {
      return +(detalle.total as any) + acum;
    }, 0) || 0;

    arregloTotales.push(
      {
        label: 'Subtotal sin impuestos',
        valor: sumaDetallesValorTotal
      }
    );
    const arregloTarifas = [...TARIFAS_NO_TOTALES, this.tarifaSeleccionadaTotales];
    arregloSubtotalesTarifa.map(datosTarifa => {
      if (arregloTarifas.includes(datosTarifa.tarifa)) {
        arregloTotales.push(
          {
            label: `Subtotal ${datosTarifa.tarifa}`,
            valor: datosTarifa.sumaDetallesValorTotalTarifa,
          }
        )
      }
    });

    const sumaDetallesValorIce = this.trabajoRealizadoDetalles?.reduce((acum: any, detalle) => {
      return +(detalle.valorIce as number) + acum;
    }, 0) || 0;
    // arregloTotales.push(
    //   {
    //     label: 'Valor ICE',
    //     valor: sumaDetallesValorIce
    //   }
    // );
    const tarifaIva = arregloSubtotalesTarifa.find(datosTarifa => datosTarifa.tarifa === this.tarifaSeleccionadaTotales);
    if (tarifaIva) {
      arregloTotales.push(
        {
          label: `Iva ${tarifaIva.tarifa}`,
          valor: tarifaIva.sumaDetallesValorTarifa,
          esTarifa: 1,
        }
      );
    }

    const sumaValoresIva = arregloSubtotalesTarifa.reduce((acum, tarifa) => {
      return tarifa.sumaDetallesValorTarifa + acum;
    }, 0)

    // arregloTotales.push(
    //   {
    //     label: `Propina`,
    //     valor: 0,
    //   }
    // );
    // arregloTotales.push(
    //   {
    //     label: 'Descuento General',
    //     // valor: sumaDetallesDescuentos
    //     valor: this.descuentoGeneral || 0,
    //   }
    // );
    const subtotalSinDescuentoGeneral = sumaDetallesValorTotal + sumaValoresIva + sumaDetallesValorIce;
    const descuento = this.descuentoGeneral / 100 * subtotalSinDescuentoGeneral;
    const valorAPagar = subtotalSinDescuentoGeneral - descuento;
    arregloTotales.push(
      {
        label: `Valor a pagar`,
        valor: valorAPagar,
      }
    );
    this.arreglototalesFactura = arregloTotales;

    this.formulario.patchValue(
      {
        subtotalIva0: arregloTotales.find(total => total.label === TarfifasIvaEnum.Iva0)?.valor || 0,
        subtotalIva: arregloTotales.find(total => total.esTarifa)?.valor || 0,
        subtotalIvaExento: arregloTotales.find(total => total.label === TarfifasIvaEnum.IvaExtento)?.valor || 0,
        subtotalIvaNoObjeto: arregloTotales.find(total => total.label === TarfifasIvaEnum.IvaNoObjeto)?.valor || 0,
        subtotalSinImpuestos: sumaDetallesValorTotal,
        descuento: descuento,
        descuentoPorcentaje: this.descuentoGeneral,
        ivaValor: sumaValoresIva,
        ivaPorcentaje: this.tarifaSeleccionadaTotales,
        iceValor: sumaDetallesValorIce,
        total: valorAPagar,
      }
    );
  }

  mostrarDetalles() {
    if (this.esProforma) {
      return true;
    } else {
      return true;
      // return this.arreglototalesFactura?.length;
    }
  }
}
