import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {cargarDatosDeInicioFormulario, escucharCampo} from "../../../utils/validar-campo-reactive-form.util";
import {debounceTime} from "rxjs/operators";
import {CuentaBancoInterface} from "../../../modulos/configuracion-datos/interfaces/cuenta-banco.interface";
import {TIPOS_CUENTA_BANCO, TIPOS_IDENTIFICACION} from "../../../constantes/tipos-identificacion";
import {ContactoInterface} from "../../../modulos/configuracion-datos/interfaces/contacto.interface";
import {ModalCrearEditarBancoComponent} from "../../modales/modal-crear-editar-banco/modal-crear-editar-banco.component";
import {BancoInterface} from "../../../modulos/configuracion-datos/interfaces/banco.interface";
import {CargandoService} from "../../../servicios/cargando.service";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {MatDialog} from "@angular/material/dialog";
import {BancoRestService} from "../../../modulos/configuracion-datos/servicios/banco-rest.service";

@Component({
  selector: 'app-formulario-cuenta-banco',
  templateUrl: './formulario-cuenta-banco.component.html',
  styleUrls: ['./formulario-cuenta-banco.component.scss']
})
export class FormularioCuentaBancoComponent implements OnInit {


  @Input()
  data: CuentaBancoInterface | undefined;

  @Output()
  datosFormulario: EventEmitter<CuentaBancoInterface | boolean> = new EventEmitter<CuentaBancoInterface | boolean>();

  formularioValido = false;
  formulario!: FormGroup;

  tiposCuentaBanco = TIPOS_CUENTA_BANCO;
  MENSAJES_DE_ERROR = {
    banco: {
      required: 'El campo banco es requerido',
    },
    tipoCuenta: {
      required: 'El campo tipo de cuenta es requerido',
    },
    nombres: {
      required: 'El campo nombres es requerido',
      minLength: 'El campo nombres debe tener al menos un caracter',
      maxlength:
        'El campo nombres debe tener un máximo de 255 caracteres',
    },
    identificacion: {
      required: 'El campo identificación es requerido',
      minLength: 'El campo identificación debe tener al menos un caracter',
      maxlength:
        'El campo identificación debe tener un máximo de 50 caracteres',
    },
    numero: {
      required: 'El campo numero es requerido',
      minLength: 'El campo numero debe tener al menos un caracter',
      maxlength:
        'El campo numero debe tener un máximo de 50 caracteres',
    },
    telefono: {
      required: 'El campo teléfono es requerido',
      minLength: 'El campo teléfono debe tener al menos un caracter',
      maxlength:
        'El campo teléfono debe tener un máximo de 15 caracteres',
    },
    correo: {
      required: 'El campo correo es requerido',
      minLength: 'El campo correo debe tener al menos un caracter',
      maxlength:
        'El campo correo debe tener un máximo de 150 caracteres',
      pattern: 'El campo correo no es válido',
    },
    observacion: {
      required: 'El campo observación es requerido',
      minLength: 'El campo observación debe tener al menos un caracter',
      maxlength:
        'El campo observación debe tener un máximo de 500 caracteres',
    },
  };
  estructuraErrores = {
    nombres: [],
    banco: [],
    identificacion: [],
    telefono: [],
    numero: [],
    observacion: [],
    correo: [],
    tipoCuenta: [],
  };
  bancos: BancoInterface[] = [];
  constructor(
    private readonly _cargandoService: CargandoService,
    private readonly _bancoRestService: BancoRestService,
    private readonly _notificacionService: NotificacionService,
    private readonly _matDialog: MatDialog,
  ) {
    this.construirFormulario();
  }

  construirFormulario(): void {
    this.formulario = new FormGroup({
      id: new FormControl(null, []),
      nombres: new FormControl(null, []),
      numero: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(50),
      ]),
      tipoCuenta: new FormControl(null, [
        Validators.required,
      ]),
      banco: new FormControl(null, [
        Validators.required,
      ]),
      identificacion: new FormControl(null, []),
      telefono: new FormControl(null, []),
      observacion: new FormControl(null, [
        Validators.minLength(1),
        Validators.maxLength(500),
      ]),
      correo: new FormControl(null, []),
    });
  }

  ngOnInit(): void {
    this.escucharCamposDeFormulario();
    this.escucharCambiosEnFormulario();
    if (this.data) {
      cargarDatosDeInicioFormulario<ContactoInterface>(
        this.formulario,
        this.data
      );
    }
  }

  escucharCamposDeFormulario(): void {
    const campos = { ...this.estructuraErrores };
    Object.keys(campos).forEach((key) => {
      escucharCampo(
        key,
        this.formulario,
        this.estructuraErrores,
        this.MENSAJES_DE_ERROR
      );
    });
  }

  escucharCambiosEnFormulario(): void {
    const formulario$ = this.formulario.valueChanges;
    formulario$.pipe(debounceTime(250)).subscribe(async (valoresForm) => {
      this.formularioValido = !this.formulario.invalid;
      if (this.formularioValido) {
        const values = {
          ...valoresForm,
        };
        this.datosFormulario.emit(values);
      } else {
        this.datosFormulario.emit(false);
      }
    });
  }

  enviarFormulario(): void {
    this.formulario.markAllAsTouched();
    this.escucharCamposDeFormulario();
  }

  buscarBancos(evento: { query: string }) {
    this._cargandoService.habilitar();
    let consulta = {
      where: {
        nombre: `Like(\"%25${evento.query.trim()}%25\")`,
        estado: 1,
      },
    };
    const banco$ = this._bancoRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    banco$.subscribe({
      next: (respuesta: [any[], number]) => {
        this.bancos = respuesta[0];
        this._cargandoService.deshabilitar();
      },
      error: (err) => {
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        console.error({
          mensage: 'Error en autocomplete',
          error: err,
        });
        this._cargandoService.deshabilitar();
      },
    });
  }

  agregarBanco() {
    const dialogRef = this._matDialog.open(ModalCrearEditarBancoComponent, {
      width: '450px',
      data: {},
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          const banco$ = this._bancoRestService.create(response);

          banco$.subscribe({
            next: (bancoCreada: BancoInterface) => {
              this._notificacionService.modalInfo(
                'success',
                'Info',
                'Registro guardado correctamente',
              );
              this.buscarBancos(
                {
                  query: ''
                }
              );
              this.formulario.patchValue(
                {
                  banco: bancoCreada,
                }
              );
            },
            error: (err) => {
              console.error({
                mensage: 'Error crear Banco',
                error: err,
              });
              this._notificacionService.modalInfo(
                'error',
                'Error',
                'Error al guardar el registro',
              );
            },
          });
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }
}
