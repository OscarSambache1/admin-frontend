import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {RecepcionInterface} from "../../../modulos/trabajos/interfaces/recepcion.interface";
import {escucharCampo} from "../../../utils/validar-campo-reactive-form.util";
import {debounceTime} from "rxjs/operators";
import {CargandoService} from "../../../servicios/cargando.service";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import * as moment from "moment";
import {EstadoGasto, EstadoTrabajo} from "../../../enums/tipo-contacto.enums";
import {ESTADOS_GASTO, ESTADOS_TRABAJO} from "../../../constantes/tipos-identificacion";

@Component({
  selector: 'app-formulario-recepcion',
  templateUrl: './formulario-recepcion.component.html',
  styleUrls: ['./formulario-recepcion.component.scss']
})
export class FormularioRecepcionComponent implements OnInit {

  @Input()
  data: RecepcionInterface | undefined;

  @Input()
  soloVer = false;

  @Output()
  datosFormulario: EventEmitter<RecepcionInterface | boolean> = new EventEmitter<
    RecepcionInterface | boolean
    >();

  formularioValido = false;
  formulario!: FormGroup;

  estados = ESTADOS_TRABAJO;

  MENSAJES_DE_ERROR = {
    fechaRecepcion: {
      required: 'El campo fecha de recepción es requerido',
    },
    observacionesTaller: {
      required: 'El campo observaciones de taller es requerido',
    },
    observacionesCliente: {
      required: 'El campo observaciones de cliente es requerido',
    },
    kmEntrada: {
      required: 'El campo km de entrada es requerido',
    },
    gasolinaEntrada: {
      required: 'El campo gasolina de entrada es requerido',
    },
    piezasRecambio: {
      required: 'El campo piezas recambio es requerido',
    },
    enGarantia: {
      required: 'El campo en garantía es requerido',
    },
    siniestro: {
      required: 'El campo siniestro es requerido',
    },
    grua: {
      required: 'El campo grua es requerido',
    },
    estadoAccion: {
      required: 'El campo estado es requerido',
    },
  };
  estructuraErrores = {
    observacionesTaller: [],
    observacionesCliente: [],
    fechaRecepcion: [],
    kmEntrada: [],
    gasolinaEntrada: [],
    piezasRecambio: [],
    enGarantia: [],
    siniestro: [],
    grua: [],
    estadoAccion: [],
  };
  estadosTrabajo = EstadoTrabajo;

  constructor(
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _notificacionService: NotificacionService,
  ) {
    this.construirFormulario();
  }

  construirFormulario(): void {
    this.formulario = new FormGroup({
      id: new FormControl(null, []),
      observacionesTaller: new FormControl(null, []),
      observacionesCliente: new FormControl(null, []),
      fechaRecepcion: new FormControl(new Date(), [
        Validators.required
      ]),
      kmEntrada: new FormControl(null, [
        Validators.required
      ]),
      gasolinaEntrada: new FormControl(null, [
        // Validators.required
      ]),
      piezasRecambio: new FormControl(null, []),
      enGarantia: new FormControl(null, []),
      siniestro: new FormControl(null, []),
      grua: new FormControl(null, []),
      estadoAccion: new FormControl('NG', [
        Validators.required,
      ]),
    });
  }

  ngOnInit(): void {
    this.escucharCamposDeFormulario();
    this.escucharCambiosEnFormulario();
    this.cargarDatosDeInicioFormulario();
  }

  escucharCamposDeFormulario(): void {
    const campos = { ...this.estructuraErrores };
    Object.keys(campos).forEach((key) => {
      escucharCampo(
        key,
        this.formulario,
        this.estructuraErrores,
        this.MENSAJES_DE_ERROR
      );
    });
  }

  escucharCambiosEnFormulario(): void {
    const formulario$ = this.formulario.valueChanges;
    formulario$.pipe(debounceTime(250)).subscribe(async (valoresForm) => {
      this.formularioValido = !this.formulario.invalid;
      if (this.formularioValido) {
        this.datosFormulario.emit(valoresForm);
      } else {
        this.datosFormulario.emit(false);
      }
    });
  }

  enviarFormulario(): void {
    this.formulario.markAllAsTouched();
    this.escucharCamposDeFormulario();
  }

  cargarDatosDeInicioFormulario() {
    if (this.data) {
      this.formulario.patchValue(
        {
          id: this.data?.id,
          observacionesTaller: this.data?.observacionesTaller,
          observacionesCliente: this.data?.observacionesCliente,
          fechaRecepcion: this.data?.fechaRecepcion,
          kmEntrada: this.data?.kmEntrada,
          gasolinaEntrada: this.data?.gasolinaEntrada,
          piezasRecambio: !!this.data?.piezasRecambio,
          enGarantia: !!this.data?.enGarantia,
          siniestro: !!this.data?.siniestro,
          grua: !!this.data?.grua,
          estadoAccion: this.data.estadoAccion
        }
      );
    } else {
      this.formulario.patchValue(
        {
          fechaRecepcion: moment().format('YYYY-MM-DD'),
        }
      )
    }
  }
}
