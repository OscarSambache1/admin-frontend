import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ContactoInterface} from "../../../modulos/configuracion-datos/interfaces/contacto.interface";
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {cargarDatosDeInicioFormulario, escucharCampo} from "../../../utils/validar-campo-reactive-form.util";
import {AccesorioInterface} from "../../../modulos/configuracion-datos/interfaces/accesorio.interface";
import {debounceTime} from "rxjs/operators";
import {TIPOS_CONTACTO} from "../../../constantes/tipos-identificacion";

@Component({
  selector: 'app-formulario-agregar-contactos',
  templateUrl: './formulario-agregar-contactos.component.html',
  styleUrls: ['./formulario-agregar-contactos.component.scss']
})
export class FormularioAgregarContactosComponent implements OnInit {

  @Input()
  idProveedor?: number;

  @Output() datosFormulario: EventEmitter<
    any
    > = new EventEmitter<any>();

  formularioValido = false;
  formularioContactos!: FormGroup;
  MENSAJES_DE_ERROR = {
    contactos: {
      required: 'El campo observación es requerido',
    },
  };
  objetoArreglosErrores = {
    contactos: [],
  };

  tiposContacto = TIPOS_CONTACTO;
  constructor() {
    this.construirFormulario();
  }

  construirFormulario() {
    this.formularioContactos = new FormGroup({
      contactos: new FormArray([], [Validators.required]),
    });
  }

  ngOnInit(): void {
    // this.escucharCamposDeFormulario();
    this.escucharCambiosEnFormulario();
    this.datosFormulario.emit(this.formularioContactos.value);
  }

  escucharCamposDeFormulario(): void {
    const campos = { ...this.objetoArreglosErrores };
    Object.keys(campos).forEach((key) => {
      escucharCampo(
        key,
        this.formularioContactos,
        this.objetoArreglosErrores,
        this.MENSAJES_DE_ERROR as any
      );
    });
  }

  escucharCambiosEnFormulario(): void {
    const formulario$ = this.formularioContactos.valueChanges;
    formulario$.pipe(debounceTime(250)).subscribe(async (valoresForm) => {
      this.formularioValido = !this.formularioContactos.invalid;
      if (this.formularioValido) {
        const values = {
          ...valoresForm,
        };
        this.datosFormulario.emit(values);
      } else {
        this.datosFormulario.emit(false);
      }
    });
  }

  enviarFormulario(): void {
    this.formularioContactos.markAllAsTouched();
    this.escucharCamposDeFormulario();
  }


  get campoContactos(): FormArray {
    return this.formularioContactos.get('contactos') as FormArray;
  }

  agregarNuevoCampoContacto() {
    const contactoFormGroup = new FormGroup({
      tipoContacto: new FormControl('', [
        Validators.required,
      ]),
      valor: new FormControl('', [
        Validators.required,
      ]),
    });
    this.campoContactos.push(contactoFormGroup);
    this.datosFormulario.emit(this.formularioContactos.value);
  }

  quitarCampoContacto(index: number): void {
    this.campoContactos.removeAt(index);
    this.datosFormulario.emit(this.formularioContactos.value);
  }

}
