import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {cargarDatosDeInicioFormulario, escucharCampo} from "../../../utils/validar-campo-reactive-form.util";
import {debounceTime} from "rxjs/operators";
import {TarifaImpuestoInterface} from "../../../modulos/configuracion-datos/interfaces/tarifa-impuesto.interface";

@Component({
  selector: 'app-formulario-tarifa-impuesto',
  templateUrl: './formulario-tarifa-impuesto.component.html',
  styleUrls: ['./formulario-tarifa-impuesto.component.scss']
})
export class FormularioTarifaImpuestoComponent implements OnInit {

  @Input()
  data: TarifaImpuestoInterface | undefined;

  @Output()
  datosFormulario: EventEmitter<TarifaImpuestoInterface | boolean> = new EventEmitter<
    TarifaImpuestoInterface | boolean
    >();

  formularioValido = false;
  formulario!: FormGroup;

  MENSAJES_DE_ERROR = {
    valor: {
      required: 'El campo valor es requerido',
    },
    descripcion: {
      required: 'El campo descripción es requerido',
    },
    esPrincipal: {
      required: 'El campo es requerido',
    }
  };
  estructuraErrores = {
    valor: [],
    esPrincipal: [],
    descripcion: [],
  };

  constructor(
  ) {
    this.construirFormulario();
  }

  construirFormulario(): void {
    this.formulario = new FormGroup({
      valor: new FormControl(null, [
        Validators.required,
      ]),
      descripcion: new FormControl(null, [
        Validators.required,
      ]),
      esPrincipal: new FormControl(0, []),
      id: new FormControl(),
    });
  }

  ngOnInit(): void {
    this.escucharCamposDeFormulario();
    this.escucharCambiosEnFormulario();
    if (this.data) {
      cargarDatosDeInicioFormulario<TarifaImpuestoInterface>(
        this.formulario,
        this.data
      );
    }
  }

  escucharCamposDeFormulario(): void {
    const campos = { ...this.estructuraErrores };
    Object.keys(campos).forEach((key) => {
      escucharCampo(
        key,
        this.formulario,
        this.estructuraErrores,
        this.MENSAJES_DE_ERROR
      );
    });
  }

  escucharCambiosEnFormulario(): void {
    const formulario$ = this.formulario.valueChanges;
    formulario$.pipe(debounceTime(250)).subscribe(async (valoresForm) => {
      this.formularioValido = !this.formulario.invalid;
      if (this.formularioValido) {
        const values = {
          ...valoresForm,
        };
        this.datosFormulario.emit(values);
      } else {
        this.datosFormulario.emit(false);
      }
    });
  }

  enviarFormulario(): void {
    this.formulario.markAllAsTouched();
    this.escucharCamposDeFormulario();
  }

}
