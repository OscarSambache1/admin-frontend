import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {cargarDatosDeInicioFormulario, escucharCampo} from "../../../utils/validar-campo-reactive-form.util";
import {debounceTime} from "rxjs/operators";
import {GastoInterface} from "../../../modulos/gastos/interfaces/gasto.interface";
import {ESTADOS_GASTO, TIPOS_GASTO} from "../../../constantes/tipos-identificacion";
import {EstadoGasto} from "../../../enums/tipo-contacto.enums";

@Component({
  selector: 'app-formulario-gasto',
  templateUrl: './formulario-gasto.component.html',
  styleUrls: ['./formulario-gasto.component.scss']
})
export class FormularioGastoComponent implements OnInit {
  @Input()
  data: GastoInterface | undefined;

  @Output()
  datosFormulario: EventEmitter<GastoInterface | boolean> = new EventEmitter<
    GastoInterface | boolean
    >();

  @Output()
  cambioGasto: EventEmitter<string> = new EventEmitter<
    string
    >();


  formularioValido = false;
  formulario!: FormGroup;

  MENSAJES_DE_ERROR = {
    tipoGasto: {
      required: 'El campo tipo de gasto es requerido',
    },
    estadoGasto: {
      required: 'El campo tipo de gasto es requerido',
    },
    valorAPagar: {
      required: 'El campo valor a pagar es requerido',
    },
    valorPagado: {
      required: 'El campo valor pagado es requerido',
    }
  };
  estructuraErrores = {
    tipoGasto: [],
    estadoGasto: [],
    valorAPagar: [],
    valorPagado: [],
  };
  tiposGasto = TIPOS_GASTO;
  estadosGasto = ESTADOS_GASTO;
  constructor(
  ) {
    this.construirFormulario();
  }

  construirFormulario(): void {
    this.formulario = new FormGroup({
      tipoGasto: new FormControl(null, [
        Validators.required,
      ]),
      estadoGasto: new FormControl(EstadoGasto.PorPagar, [
        Validators.required,
      ]),
      valorAPagar: new FormControl(0, [
        Validators.required,
      ]),
      valorPagado: new FormControl(0, [
        Validators.required,
      ]),
      id: new FormControl(null, []),
    });
  }

  ngOnInit(): void {
    this.escucharCamposDeFormulario();
    this.escucharCambiosEnFormulario();
    if (this.data) {
      this.cargarDatosDeInicioFormulario();
    }
  }
  cargarDatosDeInicioFormulario() {
    if (this.data) {
      this.formulario.patchValue(
        {
          id: this.data?.id,
          tipoGasto: this.data?.tipoGasto,
          estadoGasto: this.data?.estadoGasto,
          valorAPagar: +(this.data?.valorAPagar as number),
          valorPagado: +(this.data?.valorPagado as number)
        }
      );
    }
  }

  escucharCamposDeFormulario(): void {
    const campos = { ...this.estructuraErrores };
    Object.keys(campos).forEach((key) => {
      escucharCampo(
        key,
        this.formulario,
        this.estructuraErrores,
        this.MENSAJES_DE_ERROR
      );
    });
  }

  escucharCambiosEnFormulario(): void {
    const formulario$ = this.formulario.valueChanges;
    formulario$.pipe(debounceTime(250)).subscribe(async (valoresForm) => {
      this.formularioValido = !this.formulario.invalid;
      if (this.formularioValido) {
        const values = {
          ...valoresForm,
        };
        this.datosFormulario.emit(values);
      } else {
        this.datosFormulario.emit(false);
      }
    });
  }

  enviarFormulario(): void {
    this.formulario.markAllAsTouched();
    this.escucharCamposDeFormulario();
  }

  cambioTipoGasto(evento: any) {
    const tipoGasto = this.formulario?.get('tipoGasto')?.value;
    this.cambioGasto.emit(tipoGasto);
  }
}
