import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ClienteInterface} from "../../../modulos/configuracion-datos/interfaces/cliente.interface";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../servicios/cargando.service";
import {ClienteRestService} from "../../../modulos/configuracion-datos/servicios/cliente-rest.service";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {escucharCampo} from "../../../utils/validar-campo-reactive-form.util";
import {debounceTime} from "rxjs/operators";
import {ModalCrearEditarClienteComponent} from "../../modales/modal-crear-editar-cliente/modal-crear-editar-cliente.component";
import {EstadoTrabajo} from "../../../enums/tipo-contacto.enums";

@Component({
  selector: 'app-formulario-proforma',
  templateUrl: './formulario-proforma.component.html',
  styleUrls: ['./formulario-proforma.component.scss']
})
export class FormularioProformaComponent implements OnInit {
  @Input()
  data: any | undefined;

  @Output()
  datosFormulario: EventEmitter<any | boolean> = new EventEmitter<any | boolean>();


  formularioValido = false;
  formulario!: FormGroup;
  clientes: ClienteInterface[] = [];

  MENSAJES_DE_ERROR = {
    cliente: {
      required: 'El campo cliente es requerido',
    },
    esConsumidorFinal: {
      required: 'El campo cliente es requerido',
    },
  };
  estructuraErrores = {
    cliente: [],
    esConsumidorFinal: [],
  };
  estadosTrabajo = EstadoTrabajo;

  constructor(
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _clienteRestService: ClienteRestService,
    private readonly _notificacionService: NotificacionService,
  ) {
    this.construirFormulario();
  }


  construirFormulario(): void {
    this.formulario = new FormGroup({
      id: new FormControl(null, []),
      cliente: new FormControl(null, []),
      esConsumidorFinal: new FormControl(null, []),
    });
  }

  async ngOnInit() {
    this.escucharCamposDeFormulario();
    this.escucharCambiosEnFormulario();
    this.cargarDatosDeInicioFormulario();
  }

  cargarDatosDeInicioFormulario() {
    if (this.data) {
      this.formulario.patchValue(
        {
          id: this.data?.id,
          esConsumidorFinal: !!this.data.esConsumidorFinal,
          cliente: this.data.cliente,
        }
      );
    }
  }

  escucharCamposDeFormulario(): void {
    const campos = {...this.estructuraErrores};
    Object.keys(campos).forEach((key) => {
      escucharCampo(
        key,
        this.formulario,
        this.estructuraErrores,
        this.MENSAJES_DE_ERROR
      );
    });
  }

  escucharCambiosEnFormulario(): void {
    const formulario$ = this.formulario.valueChanges;
    formulario$.pipe(debounceTime(250)).subscribe(async (valoresForm) => {
      this.formularioValido = !this.formulario.invalid;
      if (this.formularioValido) {
        const values = {
          ...valoresForm,
        };
        this.datosFormulario.emit(values);
      } else {
        this.datosFormulario.emit(false);
      }
    });
  }

  enviarFormulario(): void {
    this.formulario.markAllAsTouched();
    this.escucharCamposDeFormulario();
  }

  buscarClientes(evento: { query: string }) {
    this._cargandoService.habilitar();
    let consulta = {};
    consulta = {
      where: {
        estado: 1,
      },
      relations: ['informacionTributaria']
    };

    const cliente$ = this._clienteRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    cliente$.subscribe({
      next: (respuesta: [ClienteInterface[], number]) => {
        this.clientes = respuesta[0].filter(cliente => {
          const coincidencia = cliente.informacionTributaria?.nombreComercial?.toLowerCase().search(evento.query.toLowerCase().trim());
          return coincidencia !== -1;
        });
        this._cargandoService.deshabilitar();
      },
      error: (err) => {
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        console.error({
          mensage: 'Error en autocomplete',
          error: err,
        });
        this._cargandoService.deshabilitar();
      },
    });
  }

  agregarProveedor() {
    const dialogRef = this._matDialog.open(ModalCrearEditarClienteComponent, {
      width: '1550px',
      data: {},
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          const cliente$ = this._clienteRestService.crearEditarCliente(response);

          cliente$.subscribe({
            next: (clienteCreado: ClienteInterface) => {
              this._notificacionService.modalInfo(
                'success',
                'Info',
                'Registro guardado correctamente',
              );
              this.formulario.patchValue(
                {
                  cliente: clienteCreado,
                }
              );
              this.setearFormulario(clienteCreado);
            },
            error: (err) => {
              console.error({
                mensage: 'Error crear Cliente',
                error: err,
              });
              this._notificacionService.modalInfo(
                'error',
                'Error',
                'Error al guardar el registro',
              );
            },
          });
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

  setearFormulario(cliente: ClienteInterface) {
    // this.formulario.patchValue(
    //   {
    //     nombreComercialCliente: cliente.informacionTributaria?.nombreComercial,
    //     razonSocialCliente: cliente.informacionTributaria?.razonSocial,
    //     identificacionCliente: cliente.informacionTributaria?.identificacion,
    //     direccionCliente: cliente.informacionTributaria?.direccion,
    //     telefonoCliente: cliente.informacionTributaria?.telefono,
    //     correoCliente: cliente.informacionTributaria?.correo,
    //   }
    // );
  }

  cambioConsumidorFinal(evento: any) {
    const esConsumidorFinal = this.formulario.get('esConsumidorFinal')?.value;
    if (esConsumidorFinal) {
      this.formulario.get('cliente')?.setValidators([]);
      this.formulario.get('cliente')?.disable();
      this.formulario.patchValue(
        {
          cliente: null,
        }
      );
    } else {
      this.formulario.get('cliente')?.setValidators([Validators.required]);
      this.formulario.get('cliente')?.enable();
    }
  }
}
