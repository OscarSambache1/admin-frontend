import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {VehiculoInterface} from "../../../modulos/configuracion-datos/interfaces/vehiculo.interface";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {TIPOS_IDENTIFICACION} from "../../../constantes/tipos-identificacion";
import {cargarDatosDeInicioFormulario, escucharCampo} from "../../../utils/validar-campo-reactive-form.util";
import {AccesorioInterface} from "../../../modulos/configuracion-datos/interfaces/accesorio.interface";
import {debounceTime} from "rxjs/operators";
import {ClienteInterface} from "../../../modulos/configuracion-datos/interfaces/cliente.interface";
import {CargandoService} from "../../../servicios/cargando.service";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {MatDialog} from "@angular/material/dialog";
import {ClienteRestService} from "../../../modulos/configuracion-datos/servicios/cliente-rest.service";
import {ModalCrearEditarClienteComponent} from "../../modales/modal-crear-editar-cliente/modal-crear-editar-cliente.component";
import {MarcaInterface} from "../../../modulos/configuracion-datos/interfaces/marca.interface";
import {TipoVehiculoInterface} from "../../../modulos/configuracion-datos/interfaces/tipo-vehiculo.interface";
import {TipoCambioInterface} from "../../../modulos/configuracion-datos/interfaces/tipo-cambio.interface";
import {TipoCombustibleInterface} from "../../../modulos/configuracion-datos/interfaces/tipo-combustible.interface";
import {MarcaRestService} from "../../../modulos/configuracion-datos/servicios/marca-rest.service";
import {TipoVehiculoRestService} from "../../../modulos/configuracion-datos/servicios/tipo-vehiculo-rest.service";
import {TipoCambioRestService} from "../../../modulos/configuracion-datos/servicios/tipo-cambio-rest.service";
import {TipoCombustibleRestService} from "../../../modulos/configuracion-datos/servicios/tipo-combustible-rest.service";
import {ModalCrearEditarMarcaComponent} from "../../modales/modal-crear-editar-marca/modal-crear-editar-marca.component";
import {ModalCrearEditarTipoVehiculoComponent} from "../../modales/modal-crear-editar-tipo-vehiculo/modal-crear-editar-tipo-vehiculo.component";
import {ModalCrearEditarTipoCambioComponent} from "../../modales/modal-crear-editar-tipo-cambio/modal-crear-editar-tipo-cambio.component";
import {ModalCrearEditarTipoCombustibleComponent} from "../../modales/modal-crear-editar-tipo-combustible/modal-crear-editar-tipo-combustible.component";
import {setearLabelInformacionTributaria} from "../formulario-trabajo/formulario-trabajo.component";

@Component({
  selector: 'app-formulario-vehiculo',
  templateUrl: './formulario-vehiculo.component.html',
  styleUrls: ['./formulario-vehiculo.component.scss']
})
export class FormularioVehiculoComponent implements OnInit {

  @Input()
  data: VehiculoInterface | undefined;

  @Input()
  esModalCliente!: boolean;

  @Input()
  soloVer!: boolean;

  @Input()
  cliente!: ClienteInterface;

  @Output()
  datosFormulario: EventEmitter<VehiculoInterface | boolean> = new EventEmitter<VehiculoInterface | boolean>();

  formularioValido = false;
  formulario!: FormGroup;

  tiposIdentificacion = TIPOS_IDENTIFICACION;
  MENSAJES_DE_ERROR = {
    modelo: {
      required: 'El campo nombre comercial es requerido',
      minLength: 'El campo nombre comercial debe tener al menos un caracter',
      maxlength:
        'El campo nombre comercial debe tener un máximo de 150 caracteres',
    },
    color: {
      required: 'El campo color es requerido',
      minLength: 'El campo color debe tener al menos un caracter',
      maxlength:
        'El campo color debe tener un máximo de 150 caracteres',
    },
    chasis: {
      required: 'El campo chasis es requerido',
      minLength: 'El campo chasis debe tener al menos un caracter',
      maxlength:
        'El campo chasis debe tener un máximo de 150 caracteres',
    },
    motor: {
      required: 'El campo motor es requerido',
      minLength: 'El campo motor debe tener al menos un caracter',
      maxlength:
        'El campo motor debe tener un máximo de 150 caracteres',
    },
    placa: {
      required: 'El campo placa es requerido',
      minLength: 'El campo placa debe tener al menos un caracter',
      maxlength:
        'El campo placa debe tener un máximo de 10 caracteres',
    },
    anio: {
      required: 'El campo anio es requerido',
    },
    cliente: {
      required: 'El campo propietario es requerido',
    },
    marca: {
      required: 'El campo marca es requerido',
    },
    tipoVehiculo: {
      required: 'El campo tipo de vehiculo es requerido',
    },
    tipoCambio: {
      required: 'El campo tipo de cambio es requerido',
    },
    tipoCombustible: {
      required: 'El campo tipo de combustible es requerido',
    },
  };
  estructuraErrores = {
    modelo: [],
    color: [],
    chasis: [],
    motor: [],
    placa: [],
    anio: [],
    cliente: [],
    marca: [],
    tipoVehiculo: [],
    tipoCambio: [],
    tipoCombustible: [],
  };
  clientes: ClienteInterface[] = [];
  marcas: MarcaInterface[] = [];
  tipoVehiculos: TipoVehiculoInterface[] = [];
  tipoCambios: TipoCambioInterface[] = [];
  tipoCombustibles: TipoCombustibleInterface[] = [];
  constructor(
    private readonly _cargandoService: CargandoService,
    private readonly _clienteRestService: ClienteRestService,
    private readonly _marcaRestService: MarcaRestService,
    private readonly _tipoVehiculoRestService: TipoVehiculoRestService,
    private readonly _tipoCambioRestService: TipoCambioRestService,
    private readonly _tipoCombustibleRestService: TipoCombustibleRestService,
    private readonly _notificacionService: NotificacionService,
    private readonly _matDialog: MatDialog,
  ) {
    this.construirFormulario();
  }

  construirFormulario(): void {
    this.formulario = new FormGroup({
      modelo: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(150),
      ]),
      color: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(150),
      ]),
      chasis: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(150),
      ]),
      motor: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(150),
      ]),
      placa: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(10),
      ]),
      anio: new FormControl(null, [
        Validators.required,
      ]),
      estado: new FormControl(1, []),
      id: new FormControl(null, []),
      cliente: new FormControl(null, [
        Validators.required,
      ]),
      marca: new FormControl(null, [
        Validators.required,
      ]),
      tipoVehiculo: new FormControl(null, [
        // Validators.required,
      ]),
      tipoCambio: new FormControl(null, [
        // Validators.required,
      ]),
      tipoCombustible: new FormControl(null, [
        // Validators.required,
      ]),
    });
  }

  ngOnInit(): void {
    this.escucharCamposDeFormulario();
    this.escucharCambiosEnFormulario();
    if (this.data) {
      cargarDatosDeInicioFormulario<AccesorioInterface>(
        this.formulario,
        this.data
      );
    }
    if (this.esModalCliente && this.cliente) {
      // this.formulario.get('cliente')?.disable();
      this.formulario.patchValue(
        {
          cliente: this.cliente,
        }
      )
    }
  }

  escucharCamposDeFormulario(): void {
    const campos = { ...this.estructuraErrores };
    Object.keys(campos).forEach((key) => {
      escucharCampo(
        key,
        this.formulario,
        this.estructuraErrores,
        this.MENSAJES_DE_ERROR
      );
    });
  }

  escucharCambiosEnFormulario(): void {
    const formulario$ = this.formulario.valueChanges;
    formulario$.pipe(debounceTime(250)).subscribe(async (valoresForm) => {
      this.formularioValido = !this.formulario.invalid;
      if (this.formularioValido) {
        const values = {
          ...valoresForm,
        };
        this.datosFormulario.emit(values);
      } else {
        this.datosFormulario.emit(false);
      }
    });
  }

  enviarFormulario(): void {
    this.formulario.markAllAsTouched();
    this.escucharCamposDeFormulario();
  }

  agregarCliente() {
    const dialogRef = this._matDialog.open(ModalCrearEditarClienteComponent, {
      width: '1750px',
      data: {},
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          const cliente$ = this._clienteRestService.crearEditarCliente(response);

          cliente$.subscribe({
            next: (clienteCreada: ClienteInterface) => {
              this._notificacionService.modalInfo(
                'success',
                'Info',
                'Registro guardado correctamente',
              );
              this.buscarClientes(
                {
                  query: ''
                }
              );
              this.formulario.patchValue(
                {
                  cliente: clienteCreada,
                }
              );
            },
            error: (err) => {
              console.error({
                mensage: 'Error crear Cliente',
                error: err,
              });
              this._notificacionService.modalInfo(
                'error',
                'Error',
                'Error al guardar el registro',
              );
            },
          });
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

  buscarClientes(evento: any) {
    this._cargandoService.habilitar();
    let consulta = {
      where: {
        estado: 1
      },
      relations: ['informacionTributaria']
      };

    const cliente$ = this._clienteRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    cliente$.subscribe({
      next: (respuesta: [any[], number]) => {
        this.clientes = respuesta[0].filter(cliente => {
          const informacionTributaria = cliente.informacionTributaria;
          cliente.label = setearLabelInformacionTributaria(informacionTributaria);
          const buscarRazonSocial = (informacionTributaria.razonSocial || '').toUpperCase().search(evento.query.toUpperCase()) !== -1;
          const buscarNombreComercial = (informacionTributaria.nombreComercial || '').toUpperCase().search(evento.query.toUpperCase()) !== -1;
          const buscarIdentificacion = (informacionTributaria.identificacion || '').toUpperCase().search(evento.query.toUpperCase()) !== -1;
          return buscarRazonSocial || buscarNombreComercial || buscarIdentificacion;
        });
        this._cargandoService.deshabilitar();
      },
      error: (err) => {
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        console.error({
          mensage: 'Error en autocomplete',
          error: err,
        });
        this._cargandoService.deshabilitar();
      },
    });
  }

  buscarMarcas(evento: any) {
    this._cargandoService.habilitar();
    let consulta = {
      where: {
        nombre: `Like(\"%25${evento.query.trim()}%25\")`,
        estado: 1,
      },
    };

    const marca$ = this._marcaRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    marca$.subscribe({
      next: (respuesta: [any[], number]) => {
        this.marcas = respuesta[0];
        this._cargandoService.deshabilitar();
      },
      error: (err) => {
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        console.error({
          mensage: 'Error en autocomplete',
          error: err,
        });
        this._cargandoService.deshabilitar();
      },
    });
  }

  buscarTipoVehiculos(evento: any) {
    this._cargandoService.habilitar();
    let consulta = {
      where: {
        nombre: `Like(\"%25${evento.query.trim()}%25\")`,
        estado: 1,
      },
    };
    const tipoVehiculo$ = this._tipoVehiculoRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    tipoVehiculo$.subscribe({
      next: (respuesta: [any[], number]) => {
        this.tipoVehiculos = respuesta[0];
        this._cargandoService.deshabilitar();
      },
      error: (err) => {
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        console.error({
          mensage: 'Error en autocomplete',
          error: err,
        });
        this._cargandoService.deshabilitar();
      },
    });
  }

  buscarTipoCambios(evento: any) {
    this._cargandoService.habilitar();
    let consulta = {
      where: {
        nombre: `Like(\"%25${evento.query.trim()}%25\")`,
        estado: 1,
      },
    };
    const tipoCambio$ = this._tipoCambioRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    tipoCambio$.subscribe({
      next: (respuesta: [any[], number]) => {
        this.tipoCambios = respuesta[0];
        this._cargandoService.deshabilitar();
      },
      error: (err) => {
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        console.error({
          mensage: 'Error en autocomplete',
          error: err,
        });
        this._cargandoService.deshabilitar();
      },
    });
  }

  buscarTipoCombustibles(evento: any) {
    this._cargandoService.habilitar();
    let consulta = {
      where: {
        nombre: `Like(\"%25${evento.query.trim()}%25\")`,
        estado: 1,
      },
    };
    const tipoCombustible$ = this._tipoCombustibleRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    tipoCombustible$.subscribe({
      next: (respuesta: [any[], number]) => {
        this.tipoCombustibles = respuesta[0];
        this._cargandoService.deshabilitar();
      },
      error: (err) => {
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        console.error({
          mensage: 'Error en autocomplete',
          error: err,
        });
        this._cargandoService.deshabilitar();
      },
    });
  }

  agregarMarca() {
    const dialogRef = this._matDialog.open(ModalCrearEditarMarcaComponent, {
      width: '550px',
      data: {},
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          const cliente$ = this._marcaRestService.create(response);

          cliente$.subscribe({
            next: (marcaCreada: MarcaInterface) => {
              this._notificacionService.modalInfo(
                'success',
                'Info',
                'Registro guardado correctamente',
              );
              this.buscarClientes(
                {
                  query: ''
                }
              );
              this.formulario.patchValue(
                {
                  marca: marcaCreada,
                }
              );
            },
            error: (err) => {
              console.error({
                mensage: 'Error crear Cliente',
                error: err,
              });
              this._notificacionService.modalInfo(
                'error',
                'Error',
                'Error al guardar el registro',
              );
            },
          });
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

  agregarTipoVehiculo() {
    const dialogRef = this._matDialog.open(ModalCrearEditarTipoVehiculoComponent, {
      width: '550px',
      data: {},
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          const cliente$ = this._tipoVehiculoRestService.create(response);

          cliente$.subscribe({
            next: (tipoVehiculoCreada: TipoVehiculoInterface) => {
              this._notificacionService.modalInfo(
                'success',
                'Info',
                'Registro guardado correctamente',
              );
              this.buscarClientes(
                {
                  query: ''
                }
              );
              this.formulario.patchValue(
                {
                  tipoVehiculo: tipoVehiculoCreada,
                }
              );
            },
            error: (err) => {
              console.error({
                mensage: 'Error crear Cliente',
                error: err,
              });
              this._notificacionService.modalInfo(
                'error',
                'Error',
                'Error al guardar el registro',
              );
            },
          });
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

  agregarTipoCambio() {
    const dialogRef = this._matDialog.open(ModalCrearEditarTipoCambioComponent, {
      width: '550px',
      data: {},
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          const cliente$ = this._tipoCambioRestService.create(response);

          cliente$.subscribe({
            next: (tipoCambioCreada: TipoCambioInterface) => {
              this._notificacionService.modalInfo(
                'success',
                'Info',
                'Registro guardado correctamente',
              );
              this.buscarClientes(
                {
                  query: ''
                }
              );
              this.formulario.patchValue(
                {
                  tipoCambio: tipoCambioCreada,
                }
              );
            },
            error: (err) => {
              console.error({
                mensage: 'Error crear Cliente',
                error: err,
              });
              this._notificacionService.modalInfo(
                'error',
                'Error',
                'Error al guardar el registro',
              );
            },
          });
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

  agregarTipoCombustible() {
    const dialogRef = this._matDialog.open(ModalCrearEditarTipoCombustibleComponent, {
      width: '550px',
      data: {},
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          const cliente$ = this._tipoCombustibleRestService.create(response);

          cliente$.subscribe({
            next: (tipoCombustibleCreada: TipoCombustibleInterface) => {
              this._notificacionService.modalInfo(
                'success',
                'Info',
                'Registro guardado correctamente',
              );
              this.buscarClientes(
                {
                  query: ''
                }
              );
              this.formulario.patchValue(
                {
                  tipoCombustible: tipoCombustibleCreada,
                }
              );
            },
            error: (err) => {
              console.error({
                mensage: 'Error crear Cliente',
                error: err,
              });
              this._notificacionService.modalInfo(
                'error',
                'Error',
                'Error al guardar el registro',
              );
            },
          });
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }


}
