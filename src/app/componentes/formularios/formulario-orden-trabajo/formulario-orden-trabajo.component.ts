import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {CargandoService} from "../../../servicios/cargando.service";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {escucharCampo} from "../../../utils/validar-campo-reactive-form.util";
import {debounceTime} from "rxjs/operators";
import * as moment from "moment";
import {OrdenTrabajoInterface} from "../../../modulos/trabajos/interfaces/orden-trabajo.interface";
import {TrabajoInterface} from "../../../modulos/trabajos/interfaces/trabajo.interface";
import {EstadoTrabajo, TipoGasto} from "../../../enums/tipo-contacto.enums";
import {ESTADOS_TRABAJO} from "../../../constantes/tipos-identificacion";

@Component({
  selector: 'app-formulario-orden-trabajo',
  templateUrl: './formulario-orden-trabajo.component.html',
  styleUrls: ['./formulario-orden-trabajo.component.scss']
})
export class FormularioOrdenTrabajoComponent implements OnInit {
  @Input()
  data: OrdenTrabajoInterface | undefined;

  @Input()
  soloVer = false;

  @Input()
  trabajo: TrabajoInterface | undefined;

  @Output()
  datosFormulario: EventEmitter<OrdenTrabajoInterface | boolean> = new EventEmitter<
    OrdenTrabajoInterface | boolean
    >();

  formularioValido = false;
  formulario!: FormGroup;

  MENSAJES_DE_ERROR = {
    fechaOrdenTrabajo: {
      required: 'El campo fecha de orden e trabajo es requerido',
    },
    ordenTrabajoDetalles: {
      required: 'El campo es requerido',
    },
    estadoAccion: {
      required: 'El campo estado es requerido',
    },
    observacion: {
      required: 'El campo observación es requerido',
    }
  };
  estructuraErrores = {
    fechaOrdenTrabajo: [],
    ordenTrabajoDetalles: [],
    estadoAccion: [],
    observacion: [],
  };
  tipoGasto = TipoGasto.Repuesto;
  tablaDetalleOrdenTrabajoValida!: boolean;
  estados = ESTADOS_TRABAJO;
  estadosTrabajo = EstadoTrabajo;
  constructor(
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _notificacionService: NotificacionService,
  ) {
    this.construirFormulario();
  }

  construirFormulario(): void {
    this.formulario = new FormGroup({
      id: new FormControl(null, []),
      fechaOrdenTrabajo: new FormControl(new Date(), [
        Validators.required
      ]),
      ordenTrabajoDetalles: new FormControl([], [
        Validators.required
      ]),
      estadoAccion: new FormControl('NG', [
        Validators.required,
      ]),
      observacion: new FormControl('', [
      ]),
    });
  }

  ngOnInit(): void {
    this.escucharCamposDeFormulario();
    this.escucharCambiosEnFormulario();
    this.cargarDatosDeInicioFormulario();
  }

  escucharCamposDeFormulario(): void {
    const campos = { ...this.estructuraErrores };
    Object.keys(campos).forEach((key) => {
      escucharCampo(
        key,
        this.formulario,
        this.estructuraErrores,
        this.MENSAJES_DE_ERROR
      );
    });
  }

  escucharCambiosEnFormulario(): void {
    const formulario$ = this.formulario.valueChanges;
    formulario$.pipe(debounceTime(250)).subscribe(async (valoresForm) => {
      this.formularioValido = !this.formulario.invalid;
      if (this.formularioValido) {
        this.datosFormulario.emit(valoresForm);
      } else {
        this.datosFormulario.emit(false);
      }
    });
  }

  enviarFormulario(): void {
    this.formulario.markAllAsTouched();
    this.escucharCamposDeFormulario();
  }

  cargarDatosDeInicioFormulario() {
    if (this.data) {
      this.formulario.patchValue(
        {
          id: this.data?.id,
          fechaOrdenTrabajo: this.data?.fechaOrdenTrabajo,
          ordenTrabajoDetalles: this.data.ordenTrabajoDetalles,
          estadoAccion: this.data.estadoAccion,
          observacion: this.data.observacion,
        }
      );
    } else {
      if (this.trabajo?.recepciones) {
        const recepcionVehiculo = this.trabajo.recepciones[0];
      }
      this.formulario.patchValue(
        {
          fechaOrdenTrabajo: moment().format('YYYY-MM-DD'),
        }
      )
    }
  }

  obtenerTablaDetalles(respuesta: any) {
    this.tablaDetalleOrdenTrabajoValida = respuesta.tablaDetalleValida;
    if (this.tablaDetalleOrdenTrabajoValida) {
      this.formulario.patchValue(
        {
          ordenTrabajoDetalles: respuesta.detalles,
        }
      );
    } else {
      this.formulario.patchValue(
        {
          ordenTrabajoDetalles: null,
        }
      );
    }

  }
}
