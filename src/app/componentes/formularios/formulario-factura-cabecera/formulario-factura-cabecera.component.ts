import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {escucharCampo} from "../../../utils/validar-campo-reactive-form.util";
import {debounceTime} from "rxjs/operators";
import {FacturaCabeceraInterface} from "../../../modulos/gastos/interfaces/factura-cabecera.interface";
import {MatDialog} from "@angular/material/dialog";
import {DomSanitizer} from "@angular/platform-browser";
import { Buffer } from 'buffer';

@Component({
  selector: 'app-formulario-factura-cabecera',
  templateUrl: './formulario-factura-cabecera.component.html',
  styleUrls: ['./formulario-factura-cabecera.component.scss']
})
export class FormularioFacturaCabeceraComponent implements OnInit {

  @Input()
  data: FacturaCabeceraInterface | undefined;

  @Output()
  datosFormulario: EventEmitter<FacturaCabeceraInterface | boolean> = new EventEmitter<
    FacturaCabeceraInterface | boolean
    >();

  archivoPdf: any;
  formularioValido = false;
  formulario!: FormGroup;

  MENSAJES_DE_ERROR = {
    fechaEmision: {
      required: 'El campo fecha de emisión es requerido',
    },
    numero: {
      required: 'El campo numero es requerido',
    },
    nombreArchivo: {
      required: 'El campo archivo es requerido',
    }
  };
  estructuraErrores = {
    fechaEmision: [],
    numero: [],
    nombreArchivo: [],
  };
  constructor(
    private readonly _matDialog: MatDialog,
    public readonly _domSanitizer: DomSanitizer,
  ) {
    this.construirFormulario();
  }

  construirFormulario(): void {
    this.formulario = new FormGroup({
      fechaEmision: new FormControl(null, [
        Validators.required,
      ]),
      numero: new FormControl(null, [
        Validators.required,
      ]),
      nombreArchivo: new FormControl(null, []),
    });
  }

  ngOnInit(): void {
    this.escucharCamposDeFormulario();
    this.escucharCambiosEnFormulario();
    this.cargarDatosDeInicioFormulario();
  }
  cargarDatosDeInicioFormulario() {
    if (this.data) {
      this.formulario.patchValue(
        {
          id: this.data?.id,
          fechaEmision: this.data?.fechaEmision,
          numero: this.data?.numero,
        }
      );
    }
  }
  escucharCamposDeFormulario(): void {
    const campos = { ...this.estructuraErrores };
    Object.keys(campos).forEach((key) => {
      escucharCampo(
        key,
        this.formulario,
        this.estructuraErrores,
        this.MENSAJES_DE_ERROR
      );
    });
  }

  escucharCambiosEnFormulario(): void {
    const formulario$ = this.formulario.valueChanges;
    formulario$.pipe(debounceTime(250)).subscribe(async (valoresForm) => {
      this.formularioValido = !this.formulario.invalid;
      if (this.formularioValido) {
        const values = {
          ...valoresForm,
          archivoPdf: this.archivoPdf,
        };
        this.datosFormulario.emit(values);
      } else {
        this.datosFormulario.emit(false);
      }
    });
  }

  enviarFormulario(): void {
    this.formulario.markAllAsTouched();
    this.escucharCamposDeFormulario();
  }

  cargarPdf(evento: any) {
    const fileList: FileList = evento.target.files;
    const existeArchivo = fileList.length > 0;
    if (existeArchivo) {
      this.archivoPdf = fileList.item(0);
    }
  }

  descargar() {
    if (this.data?.archivos?.length) {
      const archivo = this.data.archivos[0];
      const buffer = archivo.buffer?.data;
      const urlArchivo = `data:${archivo.mimetype};base64, ${Buffer.from(buffer)}`;
      const link = document.createElement('a');
      link.setAttribute('target', '_blank');
      link.setAttribute('href', urlArchivo);
      link.setAttribute('download', archivo.originalname as string);
      document.body.appendChild(link);
      link.click();
      link.remove();
    }
  }
}
