import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FacturaCabeceraInterface} from "../../../modulos/gastos/interfaces/factura-cabecera.interface";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {escucharCampo} from "../../../utils/validar-campo-reactive-form.util";
import {debounceTime} from "rxjs/operators";
import {
  EstadoGasto,
  EstadoTrabajo,
  TarfifasIvaEnum,
  TARIFAS_NO_TOTALES,
  TipoGasto
} from "../../../enums/tipo-contacto.enums";
import {ESTADOS_TRABAJO} from "../../../constantes/tipos-identificacion";
import {FacturaDetalleInterface} from "../../../modulos/gastos/interfaces/factura-detalle.interface";
import {TrabajoInterface} from "../../../modulos/trabajos/interfaces/trabajo.interface";
import {ClienteInterface} from "../../../modulos/configuracion-datos/interfaces/cliente.interface";
import {CargandoService} from "../../../servicios/cargando.service";
import {ClienteRestService} from "../../../modulos/configuracion-datos/servicios/cliente-rest.service";
import {NotificacionService} from "../../../servicios/notificacion/notificacion.service";
import {ModalCrearEditarClienteComponent} from "../../modales/modal-crear-editar-cliente/modal-crear-editar-cliente.component";
import * as moment from "moment";
import {TarifaImpuestoInterface} from "../../../modulos/configuracion-datos/interfaces/tarifa-impuesto.interface";
import {TarifaImpuestoRestService} from "../../../modulos/configuracion-datos/servicios/tarifa-impuesto-rest.service";
import {TablaDetalleFacturaComponent} from "../../tablas/tabla-detalle-factura/tabla-detalle-factura.component";
import {setearLabelInformacionTributaria} from "../formulario-trabajo/formulario-trabajo.component";
import {setearClienteAEditar} from "../../../modulos/configuracion-datos/cliente/ruta-cliente/ruta-cliente.component";
import {InformacionTributariaInterface} from "../../../modulos/configuracion-datos/interfaces/informacion-tributaria.interface";
import {AbonoFacturaInterface} from "../../../modulos/gastos/interfaces/abono-factura.interface";

@Component({
  selector: 'app-formulario-factura',
  templateUrl: './formulario-factura.component.html',
  styleUrls: ['./formulario-factura.component.scss']
})
export class FormularioFacturaComponent implements OnInit {
  @ViewChild(TablaDetalleFacturaComponent)
  tablaDetalleFactura!: TablaDetalleFacturaComponent;

  @Input()
  data: FacturaCabeceraInterface | undefined;

  @Output()
  datosFormulario: EventEmitter<FacturaCabeceraInterface | boolean> = new EventEmitter<FacturaCabeceraInterface | boolean>();

  @Input()
  trabajo: TrabajoInterface | undefined;

  archivoPdf: any;
  formularioValido = false;
  formulario!: FormGroup;
  clientes: ClienteInterface[] = [];
  abonosPrevios?: AbonoFacturaInterface[] = [];

  MENSAJES_DE_ERROR = {
    fechaEmision: {
      required: 'El campo fecha de emisión es requerido',
    },
    numero: {
      required: 'El campo numero es requerido',
    },
    nombreComercialCliente: {
      required: 'El campo nombre comercial es requerido',
      minLength: 'El campo nombre comercial debe tener al menos un caracter',
      maxlength:
        'El campo nombre comercial debe tener un máximo de 255 caracteres',
    },
    razonSocialCliente: {
      required: 'El campo razón social es requerido',
      minLength: 'El campo razón social debe tener al menos un caracter',
      maxlength:
        'El campo razón social debe tener un máximo de 255 caracteres',
    },
    identificacionCliente: {
      required: 'El campo identificación es requerido',
      minLength: 'El campo identificación debe tener al menos un caracter',
      maxlength:
        'El campo identificación debe tener un máximo de 50 caracteres',
    },
    direccionCliente: {
      required: 'El campo dirección es requerido',
      minLength: 'El campo dirección debe tener al menos un caracter',
      maxlength:
        'El campo dirección debe tener un máximo de 255 caracteres',
    },
    telefonoCliente: {
      required: 'El campo teléfono es requerido',
      minLength: 'El campo teléfono debe tener al menos un caracter',
      maxlength:
        'El campo teléfono debe tener un máximo de 15 caracteres',
    },
    correoCliente: {
      required: 'El campo correo es requerido',
      minLength: 'El campo correo debe tener al menos un caracter',
      maxlength:
        'El campo correo debe tener un máximo de 150 caracteres',
      pattern: 'El campo correo no es válido',
    },
    cliente: {
      required: 'El campo cliente es requerido',
    },
    esConsumidorFinal: {
      required: 'El campo cliente es requerido',
    },
    estadoAccion: {
      required: 'El campo estado es requerido',
    },
  };
  estructuraErrores = {
    fechaEmision: [],
    numero: [],
    identificacionCliente: [],
    nombreComercialCliente: [],
    razonSocialCliente: [],
    direccionCliente: [],
    telefonoCliente: [],
    correoCliente: [],
    cliente: [],
    esConsumidorFinal: [],
    estadoAccion: [],
  };
  tipoGasto = TipoGasto.Repuesto;
  tablaDetalleFacturaValida!: boolean;
  estados = ESTADOS_TRABAJO;
  estadosTrabajo = EstadoTrabajo;
  tarifaSeleccionadaTotales = TarfifasIvaEnum.Iva12;
  descuentoGeneral = 0;
  facturaDetalles: FacturaDetalleInterface[] = [];
  arreglototalesFactura: any[] = [];

  tarifas: TarifaImpuestoInterface[] = [];
  @Input()
  soloVer!: boolean;

  constructor(
    private readonly _matDialog: MatDialog,
    private readonly _cargandoService: CargandoService,
    private readonly _clienteRestService: ClienteRestService,
    private readonly _notificacionService: NotificacionService,
    private readonly _tarifaImpuestoRestService: TarifaImpuestoRestService,
  ) {
    this.construirFormulario();
  }

  async obtenerTarifas() {
    const consulta = {
      where: {},
      relations: ['impuesto']
    };
    const tarifaImpuesto$ = this._tarifaImpuestoRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    const respuesta = await (tarifaImpuesto$.toPromise());
    this.tarifas = respuesta[0]
      .filter(tarifaAux => tarifaAux.impuesto?.nombre === 'IVA');
  }

  construirFormulario(): void {
    this.formulario = new FormGroup({
      fechaEmision: new FormControl(null, [
        Validators.required,
      ]),
      numero: new FormControl(null, [
        Validators.required,
      ]),
      nombreComercialCliente: new FormControl(null, []),
      razonSocialCliente: new FormControl(null, []),
      identificacionCliente: new FormControl(null, []),
      direccionCliente: new FormControl(null, []),
      telefonoCliente: new FormControl(null, []),
      correoCliente: new FormControl(null, []),
      cliente: new FormControl(null, []),
      facturaDetalles: new FormControl([], [
        Validators.required
      ]),
      estadoAccion: new FormControl('NG', [
        Validators.required,
      ]),
      propina: new FormControl(null, []),
      descuento: new FormControl(null, []),
      descuentoPorcentaje: new FormControl(null, []),
      subtotalIva0: new FormControl(null, []),
      subtotalIva: new FormControl(null, []),
      subtotalIvaExento: new FormControl(null, []),
      subtotalIvaNoObjeto: new FormControl(null, []),
      ivaPorcentaje: new FormControl(null, []),
      ivaValor: new FormControl(null, []),
      iceValor: new FormControl(null, []),
      total: new FormControl(null, []),
      subtotalSinImpuestos: new FormControl(null, []),
      esConsumidorFinal: new FormControl(false, []),
      abonos: new FormControl([], []),
    });
  }

  async ngOnInit() {
    await this.obtenerTarifas();
    this.escucharCamposDeFormulario();
    this.escucharCambiosEnFormulario();
    this.cargarDatosDeInicioFormulario();
  }

  cargarDatosDeInicioFormulario() {
    if (this.data) {
      const informacionTributaria = this.data.cliente?.informacionTributaria;
      if (this.data.cliente) {
        this.data.cliente.label = setearLabelInformacionTributaria(informacionTributaria as InformacionTributariaInterface);
      }
      this.formulario.patchValue(
        {
          id: this.data?.id,
          fechaEmision: this.data?.fechaEmision,
          numero: this.data?.numero,
          facturaDetalles: this.data?.facturaDetalles,
          estadoAccion: this.data.estadoAccion,
          esConsumidorFinal: !!this.data.esConsumidorFinal,
          cliente: this.data.cliente,
          razonSocialCliente: this.data.razonSocial ?? informacionTributaria?.razonSocial,
          nombreComercialCliente: this.data.nombreComercial ?? informacionTributaria?.nombreComercial,
          identificacionCliente: this.data.identificacion ?? informacionTributaria?.identificacion,
          direccionCliente: this.data.direccion ?? informacionTributaria?.direccion,
          telefonoCliente: this.data.telefono ?? informacionTributaria?.telefono,
          correoCliente: this.data.correo ?? informacionTributaria?.correo,
        }
      );
      if (this.data.facturaDetalles) {
        this.facturaDetalles = this.data.facturaDetalles;
      }
      if (this.data.abonosFactura) {
        this.abonosPrevios = this.data.abonosFactura;
      }
    } else {
      if (this.trabajo?.trabajosRealizados) {
        const trabajosRealizado = this.trabajo.trabajosRealizados[0];
        if (trabajosRealizado && trabajosRealizado.trabajoRealizadoDetalles?.length) {
          this.facturaDetalles = trabajosRealizado.trabajoRealizadoDetalles?.map(
            trabajoRealizadoDetalle => {
              return {
                articulo: trabajoRealizadoDetalle?.articulo,
                descripcion: trabajoRealizadoDetalle?.descripcion,
                cantidad: trabajoRealizadoDetalle?.cantidad,
                tipo: trabajoRealizadoDetalle?.tipo,
                precioUnitario: trabajoRealizadoDetalle.precioUnitario,
                ivaPorcentaje: trabajoRealizadoDetalle.ivaPorcentaje,
                ivaDescripcion: trabajoRealizadoDetalle.ivaDescripcion,
                descuento: trabajoRealizadoDetalle.descuento,
                valorDescuento: trabajoRealizadoDetalle.valorDescuento,
                valorIva: trabajoRealizadoDetalle.valorIva,
                valorIce: trabajoRealizadoDetalle.valorIce,
                total: trabajoRealizadoDetalle.total,
              }
            });
        }
      }
      this.formulario.patchValue(
        {
          cliente: this.trabajo?.vehiculo?.cliente,
          fechaEmision: moment().format('YYYY-MM-DD'),
        }
      );
      if (this.trabajo?.vehiculo?.cliente) {
        this.setearFormulario(this.trabajo?.vehiculo?.cliente as ClienteInterface);
      }
    }
  }

  escucharCamposDeFormulario(): void {
    const campos = {...this.estructuraErrores};
    Object.keys(campos).forEach((key) => {
      escucharCampo(
        key,
        this.formulario,
        this.estructuraErrores,
        this.MENSAJES_DE_ERROR
      );
    });
  }

  escucharCambiosEnFormulario(): void {
    const formulario$ = this.formulario.valueChanges;
    formulario$.pipe(debounceTime(250)).subscribe(async (valoresForm) => {
      const abonos = this.formulario.get('abonos')?.value;
      console.log(this.formulario);
      this.formularioValido = !this.formulario.invalid && abonos;
      if (this.formularioValido) {
        const values = {
          ...valoresForm,
          archivoPdf: this.archivoPdf,
        };
        this.datosFormulario.emit(values);
      } else {
        this.datosFormulario.emit(false);
      }
    });
  }

  enviarFormulario(): void {
    this.formulario.markAllAsTouched();
    this.escucharCamposDeFormulario();
  }

  obtenerTablaDetalles(respuesta: any) {
    this.tablaDetalleFacturaValida = respuesta.tablaDetalleValida;
    if (this.tablaDetalleFacturaValida) {
      this.formulario.patchValue(
        {
          facturaDetalles: respuesta.detalles,
        }
      );

      if (respuesta?.detalles?.length) {
        this.facturaDetalles = respuesta?.detalles;
        this.setearArregloTotales();
      }
      // this.setearFactura();

    } else {
      this.formulario.patchValue(
        {
          facturaDetalles: null,
        }
      );
    }
  }

  recibirValorTotalAPagar(objetValorAPagar: any) {
    this.descuentoGeneral = objetValorAPagar.porcentajeDescuento || this.descuentoGeneral;
    this.formulario.patchValue(
      {
        total: objetValorAPagar.valorAPagar,
        descuentoPorcentaje: this.descuentoGeneral,
        descuento: objetValorAPagar.descuentos,
      }
    );
  }

  async recibirTarifaIva(tarifaDescripcion: any) {
    this.tarifaSeleccionadaTotales = tarifaDescripcion;
    this.tablaDetalleFactura.tarifaSeleccionadaTotales = this.tarifaSeleccionadaTotales;
    await this.tablaDetalleFactura.obtenerTarifasIva();
    const tarifa = this.tarifas.find(tarifa => tarifa.descripcion === tarifaDescripcion);
    this.tablaDetalleFactura.datos.map(detalle => {
      if (!TARIFAS_NO_TOTALES.includes(detalle.ivaDescripcion as any)) {
        detalle.ivaDescripcion = tarifaDescripcion;
        detalle.ivaPorcentaje = tarifa?.valor;
        this.tablaDetalleFactura.calcularValores(detalle);
      }
    });
    this.tablaDetalleFactura.datos = [...this.tablaDetalleFactura.datos];
    this.tablaDetalleFactura.emitirTablaDetalle();
    this.facturaDetalles = this.tablaDetalleFactura.datos;
    this.setearArregloTotales();
  }


  buscarClientes(evento: any) {
    this._cargandoService.habilitar();
    let consulta = {
      where: {
        estado: 1
      },
      relations: ['informacionTributaria']
    };

    const cliente$ = this._clienteRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    cliente$.subscribe({
      next: (respuesta: [any[], number]) => {
        this.clientes = respuesta[0].filter(cliente => {
          const informacionTributaria = cliente.informacionTributaria;
          cliente.label = setearLabelInformacionTributaria(informacionTributaria);
          const buscarRazonSocial = (informacionTributaria.razonSocial || '').toUpperCase().search(evento.query.toUpperCase()) !== -1;
          const buscarNombreComercial = (informacionTributaria.nombreComercial || '').toUpperCase().search(evento.query.toUpperCase()) !== -1;
          const buscarIdentificacion = (informacionTributaria.identificacion || '').toUpperCase().search(evento.query.toUpperCase()) !== -1;
          return buscarRazonSocial || buscarNombreComercial || buscarIdentificacion;
        });
        this._cargandoService.deshabilitar();
      },
      error: (err) => {
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al traer registros',
        );
        console.error({
          mensage: 'Error en autocomplete',
          error: err,
        });
        this._cargandoService.deshabilitar();
      },
    });
  }

  agregarCliente(cliente?: ClienteInterface) {
    if (cliente) {
      setearClienteAEditar(cliente);
    }
    const dialogRef = this._matDialog.open(ModalCrearEditarClienteComponent, {
      width: '1750px',
      data: {
        cliente
      },
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          const cliente$ = this._clienteRestService.crearEditarCliente(response);

          cliente$.subscribe({
            next: (clienteCreado: ClienteInterface) => {
              clienteCreado.label = setearLabelInformacionTributaria(clienteCreado?.informacionTributaria as InformacionTributariaInterface);
              const informacionTributaria = clienteCreado.informacionTributaria;
              this._notificacionService.modalInfo(
                'success',
                'Info',
                'Registro guardado correctamente',
              );
              this.formulario.patchValue(
                {
                  cliente: clienteCreado,
                  identificacionCliente: informacionTributaria?.identificacion,
                  direccionCliente: informacionTributaria?.direccion,
                  correoCliente: informacionTributaria?.correo,
                  telefonoCliente: informacionTributaria?.telefono,
                  nombreComercialCliente: informacionTributaria?.nombreComercial,
                  razonSocialCliente: informacionTributaria?.razonSocial,
                }
              );
              this.setearFormulario(clienteCreado);
            },
            error: (err) => {
              console.error({
                mensage: 'Error crear Cliente',
                error: err,
              });
              this._notificacionService.modalInfo(
                'error',
                'Error',
                'Error al guardar el registro',
              );
            },
          });
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }

  editarCliente() {
    const cliente = this.formulario.get('cliente')?.value;
    if (cliente) {
      this.agregarCliente(cliente);
    } else {
      this._notificacionService.modalInfoAsync(
        'question',
        'Advertencia',
        'Busque y Seleccione un cliente/propietario para cambiar la información',
      );
    }
  }

  setearFormulario(cliente: ClienteInterface) {
    this.formulario.patchValue(
      {
        nombreComercialCliente: cliente.informacionTributaria?.nombreComercial,
        razonSocialCliente: cliente.informacionTributaria?.razonSocial,
        identificacionCliente: cliente.informacionTributaria?.identificacion,
        direccionCliente: cliente.informacionTributaria?.direccion,
        telefonoCliente: cliente.informacionTributaria?.telefono,
        correoCliente: cliente.informacionTributaria?.correo,
      }
    );
  }

  cambioConsumidorFinal(evento: any) {
    const esConsumidorFinal = this.formulario.get('esConsumidorFinal')?.value;
    if (esConsumidorFinal) {
      this.formulario.get('cliente')?.setValidators([]);
      this.formulario.get('cliente')?.disable();
      this.formulario.patchValue(
        {
          cliente: null,
          nombreComercialCliente: null,
          razonSocialCliente: null,
          identificacionCliente: null,
          direccionCliente: null,
          telefonoCliente: null,
          correoCliente: null,
        }
      );
    } else {
      this.formulario.get('cliente')?.setValidators([Validators.required]);
      this.formulario.get('cliente')?.enable();
    }
  }

  setearArregloTotales() {
    const arregloTotales: any[] = [];
    const arregloSubtotalesTarifa: any[] = [];
    const arregloTarifasValidas = [...TARIFAS_NO_TOTALES, this.tarifaSeleccionadaTotales];
    this.tarifas.map(tarifa => {
      if (arregloTarifasValidas.includes(tarifa.descripcion as any)) {
        const detallesTarifas = this.facturaDetalles?.filter(detalle => detalle.ivaDescripcion === tarifa.descripcion);
        const sumaDetallesValorTotalTarifa = detallesTarifas?.reduce((acum: any, detalle) => {
          return +(detalle.total as any) + acum;
        }, 0) || 0;
        const sumaDetallesValorTarifa = detallesTarifas?.reduce((acum: any, detalle) => {
          return +(detalle.valorIva as any) + acum;
        }, 0) || 0;
        arregloSubtotalesTarifa.push(
          {
            sumaDetallesValorTotalTarifa,
            sumaDetallesValorTarifa,
            tarifa: tarifa.descripcion
          }
        );
      }
    });

    const sumaDetallesValorTotal = this.facturaDetalles?.reduce((acum: any, detalle) => {
      return +(detalle.total as any) + acum;
    }, 0) || 0;

    arregloTotales.push(
      {
        label: 'Subtotal sin impuestos',
        valor: sumaDetallesValorTotal
      }
    );
    const arregloTarifas = [...TARIFAS_NO_TOTALES, this.tarifaSeleccionadaTotales];
    arregloSubtotalesTarifa.map(datosTarifa => {
      if (arregloTarifas.includes(datosTarifa.tarifa)) {
        arregloTotales.push(
          {
            label: `Subtotal ${datosTarifa.tarifa}`,
            valor: datosTarifa.sumaDetallesValorTotalTarifa,
          }
        )
      }
    });

    const sumaDetallesValorIce = this.facturaDetalles?.reduce((acum: any, detalle) => {
      return +(detalle.valorIce as number) + acum;
    }, 0) || 0;
    // arregloTotales.push(
    //   {
    //     label: 'Valor ICE',
    //     valor: sumaDetallesValorIce
    //   }
    // );
    const tarifaIva = arregloSubtotalesTarifa.find(datosTarifa => datosTarifa.tarifa === this.tarifaSeleccionadaTotales);
    if (tarifaIva) {
      arregloTotales.push(
        {
          label: `Iva ${tarifaIva.tarifa}`,
          valor: tarifaIva.sumaDetallesValorTarifa,
          esTarifa: 1,
        }
      );
    }

    const sumaValoresIva = arregloSubtotalesTarifa.reduce((acum, tarifa) => {
      return tarifa.sumaDetallesValorTarifa + acum;
    }, 0)

    // arregloTotales.push(
    //   {
    //     label: `Propina`,
    //     valor: 0,
    //   }
    // );
    // arregloTotales.push(
    //   {
    //     label: 'Descuento General',
    //     // valor: sumaDetallesDescuentos
    //     valor: this.descuentoGeneral || 0,
    //   }
    // );
    const subtotalSinDescuentoGeneral = sumaDetallesValorTotal + sumaValoresIva + sumaDetallesValorIce;
    const descuento = this.descuentoGeneral / 100 * subtotalSinDescuentoGeneral;
    const valorAPagar = subtotalSinDescuentoGeneral - descuento;
    arregloTotales.push(
      {
        label: `Valor a pagar`,
        valor: valorAPagar,
      }
    );
    this.arreglototalesFactura = arregloTotales;

    this.formulario.patchValue(
      {
        subtotalIva0: arregloTotales.find(total => total.label === TarfifasIvaEnum.Iva0)?.valor || 0,
        subtotalIva: arregloTotales.find(total => total.esTarifa)?.valor || 0,
        subtotalIvaExento: arregloTotales.find(total => total.label === TarfifasIvaEnum.IvaExtento)?.valor || 0,
        subtotalIvaNoObjeto: arregloTotales.find(total => total.label === TarfifasIvaEnum.IvaNoObjeto)?.valor || 0,
        subtotalSinImpuestos: sumaDetallesValorTotal,
        descuento: descuento,
        descuentoPorcentaje: this.descuentoGeneral,
        ivaValor: sumaValoresIva,
        ivaPorcentaje: this.tarifaSeleccionadaTotales,
        iceValor: sumaDetallesValorIce,
        total: valorAPagar,
      }
    );
  }

  obtenerAbonosFactura(abonos: AbonoFacturaInterface[]) {
    this.formulario.patchValue(
      {
        abonos,
      }
    );
  }

  setearValorAPagarFactura() {
    return this.formulario?.get('total')?.value || 0;
  }

  setearEstado() {
    return this.data?.estadoAccion === EstadoTrabajo.Finalizado ? EstadoGasto.Pagado : EstadoGasto.PorPagar;
  }
}
