import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TipoVehiculoInterface} from "../../../modulos/configuracion-datos/interfaces/tipo-vehiculo.interface";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {cargarDatosDeInicioFormulario, escucharCampo} from "../../../utils/validar-campo-reactive-form.util";
import {debounceTime} from "rxjs/operators";

@Component({
  selector: 'app-formulario-tipo-vehiculo',
  templateUrl: './formulario-tipo-vehiculo.component.html',
  styleUrls: ['./formulario-tipo-vehiculo.component.scss']
})
export class FormularioTipoVehiculoComponent implements OnInit {


  @Input()
  data: TipoVehiculoInterface | undefined;

  @Output()
  datosFormulario: EventEmitter<TipoVehiculoInterface | boolean> = new EventEmitter<
    TipoVehiculoInterface | boolean
    >();

  formularioValido = false;
  formulario!: FormGroup;

  MENSAJES_DE_ERROR = {
    nombre: {
      required: 'El campo nombre es requerido',
      minLength: 'El campo nombre debe tener al menos un caracter',
      maxlength:
        'El campo nombre debe tener un máximo de 255 caracteres',
    }
  };
  estructuraErrores = {
    nombre: [],
  };

  constructor(
  ) {
    this.construirFormulario();
  }

  construirFormulario(): void {
    this.formulario = new FormGroup({
      nombre: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
    });
  }

  ngOnInit(): void {
    this.escucharCamposDeFormulario();
    this.escucharVehiculosEnFormulario();
    if (this.data) {
      cargarDatosDeInicioFormulario<TipoVehiculoInterface>(
        this.formulario,
        this.data
      );
    }
  }

  escucharCamposDeFormulario(): void {
    const campos = { ...this.estructuraErrores };
    Object.keys(campos).forEach((key) => {
      escucharCampo(
        key,
        this.formulario,
        this.estructuraErrores,
        this.MENSAJES_DE_ERROR
      );
    });
  }

  escucharVehiculosEnFormulario(): void {
    const formulario$ = this.formulario.valueChanges;
    formulario$.pipe(debounceTime(250)).subscribe(async (valoresForm) => {
      this.formularioValido = !this.formulario.invalid;
      if (this.formularioValido) {
        const values = {
          ...valoresForm,
        };
        this.datosFormulario.emit(values);
      } else {
        this.datosFormulario.emit(false);
      }
    });
  }

  enviarFormulario(): void {
    this.formulario.markAllAsTouched();
    this.escucharCamposDeFormulario();
  }
}
