import {Component, OnInit, ViewChild} from '@angular/core';
import {TarfifasIvaEnum, TARIFAS_NO_TOTALES, TipoGasto} from "../../../enums/tipo-contacto.enums";
import {TrabajoRealizadoDetalleInterface} from "../../../modulos/trabajos/interfaces/trabajo-realizado-detalle.interface";
import {TarifaImpuestoInterface} from "../../../modulos/configuracion-datos/interfaces/tarifa-impuesto.interface";
import {TablaDetalleFacturaComponent} from "../../tablas/tabla-detalle-factura/tabla-detalle-factura.component";
import {TarifaImpuestoRestService} from "../../../modulos/configuracion-datos/servicios/tarifa-impuesto-rest.service";

@Component({
  selector: 'app-formulario-proforma-rapida',
  templateUrl: './formulario-proforma-rapida.component.html',
  styleUrls: ['./formulario-proforma-rapida.component.scss']
})
export class FormularioProformaRapidaComponent implements OnInit {
  @ViewChild(TablaDetalleFacturaComponent)
  tablaDetalleFactura!: TablaDetalleFacturaComponent;

  tipoGasto = TipoGasto.Repuesto;
  arreglototalesFactura: any[] = [];
  tablaDetalleTrabajoRealizadoValida!: boolean;
  trabajoRealizadoDetalles?: TrabajoRealizadoDetalleInterface[];
  tarifas: TarifaImpuestoInterface[] = [];
  tarifaSeleccionadaTotales = TarfifasIvaEnum.Iva12;
  descuentoGeneral = 0;

  constructor(
    private readonly _tarifaImpuestoRestService: TarifaImpuestoRestService,
  ) { }

  async ngOnInit() {
    await this.obtenerTarifas();
  }

  async obtenerTarifas() {
    const consulta = {
      where: {},
      relations: ['impuesto']
    };
    const tarifaImpuesto$ = this._tarifaImpuestoRestService.findAll(
      `busqueda=${JSON.stringify(consulta)}`
    );
    const respuesta = await (tarifaImpuesto$.toPromise());
    this.tarifas = respuesta[0]
      .filter(tarifaAux => tarifaAux.impuesto?.nombre === 'IVA');
  }


  obtenerTablaDetalles(respuesta: any) {
    this.tablaDetalleTrabajoRealizadoValida = respuesta.tablaDetalleValida;
    if (this.tablaDetalleTrabajoRealizadoValida) {
      if (respuesta?.detalles?.length) {
        this.trabajoRealizadoDetalles = respuesta?.detalles;
        this.setearArregloTotales();
      }
    }
  }

  recibirValorTotalAPagar(objetValorAPagar: any) {
    this.descuentoGeneral = objetValorAPagar.porcentajeDescuento || this.descuentoGeneral;
  }

  async recibirTarifaIva(tarifaDescripcion: any) {
    this.tarifaSeleccionadaTotales = tarifaDescripcion;
    this.tablaDetalleFactura.tarifaSeleccionadaTotales = this.tarifaSeleccionadaTotales;
    await this.tablaDetalleFactura.obtenerTarifasIva();
    const tarifa = this.tarifas.find(tarifa => tarifa.descripcion === tarifaDescripcion);
    this.tablaDetalleFactura.datos.map(detalle => {
      if (!TARIFAS_NO_TOTALES.includes(detalle.ivaDescripcion as any)) {
        detalle.ivaDescripcion = tarifaDescripcion;
        detalle.ivaPorcentaje = tarifa?.valor;
        this.tablaDetalleFactura.calcularValores(detalle);
      }
    });
    this.tablaDetalleFactura.datos = [...this.tablaDetalleFactura.datos];
    this.tablaDetalleFactura.emitirTablaDetalle();
    this.trabajoRealizadoDetalles = this.tablaDetalleFactura.datos;
    this.setearArregloTotales();
  }

  setearArregloTotales() {
    const arregloTotales: any[] = [];
    const arregloSubtotalesTarifa: any[] = [];
    const arregloTarifasValidas = [...TARIFAS_NO_TOTALES, this.tarifaSeleccionadaTotales];
    this.tarifas.map(tarifa => {
      if (arregloTarifasValidas.includes(tarifa.descripcion as any)) {
        const detallesTarifas = this.trabajoRealizadoDetalles?.filter(detalle => detalle.ivaDescripcion === tarifa.descripcion);
        const sumaDetallesValorTotalTarifa = detallesTarifas?.reduce((acum: any, detalle) => {
          return +(detalle.total as any) + acum;
        }, 0) || 0;
        const sumaDetallesValorTarifa = detallesTarifas?.reduce((acum: any, detalle) => {
          return +(detalle.valorIva as any) + acum;
        }, 0) || 0;
        arregloSubtotalesTarifa.push(
          {
            sumaDetallesValorTotalTarifa,
            sumaDetallesValorTarifa,
            tarifa: tarifa.descripcion
          }
        );
      }
    });

    const sumaDetallesValorTotal = this.trabajoRealizadoDetalles?.reduce((acum: any, detalle) => {
      return +(detalle.total as any) + acum;
    }, 0) || 0;

    arregloTotales.push(
      {
        label: 'Subtotal sin impuestos',
        valor: sumaDetallesValorTotal
      }
    );
    const arregloTarifas = [...TARIFAS_NO_TOTALES, this.tarifaSeleccionadaTotales];
    arregloSubtotalesTarifa.map(datosTarifa => {
      if (arregloTarifas.includes(datosTarifa.tarifa)) {
        arregloTotales.push(
          {
            label: `Subtotal ${datosTarifa.tarifa}`,
            valor: datosTarifa.sumaDetallesValorTotalTarifa,
          }
        )
      }
    });

    const sumaDetallesValorIce = this.trabajoRealizadoDetalles?.reduce((acum: any, detalle) => {
      return +(detalle.valorIce as number) + acum;
    }, 0) || 0;
    const tarifaIva = arregloSubtotalesTarifa.find(datosTarifa => datosTarifa.tarifa === this.tarifaSeleccionadaTotales);
    if (tarifaIva) {
      arregloTotales.push(
        {
          label: `Iva ${tarifaIva.tarifa}`,
          valor: tarifaIva.sumaDetallesValorTarifa,
          esTarifa: 1,
        }
      );
    }
    const sumaValoresIva = arregloSubtotalesTarifa.reduce((acum, tarifa) => {
      return tarifa.sumaDetallesValorTarifa + acum;
    }, 0)
    const subtotalSinDescuentoGeneral = sumaDetallesValorTotal + sumaValoresIva + sumaDetallesValorIce;
    const descuento = this.descuentoGeneral / 100 * subtotalSinDescuentoGeneral;
    const valorAPagar = subtotalSinDescuentoGeneral - descuento;
    arregloTotales.push(
      {
        label: `Valor a pagar`,
        valor: valorAPagar,
      }
    );
    this.arreglototalesFactura = arregloTotales;
  }

}
