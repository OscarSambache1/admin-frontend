import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ImpuestoInterface} from "../../../modulos/configuracion-datos/interfaces/impuesto.interface";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {cargarDatosDeInicioFormulario, escucharCampo} from "../../../utils/validar-campo-reactive-form.util";
import {debounceTime} from "rxjs/operators";

@Component({
  selector: 'app-formulario-impuesto',
  templateUrl: './formulario-impuesto.component.html',
  styleUrls: ['./formulario-impuesto.component.scss']
})
export class FormularioImpuestoComponent implements OnInit {


  @Input()
  data: ImpuestoInterface | undefined;

  @Output()
  datosFormulario: EventEmitter<ImpuestoInterface | boolean> = new EventEmitter<
    ImpuestoInterface | boolean
    >();

  formularioValido = false;
  formulario!: FormGroup;

  MENSAJES_DE_ERROR = {
    nombre: {
      required: 'El campo nombre es requerido',
      minLength: 'El campo nombre debe tener al menos un caracter',
      maxlength:
        'El campo nombre debe tener un máximo de 255 caracteres',
    },
    codigo: {
      required: 'El campo código es requerido',
      minLength: 'El campo código Material debe tener al menos un caracter',
      maxlength:
        'El campo código debe tener un máximo de 10 caracteres',
    },
    descripcion: {
      required: 'El campo descripcion es requerido',
      minLength: 'El campo descripcion Material debe tener al menos un caracter',
      maxlength:
        'El campo descripcion debe tener un máximo de 255 caracteres',
    }
  };
  estructuraErrores = {
    nombre: [],
    codigo: [],
    descripcion: [],
  };

  constructor(
  ) {
    this.construirFormulario();
  }

  construirFormulario(): void {
    this.formulario = new FormGroup({
      nombre: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
      codigo: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(10),
      ]),
      descripcion: new FormControl(null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(255),
      ]),
    });
  }

  ngOnInit(): void {
    this.escucharCamposDeFormulario();
    this.escucharCambiosEnFormulario();
    if (this.data) {
      cargarDatosDeInicioFormulario<ImpuestoInterface>(
        this.formulario,
        this.data
      );
    }
  }

  escucharCamposDeFormulario(): void {
    const campos = { ...this.estructuraErrores };
    Object.keys(campos).forEach((key) => {
      escucharCampo(
        key,
        this.formulario,
        this.estructuraErrores,
        this.MENSAJES_DE_ERROR
      );
    });
  }

  escucharCambiosEnFormulario(): void {
    const formulario$ = this.formulario.valueChanges;
    formulario$.pipe(debounceTime(250)).subscribe(async (valoresForm) => {
      this.formularioValido = !this.formulario.invalid;
      if (this.formularioValido) {
        const values = {
          ...valoresForm,
        };
        this.datosFormulario.emit(values);
      } else {
        this.datosFormulario.emit(false);
      }
    });
  }

  enviarFormulario(): void {
    this.formulario.markAllAsTouched();
    this.escucharCamposDeFormulario();
  }
}
