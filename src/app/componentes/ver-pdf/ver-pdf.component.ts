import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";
import {CargandoService} from "../../servicios/cargando.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-ver-pdf',
  templateUrl: './ver-pdf.component.html',
  styleUrls: ['./ver-pdf.component.scss']
})
export class VerPdfComponent implements OnInit {
  mostrarVisor: boolean;
  src: any;
  cargoVisor!: boolean;

  constructor(
    private sanitized: DomSanitizer,
    private readonly _cargandoService: CargandoService,
    public dialogRef: MatDialogRef<VerPdfComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: any
  ) {
    this.mostrarVisor = true;
  }

  ngOnInit() {
    this.src = this.data.url;
    this._cargandoService.habilitar();
  }

  onError(event: any) {
    this.mostrarVisor = false;
    this.cargoVisor = true;
    this._cargandoService.deshabilitar();
  }

  onLoadComplete() {
    this.cargoVisor = true;
    this._cargandoService.deshabilitar();
  }

  descargarArchivo(url: string) {
    const link = document.createElement('a');
    link.setAttribute('target', '_blank');
    link.setAttribute('href', url);
    link.setAttribute('download', this.data.nombreArchivo ? `${this.data.nombreArchivo}.pdf` : `factura.pdf`);
    document.body.appendChild(link);
    link.click();
    link.remove();
  }

  cancelarModal() {
    this.dialogRef.close();
  }
}
