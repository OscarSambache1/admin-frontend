import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {CargandoService} from "./servicios/cargando.service";
import {locale, loadMessages} from 'devextreme/localization';
import {ConfiguracionRestService} from "./servicios/rest/configuracion-rest.service";
import {ConfiguracionInterface} from "./interfaces/configuracion.interface";
import {environment} from "../environments/environment";
import {InfoConfiguracionService} from "./servicios/info-configuracion.service";
import {AbonoFacturaInterface} from "./modulos/gastos/interfaces/abono-factura.interface";
import {AbonoFacturaRestService} from "./modulos/gastos/servicios/abono-factura-rest.service";
import {EstadoAbono} from "./enums/tipo-contacto.enums";
import * as moment from 'moment';
import {ActualizarPagosService} from "./servicios/actualizar-pagos.service";
import {BackupRestService} from "./servicios/rest/backup-rest.service";
import {DomSanitizer} from "@angular/platform-browser";
import { Buffer } from 'buffer';
import {GastoInterface} from "./modulos/gastos/interfaces/gasto.interface";
import {ModalPagosAbonosComponent} from "./componentes/modales/modal-pagos-abonos/modal-pagos-abonos.component";
import {FacturaCabeceraInterface} from "./modulos/gastos/interfaces/factura-cabecera.interface";
import {MatDialog} from "@angular/material/dialog";
import {FacturaCabeceraRestService} from "./modulos/gastos/servicios/factura-cabecera-rest.service";
import {NotificacionService} from "./servicios/notificacion/notificacion.service";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'admin';
  ocultar = false;
  // @ts-ignore
  private esMessages = require('devextreme/localization/messages/es.json');

  opcionesMenu: any[] = [
    {
      nombre: 'Base de Datos',
      segmento: [''],
      submenu: true,
      icono: 'pi pi-file-pdf',
      opcionesSubmenu: [
        // {
        //   nombre: 'Accesorios',
        //   segmento: ['configuracion-datos', 'accesorio'],
        //   icono: 'pi pi-filter',
        //   submenu: false,
        // },
        {
          nombre: 'Clientes',
          segmento: ['configuracion-datos', 'cliente'],
          icono: 'pi pi-users',
          submenu: false,
        },
        {
          nombre: 'Bancos',
          segmento: ['configuracion-datos', 'banco'],
          icono: 'pi pi-dollar',
          submenu: false,
        },
        {
          nombre: 'Formas de Pago',
          segmento: ['configuracion-datos', 'forma-pago'],
          icono: 'pi pi-money-bill',
          submenu: false,
        },
        {
          nombre: 'Impuestos',
          segmento: ['configuracion-datos', 'impuesto'],
          icono: 'pi pi-percentage',
          submenu: false,
        },
        {
          nombre: 'Marcas',
          segmento: ['configuracion-datos', 'marca'],
          icono: 'pi pi-ticket',
          submenu: false,
        },
        // {
        //   nombre: 'Modelos',
        //   segmento: ['configuracion-datos', 'modelo'],
        //   icono: 'pi pi-compass',
        //   submenu: false,
        // },
        {
          nombre: 'Proveedores',
          segmento: ['configuracion-datos', 'proveedor'],
          icono: 'pi pi-shopping-cart',
          submenu: false,
        },
        // {
        //   nombre: 'Repuestos',
        //   segmento: ['configuracion-datos', 'producto'],
        //   icono: 'pi pi-filter',
        //   submenu: false,
        // },
        {
          nombre: 'Servicios',
          segmento: ['configuracion-datos', 'servicio'],
          icono: 'pi pi-filter',
          submenu: false,
        },
        // {
        //   nombre: 'Secciones',
        //   segmento: ['configuracion-datos', 'seccion'],
        //   icono: 'pi pi-tag',
        //   submenu: false,
        // },
        // {
        //   nombre: 'Subsecciones',
        //   segmento: ['configuracion-datos', 'ubseccion'],
        //   icono: 'pi pi-tags',
        //   submenu: false,
        // },
        // {
        //   nombre: 'Tipos de Cambio',
        //   segmento: ['configuracion-datos', 'tipo-cambio'],
        //   icono: 'pi pi-clone',
        //   submenu: false,
        // },
        // {
        //   nombre: 'Tipos de Combustible',
        //   segmento: ['configuracion-datos', 'tipo-combustible'],
        //   icono: 'pi pi-chevron-circle-down',
        //   submenu: false,
        // },
        // {
        //   nombre: 'Tipos de Vehículos',
        //   segmento: ['configuracion-datos', 'tipo-vehiculo'],
        //   icono: 'pi pi-shopping-cart',
        //   submenu: false,
        // },
        {
          nombre: 'Vehículos',
          segmento: ['configuracion-datos', 'vehiculo'],
          icono: 'pi pi-shopping-cart',
          submenu: false,
        },
      ],
    },
    {
      nombre: 'Stock Bodega',
      segmento: ['stock-bodega'],
      submenu: false,
      icono: 'pi pi-file-pdf',
    },
    {
      nombre: 'Gastos',
      segmento: ['gastos'],
      submenu: false,
      icono: 'pi pi-file-pdf',
    },
    {
      nombre: 'Trabajos',
      segmento: ['trabajos'],
      submenu: true,
      icono: 'pi pi-file-pdf',
      opcionesSubmenu: [
        {
          nombre: 'Trabajos',
          segmento: ['trabajos', 'gestion-trabajo'],
          icono: 'pi pi-filter',
          submenu: false,
        },
        {
          nombre: 'Trabajos Cliente',
          segmento: ['trabajos', 'trabajos-cliente'],
          icono: 'pi pi-filter',
          submenu: false,
        },
        {
          nombre: 'Recepción Vehículo',
          segmento: ['trabajos', 'recepcion-vehiculo'],
          icono: 'pi pi-filter',
          submenu: false,
        },
        {
          nombre: 'Órdenes Trabajo',
          segmento: ['trabajos', 'orden-trabajo'],
          icono: 'pi pi-filter',
          submenu: false,
        },
        {
          nombre: 'Trabajos Realizados',
          segmento: ['trabajos', 'trabajo-realizado'],
          icono: 'pi pi-filter',
          submenu: false,
        },
        {
          nombre: 'Facturas',
          segmento: ['trabajos', 'factura'],
          icono: 'pi pi-filter',
          submenu: false,
        },
      ]
    },
    {
      nombre: 'Configuración',
      segmento: ['ruta-configuracion'],
      submenu: false,
      icono: 'pi pi-file-pdf',
    },
    {
      nombre: 'Proformas',
      segmento: ['proformas'],
      submenu: false,
      icono: 'pi pi-file-pdf',
    },
    {
      nombre: 'Proforma-rapida',
      segmento: ['proforma-rapida'],
      submenu: false,
      icono: 'pi pi-file-pdf',
    },
    {
      nombre: 'Listados',
      segmento: ['listados'],
      submenu: true,
      icono: 'pi pi-file-pdf',
      opcionesSubmenu: [
        {
          nombre: 'Clientes',
          segmento: ['listados', 'cliente'],
          submenu: false,
          icono: 'pi pi-file-pdf',
        },    {
          nombre: 'Proveedores',
          segmento: ['listados', 'proveedor'],
          submenu: false,
          icono: 'pi pi-file-pdf',
        },
      ]
    },
  ];
  configuracion?: ConfiguracionInterface;
  abonosPagos: AbonoFacturaInterface[] = [];

  constructor(
    private readonly _router: Router,
    private readonly _cargandoService: CargandoService,
    private readonly _infoConfiguracionService: InfoConfiguracionService,
    private _configuracionRestService: ConfiguracionRestService,
    private _actualizarPagosService: ActualizarPagosService,
    private _abonoFacturaRestService: AbonoFacturaRestService,
    private _backupRestService: BackupRestService,
    private readonly _matDialog: MatDialog,
    public readonly _domSanitizer: DomSanitizer,
    private readonly _facturaCabeceraRestService: FacturaCabeceraRestService,
    private readonly _notificacionService: NotificacionService,
  ) {
  }

  async ngOnInit() {
    await this.setearAbonos();
    await this.setearConfiguracion();
    this.escucharCambiosEnCargandoService();
    this.escucharCambiosInfoConfiguracion();
    this.escucharActualizarPagos();
    this.configLanguageDevExtreme();
    // await this.crearBackUp();
  }

  async setearAbonos() {
    try {
      this._cargandoService.habilitar();
      const consulta = {
        where: {
          estadoAbono: EstadoAbono.PorPagar
        },
        relations: [
          'facturaCabecera',
          'facturaCabecera.gasto',
          'facturaCabecera.trabajo',
          'facturaCabecera.vehiculo',
          'facturaCabecera.vehiculo.cliente',
          'facturaCabecera.vehiculo.cliente.informacionTributaria',
          'facturaCabecera.proveedor',
          'facturaCabecera.proveedor.informacionTributaria',
          'facturaCabecera.proveedor.cuentasBanco',
          'facturaCabecera.proveedor.cuentasBanco.banco',
          'facturaCabecera.facturaDetalles',
          'facturaCabecera.facturaDetalles.articulo',
        ],
        order: {
          fechaPago: 'DESC'
        }
      };
      const abonos$ = this._abonoFacturaRestService.findAll(
        'busqueda=' + JSON.stringify(consulta),
      );

      const respuesta = await (abonos$.toPromise());
      this.abonosPagos = respuesta[0].filter(abonos => moment(abonos.fechaPago).isSameOrBefore(new Date()));
      this._cargandoService.deshabilitar();
    } catch (error) {
      this._cargandoService.deshabilitar();
    }
  }


  async setearConfiguracion() {
    try {
      this._cargandoService.habilitar();
      const consulta = {
        where: {},
        relations: ['archivos'],
      };
      const configuracion$ = this._configuracionRestService.findAll(
        'busqueda=' + JSON.stringify(consulta),
      );

      const respuesta = await (configuracion$.toPromise());
      this.configuracion = respuesta[0][0] || null;
      this._cargandoService.deshabilitar();
    } catch (error) {
      this._cargandoService.deshabilitar();
    }
  }

  escucharCambiosEnCargandoService(): void {
    this._cargandoService.cambioEstado.subscribe((cambioEstado: boolean) => {
      this.ocultar = cambioEstado;
    });
  }

  escucharCambiosInfoConfiguracion(): void {
    this._infoConfiguracionService.cambioInfoConfig.subscribe((configuracion: ConfiguracionInterface) => {
      this.configuracion = configuracion;
    });
  }

  escucharActualizarPagos(): void {
    this._actualizarPagosService.eventoActualizarPagos.subscribe(async (respuesta: boolean) => {
      if (respuesta) {
        await this.setearAbonos();
      }
    });
  }

  irRuta(opcionMenu: any): void {
    if (!opcionMenu.submenu) {
      const ruta = ['/app', ...opcionMenu.segmento];
      void this._router.navigate(ruta);
    }
  }

  private configLanguageDevExtreme() {
    loadMessages(this.esMessages);
    locale(navigator.language);
  }


  setearUrlImagen() {
    if (this.configuracion) {
      if (this.configuracion.archivos?.length) {
        const imagen = this.configuracion.archivos[0];
        const buffer = imagen.buffer?.data;
        return this._domSanitizer.bypassSecurityTrustUrl(`data:${imagen.mimetype};base64, ${Buffer.from(buffer)}`) as any;
      }
      return '';
    } else {
      return '';
    }
  }

  private async crearBackUp() {
    // this._cargandoService.habilitar();
    // await this._backupRestService.crearBackup().subscribe(respuesta => {
    //   console.log('back up creado');
    //   this._cargandoService.deshabilitar();
    // }, error => {
    //   console.error(error);
    //   this._cargandoService.deshabilitar();
    // });
  }

  abrirModalPagosAbonos(
    factura: any
  ) {
    const dialogRef = this._matDialog.open(ModalPagosAbonosComponent, {
      width: '850px',
      data: {
        factura,
        idFactura: factura.id,
      },
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe({
      next: (response) => {
        if (response) {
          const datos = {
            idFactura: factura?.id,
            abonosPagos: response,
          };
          const factura$ = this._facturaCabeceraRestService
            .agregarAbonoGasto(datos);

          factura$
            .subscribe(
              (respuesta) => {
                this._notificacionService.modalInfo(
                  'success',
                  'Éxito',
                  `El abono/pago ha sido agregado con éxito`,
                );
                this._cargandoService.deshabilitar();
                this._actualizarPagosService.actualizarPagos(true);
              },
              error => {
                this._notificacionService.modalInfo(
                  'error',
                  'Error',
                  `Error al agregar el abono/pago`,
                );
                this._cargandoService.deshabilitar();
                console.error({
                  mensaje: `Error al agregar el abono/pago`,
                  error,
                });
              }
            );
        }
      },
      error: (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
      },
    });
  }
}
