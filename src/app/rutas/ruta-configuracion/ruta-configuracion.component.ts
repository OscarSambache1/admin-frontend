import { Component, OnInit } from '@angular/core';
import {ConfiguracionRestService} from "../../servicios/rest/configuracion-rest.service";
import {ConfiguracionInterface} from "../../interfaces/configuracion.interface";
import {MarcaInterface} from "../../modulos/configuracion-datos/interfaces/marca.interface";
import {Router} from "@angular/router";
import {ModalCrearEditarMarcaComponent} from "../../componentes/modales/modal-crear-editar-marca/modal-crear-editar-marca.component";
import {NotificacionService} from "../../servicios/notificacion/notificacion.service";
import {CargandoService} from "../../servicios/cargando.service";
import {ImpuestoRestService} from "../../modulos/configuracion-datos/servicios/impuesto-rest.service";
import {InfoConfiguracionService} from "../../servicios/info-configuracion.service";
import {BackupRestService} from "../../servicios/rest/backup-rest.service";

@Component({
  selector: 'app-ruta-configuracion',
  templateUrl: './ruta-configuracion.component.html',
  styleUrls: ['./ruta-configuracion.component.scss']
})
export class RutaConfiguracionComponent implements OnInit {

  configuracion?: ConfiguracionInterface;
  configuracionACrear?: ConfiguracionInterface;
  cargando = false;
  constructor(
    private _configuracionRestService: ConfiguracionRestService,
    private readonly _router: Router,
    private readonly _notificacionService: NotificacionService,
    private readonly _cargandoService: CargandoService,
    private readonly _infoConfiguracionService: InfoConfiguracionService,
    private _backupRestService: BackupRestService,

  ) { }

  async ngOnInit() {
    this.cargando = true;
    try {
      this._cargandoService.habilitar();
      const consulta = {
        where: {},
        relations: ['archivos'],
      };
      const configuracion$ = this._configuracionRestService.findAll(
        'busqueda=' + JSON.stringify(consulta),
      );

      const respuesta = await (configuracion$.toPromise());
      this.configuracion = respuesta[0][0] || null;
      this.cargando = false;
      this._cargandoService.deshabilitar();
    } catch (error) {
      this._cargandoService.deshabilitar();
    }
  }

  crearEditar(dato: ConfiguracionInterface | boolean): void {
    !dato
      ? (this.configuracionACrear = undefined)
      : (this.configuracionACrear = dato as ConfiguracionInterface);
  }

  add(): void {
    if (this.configuracionACrear) {
      if (this.configuracion) {
        this.configuracionACrear.id = this.configuracion.id;
      }
      const formData: FormData = new FormData();
      formData.append('imagen', this.configuracionACrear.imagen);
      formData.append('datos', JSON.stringify(this.configuracionACrear));
      this.crear(formData);
    }
  }

  crear(formData: any): void {
    const configuracion$ = this._configuracionRestService.crearEditarConfiguracion(formData);

    configuracion$.subscribe({
      next: (configuracion: ConfiguracionInterface) => {
        this._notificacionService.modalInfo(
          'success',
          'Info',
          'Registro guardado correctamente',
        );
        this._infoConfiguracionService.cambiarInfo(configuracion);
        this._router.navigate(['app']);
      },
      error: (err) => {
        console.error({
          mensage: 'Error crear registro',
          error: err,
        });
        this._notificacionService.modalInfo(
          'error',
          'Error',
          'Error al guardar el registro',
        );
      },
    });
  }

  cancelarModal(): void {
    this._router.navigate(['/app']
    );
  }

  async generarArchivoRespaldo() {
    this._cargandoService.habilitar();
    await this._backupRestService
      .crearBackup(this.configuracion?.correo as string)
      .subscribe(respuesta => {
      this._notificacionService.modalInfo(
        'success',
        'Éxito',
        `El archivo de respaldo ha sido enviado al siguiente correo ${this.configuracion?.correo}`,
      );
      this._cargandoService.deshabilitar();
    }, error => {
      console.error(error);
      this._cargandoService.deshabilitar();
    });
  }
}
