import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {NzWaveModule} from "ng-zorro-antd/core/wave";
import {BlockUIModule} from "primeng/blockui";
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {BreadcrumbModule} from "xng-breadcrumb";
import {ToastrModule, ToastrService} from "ngx-toastr";
import {NzLayoutModule} from "ng-zorro-antd/layout";
import {NzMenuModule} from "ng-zorro-antd/menu";
import {NzBreadCrumbModule} from "ng-zorro-antd/breadcrumb";
import { RutaNoEncontradaComponent } from './rutas/ruta-no-encontrada/ruta-no-encontrada.component';
import {MessageService} from "primeng/api";
import {ToasterModule, ToasterService} from 'angular2-toaster';
import {SweetAlert2Module} from "@sweetalert2/ngx-sweetalert2";
import {TieneConfiguracionGuardService} from "./servicios/guards/tiene-configuracion";
import {ComponentesModule} from "./componentes/componentes.module";
import {IMPORTS_MODULO} from "./modulos/imports/imports-modulo";
import {ToastModule} from "primeng/toast";
import {OverlayPanelModule} from "primeng/overlaypanel";
import {CardModule} from "primeng/card";

@NgModule({
  declarations: [
    AppComponent,
    RutaNoEncontradaComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    NzWaveModule,
    BlockUIModule,
    BreadcrumbModule,
    SweetAlert2Module.forRoot(),
    ToastrModule.forRoot({
      positionClass: 'toast-top-right',
      preventDuplicates: true,
    }),
    NzLayoutModule,
    NzMenuModule,
    NzBreadCrumbModule,
    ToasterModule.forRoot(),
    ComponentesModule,
    ...IMPORTS_MODULO,
    ToastModule,
    OverlayPanelModule,
    CardModule,
  ],
  providers: [
    MessageService,
    ToasterService,
    TieneConfiguracionGuardService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
