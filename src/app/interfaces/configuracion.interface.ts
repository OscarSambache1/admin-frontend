export interface ConfiguracionInterface {
  id?: number;
  nombreComercial?: string;
  razonSocial?: string;
  direccion?: string;
  telefono?: string;
  correo?: string;
  urlImagen?: string;
  imagen?: any;
  archivos?: any[];
}
